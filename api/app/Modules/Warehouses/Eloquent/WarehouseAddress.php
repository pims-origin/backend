<?php
namespace App\Modules\Warehouses\Eloquent;

use Illuminate\Database\Eloquent\Model;

class WarehouseAddress extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'whs_address';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'whs_add_id';

    /**
     * @var array
     */
    protected $fillable = [
        'whs_add_line_1',
        'whs_add_line_2',
        'whs_add_city_name',
        'whs_add_state_id',
        'whs_add_postal_code',
        'whs_add_whs_id',
        'whs_add_type',
    ];

    public function systemState()
    {
        return $this->hasOne(__NAMESPACE__ . '\SystemState', 'sys_state_id', 'whs_add_state_id');
    }

    public function warehouses()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Warehouse', 'whs_add_whs_id', 'whs_id');
    }
}
