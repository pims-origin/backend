<?php

namespace App\Api\V1\Validators;

class DeleteMassCustomerValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'cus_id'   => 'required',
            'cus_id.*' => 'required|exists:customer,cus_id'
        ];
    }
}