<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 27-Sep-16
 * Time: 10:50
 */

namespace App\Api\V1\Transformers;

use \Seldat\Wms2\Models\WorkOrderDtl;
use \Seldat\Wms2\Models\WorkOrderHdr;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Utils\Status;

class WorkOrderDtlTransformer extends TransformerAbstract
{

    public function transform(WorkOrderHdr $workOrderHdr)
    {
        //Pack By
        $type = object_get($workOrderHdr, 'type', '');
        switch ($type) {
            case 1:
                $packBy = config('constants.pack_by.ChargeCode');
                break;
            case 0:
                $packBy = config('constants.pack_by.SKU');
                break;
            default:
                break;
        }
        //Work order status
        $wo_status = object_get($workOrderHdr, 'wo_sts', '');
        switch ($wo_status) {
            case 'NW':
                $wo_sts = config('constants.wo_status.NW');
                break;
            case 'CO':
                $wo_sts = config('constants.wo_status.CO');
                break;
            case 'FN':
                $wo_sts = config('constants.wo_status.FN');
                break;
            default:
                break;
        }

        return [
            //WO_Hdr
            'odr_hdr_id'  => $workOrderHdr->odr_hdr_id,
            'odr_hdr_num' => object_get($workOrderHdr, 'odr_hdr_num', ''),
            'cus_id'      => object_get($workOrderHdr, 'cus_id', ''),
            'cus_name'    => object_get($workOrderHdr, 'customer.cus_name', ''),
            'wo_hdr_num'  => object_get($workOrderHdr, 'wo_hdr_num', ''),
            'wo_sts'      => Status::getByKey("wo_status", object_get($workOrderHdr, 'wo_sts', '')),
            'type'        => $packBy,
            'created_at'  => object_get($workOrderHdr, 'created_at', '')->format('m/d/Y'),
            'updated_at'  => object_get($workOrderHdr, 'updated_at', '')->format('m/d/Y'),
            'created_by'  => object_get($workOrderHdr, 'createdBy.first_name', '') . ' ' .
                object_get($workOrderHdr, 'createdBy.last_name', ''),

            //WO_Dtl
            'wo_hdr_id'   => $workOrderHdr->wo_hdr_id,
            'sku'         => WorkOrderDtl::where('qty', '>', 0)
                ->where('wo_hdr_id', $workOrderHdr->wo_hdr_id)->distinct('item_id')->count('item_id'),
            'chg_code'    => WorkOrderDtl::where('qty', '>', 0)
                ->where('wo_hdr_id', $workOrderHdr->wo_hdr_id)->distinct('chg_code_id')->count('chg_code_id'),


        ];
    }

}
