<?php
namespace backend\controllers;

use backend\models\AssignRoleToUserForm;
use backend\models\CreateRoleForm;
use backend\models\RoleSearch;
use backend\models\RoleSearchForm;
use backend\models\UpdateRoleForm;
use backend\models\RoleUsers;
use common\models\Authority;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

class RoleController extends Controller
{
    public function actionIndex()
    {
        $form = new RoleSearchForm();
        $form->load(Yii::$app->request->get());
        $form->type = Authority::ROLE_TYPE;
        if (!$form->validate()) {
            return [
                'error' => $form->errors
            ];
        }
        $result = RoleSearch::getList($form);

        return $result;
    }

    public function actionCreate()
    {
        $model = new CreateRoleForm();
        $model->load(Yii::$app->request->post());

        if ($model->createRole()) {
            Yii::$app->response->statusCode = 201;

            return ['data' => ['success' => true]];
        } else {
            Yii::$app->response->statusCode = 400;

            return ['error' => $model->errors];
        }
    }

    public function actionUpdate()
    {
        $model = new UpdateRoleForm();
        $model->load(Yii::$app->request->post());

        $role = Authority::findOne($model->name);

        if ($model->updateRole($role)) {
            Yii::$app->response->statusCode = 200;

            return [
                'data' =>
                    [
                        'success' => true,
                    ]
            ];
        } else {
            Yii::$app->response->statusCode = 400;

            return ['error' => $model->errors];
        }
    }

    public function actionView($roleName)
    {
        $roleName = str_replace(["%2520", "%20"], " ", $roleName);
        $auth = Yii::$app->authManager;
        $result = Authority::findOne($roleName);
        if (!$result) {
            throw new BadRequestHttpException('Role not found');
        }

        $permission = $auth->getPermissionsByRole($roleName);
        $permission = array_keys($permission);
        $perms = $auth->getPermWithGrp($permission);

        $result = $result->toArray();
        $result['group_permissions'] = $perms;

        return ['data' => $result];
    }

        public function actionPermissions($roleName)
    {
        $roleName = str_replace(["%2520", "%20"], " ", $roleName);
        $permissions = Yii::$app->authManager->getPermissionsByRole($roleName);
        if (!$permissions) {
            throw new BadRequestHttpException('This role has not any permission');
        }

        return ['data' => array_values($permissions)];
    }

    public function actionAssignPermission($roleName)
    {
        $roleName = str_replace(["%2520", "%20"], " ", $roleName);
        $auth = Yii::$app->authManager;
        $body = Yii::$app->request->post();
        $permissions = explode(',', $body['permissions']);
        $parent = $auth->getRole($roleName);

        if (!$parent) {
            throw new BadRequestHttpException('Role not found');
        }

        //Remove all permission from role
        $auth->removeChildren($parent);

        foreach ($permissions as $permission) {
            $child = $auth->getPermission($permission);
            $auth->addChild($parent, $child);
        }

        return [
            'data' => [
                'message' => 'OK'
            ]
        ];
    }

    public function actionByUser($id)
    {
        $get = Yii::$app->request->get();
        $roles = RoleSearch::getRolesByUserId($id, $get);
        if (!$roles) {
            return ['data' => []];
        }

        return $roles;
    }

    public function actionAssignToUser($id)
    {
        $permission = "userAssignAll";
        $body = Yii::$app->request->post();
        $model = new AssignRoleToUserForm;
        $model->load($body);

        if (!$model->validate()) {
            Yii::$app->response->statusCode = 422;

            return ['error' => $model->errors];
        }
        $auth = Yii::$app->authManager;

        $assigner = $body['user_id'];
        $roles = explode(',', $body['roles']);
        $listPerms = $auth->getPermissionsByUser($assigner);

        //check permission
        $hasPermission = array_key_exists($permission, $listPerms);
        if (!$hasPermission) {
            throw new BadRequestHttpException('User have not permission to assign Role');
        }

        $transaction = Yii::$app->db->beginTransaction();
        //revoke all role by userID
        $auth->revokeAll($id);

        foreach ($roles as $role) {
            $assign = $auth->getRole($role);

            if (!$auth->getAssignment($role, $id) && $assign) {
                $auth->assign($assign, $id);
            }
        }

        $transaction->commit();

        return [
            'data' => [
                'message' => 'OK'
            ]
        ];
    }

    public function actionUsers($roleName)
    {
        $roleName = urldecode(urldecode($roleName));
        $user_ids = Yii::$app->authManager->getUserIdsByRole($roleName);
        $model = new RoleUsers();
        $requestHeader = Yii::$app->request->headers;

        if ($result = $model->getUsers($user_ids, $requestHeader)) {
            return [
                'data' => $result
            ];
        } else {
            throw new BadRequestHttpException('Users not found');
        }
    }

    public function actionDeletes()
    {
        $body = Yii::$app->request->post();
        if (!empty($body['roles'])) {
            Authority::deleteAll(['name' => $body['roles'], 'type' => Authority::ROLE_TYPE]);

            return ['data' => ['message' => 'OK']];
        } else {
            throw new BadRequestHttpException('roles parameter is empty');
        }
    }

}
