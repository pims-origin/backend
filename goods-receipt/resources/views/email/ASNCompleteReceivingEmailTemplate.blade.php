﻿﻿
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta charset="utf-8"/>
    <title>Complete Receiving Wait Approve Items</title>
    <style type="text/css">

    h4 {
        margin-bottom: 5px;
    }

    h1 {
        color: #504e4f;
        font-size: 32px;
        font-weight: 100;
        margin: 0;
    }

    h2 {
        color: #364150;
        text-transform: uppercase;
        margin: 5px 0;
        font-weight: 500;
    }

    body {
        background-color: #fff;
        color: #000;
        font-size: 14px;
        font-family: "Calibri";
        width: 21cm;
        height: 29.7cm;
        margin: 0 auto;
        border: 1px solid #ddd;
        padding: 20px;
        margin: 0 auto;

    }

    .table-style thead tr th {
        font-weight: 400;
        text-transform: uppercase;
        text-align: left;
        color: #a9a9a9;
        font-size: 19px;
    }

    .table-style tbody tr td {
        padding-top: 5px;
        vertical-align: top;
        padding-bottom: 20px;
    }

    .table-style tbody tr td b {
        font-size: 20px;
        font-weight: 600;
        color: #282828;
    }

    .table-style1 {
        width: 100%;
        border: 1px solid #ddd;
        line-height: 30px;
        margin-bottom: 15px;
    }

    .table-style1 thead tr th {
        color: #36c6d3;
        padding: 0 5px;
        border-bottom: 1px dashed #d2d2d2;
        font-size: 16px;
        text-transform: uppercase;
        font-weight: bold;
        text-align: left;
        border-right: 1px solid #e3f1f2;
    }

    .table-style1 tbody tr td {
        border-bottom: 1px dashed #ddd;
        padding: 0 5px;
        color: #595959;
    }

    .table-style1 tbody tr:last-child td {
        border-bottom: none;
    }

    .table-style1 tbody tr td.title {
        border-right: 1px solid #ddd;
        background-color: #e3f1f2;
        width: 30%;
    }

    .table-style2 {
        border: 1px solid #a9a9a9;
        margin: 15px 0;
    }

    .table-style2 thead tr th {
        font-weight: 600;
        border-bottom: 1px solid #c0c0c0;
        text-align: left;
        background-color: #e7e7e7;
        padding-left: 5px;
        border-right: 1px solid #c1c1c1;
        color: #595959;
        /*text-transform: capitalize;*/
        line-height: 20px;
    }

    .table-style2 tbody tr td {
        padding-left: 5px;
        border-right: 1px dashed #d5d5d5;
        border-bottom: 1px dashed #d5d5d5;
        color: #595959;
        line-height: 30px;
    }

    .table-style2 tbody tr td:last-child {
        border-right: none;
    }

    .table-style2 tbody tr td.title {
        font-weight: 600;
        color: #364150;
    }

    .table-style2 tbody tr:nth-child(2n) td {
        background-color: #e3f1f2;
    }

    .table-style2 tbody tr:last-child td {
        border-bottom: none
    }

    .text-right {
        text-align: right !important;
    }

    .table-style3 thead tr th {
        font-weight: 600;
        text-align: left;
        color: #364150;
        text-transform: capitalize;
    }

    .table-style3 tr td {
        line-height: 20px;
        font-size: 14px;
        color: #595959;
        padding-right: 5px;
    }

    .table-style3 tr td b {
        color: #1c1c1c;
    }

    footer {
        border-top: 1px dashed #b7bbc1;
        margin-top: 20px;
        padding-top: 5px;
    }

    .img-style {
        width: 16px;
        float: left;
        margin-right: 5px;
    }

    .uppercase-class{
        text-transform: uppercase;
    }

</style>
</head>
<body>
    <?php
    date_default_timezone_set(config('app.timezone'));
    ?>
    <table style="width: 100%; border-bottom:1px solid #ddd;margin-bottom: 10px" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td width="25%" style="vertical-align: middle">
                    <img src="https://apigw2.seldatdirect.com/qc/pims/wms360/api/core/report-master/v1/logo-gallery/view/L2ZpbGVzLzEvNWM3Y2NhZjY2ZGRiMi5wbmc=" alt="Seldat" width="180">
                </td>
                <td class="text-right">
                    <h1>Receiving Slip</h1>
                </td>
            </tr>
        </tbody>
    </table>
    <h2>
        {{ $asnHrd['asn_hdr_num'] }}
    </h2>
    <i>{{ $grStatus }}</i>
    <br><br>

    @if($asnHrd)

    <table cellspacing="0" cellpadding="0" style="width: 100%">
        <tr>
            <td style="width: 50%">
                <table class="table-style1" cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr>
                            <td class="title">ASN</td>
                            <td>{{object_get($asnHrd,'asn_hdr_num','')}}</td>
                        </tr>
                        <tr>
                            <td class="title">Ref Code:</td>
                            <td>{{object_get($asnHrd, 'asn_hdr_ref', '')}}</td>
                        </tr>
                        <tr>
                            <td class="title">Expected Date:</td>
                            <td>
                                {{(object_get($asnHrd, 'asn_hdr_ept_dt', '') ? date('m/d/Y', object_get($asnHrd, 'asn_hdr_ept_dt', '')) : '')}}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td style="width: 50%">
                <table class="table-style1" cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr>
                            <td class="title">Customer:</td>
                            <td>
                                {{object_get($asnHrd, 'customer.cus_code', '')}}
                                -
                                {{object_get($asnHrd, 'customer.cus_name', '')}}
                            </td>
                        </tr>
                        <tr>
                            <td class="title">Carrier:</td>
                            <td>{{$items[0]['ctnr_num']}}</td>
                        </tr>
                        <tr>
                            <td class="title"># SKUs:</td>
                            <td>
                                {{$totalSku}}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <!--Item: Receiving list-->
    <br>
    <i>
        Receiving Wait Approve Item List
    </i>
    <table class="table-style2" cellpadding="0" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Status</th>
                <th>UPC</th>
                <th>SKU</th>
                <th>Size</th>
                <th>Color</th>
                <th>Lot</th>
                <th>Description</th>
                <th>UOM</th>
                <th>Pack Size</th>
                <th>L-IN</th>
                <th>W-IN</th>
                <th>H-IN</th>
                <th>WT</th>
                <th>Exp CTNS</th>
                <th>Actual CTNS</th>
                <th>Discrep</th>
                <th>Damage</th>
                <th>X-Dock</th>
                <th>PO</th>
                <th>PO Date</th>
                <th>Expired Date</th>
            </tr>
        </thead>
        <tbody>

            @if(isset($items))
            @foreach($items as $item)
            <tr>
                <td>RECV-WAITAPP</td>
                <td>{{ array_get($item, 'cus_upc', '') }}</td>
                <td>{{ array_get($item, 'sku', '') }}</td>
                <td>{{ array_get($item, 'size', '') }}</td>
                <td>{{ array_get($item, 'color', '') }}</td>
                <td>{{ array_get($item, 'asn_dtl_lot', '') }}</td>
                <td><a href="{{ $editLink . array_get($item, 'item_id', '') . '/edit' }}">Edit</a></td>
                <td>{{ array_get($item, 'uom_name', '') }}</td>
                <td>{{ array_get($item, 'pack', '') }}</td>
                <td>{{ array_get($item, 'length', '') }}</td>
                <td>{{ array_get($item, 'width', '') }}</td>
                <td>{{ array_get($item, 'height', '') }}</td>
                <td>{{ array_get($item, 'weight', '') }}</td>
                <td>{{ array_get($item, 'exp_ctns', '') }}</td>
                <td>{{ array_get($item, 'act_ctn', '') }}</td>
                <td>{{ array_get($item, 'discrep', '') }}</td>
                <td>{{ array_get($item, 'dmg_ttl', '') }}</td>
                <td>{{ array_get($item, 'xdock', '') }}</td>
                <td>{{ array_get($item, 'asn_dtl_po', '') }}</td>
                <td>{{ array_get($item, 'asn_dtl_po_dt', '') > 0 ? date('m/d/Y', array_get($item, 'asn_dtl_po_dt', '')) : null }}</td>
                <td>{{ array_get($item, 'expired_dt', '') > 0 ? date('m/d/Y', array_get($item, 'expired_dt', '')) : null }}</td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
    <!--/Item: Receiving list-->
    <br>

    <footer>
        <table width="100%">
            <tr>
                <td width="50%" style="padding: 0; color: #404040; font-size: 14px">
                    {{$currentDate}}
                    By
                    {{$completeBy}}
                </td>
                <td width="50%" style="text-align: right; padding: 0; font-size: 16px; color: #3f3f3f">seldatinc.com</td>
            </tr>
        </table>
    </footer>
    @endif

</body>
</html>
