<?php

namespace App\Modules\Orders\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{

    public function toArray($request)
    {
        $odrReqDt   = ($this->odr_req_dt && strtotime($this->odr_req_dt) != 0) ? $this->odr_req_dt->format('Y-m-d') : NULL;
        $shipByDt   = ($this->ship_by_dt && strtotime($this->ship_by_dt) != 0) ? $this->ship_by_dt->format('Y-m-d') : NULL;
        $cancleByDt = ($this->cancle_by_dt && strtotime($this->cancle_by_dt) != 0) ? $this->cancle_by_dt->format('Y-m-d') : NULL;
        
        return [
            'type' => 'orders',
            'id' => (int) $this->odr_id,
            'attributes' => [
                'order_type'                => $this->odr_type,
                'order_num'                 => $this->cus_odr_num,
                'po_num'                    => $this->cus_po,
                'pick_ref'                  => $this->cus_pick_num,
                'dept_ref'                  => $this->cus_dept_ref,
                'rush_priority'             => $this->rush_odr ?: 0,
                'order_date'                => $odrReqDt,
                'ship_date'                 => $shipByDt,
                'cancel_date'               => $cancleByDt,
                'additional_information'    => $this->cus_notes,
                'special_instruction'       => $this->spec_instruct,
                'meta'                      => $this->odrHdrMeta ? json_decode($this->odrHdrMeta->value) : NULL,
            ]
        ];
    }
}