<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\CustomersModel;
use App\Api\V1\Models\DailyPalletModel;
use App\Api\V1\Models\InventoryModel;
use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\SystemStateModel;
use App\Api\V1\Transformers\InventoryTransformer;
use App\Api\V1\Transformers\SystemStateTransformer;
use Maatwebsite\Excel\Facades\Excel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;

class DailyPalletController extends AbstractController
{

    protected $systemStateModel;
    protected $InventoryModel;
    protected $dailyPalletModel;
    protected $customersModel;

    protected $InventoryTransformer;
    protected $systemStateTransformer;

    protected $orderCartonModel;

    /*
     ****************************************************************************
     */

    public function __construct(Request $request)
    {
        $object = new AuthenticationService($request);

        parent::__construct($request, $object);

        $this->InventoryModel         = new InventoryModel();
        $this->InventoryTransformer   = new InventoryTransformer();
        $this->systemStateModel       = new SystemStateModel();
        $this->systemStateTransformer = new SystemStateTransformer();

        $this->orderCartonModel = new OrderCartonModel();
        $this->dailyPalletModel = new DailyPalletModel();
        $this->customersModel   = new CustomersModel();
    }

    /*
     ****************************************************************************
     */

    /**
     * @param $whs_id
     * @param $cus_id
     * @param Request $request
     */
    public function dailyPalletReport(
        $whs_id,
        $cus_id,
        Request $request
    ) {
        //date_default_timezone_set(config('constants.timezone_utc_zero'));

        $input      = $request->getQueryParams();
        $limit      = array_get($input, 'limit', PAGING_LIMIT);
        $startDay   = array_get($input, 'select_day', date('Y-m-d'));
        $endDay     = $startDay . ' 23:59:59';

        $betweenDay = [strtotime($startDay . ' UCT'), strtotime($endDay . ' UCT')];

        try {
            $cusInfo = $this->customersModel->getFirstBy('cus_id', $cus_id);
            if (empty($cusInfo)) {
                throw new \Exception(Message::get("BM017", "Customer"));
            }
            $cusName = object_get($cusInfo, 'cus_name', '');

            //Daily Pallet On Rack for RM Kayu
            $dailyPalletReports = $this->dailyPalletModel->getDailyPalletReport(
                $whs_id,
                $cus_id,
                $betweenDay,
                $input, [],
                $limit,
                //[1],
                []
            );

            $dailyPalletOnRack = $this->convertDailyPalletReport($dailyPalletReports, 1);

            //Subtotal RM Kayu for Daily Pallet On Rack
            $subtotalForDailyPalletOnRack = $this->sumDailyInventoryReport($dailyPalletOnRack);
            $dailyPalletOnRackInfos       = $this->convertNumberFormat($dailyPalletOnRack);

            // //Daily Pallet Block Stack
            // $dailyPalletBlockStack = $this->convertDailyPalletReport($dailyPalletReports, 0);

            // //Subtotal Block Stack for Daily Pallet
            // $subtotalBlockStackForDailyPallet = $this->sumDailyInventoryReport($dailyPalletBlockStack);
            // $dailyPalletBlockStackInfos       = $this->convertNumberFormat($dailyPalletBlockStack);
            //Total Daily Pallet
            $totalForDailyPallet = $this->sumDailyInventoryReport($dailyPalletReports);

            $userInfo  = new \Wms2\UserInfo\Data();
            $userInfo  = $userInfo->getUserInfo();
            $userId    = $userInfo['user_id'];
            $printedBy = $userInfo['first_name'] . ' ' . $userInfo['last_name'];

            //Export to Excel File
            $ext = 'xls';

            return Excel::create('DailyPalletReport', function ($excel) use (
                &$dailyPalletOnRackInfos,
                &$subtotalForDailyPalletOnRack,
                // &$dailyPalletBlockStackInfos,
                // &$subtotalBlockStackForDailyPallet,
                &$totalForDailyPallet,
                &$cusName,
                &$printedBy,
                $startDay
            ) {
                // Set sheet 1
                $excel->sheet('Daily Pallet Report', function ($sheet) use (
                    &$dailyPalletOnRackInfos,
                    &$subtotalForDailyPalletOnRack,
                    // &$dailyPalletBlockStackInfos,
                    // &$subtotalBlockStackForDailyPallet,
                    &$totalForDailyPallet,
                    &$cusName,
                    &$printedBy,
                    $startDay
                ) {
                    $totalOnRack     = $dailyPalletOnRackInfos->count();
                    $totalBlockStack = -2;

                    $sheet->setStyle(array(
                        'font' => array(
                            'name' => 'Arial',
                            'size' => 11,
                            'bold' => false,
                        ),
                    ))
                        ->row(4, array(
                            'DAILY PALLET REPORT',
                        ))->mergeCells('A4:X4')
                        ->cells('A4:X4', function ($cells) {
                            $cells->setFontSize(32)
                                ->setFontWeight('bold');
                        })
                        ->row(5, array(
                            'Customer: ' . $cusName,
                        ))
                        ->cells('A5:X5', function ($cells) {
                            $cells->setFontSize(18)
                                ->setFontWeight('bold');
                        })
                        ->mergeCells('A5:R5')
                        ->row(6, [
                            $startDay,
                            '', '', '', '', '', '', '', '', '', '',
                            date('h:i:s a') . ' WB',
                        ])
                        ->cells('A6:X6', function ($cells) {
                            $cells->setFontSize(12)
                                ->setFontWeight('bold italic');
                        })
                        ->mergeCells('A6:K6')
                        ->mergeCells('L6:X6')
                        ->cells('L6:X6', function ($cells) {
                            $cells->setAlignment('right');
                        })
                        ->row(9, [
                            'Pallet ID',
                            'Item/SKU',
                            'Batch',
                            'Description',

                            'Size',
                            'Color ',
                            'Length',
                            'Width',
                            'Height',
                            'Weight',

                            'UOM',
                            'Pack',
                            'Date In',
                            'Expired Date',
                            'Information Recap',
                        ])
                        ->cells('A9:X9', function ($cells) {
                            $cells->setBorder('1px solid #c0c0c0', '1px solid #ddd', '1px solid #c0c0c0', 'solid');
                        })
                        ->setMergeColumn(array(
                            'columns' => array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N'),
                            'rows'    => array(
                                array(9, 11),
                            ),
                        ))->mergeCells('O9:X9')
                        ->mergeCells('O10:P10')
                        ->mergeCells('Q10:R10')
                        ->mergeCells('S10:T10')
                        ->mergeCells('U10:V10')
                        ->mergeCells('W10:X10')
                        ->row(10, [
                            '', '', '', '', '', '', '', '',
                            '', '', '', '', '', '',
                            'Initial Stock',
                            '',
                            'In Activity',
                            '',
                            'Out Activity',
                            '',
                            'Other Activity',
                            '',
                            'Current Stock',
                        ])->row(11, [
                        '', '', '', '', '', '', '', '',
                        '', '', '', '', '', '',
                        'Carton',
                        'QTY',
                        'Carton',
                        'QTY',
                        'Carton',
                        'QTY',
                        'Carton',
                        'QTY',
                        'Carton',
                        'QTY',
                    ])->cells('A9:X9', function ($cells) {
                        $cells->setBackground('#e3f1f2')
                            ->setValignment('center')
                            ->setAlignment('center');
                    })->cells('A10:X10', function ($cells) {
                        $cells->setBackground('#e3f1f2')
                            ->setValignment('center')
                            ->setAlignment('center');
                    })->cells('A11:X11', function ($cells) {
                        $cells->setBackground('#e3f1f2')
                            ->setValignment('center')
                            ->setAlignment('center');
                    })
                        ->appendRow(['Raw Material (Pallet)'])
                        ->mergeCells('A12:X12')
                        ->cells('A12:X12', function ($cells) {
                            $cells->setBackground('#c6efce');
                        })
                        ->rows($dailyPalletOnRackInfos->toArray())
                        ->row($totalOnRack + 12 + 1, [
                            '', '',
                            '', '', '', '', '', '',
                            'Subtotal',
                            '', '', '', '', '',
                            $subtotalForDailyPalletOnRack['init_ctns_qty'] ?? '0',
                            $subtotalForDailyPalletOnRack['init_qty'] ?? '0',
                            $subtotalForDailyPalletOnRack['in_ctns_qty'] ?? '0',
                            $subtotalForDailyPalletOnRack['in_qty'] ?? '0',
                            $subtotalForDailyPalletOnRack['out_ctns_qty'] ?? '0',
                            $subtotalForDailyPalletOnRack['out_qty'] ?? '0',
                            $subtotalForDailyPalletOnRack['other_ctns_qty'] ?? '0',
                            $subtotalForDailyPalletOnRack['other_qty'] ?? '0',
                            $subtotalForDailyPalletOnRack['cur_ctns_qty'] ?? '0',
                            $subtotalForDailyPalletOnRack['cur_qty'] ?? '0',
                        ])
                        ->cells('A' . ($totalOnRack + 12) . ':X' . ($totalOnRack + 12), function ($cells) {
                            $cells->setBackground('#e7e7e7');
                        })
                        //->mergeCells('I' . ($totalOnRack + 12) . ':N' . ($totalOnRack + 12))

                        // ->appendRow(['Raw Material (Block Stack)'])
                        // ->mergeCells('A' . ($totalOnRack + 12 + 2) . ':R' . ($totalOnRack + 12 + 2))
                        // ->cells('A' . ($totalOnRack + 12 + 2) . ':R' . ($totalOnRack + 12 + 2), function ($cells) {
                        //     $cells->setBackground('#c6efce');
                        // })
                        // ->rows($dailyPalletBlockStackInfos->toArray())
                        // ->row($totalOnRack + 12 + 1 + $totalBlockStack + 2, [
                        //     '', '',
                        //     'Subtotal',
                        //     '', '', '', '', '',
                        //     $subtotalBlockStackForDailyPallet['init_ctns_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyPallet['init_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyPallet['in_ctns_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyPallet['in_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyPallet['out_ctns_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyPallet['out_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyPallet['other_ctns_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyPallet['other_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyPallet['cur_ctns_qty'] ?? '0,00',
                        //     $subtotalBlockStackForDailyPallet['cur_qty'] ?? '0,00',
                        // ])
                        ->cells('A' . ($totalOnRack + 12 + 1 + $totalBlockStack + 2) . ':X' . ($totalOnRack + 12 + 1
                                + $totalBlockStack + 2), function ($cells) {
                            $cells->setBackground('#e7e7e7');
                        })
                        ->appendRow([
                            '', '',
                            '', '', '', '', '', '',
                            'Total',
                            '', '', '', '', '',
                            $totalForDailyPallet['init_ctns_qty'] ?? '0,00',
                            $totalForDailyPallet['init_qty'] ?? '0,00',
                            $totalForDailyPallet['in_ctns_qty'] ?? '0,00',
                            $totalForDailyPallet['in_qty'] ?? '0,00',
                            $totalForDailyPallet['out_ctns_qty'] ?? '0,00',
                            $totalForDailyPallet['out_qty'] ?? '0,00',
                            $totalForDailyPallet['other_ctns_qty'] ?? '0,00',
                            $totalForDailyPallet['other_qty'] ?? '0,00',
                            $totalForDailyPallet['cur_ctns_qty'] ?? '0,00',
                            $totalForDailyPallet['cur_qty'] ?? '0,00',
                        ])
                        ->mergeCells('I' . ($totalOnRack + 12 + 1 + $totalBlockStack + 2 + 1) . ':N' . ($totalOnRack
                                + 12 + 1 + $totalBlockStack + 2 + 1))
                        ->cells('A' . ($totalOnRack + 12 + 1 + $totalBlockStack + 2 + 1) . ':X' . ($totalOnRack + 12
                                + 1 + $totalBlockStack + 2 + 1), function ($cells) {
                            $cells->setBackground('#fff000');
                        })
                        ->setBorder('A9:X' . ($totalOnRack + 12 + 1 + $totalBlockStack + 2 + 1), 'thin')
                        ->rows(
                            [
                                [], [], [],
                                [date("m/d/Y h:i:s a") . ' By ' . $printedBy],
                            ]
                        );
                });
            })
                ->download($ext);
                //->export($ext);
                //->store($ext, storage_path('excel/exports'));

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /**
     * [convertDailyPalletReport description]
     * @param  [type] $items  [description]
     * @param  int    $isRack [description]
     * @return [type]         [description]
     */
    private function convertDailyPalletReport(&$items, int $isRack)
    {
        return $items/*->reject(function ($item) use ($isRack) {
            return (int) $item->is_rack !== $isRack;
        })*/
            ->sortbydesc('updated_at')
            ->transform(function ($item) {
                return [
                    'plt_num'        => $item['plt_num'],
                    'sku'            => $item['sku'],
                    'lot'            => $item['lot'],
                    'description'    => $item['description'],

                    'size'   => $item['size'],
                    'color'  => $item['color'],
                    'length' => $item['length'],
                    'width'  => $item['width'],
                    'height' => $item['height'],
                    'weight' => $item['weight'],

                    'uom'            => $item['uom'],
                    'pack'           => $item['pack'],
                    'gr_dt'          => $item['gr_dt'] ? date('Y-m-d', $item['gr_dt']) : '',
                    'exp_dt'         => $item['exp_dt'] ? date('Y-m-d', $item['exp_dt']) : '',
                    'init_ctns_qty'  => $item['init_ctns_qty'],
                    'init_qty'       => $item['init_qty'],
                    'in_ctns_qty'    => $item['in_ctns_qty'],
                    'in_qty'         => $item['in_qty'],
                    'out_ctns_qty'   => $item['out_ctns_qty'],
                    'out_qty'        => $item['out_qty'],
                    'other_ctns_qty' => $item['other_ctns_qty'],
                    'other_qty'      => $item['other_qty'],
                    'cur_ctns_qty'   => $item['cur_ctns_qty'],
                    'cur_qty'        => $item['cur_qty'],
                ];
            });
    }

    private function sumDailyInventoryReport(&$items)
    {
        $item = $items->first() ?? null;
        if (!$item) {
            return [];
        }

        return [
            'whs_id'         => $item['whs_id'] ?? '',
            'cus_id'         => $item['cus_id'] ?? '',
            'item_id'        => $item['item_id'] ?? '',
            'description'    => $item['description'] ?? '',
            'sku'            => $item['sku'] ?? '',
            'size'           => $item['size'] ?? '',
            'color'          => $item['color'] ?? '',
            'pack'           => $item['pack'] ?? '',
            'uom'            => $item['uom'] ?? '',
            'lot'            => $item['lot'] ?? '',
            'is_rack'        => $item['is_rack'] ?? '',
            'plt_id'         => $item['plt_id'] ?? '',
            'plt_num'        => $item['plt_num'] ?? '',
            'loc_id'         => $item['loc_id'] ?? '',
            'loc_code'       => $item['loc_code'] ?? '',
            'gr_dt'          => $item['gr_dt'] ?? '',
            'exp_dt'         => $item['exp_dt'] ?? '',
            'created_at'     => $item['created_at'] ?? '',
            'updated_at'     => $item['updated_at'] ?? '',
            'inv_dt'         => $item['inv_dt'] ?? '',
            'init_ctns_qty'  => ceil($items->sum('init_ctns_qty')),
            'init_qty'       => ceil($items->sum('init_qty')),
            'in_ctns_qty'    => ceil($items->sum('in_ctns_qty')),
            'in_qty'         => ceil($items->sum('in_qty')),
            'out_ctns_qty'   => ceil($items->sum('out_ctns_qty')),
            'out_qty'        => ceil($items->sum('out_qty')),
            'other_ctns_qty' => ceil($items->sum('other_ctns_qty')),
            'other_qty'      => ceil($items->sum('other_qty')),
            'cur_ctns_qty'   => ceil($items->sum('cur_ctns_qty')),
            'cur_qty'        => ceil($items->sum('cur_qty')),
        ];
    }

    private function convertNumberFormat($items)
    {
        return $items->transform(function ($item) {
            return [
                'plt_num'        => $item['plt_num'],
                'sku'            => $item['sku'],
                'lot'            => $item['lot'],
                'description'    => $item['description'],

                'size'        => $item['size'],
                'color'       => $item['color'],
                'length'      => $item['length'],
                'width'       => $item['width'],
                'height'      => $item['height'],
                'weight'      => $item['weight'],

                'uom'            => $item['uom'],
                'pack'           => $item['pack'],
                'gr_dt'          => $item['gr_dt'],
                'exp_dt'         => $item['exp_dt'],
                'init_ctns_qty'  => ceil($item['init_ctns_qty']),
                'init_qty'       => ceil($item['init_qty']),
                'in_ctns_qty'    => ceil($item['in_ctns_qty']),
                'in_qty'         => ceil($item['in_qty']),
                'out_ctns_qty'   => ceil($item['out_ctns_qty']),
                'out_qty'        => ceil($item['out_qty']),
                'other_ctns_qty' => ceil($item['other_ctns_qty']),
                'other_qty'      => ceil($item['other_qty']),
                'cur_ctns_qty'   => ceil($item['cur_ctns_qty']),
                'cur_qty'        => ceil($item['cur_qty']),
            ];
        });
    }
}
