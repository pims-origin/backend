<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CustomerAddressModel;
use App\Api\V1\Models\CustomerChargeDetailModel;
use App\Api\V1\Models\CustomerColorModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\CustomerWarehouseModel;
use App\Api\V1\Models\CustomerZoneModel;
use App\Api\V1\Models\CustomerModel;
use App\Api\V1\Models\CustomerContactModel;
use App\Api\V1\Models\InvoiceCostModel;
use App\Api\V1\Models\WODetailModel;
use App\Api\V1\Models\ZoneModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Transformers\CustomerConfigsTransformer;
use App\Api\V1\Transformers\CustomerDropDownTransformer;
use App\Api\V1\Validators\CustomerConfigValidator;
use App\Api\V1\Validators\CustomerSaveMassValidator;
use App\Api\V1\Transformers\CustomerTransformer;
use App\Api\V1\Transformers\CustomerListTransformer;
use App\Api\V1\Transformers\CustomerDamageTransformer;
use App\Api\V1\Validators\DeleteMassCustomerValidator;
use App\Api\V1\Validators\CustomerValidator;
use Dingo\Api\Facade\Route;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SelExport;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Seldat\Wms2\Utils\Message;
use Wms2\UserInfo\Data;
use App\Api\V1\Models\CustomerDamageModel;

class CustomerController extends AbstractController
{
    /*
     * @var CustomerModel
     * */
    protected $customerModel;

    /**
     * @var CustomerColorModel
     */
    protected $customerColorModel;
    /*
     * @var $customerSaveMassValidator
     * */
    protected $customerSaveMassValidator;
    /*
     * @var $customerTransformer
     * */
    protected $customerTransformer;
    /*
     * @var $customerZoneModel
     * */
    protected $customerZoneModel;
    /*
     * @var $customerWarehouseModel
     * */
    protected $customerWarehouseModel;
    /*
     * @var $customerAddressModel
     * */
    protected $customerAddressModel;
    /*
     * @var $customerContactModel
     * */
    protected $customerContactModel;
    /*
     * @var $customerChargeDetailModel
     * */
    protected $customerChargeDetailModel;
    /*
     * @var $zoneModel
     * */
    protected $zoneModel;
    /*
     * @var $customerListTransformer
     * */
    protected $customerListTransformer;

	/**
	 * @var $customerDropDownTransformer
	 */
    protected $customerDropDownTransformer;
    /*
     * @var $customerValidator
     * */
    protected $customerValidator;
    /*
     * @var $delMassCustomerValidator
     * */
    protected $delMassCustomerValidator;

    /**
     * @var CustomerConfigsTransformer
     */
    protected $customerConfigsTransformer;

    /**
     * @var CustomerConfigModel
     */
    protected $customerConfigModel;

    /**
     * @var CustomerConfigValidator
     */
    protected $customerConfigValidate;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var $cartonModel
     */
    protected $cartonModel;


    protected $invoiceCostModel;

    public function __construct(
        LocationModel $locationModel,
        CartonModel $cartonModel
    ) {
        parent::__construct();

        $this->customerModel = new CustomerModel();
        $this->customerColorModel = new CustomerColorModel();
        $this->customerSaveMassValidator = new CustomerSaveMassValidator();
        $this->customerTransformer = new CustomerTransformer();
        $this->customerZoneModel = new CustomerZoneModel();
        $this->customerWarehouseModel = new CustomerWarehouseModel();
        $this->customerAddressModel = new CustomerAddressModel();
        $this->customerContactModel = new CustomerContactModel();
        $this->customerChargeDetailModel = new CustomerChargeDetailModel();
        $this->zoneModel = new ZoneModel();
        $this->customerListTransformer = new CustomerListTransformer();
        $this->customerDropDownTransformer = new CustomerDropDownTransformer();
        $this->customerValidator = new CustomerValidator();
        $this->delMassCustomerValidator = new DeleteMassCustomerValidator();
        $this->customerConfigModel = new CustomerConfigModel();
        $this->customerConfigsTransformer = new CustomerConfigsTransformer();
        $this->customerConfigModel = new CustomerConfigModel();
        $this->customerConfigsTransformer = new CustomerConfigsTransformer();
        $this->customerConfigValidate = new CustomerConfigValidator();
        $this->locationModel = $locationModel;
        $this->cartonModel = $cartonModel;
        $this->invoiceCostModel = new InvoiceCostModel();
    }

    public function show($customerId)
    {
        try {
            $customer = $this->customerModel->getFirstBy('cus_id', $customerId);

            return $this->response->item($customer, $this->customerTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function search(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $currentAction = array_get(Route::getCurrentRoute()->getAction(), 'as', '');
        if ($currentAction == 'allCustomer') {
            $input['mode'] = 1;
        }
        if ($currentAction == 'userCustomer') {
            $input['mode'] = 2;
        }
        try {
            $customers = $this->customerModel->search($input,
                ['customerAddress', 'customerContact', 'customerWarehouse', 'customerZone'],
                array_get($input, 'limit'));

            // Show first contact
            if (!empty($customers)) {
                foreach ($customers as $key => $customer) {
                    $customerContact = $customer->customerContact;

                    if (!empty($customerContact)) {
                        foreach ($customerContact as $contact) {
                            $customers[$key]['customerContact'] = $contact;
                            break;
                        }
                    }
                }
            }

            return $this->response->paginator($customers, $this->customerListTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

	public function dropDown(Request $request)
	{
		// get data from HTTP
		$input = $request->getQueryParams();
		try {
			$customers = $this->customerModel->dropDown($input);
			return $this->response->paginator($customers, $this->customerDropDownTransformer);
		} catch (\PDOException $e) {
			return $this->response->errorBadRequest(
				SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
			);
		} catch (\Exception $e) {
			return $this->response->errorBadRequest($e->getMessage());
		}
	}

    public function store(
        Request $request
    ) {

        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->customerValidator->validate($input);

        $params = [
            'cus_name'        => $input['cus_name'],
            'cus_status'      => $input['cus_status'],
            'cus_code'        => $input['cus_code'],
            'cus_country_id'  => array_get($input, 'cus_country_id', null),
            'cus_state_id'    => array_get($input, 'cus_state_id', null),
            'cus_city_name'   => array_get($input, 'cus_city_name', ''),
            'cus_postal_code' => array_get($input, 'cus_postal_code', '')
        ];

        try {
            // check duplicate code
            if ($this->customerModel->getFirstBy('cus_code', $params['cus_code'])) {
                throw new \Exception(Message::get("BM006", "Customer Code"));
            }

            if ($customer = $this->customerModel->create($params)) {
                return $this->response->item($customer,
                    $this->customerTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function update(
        $customerId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->customerValidator->validate($input);

        $params = [
            'cus_id'         => $customerId,
            'cus_name'       => $input['cus_name'],
            'cus_status'     => $input['cus_status'],
            'cus_code'       => $input['cus_code'],
            'cus_country_id' => array_get($input, 'cus_country_id', null),
            'cus_state_id'   => array_get($input, 'cus_state_id', null),
            'cus_city_name'  => array_get($input, 'cus_city_name', ''),
        ];

        try {
            // check duplicate code
            if (($customer = $this->customerModel->getFirstBy('cus_code', $params['cus_code']))
                && $customer->cus_id != $customerId
            ) {
                throw new \Exception(Message::get("BM006", "Customer Code"));
            }

            if ($customer = $this->customerModel->update($params)) {
                return $this->response->item($customer, $this->customerTransformer);
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function destroy($customerId)
    {
        try {
            if ($this->customerModel->deleteCustomer($customerId)) {
                return $this->response->noContent();
            }
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function deleteMass(Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $this->delMassCustomerValidator->validate($input);

        $input['cus_id'] = is_array($input['cus_id']) ? $input['cus_id'] : [$input['cus_id']];

        try {
            $this->customerModel->deleteMassCustomer($input['cus_id']);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->noContent();
    }

    /**
     * Store Customer, Zone, Cus Warehouse Cus Address, Cus Contact
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function storeWhole(Request $request)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->customerSaveMassValidator->validate($input);

        $params = [
            'cus_name'            => $input['cus_name'],
            'cus_status'          => $input['cus_status'],
            'cus_code'            => $input['cus_code'],
            'cus_country_id'      => array_get($input, 'cus_country_id', null),
            'cus_state_id'        => array_get($input, 'cus_state_id', null),
            'cus_city_name'       => array_get($input, 'cus_city_name', ''),
            'cus_postal_code'     => array_get($input, 'cus_postal_code', ''),
            'cus_billing_account' => array_get($input, 'cus_billing_account', ''),
            'cus_des'             => array_get($input, 'cus_des', ''),
            'sales_rep'           => array_get($input, 'sales_rep', ''),
            'billed_through'      => array_get($input, 'billed_through', ''),
        ];

        try {
            // check duplicate code
            if ($this->customerModel->getFirstBy('cus_code', $params['cus_code'])) {
                throw new \Exception(Message::get("BM006", "Customer Code"));
            }

            DB::beginTransaction();
            // Create Customer
            $customer = $this->customerModel->create($params);
            $customerId = $customer->cus_id;

            // Create Customer Color
            if (!empty($input['cl_code'])) {
                $this->customerColorModel->create([
                    'cl_name' => array_get($input, 'cl_name', "NA"),
                    'cl_code' => $input['cl_code'],
                    'cus_id'  => $customerId,
                ]);
            }

            // Save Mass Customer Zone
            $this->saveMassCustomerZone(array_get($input, 'zone_id'), $customerId, $this->customerZoneModel);

            // get whs ID from list zones
            //$whsIds = $this->getWarehouseIdOfZoneIds(array_get($input, 'zone_id'), $this->zoneModel);
            $whsIds = DB::table('warehouse')->select('whs_id')->get();
            $whsIds = array_pluck($whsIds, 'whs_id');

            foreach ($whsIds as $whsId) {
                $virtualZoneInWarehouse = $this->zoneModel->getFirstWhere(['zone_whs_id' => $whsId, 'zone_code' => 'VTZ']);
                $virtualZoneId = object_get($virtualZoneInWarehouse, 'zone_id');
                if ($virtualZoneId) {
                    DB::table('customer_zone')->insert([
                        'zone_id' => $virtualZoneId,
                        'deleted_at' => 915148800,
                        'cus_id' => $customerId,
                        'created_at' => time(),
                        'updated_at' => time(),
                        'created_by' => Data::getCurrentUserId(),
                        'updated_by' => Data::getCurrentUserId(),
                        'deleted' => 0
                    ]);
                }
            }

            // Save Mass Customer Warehouse
            $this->saveMassCustomerWarehouse($whsIds, $customerId, $this->customerWarehouseModel);

            // Save Mass Customer Address
            $this->saveMassCustomerAddress($customerId, $this->customerAddressModel, $input['addresses']);

            // Create Customer Contact
            $this->saveMassCustomerContact($customerId, $this->customerContactModel, $input['contacts']);

            // Create Customer Charge code
//            $this->saveMassCustomerChargeCode($customerId, $this->invoiceCostModel, $input['charge_codes']);

            //Create Customer configuration
            $this->upSertCusConf($customerId, $this->customerConfigModel, $input['configs']);

            $this->saveCustomerInUser($whsIds, $customerId);

            DB::commit();

            return $this->response->item($customer,
                $this->customerTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * Show Customer, Zone, Cus Warehouse Cus Address, Cus Contact
     *
     * @param $customerId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function showWhole($customerId)
    {
        try {
            $customer = $this->customerModel->getFirstBy('cus_id', $customerId, ['customerContact', 'customerColor']);

            if (empty($customer)) {
                return $this->response->item($customer, $this->customerTransformer);
            }

            // View Customer Zone
            $customerZoneModel = new CustomerZoneModel();
            $customerZone = $customerZoneModel->loadBy(['cus_id' => $customerId], ['zone'])->toArray();
            if (!empty($customerZone)) {
                $tempZone = [];
                foreach ($customerZone as $zone) {
                    $tempZone[] = [
                        'zone_id'                  => $zone['zone_id'],
                        'zone_name'                => array_get($zone, 'zone.zone_name', ''),
                        'zone_code'                => array_get($zone, 'zone.zone_code', ''),
                        'zone_description'         => array_get($zone, 'zone.zone_description', ''),
                        'zone_min_count'           => array_get($zone, 'zone.zone_min_count', ''),
                        'zone_max_count'           => array_get($zone, 'zone.zone_max_count', ''),
                        'zone_num_of_loc'          => array_get($zone, 'zone.zone_num_of_loc', ''),
                        'zone_num_of_inactive_loc' => array_get($zone, 'zone.zone_num_of_inactive_loc', ''),
                        'zone_num_of_active_loc'   => array_get($zone, 'zone.zone_num_of_active_loc', ''),
                    ];
                }
                $customer['cus_zone'] = $tempZone;
            }

            // View Customer Warehouse
            $customerWarehouseModel = new CustomerWarehouseModel();
            $customerWarehouse = $customerWarehouseModel->loadBy(['cus_id' => $customerId],
                ['warehouse', 'warehouse.warehouseStatus', 'warehouse.systemCountry', 'warehouse.systemState']);
            if (!$customerWarehouse->isEmpty()) {
                $tempWhs = [];
                foreach ($customerWarehouse as $cusWhs) {
                    $tempWhs[] = [
                        'whs_id'           => $cusWhs->warehouse->whs_id,
                        'whs_name'         => $cusWhs->warehouse->whs_name,
                        'whs_status'       => $cusWhs->warehouse->whs_status,
                        'whs_status_name'  => $cusWhs->warehouse->warehouseStatus->whs_sts_name,
                        'whs_status_desc'  => $cusWhs->warehouse->warehouseStatus->whs_sts_desc,
                        'whs_short_name'   => $cusWhs->warehouse->whs_short_name,
                        'whs_code'         => $cusWhs->warehouse->whs_code,
                        'whs_country_id'   => $cusWhs->warehouse->whs_country_id,
                        'whs_country_name' => $cusWhs->warehouse->systemCountry->sys_country_name,
                        'whs_state_id'     => $cusWhs->warehouse->whs_state_id,
                        'whs_state_name'   => $cusWhs->warehouse->systemState->sys_state_name,
                        'whs_city_name'    => $cusWhs->warehouse->whs_city_name,

                    ];
                }

                $customer->cus_whs = $tempWhs;
            }

            // View Customer Contact
            $customerContactModel = new CustomerContactModel();
            $customerContact = $customerContactModel->loadBy(['cus_ctt_cus_id' => $customerId])->toArray();
            if (!empty($customerContact)) {
                $tempCtt = [];
                foreach ($customerContact as $contact) {
                    $tempCtt[] = [
                        'cus_ctt_id'         => $contact['cus_ctt_id'],
                        'cus_ctt_fname'      => $contact['cus_ctt_fname'],
                        'cus_ctt_lname'      => $contact['cus_ctt_lname'],
                        'cus_ctt_email'      => $contact['cus_ctt_email'],
                        'cus_ctt_phone'      => $contact['cus_ctt_phone'],
                        'cus_ctt_ext'        => $contact['cus_ctt_ext'],
                        'cus_ctt_mobile'     => $contact['cus_ctt_mobile'],
                        'cus_ctt_position'   => $contact['cus_ctt_position'],
                        'cus_ctt_department' => $contact['cus_ctt_department'],

                    ];
                }
                $customer['cus_contact'] = $tempCtt;
            }

            // View Customer Address
            $customerAddressModel = new CustomerAddressModel();
            $customerAddress = $customerAddressModel->loadBy(
                ['cus_add_cus_id' => $customerId],
                ['systemCountry', 'systemState']
            )->toArray();

            if (!empty($customerAddress)) {
                $tempAddr = [];
                foreach ($customerAddress as $address) {
                    $tempAddr[] = [
                        'cus_add_id'           => $address['cus_add_id'],
                        'cus_add_type'         => $address['cus_add_type'],
                        'cus_add_line_1'       => $address['cus_add_line_1'],
                        'cus_add_line_2'       => $address['cus_add_line_2'],
                        'cus_add_city_name'    => $address['cus_add_city_name'],
                        'cus_add_state_id'     => $address['cus_add_state_id'],
                        'sys_state_code'       => array_get($address, 'system_state.sys_state_code', ''),
                        'sys_state_name'       => array_get($address, 'system_state.sys_state_name', ''),
                        'sys_state_desc'       => array_get($address, 'system_state.sys_state_desc', ''),
                        'cus_add_postal_code'  => $address['cus_add_postal_code'],
                        'cus_add_country_id'   => $address['cus_add_country_id'],
                        'cus_add_country_code' => array_get($address, 'system_country.sys_country_code', ''),
                        'cus_add_country_name' => array_get($address, 'system_country.sys_country_name', ''),
                        'cus_add_country_desc' => array_get($address, 'system_country.sys_country_desc', ''),
                    ];
                }
                $customer['cus_address'] = $tempAddr;
            }

            // View Customer Charge Code

            $chargeCodes = $this->invoiceCostModel->loadBy(
                ['cus_id' => $customerId],
                ['warehouse', 'chargeCode', 'chargeCode.systemUom', 'chargeCode.chargeType']
            );

            if (!empty($chargeCodes)) {
                $temp = [];
                foreach ($chargeCodes as $each) {
                    $temp[] = [
                        'cus_id'        => object_get($each, 'cus_id', null),
                        'whs_id'        => object_get($each, 'whs_id', null),
                        'whs_name'      => object_get($each, 'warehouse', null),
                        'whs_code'      => object_get($each, 'warehouse.whs_code', null),
                        'chg_code_id'   => object_get($each, 'chg_code_id', null),
                        'chg_code'      => object_get($each, 'chargeCode.chg_code', null),
                        'chg_code_des'  => object_get($each, 'chargeCode.chg_code_des', null),
                        'chg_uom_id'    => object_get($each, 'chargeCode.chg_uom_id', null),
                        'chg_uom_code'  => object_get($each, 'chargeCode.systemUom.sys_uom_code', null),
                        'chg_uom_name'  => object_get($each, 'chargeCode.systemUom.sys_uom_name', null),
                        'chg_type_id'   => object_get($each, 'chargeCode.chg_type_id', null),
                        'chg_type_code' => object_get($each, 'chargeCode.chargeType.chg_type_code', null),
                        'chg_type_name' => object_get($each, 'chargeCode.chargeType.chg_type_name', null),
                        'chg_cd_price'  => object_get($each, 'chg_cd_price', null),
                        'chg_cd_des'    => object_get($each, 'chg_cd_des', null),
                        'chg_cd_cur'    => object_get($each, 'chg_cd_cur', null),
                    ];
                }

                $customer->cus_charge_code = $temp;
            }
            // View Customer Configuration
            $customerConfModel = new CustomerConfigModel();
            $cusConf = $customerConfModel->loadByMany(
                ['cus_id' => $customerId, 'whs_id' => $this->getCurrentWhs()]
            );

            if (!empty($cusConf)) {
                $temp = [];
                foreach ($cusConf as $each) {
                    $temp[] = [
                        'cus_id'       => object_get($each, 'cus_id', null),
                        'whs_id'       => object_get($each, 'whs_id', null),
                        'config_name'  => object_get($each, 'config_name', null),
                        'config_value' => object_get($each, 'config_value', null),
                        'ac'           => object_get($each, 'ac', null),
                    ];
                }
                $customer->customer_config = $temp;
            }

            return $this->response->item($customer, $this->customerTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * ps:API Update Customer
     *
     * @param int $customerId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function updateWhole(
        $customerId,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        $input['cus_id'] = $customerId;

        // validation
        $this->customerSaveMassValidator->validate($input);

        $params = [
            'cus_id'              => $customerId,
            'cus_name'            => $input['cus_name'],
            'cus_status'          => $input['cus_status'],
            'cus_code'            => $input['cus_code'],
            'cus_country_id'      => array_get($input, 'cus_country_id', null),
            'cus_state_id'        => array_get($input, 'cus_state_id', null),
            'cus_city_name'       => array_get($input, 'cus_city_name', ''),
            'cus_postal_code'     => array_get($input, 'cus_postal_code', ''),
            'cus_billing_account' => array_get($input, 'cus_billing_account', ''),
            'cus_des'             => array_get($input, 'cus_des', ''),
            'sales_rep'           => array_get($input, 'sales_rep', ''),
            'billed_through'      => array_get($input, 'billed_through', '')
        ];

        try {

            // check duplicate code
            if (($customer = $this->customerModel->getFirstBy('cus_code', $params['cus_code'])) &&
                $customer->cus_id != $customerId
            ) {
                throw new \Exception(Message::get("BM006", "Customer Code"));
            }

            DB::beginTransaction();

            // Create Customer
            $customer = $this->customerModel->update($params);

            // Update Customer Color
            if (!empty($input['cl_code'])) {
                $cus = $this->customerColorModel->getFirstBy('cus_id', $customerId);
                if (empty($cus->cl_id)) {
                    // Create
                    $this->customerColorModel->refreshModel();
                    $this->customerColorModel->create([
                        'cl_name' => array_get($input, 'cl_name', "NA"),
                        'cl_code' => $input['cl_code'],
                        'cus_id'  => $customerId,
                    ]);
                } else {
                    // Update
                    $this->customerColorModel->refreshModel();
                    $this->customerColorModel->update([
                        'cl_id'   => $cus->cl_id,
                        'cl_name' => array_get($input, 'cl_name', "NA"),
                        'cl_code' => $input['cl_code'],
                        'cus_id'  => $customerId,
                    ]);
                }
            }

            // Save Mass Customer Zone
            $whsIds = [];
            $zoneIds = array_pluck($input['zones'], 'zone_id');
            $getVZ = DB::table("zone")
                ->join("zone_type", "zone_type.zone_type_id", "=", "zone.zone_type_id")
                ->select('zone.zone_id')
                ->whereIn("zone.zone_id",$zoneIds)
                ->where("zone_type.zone_type_code", "VZ")->get();

            $zone_vz = array_map(function ($value) {
                return $value['zone_id'];
            }, $getVZ);

            $customer_zone = [];
            foreach ($zoneIds as $zone) {
                if (!in_array($zone, $zone_vz)) {
                    $customer_zone[] = $zone;
                }
            }

            $this->saveMassCustomerZone($customer_zone, $customerId, $this->customerZoneModel);

            // get whs ID from list zones
            if (!empty($zoneIds)) {
                $whsIds = $this->getWarehouseIdOfZoneIds($zoneIds, $this->zoneModel);
            }


            // insert location child with cus
            $this->saveMassCustomerLoc($zone_vz, $customerId, $whsIds, $this->customerZoneModel);


            // Save Mass Customer Warehouse
            $this->saveMassCustomerWarehouse($whsIds, $customerId, $this->customerWarehouseModel);

            // Save Mass Customer Address
            $this->saveMassCustomerAddress($customerId, $this->customerAddressModel, $input['addresses']);

            // Create Customer Contact
            $this->saveMassCustomerContact($customerId, $this->customerContactModel, $input['contacts']);

            // Create Customer Charge Code
//            $this->saveMassCustomerChargeCode($customerId, $this->invoiceCostModel, $input['charge_codes']);

            //Create Customer configuration
            $this->upSertCusConf($customerId, $this->customerConfigModel, $input['configs']);

            // WMS2-5381 - auto assign customers to users
            // $this->saveCustomerInUser($whsIds, $customerId);

            DB::commit();

            return $this->response->item($customer, $this->customerTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /////////////////////////   COMMON FUNCTION //////////////////////

    private function saveMassCustomerZone($zoneIds, $customerId, CustomerZoneModel $customerZoneModel)
    {
        if (!empty($zoneIds) && !$customerZoneModel->checkAvailableZonesForSaving($zoneIds, $customerId)) {
            throw new \Exception('There are some zones was used by other customers');
        }

//        $allCustomerZone = $customerZoneModel->loadBy(['cus_id' => $customerId]);
        $allCustomerZone = $customerZoneModel->getModel()
                            ->join('zone', 'zone.zone_id', '=', 'customer_zone.zone_id')
                            ->join('zone_type', 'zone_type.zone_type_id', '=', 'zone.zone_type_id')
                            ->where('customer_zone.cus_id', '=', $customerId)
                            ->where('zone_type.zone_type_code', '<>', 'VZ')
                            ->select('customer_zone.*')
                            ->get();

        $customerDeleteIds = array_pluck($allCustomerZone, 'zone_id', 'zone_id');

        if ($zoneIds) {
            foreach ($zoneIds as $zoneId) {
                if (isset($customerDeleteIds[$zoneId])) {
                    unset($customerDeleteIds[$zoneId]);
                    continue;
                }

                $customerZoneModel->refreshModel();
                $customerZoneModel->create([
                    'cus_id'  => $customerId,
                    'zone_id' => $zoneId,
                ]);
            }
        }

        if ($customerDeleteIds) {
            foreach ($customerDeleteIds as $customerDeleteId) {
                //Zones cannot be removed while having inventories! (Zone has already item)
                $location = $this->locationModel->loadByZoneId($customerDeleteId);
                $locationArr = json_decode($location, true);
                $locationIdArr = array_pluck($locationArr, 'loc_id');
                if ($this->cartonModel->checkWhereIn($locationIdArr)) {
                    throw new \Exception("Zones cannot be removed while having inventories!");
                }

                $customerZoneModel->deleteByKeys([
                    'cus_id'  => $customerId,
                    'zone_id' => $customerDeleteId,
                ]);

            }
        }
    }

    private function saveMassCustomerLoc($zoneIds, $customerId, $whsIds, CustomerZoneModel $customerZoneModel)
    {
        ini_set('max_execution_time', 0);

        $allCustomerZone = $customerZoneModel->getModel()
            ->join('zone', 'zone.zone_id', '=', 'customer_zone.zone_id')
            ->join('zone_type', 'zone_type.zone_type_id', '=', 'zone.zone_type_id')
            ->where('customer_zone.cus_id', '=', $customerId)
            ->where('zone_type.zone_type_code', '=', 'VZ')
            ->select('customer_zone.*')
            ->get();

        $allCustomerZone = array_pluck($allCustomerZone, 'zone_id', 'zone_id');

        if ($zoneIds) {
            foreach ($zoneIds as $key => $zoneId) {
                if (in_array($zoneId, $allCustomerZone)) {
                    unset($zoneIds[$key]);
                }
                continue;
            }
        }

        $cus_loc = DB::table('location')
            ->where('loc_cus_id', '=', $customerId)
            ->where('is_block_stack', '=', 1)
            ->whereIn('loc_whs_id', $whsIds)
            ->whereIn('loc_zone_id', $zoneIds)
            ->select(['loc_id', 'parent_id', 'loc_cus_id', 'loc_zone_id', 'loc_whs_id'])
            ->first();

        if ($zoneIds && empty($cus_loc)) {
            $max_loc_ids = DB::table('location')
                ->join('zone', 'zone.zone_id', '=', 'location.loc_zone_id')
                ->join('zone_type', 'zone_type.zone_type_id', '=', 'zone.zone_type_id')
                ->where('location.is_block_stack', '=', 1)
                ->whereIn('location.loc_whs_id', $whsIds)
                ->whereIn('location.loc_zone_id', $zoneIds)
                ->whereNotNull('location.parent_id')
                ->where('zone_type.zone_type_code', '=', 'VZ')
                ->groupBy('location.parent_id', 'location.spc_hdl_code')
                ->select(DB::raw('MAX(location.loc_id) as loc_id'))
                ->get();

            $array_max_loc_ids = array_map(function($location) {
                return array_get($location, 'loc_id');
            }, $max_loc_ids);

            $max_locations = DB::table('location')
                ->whereIn('loc_id', $array_max_loc_ids)
                ->get();

            $new_cus_loc = [];
            foreach ($max_locations as $max_loc) {
                $max_loc_arr = explode('-', $max_loc['loc_code']);
                $old_bin = $max_loc_arr[max(array_keys($max_loc_arr))];
                $new_bin = intval(trim($old_bin)) + 1;
                $max_loc_arr[max(array_keys($max_loc_arr))] = $new_bin;
                $new_loc_code = implode('-', $max_loc_arr);
                $new_cus_loc[] = [
                    "loc_code" => $new_loc_code,
                    "loc_alternative_name" => $new_loc_code,
                    "loc_whs_id" => $max_loc['loc_whs_id'],
                    "loc_cus_id" => $customerId,
                    "loc_zone_id" => $max_loc['loc_zone_id'],
                    "loc_type_id" => $max_loc['loc_type_id'],
                    "loc_sts_code" => $max_loc['loc_sts_code'],
                    "loc_available_capacity" => $max_loc['loc_available_capacity'],
                    "loc_width" => $max_loc['loc_width'],
                    "loc_length" => $max_loc['loc_length'],
                    "loc_height" => $max_loc['loc_height'],
                    "loc_weight_capacity" => $max_loc['loc_weight_capacity'],
                    "loc_max_count" => $max_loc['loc_max_count'],
                    "created_at" => time(),
                    "updated_at" => time(),
                    "created_by" => Data::getCurrentUserId(),
                    "updated_by" => Data::getCurrentUserId(),
                    "deleted_at" => 915148800,
                    "deleted" => 0,
                    "max_lpn" => $max_loc['max_lpn'],
                    "parent_id" => $max_loc['parent_id'],
                    "plt_lv" => $new_bin,
                    "spc_hdl_code" => $max_loc['spc_hdl_code'],
                    "spc_hdl_name" => $max_loc['spc_hdl_name'],
                    "is_block_stack" => $max_loc['is_block_stack'],
                    "plt_type" => $max_loc['plt_type'],
                    "room" => $max_loc['room'],
                    "aisle" => $max_loc['aisle'],
                    "row" => $max_loc['row'],
                    "level" => $max_loc['level'],
                    "bin" => $new_bin,
                    "area" => $max_loc['area'],
                    "reserved_at" => $max_loc['reserved_at'],
                    "fixed_pick" => $max_loc['fixed_pick']
                ];
            }

            $insert_data = collect($new_cus_loc);
            $chunks = $insert_data->chunk(500);
            foreach ($chunks as $chunk) {
                DB::table('location')->insert($chunk->toArray());
            }

            $cus_zone_data = [];
            foreach ($zoneIds as $zoneId) {
                $cus_zone_data[] = [
                    'zone_id' => $zoneId,
                    'deleted_at' => 915148800,
                    'cus_id' => $customerId,
                    'created_at' => time(),
                    'updated_at' => time(),
                    'created_by' => Data::getCurrentUserId(),
                    'updated_by' => Data::getCurrentUserId(),
                    'deleted' => 0
                ];
            }

            DB::table('customer_zone')->insert($cus_zone_data);
        }
    }


    private function saveMassCustomerWarehouse($whsIds, $customerId, CustomerWarehouseModel $customerWarehouseModel)
    {
        $allCustomerWarehouse = $customerWarehouseModel->loadByMany(['cus_id' => $customerId]);

        $customerDeleteIds = array_pluck($allCustomerWarehouse, 'whs_id', 'whs_id');

        if ($whsIds) {
            foreach ($whsIds as $whsId) {
                if (isset($customerDeleteIds[$whsId])) {
                    unset($customerDeleteIds[$whsId]);
                    continue;
                }

                $customerWarehouseModel->refreshModel();
                $customerWarehouseModel->create([
                    'cus_id' => $customerId,
                    'whs_id' => $whsId
                ]);
            }
        }

        // if ($customerDeleteIds) {
        //     $customerWarehouseModel->deleteByMany($customerId, $customerDeleteIds);
        // }
    }

    private function saveCustomerInUser($whsIds, $customerId)
    {
//        DB::setFetchMode(\PDO::FETCH_ASSOC);
//        $userIds = DB::table('user_whs')
//            ->join('user_role', function($join)
//                {
//                    $join->on('user_whs.user_id', '=', 'user_role.user_id');
//                    $join->whereNotIn('user_role.item_name', ['CSR', 'CSR Manager']);
//                })
//            ->whereIn('user_whs.whs_id', $whsIds)
//            ->pluck('user_whs.user_id');
//        $userIds = array_unique($userIds);
//        $inserts = [];
//        $data = [
//            'cus_id' => $customerId,
//        ];
//        foreach ($whsIds as $whsId) {
//            $data['whs_id'] = $whsId;
//            foreach ($userIds as $userId) {
//                $data['user_id'] = $userId;
//                $inserts[] = $data;
//            }
//
//        }
//        DB::table('cus_in_user')
//            ->where('cus_id', $customerId)
//            ->whereIn('whs_id', $whsIds)
//            ->delete();
//        DB::table('cus_in_user')->insert($inserts);

            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $whs_id = $this->getCurrentWhs();
            $all_user = DB::table('users')
                ->join('user_whs', 'user_whs.user_id', '=', 'users.user_id')
                ->join('warehouse', 'warehouse.whs_id', '=', 'user_whs.whs_id')
                ->select(['users.user_id', 'user_whs.whs_id'])
                //->where('user_whs.whs_id', '=', $whs_id)
                ->get();

            $data_insert = [];
            foreach ($all_user as $user) {
                $data_insert[] = [
                    'cus_id'    => $customerId,
                    'user_id'   => $user['user_id'],
                    'whs_id'    => $user['whs_id']
                ];
            }

            DB::table('cus_in_user')->insert($data_insert);
    }

    private function saveMassCustomerAddress($customerId, CustomerAddressModel $customerAddressModel, $addresses = [])
    {
        $allCustomerAddress = $customerAddressModel->loadByMany(['cus_add_cus_id' => $customerId]);

        $customerDeleteIds = array_pluck($allCustomerAddress, 'cus_add_id', 'cus_add_id');

        if (!empty($addresses)) {
            foreach ($addresses as $address) {
                if (isset($address['cus_add_id']) && isset($customerDeleteIds[$address['cus_add_id']])) {
                    unset($customerDeleteIds[$address['cus_add_id']]);
                } elseif (!empty($address['cus_add_id']) && !isset($customerDeleteIds[$address['cus_add_id']])) {
                    throw new \Exception('Address input doesn\'t belong to this customer');
                }

                $data = [
                    'cus_add_cus_id'      => $customerId,
                    'cus_add_type'        => array_get($address, 'cus_add_type', ''),
                    'cus_add_line_1'      => array_get($address, 'cus_add_line_1', ''),
                    'cus_add_line_2'      => array_get($address, 'cus_add_line_2', ''),
                    'cus_add_city_name'   => array_get($address, 'cus_add_city_name', ''),
                    'cus_add_state_id'    => array_get($address, 'cus_add_state_id', null),
                    'cus_add_postal_code' => array_get($address, 'cus_add_postal_code', ''),
                    'cus_add_country_id'  => array_get($address, 'cus_add_country_id', null),
                ];

                $customerAddressModel->getModel()
                    ->where([
                        'cus_add_cus_id'      => $customerId,
                        'cus_add_type'        => array_get($address, 'cus_add_type', '')
                    ])->update([
                        'deleted_at' => time(),
                        'deleted'    => 1
                    ]);

                if (!empty($address['cus_add_id'])) {
                    $data['deleted_at'] = 915148800;
                    $data['deleted'] = 0;

                    $customerAddressModel->getModel()->withTrashed()
                        ->where([
                            'cus_add_id' => $address['cus_add_id']
                        ])->update($data);
                } else {
                    $customerAddressModel->refreshModel();
                    $customerAddressModel->create($data);
                }

            }
        }

        if ($customerDeleteIds) {
            $customerAddressModel->deleteById($customerDeleteIds);
        }
    }

    private function saveMassCustomerContact($customerId, CustomerContactModel $customerContactModel, $contacts = [])
    {
        $allCustomerContact = $customerContactModel->loadByMany(['cus_ctt_cus_id' => $customerId]);

        $customerDeleteIds = array_pluck($allCustomerContact, 'cus_ctt_id', 'cus_ctt_id');

        if (!empty($contacts)) {
            foreach ($contacts as $contact) {
                if (isset($contact['cus_ctt_id']) && isset($customerDeleteIds[$contact['cus_ctt_id']])) {
                    unset($customerDeleteIds[$contact['cus_ctt_id']]);
                } elseif (!empty($contact['cus_ctt_id']) && !isset($customerDeleteIds[$contact['cus_ctt_id']])) {
                    throw new \Exception('Contact input doesn\'t belongs to this customer');
                }

                $data = [
                    'cus_ctt_cus_id'     => $customerId,
                    'cus_ctt_fname'      => array_get($contact, 'cus_ctt_fname', ''),
                    'cus_ctt_lname'      => array_get($contact, 'cus_ctt_lname', ''),
                    'cus_ctt_email'      => array_get($contact, 'cus_ctt_email', ''),
                    'cus_ctt_phone'      => array_get($contact, 'cus_ctt_phone', ''),
                    'cus_ctt_ext'        => array_get($contact, 'cus_ctt_ext', ''),
                    'cus_ctt_mobile'     => array_get($contact, 'cus_ctt_mobile', ''),
                    'cus_ctt_position'   => array_get($contact, 'cus_ctt_position', ''),
                    'cus_ctt_department' => array_get($contact, 'cus_ctt_department', ''),
                ];

                $customerContactModel->refreshModel();
                if (!empty($contact['cus_ctt_id'])) {
                    $data['cus_ctt_id'] = $contact['cus_ctt_id'];

                    $customerContactModel->update($data);
                } else {
                    $customerContactModel->create($data);
                }
            }
        }

        if ($customerDeleteIds) {
            $customerContactModel->deleteById($customerDeleteIds);
        }
    }

    private function saveMassCustomerChargeCode(
        $customerId,
        InvoiceCostModel $invoiceCostModel,
        $chargeCodes = []
    ) {
        // Duplicate data
        $duplicate = [];
        if (!empty($chargeCodes)) {
            foreach ($chargeCodes as $chargeCode) {
                if (in_array($customerId . '-' . $chargeCode['whs_id'] . '-' . $chargeCode['chg_code_id'],
                    $duplicate)) {
                    return $this->response->errorBadRequest(Message::get("BM006", "Charge Code"));
                }
                $duplicate[] = $customerId . '-' . $chargeCode['whs_id'] . '-' . $chargeCode['chg_code_id'];
            }
        }

        $allCustomerChargeDetail = $invoiceCostModel->loadByMany([
            'cus_id' => $customerId
        ]);

        $customerChargeCodeGot = [];
        if (!$allCustomerChargeDetail->isEmpty()) {
            foreach ($allCustomerChargeDetail as $item) {
                $customerChargeCodeGot[$customerId . '-' . $item->whs_id . '-' . $item->chg_code_id] = $item;
            }
        }

        if (!empty($chargeCodes)) {
            foreach ($chargeCodes as $chargeCode) {
                // Create new customer charge code if has new charge code
                $checkItem = $customerId . '-' . $chargeCode['whs_id'] . '-' . $chargeCode['chg_code_id'];

                $invoiceCostModel->refreshModel();

                if (isset($customerChargeCodeGot[$checkItem])) {
                    unset($customerChargeCodeGot[$checkItem]);
                }

                $invoiceCostModel->saveChargeDetail([
                    'cus_id'       => $customerId,
                    'whs_id'       => $chargeCode['whs_id'],
                    'chg_code_id'  => $chargeCode['chg_code_id'],
                    'chg_cd_price' => array_get($chargeCode, 'chg_cd_price', null),
                    'chg_cd_cur'   => array_get($chargeCode, 'chg_cd_cur', 'USD'),
                    'chg_cd_des'   => array_get($chargeCode, 'chg_code_des', '')
                ]);
            }
        }

        if ($customerChargeCodeGot) {
            foreach ($customerChargeCodeGot as $key => $value) {

                $invoiceCostModel->deleteByKeys([
                    'cus_id'      => $value->cus_id,
                    'whs_id'      => $value->whs_id,
                    'chg_code_id' => $value->chg_code_id
                ]);
            }
        }
    }

    private function checkChargeCodeIsUsing($chargeId, $whs_id, $cus_id)
    {
        $woDtl = new WODetailModel();
        $woDetail = $woDtl->getWODetailByCusUnique($chargeId, $whs_id, $cus_id);
        if ($woDetail > 0) {
            return true;
        }

        return false;
    }

    private function getWarehouseIdOfZoneIds($zoneIds, ZoneModel $zoneModel)
    {
        return $zoneModel->getWhsIdsByZoneIds($zoneIds);
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function export(Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $type = strtolower(array_get($input, 'type', 'csv'));

        try {
            if (!in_array($type, ['csv', 'pdf'])) {
                throw new \Exception(Message::get("VR005", "(.csv|.pdf)"));
            }

            $customers = $this->customerModel->search($input,
                ['customerAddress.systemState', 'customerContact', 'customerWarehouse', 'customerZone'],
                array_get($input, 'limit', 999999999));

            // Show first contact
            if (!empty($customers)) {
                foreach ($customers as $key => $customer) {
                    $customerContact = $customer->customerContact;

                    if (!empty($customerContact)) {
                        foreach ($customerContact as $contact) {
                            $customers[$key]['customerContact'] = $contact;
                            break;
                        }
                    }
                }
            }

            $dataTitle = [
                'Customer Code'        => "cus_code",
                'Billing Account Code' => "cus_billing_account",
                'Customer Name'        => "cus_name",
                'City'                 => "customerAddress.cus_add_city_name",
                'State'                => "customerAddress.systemState.sys_state_name",
                'Zipcode'              => "customerAddress.cus_add_postal_code",
                'Contact'              => "customerContact.cus_ctt_fname|customerContact.cus_ctt_lname",
                'Phone'                => "customerContact.cus_ctt_phone",
                'Email'                => "customerContact.cus_ctt_email",
            ];

            if (SelExport::setDataList($customers, $dataTitle)) {
                SelExport::exportList("Export_Customer_List_" . date("Y-m-d", time()), $type);
            }

            return $this->response->noContent();

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_CUSTOMER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $customerId
     * @param CustomerConfigModel $cusConfModel
     * @param $input
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    private function upSertCusConf($customerId, CustomerConfigModel $cusConfModel, $input)
    {
        $params = [];

        foreach ($input as $each) {
            $params[] = [
                'cus_id'       => $customerId,
                'whs_id'       => $this->getCurrentWhs(),
                'config_name'  => array_get($each, 'config_name', ''),
                'config_value' => array_get($each, 'config_value', ''),
                'ac'           => array_get($each, 'ac', ''),
            ];
        }

        try {
            foreach ($params as $item) {
                // check user exist on table cus_conf
                $isExisted = $cusConfModel->getCustomerConfigs($item);

                if (is_null($isExisted)) {
                    //insert
                    $item['sts'] = 'i';
                    $this->customerConfigModel->refreshModel();
                    $this->customerConfigModel->create($item);
                } else {
                    //update
                    $this->customerConfigModel->refreshModel();
                    $this->customerConfigModel->updateCustomerConfigs($item);
                }
            }

            return $this->response->noContent()->setContent("{}")
                ->setStatusCode(IlluminateResponse::HTTP_OK);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ITEM_MASTER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @return mixed
     */
    private function getCurrentWhs()
    {
        $redis = new Data();
        $userInfo = $redis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        return $currentWH;
    }

    /**
     * Return customers(in current user) have damage cartons
     *
     * @return Array
     */
    public function customerDamage(Request $request) {
        $redis = new Data();
        $userInfo = $redis->getUserInfo();
        $userId = array_get($userInfo, 'user_id', null);
        $input = $request->getQueryParams();
        $md = new CustomerDamageModel();
        $data = $md->getCustomerHaveDamagedCarton($userId, $input, PHP_INT_MAX);

        return $this->response->paginator($data, new CustomerDamageTransformer);
    }

    public function generateColor()
    {
        ini_set('max_execution_time', 0); // 0 = Unlimited
        $customer = \DB::table('customer')->select('cus_id')->get();
        if(!empty($customer)){
            $cusIdArr = array_pluck($customer,'cus_id',0);
            foreach ($cusIdArr as $customerId) {
                $cus = $this->customerColorModel->getFirstBy('cus_id', $customerId);
                if (empty($cus->cl_id)) {
                    // Create
                    $this->customerColorModel->refreshModel();
                    $this->customerColorModel->create([
                        'cl_name' => "NA",
                        'cl_code' => '#'.$this->random_color(),
                        'cus_id' => $customerId,
                    ]);
                } else {
                    // Update
                    $this->customerColorModel->refreshModel();
                    $this->customerColorModel->update([
                        'cl_id' => $cus->cl_id,
                        'cl_name' => "NA",
                        'cl_code' => '#'.$this->random_color(),
                        'cus_id' => $customerId,
                    ]);
                }
            }
        }
    }

    private function random_color_part()
    {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    private function random_color()
    {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }
}
