<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\Gate\Models;

use Seldat\Wms2\Models\GateDtl;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SelArr;

class GateDtlModel extends AbstractModel
{
    /**
     * GateDtlModel constructor.
     *
     * @param GateDtl|null $model
     */
    public function __construct(GateDtl $model = null)
    {
        $this->model = ($model) ?: new GateDtl();
    }

    /**
     * @param $attributes
     * @param array $with
     * @param $limit
     *
     * @return mixed
     */
    public function search($attributes, array $with, $limit)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $this->sortBuilder($query, $attributes);
        $this->model->filterData($query);

        return $query->paginate($limit);
    }

    /**
     * @param $gateIds
     *
     * @return mixed
     */
    public function getGateDtlOpenByGateIds($gateIds)
    {
        $gateIds = is_array($gateIds) ? $gateIds : [$gateIds];

        $query = $this->model
            ->where('gate_dtl_sts', Status::getByValue('Open', 'Gate_Dtl_Status'))
            ->whereIn('gate_id', $gateIds);

        $this->model->filterData($query, false, false, false);

        $rows = $query->get();

        return $rows;
    }

    /**
     * @param $attributes
     * @param array $with
     * @param $limit
     *
     * @return mixed
     */
    public function report($attributes, array $with, $limit, array $orderBy = [])
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        if (!empty($attributes['gate_id'])) {
            $query->where('gate_id', $attributes['gate_id']);
        }

        if (!empty($attributes['cus_id'])) {
            $query->where('cus_id', $attributes['cus_id']);
        }

        if (!empty($attributes['ctnr_id'])) {
            $query->where('ctnr_id', $attributes['ctnr_id']);
        }

        if (!empty($attributes['open_from'])) {
            $query->where('open', ">=", date("Y-m-d H:i:s", strtotime($attributes['open_from'] . " 00:00:00")));
        }

        if (!empty($attributes['open_to'])) {
            $query->where('open', "<=", date("Y-m-d H:i:s", strtotime($attributes['open_to'] . " 23:59:59")));
        }

        if (!empty($attributes['close_from'])) {
            $query->where('close', ">=", date("Y-m-d H:i:s", strtotime($attributes['close_from'] . " 00:00:00")));
        }

        if (!empty($attributes['close_to'])) {
            $query->where('close', "<=", date("Y-m-d H:i:s", strtotime($attributes['close_to'] . " 23:59:59")));
        }

        $this->sortBuilder($query, $attributes, ['gate_name']);

        $this->model->filterData($query);

        foreach ($orderBy as $column => $sortType) {
            $query->orderBy($column, $sortType);
        }

        return $query->paginate($limit);
    }
}
