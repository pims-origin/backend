<?php

namespace App\Api\V1\Traits;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\CycleDisModel;
use App\Api\V1\Models\CycleDtlModel;
use App\Api\V1\Models\CycleHdrModel;
use App\Api\V1\Models\CycleCountNotificationModel;
use App\Api\V1\Models\CycleProcess;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\LockModel;
use App\Api\V1\Models\PalletModel;
use App\Api\V1\Services\CycleCountService;
use App\Api\V1\Validators\RecountValidator;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\CycleDtl;
use Seldat\Wms2\Models\CycleHdr;
use Seldat\Wms2\Models\Item;
use Wms2\UserInfo\Data;
use Illuminate\Http\Response as IlluminateResponse;
use App\Api\V1\Models\LocationModel;
use Seldat\Wms2\Utils\Message;
use App\Api\V1\Models\ItemModel;

trait CycleNotificationModelTrait
{

    /**
     * @param $input
     *
     * @return bool
     */
    public function processCreateCycleHdrForListLocation($input, $fromCcn = false)
    {
        $cycleHdr = new CycleHdr();

        //validate and get CycleDtl data
        $validate = $this->validateCreateCycleHdrForListLocation($input, $fromCcn);        
        
        if (!$validate) {
            return false;
        }

        $itemIds = array_column($this->cycleDtlData, 'item_id');
        $locIds = array_column($this->cycleDtlData, 'sys_loc_id');
        $cycleDetail = $input['cycle_type'] == self::CYCLE_TYPE_LOCATION ?
            $this->locIdsRange : $input['cycle_detail'];
        try {

            DB::beginTransaction();

            $model = $cycleHdr->create([
                'cycle_name'     => $input['cycle_name'],
                'cycle_num'      => $input['cycle_num'],
                'whs_id'         => $input['whs_id'],
                'count_by'       => $input['cycle_count_by'],
                'cycle_method'   => $input['cycle_method'],
                'cycle_type'     => $input['cycle_type'],
                'cycle_detail'   => implode(',', $cycleDetail),
                'description'    => $input['cycle_des'],
                'assignee_id'    => $input['cycle_assign_to'],
                'due_dt'         => $input['cycle_due_date'],
                'has_color_size' => $input['cycle_has_color_size'],
                'cycle_sts'      => self::STATUS_CYCLE_ASSIGNED,
                'sts'            => self::STATUS_INSERT
            ]);
            $this->createCycleDtls($model, $this->cycleDtlData, explode(',', $input['ccn_ids']));

            $this->processLockWithCycleHdr($model, $itemIds, $locIds);

            DB::commit();
        } catch (Exception $e) {

            DB::rollback();
            $this->errors[] = $e->getMessage();

            return false;
        }

        return $model;
    }

    /**
     * @param $data
     *
     * @return bool
     */
    public function validateCreateCycleHdrForListLocation(&$data, $fromCcn = false)
    {        
        $ccNotificationModel = new CycleCountNotificationModel();
        $ccDtlModel = new CycleDtlModel();
        $validInput = $this->validateInputCreateCycleHdr2($data, $fromCcn);

        //validate cc_notification is New or not before create cycleDtl
        $ccnIds = explode(',', $data['ccn_ids']);
        $skus = [];

        foreach ($ccnIds as $ccnId) {
            $ccNotificationInfo = $ccNotificationModel->getStatusCCNByIds($ccnId);

            array_push($skus, $ccNotificationInfo['sku']);
            if ($ccNotificationInfo) {
                if ($ccNotificationInfo['cc_ntf_sts'] != 'NW') {
                    $this->errors['status'] = ' Status of cycle count notification is not NEW';

                    return false;
                }
            }

            if ($ccNotificationInfo) {
                if ($ccNotificationInfo['cc_ntf_sts'] == 'NW') {
                    //check Cycle count with item_id, lot, pack, remain, sys_loc_id is existed or not before create cycleDtl
                    $ccDtlInfo = $ccDtlModel->getCCDtlBySku($ccNotificationInfo);
                    if ($ccDtlInfo) {                        
                        $this->errors['CCDtl'] = 'A respective cycle count is created.';

                        return false;
                    }
                    //check Location is being cycle counted or not
                    $checkLocationBeingCC = $ccDtlModel->getFirstWhere([
                        'sys_loc_id' => $ccNotificationInfo['loc_id'],
                        ['cycle_dtl_sts', '!=' , 'AC']
                    ]);

                    if ($checkLocationBeingCC) {                        
                        $this->errors['CCDtl'] = 'Location ' . $ccNotificationInfo['loc_code'] . ' is being cycle counted.';

                        return false;
                    }
                }
            }
        }
        
        if (!$validInput) {            
            return $validInput;
        }

        $itemModel = new ItemModel();

        $cycleDetail = $data['cycle_type'] == self::CYCLE_TYPE_LOCATION ?
            $this->locIdsRange : $data['cycle_detail'];
        $loccationLockOnCycle = LocationModel::getLocationLockByCycleType(
            $data['whs_id'],
            $data['cycle_type'],
            $cycleDetail
        );

        if ($loccationLockOnCycle && !$fromCcn) {
            $this->errors['sku'] = 'This Cycle count can not create. Locations locked: ' ;/*implode(',',
                    $loccationLockOnCycle)*/;

            return false;
        }

        if($fromCcn){
            $cycleDtls = $this->getCycleDataWhenCreateFromCcn(            
                $data['whs_id'],
                $data['cycle_type'],
                $cycleDetail              
            );
        }else{
            $cycleDtls = $this->getCycleData(            
                $data['whs_id'],
                $data['cycle_type'],
                $cycleDetail            
            );
        }

        if (!$cycleDtls) {
            $this->errors['cus_id'] = $this->getErrorMsgWithNoInventory($data['cycle_type'], $cycleDetail);

            return false;
        }

        $itemIds = array_unique(array_column($cycleDtls, 'item_id'));
        $itemsLock = $itemModel->getItemLocked($data['whs_id'], $itemIds);

        //if ($itemsLock) {
        //    $this->errors[] = $itemModel->getErrorMsgWithSKUExist($itemsLock);
        //
        //    return false;
        //}

        if (!$data['cycle_has_color_size']) {
            $hasColorSize = $this->checkHasColorSize($cycleDtls);
            if ($hasColorSize) {
                $this->errors['sku'] = Message::get('BM071', implode(',', array_keys($hasColorSize)));

                return false;
            }
        }

        $this->cycleDtlData = $cycleDtls;

        return true;
    }

    /**
     * @param $data
     *
     * @return bool
     */
    public function validateInputCreateCycleHdr2(&$data, $fromCcn = false)
    {        
        if (!$data) {
            return false;
        }
        //VR029:{0} is not empty
        //BM067:{0} not found.
        if (!$data['whs_id']) {
            $this->errors['whs_id'] = Message::get('VR029', 'Warehouse');
        } else if (!$this->checkExitsWarehouse($data['whs_id'])) {
            $this->errors['whs_id'] = Message::get('BM067', 'Warehouse');
        }

        if (!$data['cycle_detail']) {
            $this->errors['cycle_detail'] = Message::get('VR029', 'Cycle detail');
        } else {
            $data['cycle_detail'] = array_map('trim', explode(',', $data['cycle_detail']));
        }

        //BM088: {0} is required
        if (!isset($data['cycle_has_color_size'])) {
            $this->errors['cycle_has_color_size'] = Message::get('BM088', 'has_color_size');
        } else {
            $data['cycle_has_color_size'] = $data['cycle_has_color_size'] == 'true' ? 1 : 0;
        }

        if (!isset($data['cycle_count_by'])) {
            $this->errors['cycle_count_by'] = Message::get('BM088', 'count_by');
        } else {
            $data['cycle_count_by'] = strtoupper($data['cycle_count_by']);
        }

        //BM081: {0} is invalid
        if (!$data['cycle_type']) {
            $this->errors['cycle_type'] = Message::get('VR029', 'Cycle type');
        } else if (!in_array(strtoupper($data['cycle_type']), $this->cycleType)) {
            $this->errors['cycle_type'] = Message::get('BM081', 'Cycle type');
        } else {
            $data['cycle_type'] = strtoupper($data['cycle_type']);
            //validate by cycle type
            $this->validateDataByCycleType2($data, $fromCcn);
        }

        if ($data['cycle_due_date']) {
            $data['cycle_due_date'] = $this->reFormatDueDate($data['cycle_due_date']);
        }

        return $this->errors ? false : true;
    }

    /**
     * @param $cycleType
     * @param $cycleDetail
     */
    public function validateDataByCycleType2($data, $fromCcn = false)
    {
        $result = false;

        switch (strtoupper($data['cycle_type'])) {
            case self::CYCLE_TYPE_CUSTOMER:
                $result = $this->checkValidCusInput($data);
                break;
            case self::CYCLE_TYPE_SKU:
                $result = $this->checkValidSKUInput($data);
                break;
            case self::CYCLE_TYPE_LOCATION:
                $result = $this->checkValidLocIdsInput2($data, $fromCcn);
                break;
        }

        return $result;
    }

    /**
     * @param $locIds
     * @param $whsId
     */
    private function checkValidLocIdsInput2($data, $fromCcn = false)
    {        
        $locCodes = isset($data['cycle_detail']) ? $data['cycle_detail'] : null;
        $whsId = isset($data['whs_id']) ? $data['whs_id'] : null;

        if (!($locCodes && $whsId)) {
            $this->errors['locId'] = Message::get('BM081', 'Locations');

            return false;
        }

        $locInfos = LocationModel::getLocationByCodes($locCodes)->toArray();
        $locCodeValid = array_column($locInfos, 'loc_code');

        $locInValid = array_diff($locCodes, $locCodeValid);

        if ($locInValid && !$fromCcn) {

            $this->errors['locId'] = Message::get('BM094', implode(', ', array_unique($locInValid)));

            return false;
        }

        $locRange = LocationModel::getRangeLocationByArray($whsId, $locCodes)->toArray();
        if (!$locRange  && !$fromCcn) {
            $strList = implode(', ', array_unique($locCodes));
            $this->errors['locId'] = 'No location in list ' . $strList;

            return false;
        }

        $this->locIdsRange = array_column($locRange, 'loc_id');

        return true;
    }
}