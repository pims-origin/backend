<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen
 * Date: 26-Jul-2016
 * Time: 3:08
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\DamageTypeModel;
use App\Api\V1\Transformers\DamageTypeTransformer;
use App\Api\V1\Validators\DamageTypeValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Seldat\Wms2\Utils\Message;

class DamageTypeController extends AbstractController
{
    /**
     * @var damageTypeModel
     */
    protected $damageTypeModel;

    /**
     * DamageTypeController constructor.
     *
     * @param DamageTypeModel $dmgTypeModel
     */
    public function __construct(DamageTypeModel $dmgTypeModel)
    {
        $this->damageTypeModel = $dmgTypeModel;
    }

    /**
     * Damage Type Detail
     *
     * @param int $dmgTypeId
     * @param DamageTypeTransformer $damageTypeTransformer
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($dmgTypeId, DamageTypeTransformer $damageTypeTransformer)
    {
        try {
            $dmgType = $this->damageTypeModel->getFirstBy('dmg_id', $dmgTypeId);

            return $this->response->item($dmgType, $damageTypeTransformer);
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * Damage Type Search
     *
     * @param DamageTypeTransformer $dmgTypeTransformer
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request, DamageTypeTransformer $dmgTypeTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $dmgType = $this->damageTypeModel->search($input, [], array_get($input, 'limit', 20));

            return $this->response->paginator($dmgType, $dmgTypeTransformer);
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     *
     * Damage Type Create
     *
     * @param Request $request
     * @param DamageTypeValidator $dmgTypeValidator
     * @param DamageTypeTransformer $dmgTransformer
     *
     * @return mixed
     */
    public function store(
        Request $request,
        DamageTypeValidator $dmgTypeValidator,
        DamageTypeTransformer $dmgTransformer
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $dmgTypeValidator->validate($input);

        $params = [
            'dmg_code' => $input['dmg_code'],
            'dmg_name' => array_get($input, 'dmg_name', ''),
            'dmg_des'  => array_get($input, 'dmg_des', ''),
        ];

        try {

            // Check exist damage type
            if ($this->damageTypeModel->getFirstBy('dmg_code', $input['dmg_code'])) {
                return $this->response->errorBadRequest(Message::get("BM006", "damage type code"));
            }

            // check duplicate damage type
            $dmgType = $this->damageTypeModel->getFirstWhere([
                'dmg_name' => $params['dmg_name']
            ]);

            if (!empty($dmgType)) {
                return $this->response->errorBadRequest(Message::get("BM006", "damage type name"));
            }

            if ($dmgType = $this->damageTypeModel->create($params)) {
                return $this->response->item($dmgType,
                    $dmgTransformer)->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param int $dmgTypeId
     * @param Request $request
     * @param DamageTypeValidator $dmgTypeValidator
     * @param DamageTypeTransformer $dmgTypeTransformer
     *
     * @return mixed
     */
    public function update(
        $dmgTypeId,
        Request $request,
        DamageTypeValidator $dmgTypeValidator,
        DamageTypeTransformer $dmgTypeTransformer
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $dmgTypeValidator->validate($input);

        $params = [
            'dmg_id'   => $dmgTypeId,
            'dmg_code' => $input['dmg_code'],
            'dmg_name' => array_get($input, 'dmg_name', ''),
            'dmg_des'  => array_get($input, 'dmg_des', ''),
        ];

        try {
            // check duplicate damage code
            if (($dmgType = $this->damageTypeModel->getFirstBy('dmg_code', $params['dmg_code']))
                && $dmgType->dmg_id != $dmgTypeId
            ) {
                throw new \Exception(Message::get("BM006", "damage type code"));
            }

            // check duplicate damage type
            $dmgType = $this->damageTypeModel->getFirstWhere([
                'dmg_name' => $params['dmg_name']
            ]);

            if (!empty($dmgType) && $dmgType->dmg_id != $dmgTypeId) {
                return $this->response->errorBadRequest(Message::get("BM006", "damge type name"));
            }

            if ($dmgType = $this->damageTypeModel->update($params)) {
                return $this->response->item($dmgType, $dmgTypeTransformer);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * Delete damage type
     *
     * @param int $dmgId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($dmgId)
    {
        try {
            if ($this->damageTypeModel->deleteDamageType($dmgId)) {
                return $this->response->noContent();
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }
}
