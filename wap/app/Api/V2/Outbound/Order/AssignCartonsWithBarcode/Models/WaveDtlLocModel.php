<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\V2\Outbound\Order\AssignCartonsWithBarcode\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\WaveDtlLoc;
use Wms2\UserInfo\Data;

class WaveDtlLocModel extends AbstractModel
{

    /**
     * WaveDtlLocModel constructor.
     *
     * @param WaveDtlLoc|null $model
     */
    public function __construct(WaveDtlLoc $model = null)
    {
        $this->model = ($model) ?: new WaveDtlLoc();
    }


    /**
     * @param $nwActLoc
     * @param $wvDtlId
     *
     * @return
     */
    public function updateWaveDtlLoc($nwActLoc, $wvId, $wvDtlId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $wvDtlLoc = $this->model->where('wv_dtl_id', $wvDtlId)->first();
        if ($wvDtlLoc) {
            $actLocIds = \GuzzleHttp\json_decode($wvDtlLoc->act_loc_ids ?: '[]', true);
            $actLocIds[] = $nwActLoc;
            $strActLocId = \GuzzleHttp\json_encode($actLocIds);

            return $this->model
                ->where('wv_dtl_id', '=', $wvDtlId)
                ->update([
                    'act_loc_ids' => $strActLocId,
                    'updated_by'  => Data::getCurrentUserId(),
                    'updated_at'  => time()
                ]);
        }

        $strActLocId = \GuzzleHttp\json_encode([$nwActLoc]);

        return $this->model->insert([
            'wv_id'       => $wvId,
            'wv_dtl_id'   => $wvDtlId,
            'sug_loc_ids' => $nwActLoc['loc_id'],
            'act_loc_ids' => $strActLocId,
            'updated_by'  => Data::getCurrentUserId(),
            'created_by'  => Data::getCurrentUserId(),
            'updated_at'  => time(),
            'created_at'  => time()
        ]);

    }

    /**
     * @param $nwActLoc
     * @param $wvDtlId
     *
     * @return
     */
    public function updateWaveDtlLocByRfid($nwActLocs, $wvId, $wvDtlId)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $wvDtlLoc = $this->model->where('wv_dtl_id', $wvDtlId)->first();
        if ($wvDtlLoc) {
            $actLocIds = \GuzzleHttp\json_decode($wvDtlLoc->act_loc_ids ?: '[]', true);
            // $actLocIds[] = $nwActLoc;
            $actLocIds = array_merge($actLocIds, $nwActLocs);
            $strActLocId = \GuzzleHttp\json_encode($actLocIds);

            return $this->model
                ->where('wv_dtl_id', '=', $wvDtlId)
                ->update([
                    'act_loc_ids' => $strActLocId,
                    'updated_by'  => Data::getCurrentUserId(),
                    'updated_at'  => time()
                ]);
        }

        // $strActLocId = \GuzzleHttp\json_encode([$nwActLoc]);
        $strActLocId = \GuzzleHttp\json_encode($nwActLocs);
        $locIds = array_pluck($nwActLocs, 'loc_id');
        $sugLocIds = \GuzzleHttp\json_encode($locIds);

        return $this->model->insert([
            'wv_id'       => $wvId,
            'wv_dtl_id'   => $wvDtlId,
            // 'sug_loc_ids' => $nwActLoc['loc_id'],
            'sug_loc_ids' => $sugLocIds,
            'act_loc_ids' => $strActLocId,
            'updated_by'  => Data::getCurrentUserId(),
            'created_by'  => Data::getCurrentUserId(),
            'updated_at'  => time(),
            'created_at'  => time()
        ]);

    }

    public function getSugLocIdByWvDtlId($wvDtlId)
    {
        return $this->model
            ->where('wv_dtl_id', $wvDtlId)
            ->select('sug_loc_ids')
            ->first();
    }
}
