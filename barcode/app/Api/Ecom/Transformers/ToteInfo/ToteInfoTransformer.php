<?php

namespace App\Api\Ecom\Transformers\ToteInfo;

use League\Fractal\TransformerAbstract;

class ToteInfoTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'odr_hdrs'  => 'odrHdrs'
    ];

    public function transform($tote)
    {
        return [
            'tote_id'   => $tote->tote_id,
            'tote_num'  => $tote->tote_num,
            'tote_sts'  => $tote->tote_sts
        ];
    }

    public function includeOdrHdrs($tote)
    {
        $odrHdrs = $tote->odrHdrs;
        if ($odrHdrs) {
            return $this->collection( $odrHdrs, new OrderHdrTransformer() );
        }
        return [];
    }
}
