<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Utils\SelArr;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\SelStr;

class LocationUtilizationModel extends AbstractModel {

    public function __construct(Storages $model = null) {
        $this->model = ($model) ?: new Location();
    }

    /*
     * **************************************************************************
     */

    public function search($attributes = [], $with = [], $limit = PAGING_LIMIT) {
        $c_status = config('constants.ctn_status');
        $searchAttr = SelArr::removeNullOrEmptyString($attributes);
        $query = $this->make($with)
                ->where('loc_whs_id', $attributes['whs_id'])
                ->leftjoin('pallet', 'pallet.loc_id', '=', 'location.loc_id')
                ->leftjoin('cartons','cartons.plt_id','=','pallet.plt_id')
                ->leftjoin('customer_zone','customer_zone.zone_id', '=', 'location.loc_zone_id')
                ->leftjoin('customer','customer.cus_id','=','customer_zone.cus_id')
                ->join('loc_type','loc_type.loc_type_id','=','location.loc_type_id')
                ->select('customer.cus_name','location.loc_code','loc_type.loc_type_name','pallet.plt_num','location.loc_whs_id','location.loc_sts_code')
                ->selectRaw('count( distinct customer.cus_id) as TTL_CUS')
                ->selectRaw('count(cartons.ctn_id) as cartons')
                ->selectRaw('COALESCE(sum(cartons.piece_ttl),0) as pieces')
                ->where('location.loc_sts_code', '=',$c_status['ACTIVE']);

        if (!empty($attributes['cus_id'])) {
            $query->where('customer.cus_id', $attributes['cus_id']);
        }
        if (!empty($attributes['loc_code'])) {
            $query->where('location.loc_code', 'like', "%" . SelStr::escapeLike($attributes['loc_code']) . "%");

        }
        if(!empty($attributes['loc_type_name'])) {
            $query->where('loc_type.loc_type_name', 'like', "%" . SelStr::escapeLike($attributes['loc_type_name']) . "%");
           // $query->where('loc_type.loc_type_name','=',$attributes['loc_type_name']);
        }
        if(!empty($attributes['plt_num'])){
            $query->where('pallet.plt_num', 'like', "%" . SelStr::escapeLike($attributes['plt_num']) . "%");
//            $query->where('pallet.plt_num','=',$attributes['plt_num']);
        }
        if(isset($attributes['total_cartons'])&&$attributes['total_cartons'] != '') {
            if(!empty($attributes['autocomplete'])){
                $attributes['autocomplete'] = null;
            }

            $query->havingRaw('count(cartons.ctn_id) = '.$attributes['total_cartons']);
        }
        if(isset($attributes['ttl_cus'])&&$attributes['ttl_cus']!= '') {

             if(!empty($attributes['autocomplete'])){
                $attributes['autocomplete'] = null;
            }

            $query->havingRaw('count(DISTINCT customer.cus_id ) = '.$attributes['ttl_cus']);
        }
        if(isset($attributes['total_pieces'])&&$attributes['total_pieces'] != '') {
             if(!empty($attributes['autocomplete'])){
                $attributes['autocomplete'] = null;
            }
            $query->havingRaw('sum(cartons.piece_ttl) = '.$attributes['total_pieces']);
        }

       $this->sortBuilder($query, $searchAttr);

        $query->groupBy([
            'location.loc_code'
        ]);

        return $this->runSearch([
                    'query' => $query,
                    'attributes' => $attributes,
                    'searchAttr' => $searchAttr,
                    'limit' => $limit,
        ]);
    }


    public function getColumn(String $rowName, String $keyWord = '')
    {
        $query = DB::table('location')
                    ->select(
                        DB::raw("IFNULL($rowName, '') as $rowName")
                    )
                    ->distinct()
                    ->orderBy($rowName);

        if($keyWord){
            $query->where($rowName, 'LIKE', '%' . $keyWord . '%');
        }

        return $query;
    }
}
