<?php
/**
 * Created by PhpStorm.
 * Phuong Hong
 * Date: 15-Aug-16
 * Time: 09:24
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\BOLOrderDetailModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\InventorySummaryModel;
use App\Api\V1\Models\OdrHdrMetaModel;
use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use App\Api\V1\Models\PackHdrModel;
use App\Api\V1\Models\PackDtlModel;
use App\Api\V1\Models\ShippingOrderModel;
use App\Api\V1\Models\UserModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\OutPalletModel;
use App\Api\V1\Models\WarehouseModel;
use App\Api\V1\Models\WavepickDtlModel;
use App\Api\V1\Models\WavepickHdrModel;
use App\Api\V1\Models\WorkOrderModel;
use App\Api\V1\Models\WorkOrderDtlModel;
use App\Api\V1\Models\CustomerConfigModel;
use App\Api\V1\Models\OrderCartonModel;
use App\Api\V1\Models\ReturnOrderModel;
use App\Api\V1\Traits\OrderHdrControllerTrait;
use App\Api\V1\Transformers\OrderDtlTransformer;
use App\Api\V1\Transformers\OrderHdrTransformer;
use App\Api\V1\Transformers\PackHdrTransformer;
use App\Api\V1\Transformers\OrderCancelListTransformer;
use App\Api\V1\Transformers\OrderCartonTransformer;
use App\Api\V1\Transformers\OrderCartonNotAssignTransformer;
use App\Api\V1\Validators\OrderHdrValidator;
use App\Api\V1\Validators\CsrValidator;
use App\Api\V1\Validators\OrderImportValidator;
use App\Library\OrderImport;
use Illuminate\Http\Request as IRequest;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

/**
 * Class OdrCartonsController
 *
 * @package App\Api\V1\Controllers
 */
class OrderCartonController extends AbstractController
{
    use OrderHdrControllerTrait;

    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;

    /**
     * @var InventorySummaryModel
     */
    protected $inventorySummaryModel;

    /**
     * @var ShippingOrderModel
     */
    protected $shippingOrderModel;

    /**
     * @var UserModel
     */
    protected $userModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var WarehouseModel
     */
    protected $warehouseModel;

    /**
     * @var ItemModel
     */
    protected $itemModel;

    /**
     * @var OrderImportValidator
     */
    protected $orderImportValidator;

    /**
     * @var PackHdrModel
     */
    protected $packHdrModel;

    /**
     * @var PackDtlModel
     */
    protected $packDtlModel;

    /**
     * @var WavepickHdrModel
     */
    protected $wavepickHdrModel;

    /**
     * @var WavepickDtlModel
     */
    protected $wavepickDtlModel;

    /**
     * @var WorkOrderModel
     */
    protected $workOrderModel;

    /**
     * @var WorkOrderDtlModel
     */
    protected $workOrderDtlModel;

    /**
     * @var $customerConfigModel
     */
    protected $customerConfigModel;

    /**
     * @var $outPalletModel
     */
    protected $outPalletModel;

    /**
     * @var BOLOrderDetailModel
     */
    protected $bolOrderDetailModel;

    /**
     * @var Order Hdr Meta Model
     */
    protected $orderHdrMetaModel;

    /**
     * @var OrderCartonModel
     */
    protected $orderCartonModel;

    /**
     * @var ReturnOrderModel
     */
    protected $returnOrderModel;

    /**
     * @param OrderHdrModel $orderHdrModel
     * @param OrderDtlModel $orderDtlModel
     * @param InventorySummaryModel $inventorySummaryModel
     * @param ShippingOrderModel $shippingOrderModel
     * @param UserModel $userModel
     * @param EventTrackingModel $eventTrackingModel
     * @param WarehouseModel $warehouseModel
     * @param ItemModel $itemModel
     * @param OrderImportValidator $orderImportValidator
     * @param PackHdrModel $packHdrModel
     * @param PackDtlModel $packDtlModel
     * @param WavepickHdrModel $wavepickHdrModel
     * @param WavepickDtlModel $wavepickDtlModel
     * @param WorkOrderDtlModel $workOrderDtlModel
     * @param WorkOrderModel $workOrderModel
     * @param CustomerConfigModel $customerConfigModel
     * @param OutPalletModel $outPalletModel
     * @param BOLOrderDetailModel $bolOrderDetailModel
     * @param OrderCartonModel $orderCartonModel
     * @param ReturnOrderModel $returnOrderModel
     */
    public function __construct(
        OrderHdrModel $orderHdrModel,
        OrderDtlModel $orderDtlModel,
        InventorySummaryModel $inventorySummaryModel,
        ShippingOrderModel $shippingOrderModel,
        UserModel $userModel,
        EventTrackingModel $eventTrackingModel,
        WarehouseModel $warehouseModel,
        ItemModel $itemModel,
        OrderImportValidator $orderImportValidator,
        PackHdrModel $packHdrModel,
        PackDtlModel $packDtlModel,
        WavepickHdrModel $wavepickHdrModel,
        WavepickDtlModel $wavepickDtlModel,
        WorkOrderDtlModel $workOrderDtlModel,
        WorkOrderModel $workOrderModel,
        CustomerConfigModel $customerConfigModel,
        OutPalletModel $outPalletModel,
        BOLOrderDetailModel $bolOrderDetailModel,
        OrderCartonModel $orderCartonModel,
        ReturnOrderModel $returnOrderModel

    ) {
        $this->orderHdrModel = $orderHdrModel;
        $this->orderDtlModel = $orderDtlModel;
        $this->inventorySummaryModel = $inventorySummaryModel;
        $this->shippingOrderModel = $shippingOrderModel;
        $this->userModel = $userModel;
        $this->eventTrackingModel = $eventTrackingModel;
        $this->warehouseModel = $warehouseModel;
        $this->itemModel = $itemModel;
        $this->orderImportValidator = $orderImportValidator;
        $this->packHdrModel = $packHdrModel;
        $this->wavepickHdrModel = $wavepickHdrModel;
        $this->wavepickDtlModel = $wavepickDtlModel;
        $this->workOrderDtlModel = $workOrderDtlModel;
        $this->workOrderModel = $workOrderModel;
        $this->packDtlModel = $packDtlModel;
        $this->customerConfigModel = $customerConfigModel;
        $this->outPalletModel = $outPalletModel;
        $this->bolOrderDetailModel = $bolOrderDetailModel;
        $this->orderHdrMetaModel = new OdrHdrMetaModel();
        $this->orderCartonModel = $orderCartonModel;
        $this->returnOrderModel = $returnOrderModel;
    }

    /**
     * @param Request $request
     * @param OrderCartonTransformer $orderCartonTransformer
     *
     * @return Response|void
     */
    public function _CanceledOrdersSearch(
        Request $request,
        OrderCartonTransformer $orderCartonTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        //get orderIds in Order
        $orderInfos = $this->orderHdrModel->getOrderIdAccordingToOdrNum($input);
        $orderIds = array_pluck($orderInfos, 'odr_id', 'odr_id'); //[35=>35,36=>36,..49=>49]

        //get all orderIds in returnHdr
        $returnOrderInfos = $this->returnOrderModel->getAllOrderIdInReturnHdr();
        $returnOrderIds = array_pluck($returnOrderInfos, 'odr_hdr_id', 'odr_hdr_id');//[35=>35]

        foreach ($orderInfos as $orderInfo) {
            if (isset($orderInfo['odr_id']) && isset($returnOrderIds[$orderInfo['odr_id']]) &&
                ($orderInfo['odr_id'] == $returnOrderIds[$orderInfo['odr_id']])
            ) {
                unset($orderIds[$returnOrderIds[$orderInfo['odr_id']]]);
                continue;
            }
        }

        try {
            $ordCarton = $this->orderHdrModel->CanceledOrdersSearch($orderIds, $input, [
                'oderCarton',
                'customer',
                'returnHdr'

            ], array_get($input, 'limit'));

            return $this->response->paginator($ordCarton, $orderCartonTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function CanceledOrdersSearch(
        Request $request,
        OrderCartonTransformer $orderCartonTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            $ordCarton = $this->orderHdrModel->CanceledOrdersSearch($input, ['customer'], array_get($input, 'limit'));

            return $this->response->paginator($ordCarton, $orderCartonTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param OrderCartonNotAssignTransformer $orderCartonNotAssignTransformer
     * @return Response|void
     */
    public function listCartonNotAssignByOrder(
        Request $request,
        OrderCartonNotAssignTransformer $orderCartonNotAssignTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        try {
            $ordCartons = $this->orderCartonModel->loadCartonNotAssign($input, [], array_get($input, 'limit'));
            return $this->response->paginator($ordCartons, $orderCartonNotAssignTransformer);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function updateTrackingNumPackedCtns($orderId, Request $request)
    {
        $input = $request->getParsedBody();
        $packHdrId = $input['pack_hdr_id'];
        $trackingNum = trim($input['tracking_number']);
        try {
            $odrHdr = $this->orderHdrModel->getFirstWhere(['odr_id' => $orderId]);
            if ($odrHdr->odr_sts == 'SH' || $odrHdr->odr_sts == 'CC') {
                return $this->response->errorBadRequest('The order was Shipped or Cancel. Can not update tracking number');
            } else {
                $this->packHdrModel->updateWhere(['tracking_number' => $trackingNum], ['pack_hdr_id' => $packHdrId, 'odr_hdr_id' => $orderId]);
                return $this->response->noContent()->setContent([
                    'status' => 'OK',
                    'data'   => [
                        'pack_hdr_id' => $packHdrId,
                        'odr_hdr_id' => $orderId,
                        'msg'  => "Updated successful"
                    ]
                ])->setStatusCode(Response::HTTP_OK);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}
