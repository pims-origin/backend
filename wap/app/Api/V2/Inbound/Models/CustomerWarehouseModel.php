<?php

namespace App\Api\V2\Inbound\Models;

//use App\Api\V2\Inbound\Traits\CusZoneCusWarehouseService;
use \Seldat\Wms2\Models\CustomerWarehouse;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\SelArr;

class CustomerWarehouseModel extends AbstractModel
{
    //use CusZoneCusWarehouseService;
    protected $model;

    public function __construct(CustomerWarehouse $model = null)
    {
        $this->model = ($model) ?: new CustomerWarehouse();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     * @deprecated  NOT IN USE
     * @return mixed
     */
    public function searchCustomerByWarehouse1($attributes = [], $with = [], $limit = null)
    {
        $statusNew = Status::getByKey("ASN_STATUS", "NEW");
        $statusRC = Status::getByKey("ASN_STATUS", "RECEIVING");

        $query = $this->make($with)
            ->where('whs_id', $attributes['whs_id']);

        $query = $query->whereHas('asnHdr', function ($query) use ($statusNew, $statusRC) {
            $query->where('asn_sts', $statusNew)
                    ->orWhere('asn_sts', $statusRC);
        });

        $this->sortBuilder($query, $attributes);

        if (!$limit) {
            $limit = 20;
        }

        $models = $query->paginate($limit);

        return $models;
    }

    public function searchCustomerByWarehouse($attributes = [], $limit = null)
    {
        $statusNew = Status::getByKey("ASN_STATUS", "NEW");
        $statusRC  = Status::getByKey("ASN_STATUS", "RECEIVING");

        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $query = $this->model
            ->join('asn_hdr', 'asn_hdr.cus_id', '=', 'customer_warehouse.cus_id')
            ->join('customer', 'customer.cus_id', '=', 'customer_warehouse.cus_id')
            ->where('asn_hdr.whs_id', $attributes['whs_id'])
            ->whereIn('asn_hdr.asn_sts', [$statusNew, $statusRC]);

        // if (!$limit) {
        //     $limit = 20;
        // }

        if (isset($attributes['cus_name'])) {
            $isLikeFromLeft = array_get($attributes, 'like_from_left', 0);
            if($isLikeFromLeft) {
                $query->where('customer.cus_name', 'like', SelStr::escapeLike($attributes['cus_name']) . "%");
            } else {
                $query->where('customer.cus_name', 'like', "%" . SelStr::escapeLike($attributes['cus_name']) . "%");
            }
        } else {
            $query->where('customer.cus_id', -100);
        }

        $query = $query->groupBy('customer_warehouse.cus_id');

        $query->orderBy('customer.cus_name');

        //[WMS2-4275][WAP API - OUTBOUND] order-skus api depends on limit param while other apis: get customers, container etc do not
        if ($limit !== null) {
            $query->limit($limit);
        }

        $this->sortBuilder($query, $attributes);

        // $models = $query->paginate($limit);
        $models = $query->get();

        return $models;
    }
}
