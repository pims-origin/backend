<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V2\PackDetail\Models;

use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

/**
 * Class OrderDtlModel
 *
 * @package App\Api\V1\Models
 */
class OrderDtlModel extends AbstractModel
{
    /**
     * OrderDtlModel constructor.
     *
     * @param OrderDtl|null $model
     */
    public function __construct(OrderDtl $model = null)
    {
        $this->model = ($model) ?: new OrderDtl();
    }

    /**
     * @param $odrDtlIds
     *
     * @return mixed
     */
    public function deleteDtls($odrDtlIds)
    {
        return $this->model
            ->whereIn('odr_dtl_id', $odrDtlIds)
            ->delete();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $query->where('ord_hdr_id', $attributes['ord_hdr_id']);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $arrLike = ['color', 'sku', 'size', 'lot'];
        $arrEqual = [
            'ord_hdr_id',
            'whs_id',
            'cus_id',
            'item_id',
            'uom_id',
            'request_qty',
            'qty_to_allocated',
            'allocated_qty',
            'damaged',
            'special_handling',
            'back_ord'
        ];
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, $arrLike)) {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
                if (in_array($key, $arrEqual)) {
                    $query->where($key, SelStr::escapeLike($value));
                }
            }
        }
        $this->sortBuilder($query, $attributes);

        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $orderHdrId
     * @param $itemIds
     * @param array $with
     *
     * @return mixed
     */
    public function loadByItems($orderHdrId, $itemIds, $with = [])
    {
        $query = $this->make($with);
        $query->where('odr_id', $orderHdrId)
            ->whereIn('item_id', $itemIds);
        $this->model->filterData($query, true);

        $models = $query->get();

        return $models;
    }

    public function checkDtlHasItem($orderHdrId, $itemIds) {
        $query = $this->make([]);
        $count =$query->where('odr_id', $orderHdrId)
            ->whereIn('item_id', $itemIds)->count();

        return $count<count($itemIds) ? false : true;
    }

    /**
     * @param $orderHdrId
     * @param $itemIds
     * @param array $with
     *
     * @return mixed
     */
    public function getOrderDtlById($orderDtlId)
    {
        $query = $this->make([]);
        $query->whereIn('odr_dtl_id', $orderDtlId);
        $this->model->filterData($query, true);

        $models = $query->get();

        return $models;
    }
}
