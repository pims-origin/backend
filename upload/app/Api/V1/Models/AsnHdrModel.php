<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\AsnHdr;

class AsnHdrModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new AsnHdr();
    }

    /**
     * @param array $params
     * @param array $with
     *
     * @return mixed
     */
    public function loadBy($params = [], $with = [])
    {
        $query = $this->make($with);

        if (!empty($params) && is_array($params)) {
            foreach ($params as $key => $value) {
                $query->where($key, $value);
            }
        }
        // Get
        $models = $query->get();
        return $models;
    }
}
