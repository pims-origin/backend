<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Http\Response as IlluminateResponse;

class OrderPickingControllerTest extends TestCase
{

    use DatabaseTransactions;

    protected $warehouseId;
    protected $zoneId;
    protected $typeId;
    protected $statusCode;

    protected $urlOdrPkList = '/v1/order-picking-list';
    protected $urlDtlWv = '/v1/detail-wave-pick';
    protected $urlUpdtWv = '/v1/update-wave-pick';

    public function setUp()
    {
        parent::setUp();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetOderPickingList_ok()
    {
        $this->call('GET', $this->urlOdrPkList);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testSortOderPickingList_ok()
    {
        $this->call('GET', $this->urlOdrPkList . "?sort[created_at]=desc&wv_sts=NW");

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testGetDetailWavePick_ok()
    {
        $wvId = factory(\Seldat\Wms2\Models\WavepickDtl::class)->create()->wv_id;

        $this->call('GET', $this->urlDtlWv . "?wv_id=" . $wvId);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testUpdateWv_fail()
    {
        $updateParams[] = [
            'itm_id'        => 555555,
            'act_loc_id'    => 83416,
            'act_loc'       => "L1-01-03",
            'act_piece_qty' => 50,
        ];
        $wv_id = factory(\Seldat\Wms2\Models\WavepickDtl::class)->create()->wv_id;
        $this->call('PUT', $this->urlUpdtWv . "/" . $wv_id, $updateParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    /*public function testUpdateWv_ok()
    {
        $wvDtl = factory(\Seldat\Wms2\Models\WavepickDtl::class)->create();
        $wv_id = $wvDtl->wv_id;
        $updateParams[] = [
            'itm_id'        => $wvDtl->item_id,
            'act_loc_id'    => $wvDtl->primary_loc_id,
            'act_loc'       => $wvDtl->primary_loc,
            'act_piece_qty' => 50,
        ];
        $this->call('PUT', $this->urlUpdtWv . "/" . $wv_id, $updateParams);
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }*/
}
