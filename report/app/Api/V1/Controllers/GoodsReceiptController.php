<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 03-Mar-2017
 * Time: 09:27
 */


namespace App\Api\V1\Controllers;

use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\AsnDtlModel;

use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Illuminate\Support\Facades\DB;
use App\Api\V1\Transformers\GoodsReceiptTransformer;
use Maatwebsite\Excel\Facades\Excel;
use phpDocumentor\Reflection\DocBlock\StandardTagFactoryTest;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\Container;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SelExport;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Seldat\Wms2\Utils\Message;
use Wms2\UserInfo\Data;

class GoodsReceiptController extends AbstractController
{
    /**
     * @var GoodsReceiptModel
     */
    protected $goodsReceiptModel;

    /**
     * @var GoodsReceiptDetailModel
     */
    protected $goodsReceiptDetailModel;

    /**
     * @var AsnDtlModel
     */
    protected $asnDtlModel;

    /**
     * @param GoodsReceiptModel $goodsReceiptModel
     * @param GoodsReceiptDetailModel $goodsReceiptDetailModel
     */
    public function __construct
    (
        GoodsReceiptModel $goodsReceiptModel,
        GoodsReceiptDetailModel $goodsReceiptDetailModel
    ) {
        $this->goodsReceiptModel = $goodsReceiptModel;
        $this->goodsReceiptDetailModel = $goodsReceiptDetailModel;
        $this->asnDtlModel = new AsnDtlModel;
    }


    /**
     * @param $whs_id
     * @param Request $request
     * @param GoodsReceiptTransformer $goodsReceiptTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(
        $whs_id,
        Request $request,
        GoodsReceiptTransformer $goodsReceiptTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        $input['gr_sts'] = 'RE';

        if (!empty($input['export']) && $input['export'] == 1) {
            $this->export($whs_id, $input);
            die;
        }
        try {
            $goodsReceiptDtl = $this->goodsReceiptDetailModel->search($whs_id, $input, [
                'goodsReceipt',
                'goodsReceipt.container',
                'goodsReceipt.customer',
                'goodsReceipt.updatedBy',
                'goodsReceipt.asnHdr',

            ], array_get($input, 'limit'));

//            return $this->response->paginator($goodsReceiptDtl, $goodsReceiptTransformer);
            return $goodsReceiptDtl;

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whs_id
     * @param $input
     *
     * @return $this|void
     * @throws \Exception
     */
    private function export(
        $whs_id,
        $input
    ) {
        $warehouse = DB::table('warehouse')->where([
            'whs_id'     => $whs_id,
            'deleted_at' => 915148800,
            'deleted'    => 0
        ])->first();

        if (empty($warehouse)) {
            throw new \Exception(Message::get("BM017", "Warehouse"));
        }

        try {
            $goodsReceiptDtl = $this->goodsReceiptDetailModel->search($whs_id, $input, [
                'goodsReceipt',
                'goodsReceipt.container',
                'goodsReceipt.customer',
                'goodsReceipt.updatedBy',
                'goodsReceipt.asnHdr',
            ], null, true)->toArray();

            for($i = 0; $i < count($goodsReceiptDtl); $i++){
                $goodsReceiptDtl[$i]['length'] = round($goodsReceiptDtl[$i]['length'], 2);
                $goodsReceiptDtl[$i]['width'] = round($goodsReceiptDtl[$i]['width'], 2);
                $goodsReceiptDtl[$i]['height'] = round($goodsReceiptDtl[$i]['height'], 2);
                $goodsReceiptDtl[$i]['weight'] = round($goodsReceiptDtl[$i]['weight'], 1);
                $goodsReceiptDtl[$i]['volume'] = round($goodsReceiptDtl[$i]['volume'], 2);
                $goodsReceiptDtl[$i]['asn_hdr_act_dt'] = (array_get($goodsReceiptDtl[$i], 'asn_hdr_act_dt', null)) ? date('m/d/Y', array_get($goodsReceiptDtl[$i], 'asn_hdr_act_dt', null)) : '';
            }
            $title = [
                'cus_code' => 'Customer Code',
                'cus_name' => 'Customer Name',
                'gr_hdr_num'        => 'GR Num',
                'ctnr_num'                        => 'Carrier',
                'ref_code'          => 'Ref Code',
                'item_id'                         => 'Item ID',
                'sku'                             => 'SKU',
                'size'                            => 'Size',
                'color'                           => 'Color',
                'lot'                             => 'Lot',
                'pack'                            => 'Pack size',
                'upc'                             => 'UPC/EAN',
                'gr_dtl_act_ctn_ttl|*|pack'       => 'Actual Qty',

                'gr_dtl_ept_ctn_ttl'                      => 'Expected CTNS',
                'gr_dtl_act_ctn_ttl'                      => 'Actual CTNS',
                'gr_dtl_act_ctn_ttl|-|gr_dtl_ept_ctn_ttl' => 'Discrepany CTNS',
                'gr_dtl_plt_ttl'                          => 'PLT',
                'length'                                  => 'Length',
                'width'                                   => 'Width',
                'height'                                  => 'Height',
                'weight'                                  => 'Weight',
                'volume'                                  => 'Volume',

                'crs_doc|*|pack' => 'X-Dock QTY',
                'gr_dtl_dmg_ttl' => 'Dam CTNS',

                //'goods_receipt.asn_hdr.asn_hdr_ept_dt|format()|m/d/Y' => 'Expected Date',
                'asn_hdr_ept_dt|format()|m/d/Y' => 'Expected Date',
                'asn_hdr_act_dt'                => 'Goods Receipt Date',

                'updated_at|format()|m/d/Y'     => 'Putaway Date',
                'updated_by' => 'GR By',
                'cube' => 'Cube',
            ];
            $filePath = storage_path() . "/Report_GR_{$warehouse['whs_name']}.csv";
            $filePath = "Report_GR_{$warehouse['whs_name']}";
            $this->saveFile($title, $goodsReceiptDtl, $filePath);

            //$output = fopen($filePath, 'w+');
            //fputcsv($output, ["Number", "Description", "test"]);
            //fputcsv($output, ["100", "testDescription", "10"]);
            //
            //header('Content-Type: application/octet-stream');
            //header('Content-Disposition: attachment; filename="' . $filePath . '"');
            //header('Content-Length: ' . filesize($filePath));
            //echo readfile($filePath);

            return $this->response->noContent()->setContent(['status' => 'OK', 'data' => []]);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return mixed|void
     */
    public function skuTrackingSearch(
        $whs_id,
        Request $request
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        $input['gr_sts'] = 'RE';

        if (!empty($input['export']) && $input['export'] == 1) {
            $this->exportSkuTrackingSearch($whs_id, $input);
            die;
        }

        try {
            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $skuTrackingInfo = $this->goodsReceiptDetailModel->skuTrackingSearch($whs_id, $input, [

            ], array_get($input, 'limit'));


            return $skuTrackingInfo;

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whs_id
     * @param $input
     *
     * @return $this|void
     * @throws \Exception
     */
    private function exportSkuTrackingSearch(
        $whs_id,
        $input
    ) {
        $warehouse = DB::table('warehouse')->where([
            'whs_id'     => $whs_id,
            'deleted_at' => 915148800,
            'deleted'    => 0
        ])->first();

        if (empty($warehouse)) {
            throw new \Exception(Message::get("BM017", "Warehouse"));
        }

        try {
            $skuTrackingInfo = $this->goodsReceiptDetailModel->skuTrackingSearch($whs_id, $input, [

            ], null, true)->toArray();

            $ctns_ttl = array_sum(array_pluck($skuTrackingInfo, 'ctns'));
            $qty_ttl = array_sum(array_pluck($skuTrackingInfo, 'qty'));
            $cube_ttl = array_sum(array_pluck($skuTrackingInfo, 'cube'));

            $title = [
                'cus_code'  => 'Customer Code',
                'cus_name'  => 'Customer Name',
                //'type'      => 'Type',
                'trans_num' => 'Trans Num',
                'ref_cus_order' => 'Ref/Cus Order#',
                'po_ctnr'       => 'PO/Carrier',
                'actual_date|format()|m/d/Y' => 'Actual Date',
                'item_id'                    => 'Item ID',
                'sku'                        => 'SKU',
                'size'                       => 'Size',
                'color'                      => 'Color',
                'lot'                        => 'Lot',
                'pack'                       => 'Pack',

                'ctns'                       => 'CTNS',
                'qty'                        => 'QTY',

                //'cube'                       => 'Cube',


            ];

            $filePath = storage_path() . "/SKU_Tracking_{$warehouse['whs_name']}.csv";
            $filePath = "SKU_Tracking_{$warehouse['whs_name']}";
            $this->saveFileSkuTracking($title, $skuTrackingInfo, $filePath, $ctns_ttl, $qty_ttl, $cube_ttl);

            return $this->response->noContent()->setContent(['status' => 'OK', 'data' => []]);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }


    private function saveFileSkuTracking(array $title, array $data, $filePath,
                                            $ctns_ttl = false, $qty_ttl = false, $cube_ttl = false)
    {
        if (empty($data) || empty($title)) {
            return false;
        }

        $writer = WriterFactory::create(Type::CSV);
        $writer->openToBrowser($filePath);

        $dataSave[] = array_values($title);

        foreach ($data as $item) {
            $temp = [];
            foreach ($title as $key => $field) {
                $values = explode("|", $key);
                $value = "";
                if (count($values) == 1) {
                    $value = array_get($item, $values[0], null);
                } else if (!empty($values[2])) {
                    switch ($values[1]) {
                        case "*":
                            $value = array_get($item, $values[0], null) * array_get($item, $values[2], null);
                            break;
                        case ".":
                            $value = trim(array_get($item, $values[0], null) . " " .
                                array_get($item, $values[2], null));
                            break;
                        case "format()":
                            $value = !empty(array_get($item, $values[0], null)) ? date($values[2], array_get($item,
                                $values[0], null)) : '';
                            break;
                        case "-":
                            $value = array_get($item, $values[0], null) - array_get($item, $values[2], null);
                            break;
                    }
                }
                $temp[] = $value;
            }
            $dataSave[] = $temp;
        }

        $totalArr = [
            0  => "",
            1  => "",
            2  => "",
            3  => "",
            4  => "",
            5  => "",
            6  => "",
            7  => "",
            8  => "",
            9  => "",
            10 => "",
            11 => "",
            12 => "Sum: ",
            13 => $ctns_ttl,
            14 => $qty_ttl,
            15 => $cube_ttl,
        ];
        array_push($dataSave, $totalArr);

        $writer->addRows($dataSave);

        $writer->close();


        //Excel::create($filePath, function ($file) use ($dataSave) {
        //
        //    $file->sheet('Report', function ($sheet) use ($dataSave) {
        //        //$sheet->setAutoSize(true);
        //        //$sheet->setHeight(1, 30);
        //
        //        //$countData = count($dataSave);
        //        // Fill suggest Data
        //        $sheet->fromArray($dataSave, null, 'A1', true, false);
        //
        //        // Wrap Text
        //        //$sheet->getStyle("N1:M" . ($countData))->getAlignment()->setWrapText(true);
        //
        //        //$sheet->setBorder('A1:M' . ($countData), 'thin');
        //        //$sheet->cell("A1:M1", function ($cell) {
        //        //    $cell->setAlignment('center');
        //        //    $cell->setFontWeight();
        //        //    $cell->setBackground("#3399ff");
        //        //    $cell->setFontColor("#ffffff");
        //        //});
        //
        //        //$sheet->cell("A1:M" . ($countData), function ($cell) {
        //        //    $cell->setValignment('center');
        //        //});
        //
        //        //$columsAlign = [
        //        //    "A1:C" . $countData => "left",
        //        //    "D1:J" . $countData => "center",
        //        //    "L1:M" . $countData => "center",
        //        //    "K1:K" . $countData => "left",
        //        //    "N1:M" . $countData => "left"
        //        //];
        //
        //        //foreach ($columsAlign as $cols => $align) {
        //        //    $sheet->cell($cols, function ($cell) use ($align) {
        //        //        $cell->setAlignment($align);
        //        //    });
        //        //}
        //
        //    });
        //})->store('xlsx', storage_path());
    }

    /**
     * @param array $title
     * @param array $data
     * @param $filePath
     *
     * @return bool
     */
    private function saveFile(array $title, array $data, $filePath)
    {
        if (empty($data) || empty($title)) {
            return false;
        }

        $writer = WriterFactory::create(Type::CSV);
        $writer->openToBrowser($filePath);

        $dataSave[] = array_values($title);

        foreach ($data as $item) {
            $temp = [];
            foreach ($title as $key => $field) {
                $values = explode("|", $key);
                $value = "";
                if (count($values) == 1) {
                    $value = array_get($item, $values[0], null);
                } else if (!empty($values[2])) {
                    switch ($values[1]) {
                        case "*":
                            $value = array_get($item, $values[0], null) * array_get($item, $values[2], null);
                            break;
                        case ".":
                            $value = trim(array_get($item, $values[0], null) . " " .
                                array_get($item, $values[2], null));
                            break;
                        case "format()":
                            $value = date($values[2], array_get($item, $values[0], null));
                            break;
                        case "-":
                            $value = array_get($item, $values[0], null) - array_get($item, $values[2], null);
                            break;
                    }
                }
                $temp[] = $value;
            }
            $dataSave[] = $temp;
        }


        $writer->addRows($dataSave);

        $writer->close();


        //Excel::create($filePath, function ($file) use ($dataSave) {
        //
        //    $file->sheet('Report', function ($sheet) use ($dataSave) {
        //        //$sheet->setAutoSize(true);
        //        //$sheet->setHeight(1, 30);
        //
        //        //$countData = count($dataSave);
        //        // Fill suggest Data
        //        $sheet->fromArray($dataSave, null, 'A1', true, false);
        //
        //        // Wrap Text
        //        //$sheet->getStyle("N1:M" . ($countData))->getAlignment()->setWrapText(true);
        //
        //        //$sheet->setBorder('A1:M' . ($countData), 'thin');
        //        //$sheet->cell("A1:M1", function ($cell) {
        //        //    $cell->setAlignment('center');
        //        //    $cell->setFontWeight();
        //        //    $cell->setBackground("#3399ff");
        //        //    $cell->setFontColor("#ffffff");
        //        //});
        //
        //        //$sheet->cell("A1:M" . ($countData), function ($cell) {
        //        //    $cell->setValignment('center');
        //        //});
        //
        //        //$columsAlign = [
        //        //    "A1:C" . $countData => "left",
        //        //    "D1:J" . $countData => "center",
        //        //    "L1:M" . $countData => "center",
        //        //    "K1:K" . $countData => "left",
        //        //    "N1:M" . $countData => "left"
        //        //];
        //
        //        //foreach ($columsAlign as $cols => $align) {
        //        //    $sheet->cell($cols, function ($cell) use ($align) {
        //        //        $cell->setAlignment($align);
        //        //    });
        //        //}
        //
        //    });
        //})->store('xlsx', storage_path());
    }

    /**
     * @param $whs_id
     * @param $asn_id
     * @param Request $request
     *
     * @return $this|void
     * @throws \Exception
     */
    public function exportReceivingSlip(
        $whs_id,
        $asn_id,
        Request $request
    ) {
        $input = $request->getQueryParams();

        $warehouse = DB::table('warehouse')->where([
            'whs_id'     => $whs_id,
            'deleted_at' => 915148800,
            'deleted'    => 0
        ])->first();

        if (empty($warehouse)) {
            throw new \Exception(Message::get("BM017", "Warehouse"));
        }

        try {
            $asnDtls = $this->asnDtlModel->findWhere(
                ['asn_hdr_id' => $asn_id],
                ['grDtl', 'grDtl.goodsReceipt', 'systemUom']
            );

            $details = [];

            if ($asnDtls) {
                foreach ($asnDtls as $key => $asnDtl) {

                    $putaway_date = [];

                    if(!empty($asnDtl->grDtl)) {
                        $cartons = DB::table('cartons')
                            ->join('gr_dtl', 'gr_dtl.gr_dtl_id', '=', 'cartons.gr_dtl_id')
                            ->where('cartons.gr_dtl_id', '=', $asnDtl->grDtl['gr_dtl_id'])
                            ->first();
                        $user = DB::table('users')
                            ->where('user_id', $asnDtl->grDtl->goodsReceipt['updated_by'])
                            ->first();

                        $putawayDate[$key] = $cartons['created_at'];
                        $grBy[$key] = $user['first_name'].' '.$user['last_name'];
                    }
                    
                    $details[] = [
                        'cus_name'           => $asnDtl->item->customer['cus_name'],
                        'ctnr_num'              => $asnDtl->ctnr_num,
                        'upc'                   => $asnDtl->item['cus_upc'],
                        'lot'                   => $asnDtl->asn_dtl_lot,
                        'sku'                   => $asnDtl->item['sku'],
                        'description'           => $asnDtl->item['description'],
                        'pack'                  => $asnDtl->item['pack'],

                        'asn_hdr_num'           => $asnDtl->asnHdr['asn_hdr_num'],
                        'ref_code'              => $asnDtl->asnHdr['asn_hdr_ref'],

                        'gr_dtl_ept_ctn_ttl'    => object_get($asnDtl, 'asn_dtl_ctn_ttl', 0),
                        'gr_dtl_act_ctn_ttl'    => object_get($asnDtl, 'grDtl.gr_dtl_act_ctn_ttl', 0),

                        'gr_date'               => object_get($asnDtl, 'grDtl.gr_dtl_id', 0) ? $asnDtl->updated_at->format('m/d/Y') : null,
                        'putaway_date'          => !empty($putawayDate[$key]) ? date('m/d/Y', $putawayDate[$key]) : null,
                        'gr_by'                 => $grBy[$key] ?? null
                    ];
                }
            }

            $title = [
                'cus_name'                          => 'Customer Name',
                'asn_hdr_num'                       => 'ASN Num',
                'ctnr_num'                          => 'Carrier',
                'ref_code'                          => 'Ref Code',
                'description'                       => 'Description',
                'sku'                               => 'SKU',
                'lot'                               => 'Lot',
                'upc'                               => 'UPC/EAN',
                'gr_dtl_ept_ctn_ttl|*|pack'         => 'Exp Qty',
                'gr_dtl_act_ctn_ttl|*|pack'         => 'Actual Qty',
                'gr_date'                           => 'Goods Receipt Date',
                'putaway_date'                      => 'Putaway Date',
                'gr_by'                             => 'GR By'
            ];

            $filePath = storage_path() . "/Report_GR_{$warehouse['whs_name']}.csv";
            $filePath = "Report_GR_{$warehouse['whs_name']}";
            $this->saveFile($title, $details, $filePath);
            die;

            return $this->response->noContent()->setContent(['status' => 'OK', 'data' => []]);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function customersGrDropDown($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $grCustomers = $this->goodsReceiptModel->customers($whs_id, $input);

            return ['data' => $grCustomers];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function skuGrAutoComplete($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $grSkus = $this->goodsReceiptDetailModel->skuGrAutoComplete($whs_id, $input, array_get($input, 'limit'));

            return ['data' => $grSkus];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function skuTrackingAutoComplete($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $skusTracking = $this->goodsReceiptDetailModel->skuTrackingAutoComplete($whs_id, $input, array_get($input,
                'limit'));

            return ['data' => $skusTracking];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function autoGrNum($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $GrInfos = $this->goodsReceiptModel->autoGrNum($whs_id, $input, array_get($input, 'limit'));

            return ['data' => $GrInfos];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function autoRefCode($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $refCodeInfos = $this->goodsReceiptModel->autoRefCode($whs_id, $input, array_get($input, 'limit'));

            return ['data' => $refCodeInfos];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function autoCtnrNum($whs_id, Request $request)
    {
        $input = $request->getQueryParams();
        try {
            $ctnrNumInfos = $this->goodsReceiptModel->autoCtnrNum($whs_id, $input, array_get($input, 'limit'));

            return ['data' => $ctnrNumInfos];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }


}
