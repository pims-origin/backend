<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 11:19
 */

namespace App\Api\V1\Transformers;


use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\WarehouseContact;

class WarehouseContactTransformer extends TransformerAbstract
{
    /**
     * @param WarehouseContact $warehouseContact
     *
     * @return array
     */
    public function transform(WarehouseContact $warehouseContact)
    {
        return [
            'whs_con_id'         => $warehouseContact->whs_con_id,
            'whs_con_fname'      => $warehouseContact->whs_con_fname,
            'whs_con_lname'      => $warehouseContact->whs_con_lname,
            'whs_con_email'      => $warehouseContact->whs_con_email,
            'whs_con_phone'      => $warehouseContact->whs_con_phone,
            'whs_con_mobile'     => $warehouseContact->whs_con_mobile,
            'whs_con_ext'        => $warehouseContact->whs_con_ext,
            'whs_con_position'   => $warehouseContact->whs_con_position,
            'whs_con_whs_id'     => $warehouseContact->whs_con_whs_id,
            'whs_con_department' => $warehouseContact->whs_con_department,
            'created_at'         => $warehouseContact->created_at,
            'updated_at'         => $warehouseContact->updated_at,
            'deleted_at'         => $warehouseContact->deleted_at,
        ];
    }
}
