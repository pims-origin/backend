<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\DailyInventorySummary;

class DailyInventoryModel extends AbstractModel
{
    public function __construct(DailyInventorySummary $model = null)
    {
        $this->model = ($model) ?: new DailyInventorySummary();
    }

    /*
     ****************************************************************************
     */
    //Daily Inventory On Rack for RM Kayu
    public function dailyInventoryOnRackInfos($whs_id, $cus_id, $curDate, $attributes = [], $with = [], $limit = PAGING_LIMIT)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('daily_inv_rpt')
            ->select([
                'daily_inv_rpt.whs_id',
                'daily_inv_rpt.cus_id',
                'daily_inv_rpt.item_id',
                'daily_inv_rpt.description',
                'daily_inv_rpt.sku',
                'daily_inv_rpt.size',
                'daily_inv_rpt.color',
                'daily_inv_rpt.pack',
                'daily_inv_rpt.uom',
                'daily_inv_rpt.lot',
                'daily_inv_rpt.is_rack',
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.created_at), '%Y-%m-%d') AS created_at"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.updated_at), '%Y-%m-%d') AS updated_at"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.inv_dt), '%Y-%m-%d') AS inv_dt"),
                DB::raw('SUM(daily_inv_rpt.init_ctns) AS init_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.init_qty) AS init_qty'),
                DB::raw('SUM(daily_inv_rpt.init_plt) AS init_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.in_ctns) AS in_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.in_qty) AS in_qty'),
                DB::raw('SUM(daily_inv_rpt.in_plt) AS in_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.out_ctns) AS out_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.out_qty) AS out_qty'),
                DB::raw('SUM(daily_inv_rpt.out_plt) AS out_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.other_ctns) AS other_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.other_qty) AS other_qty'),
                DB::raw('SUM(daily_inv_rpt.other_plt) AS other_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.cur_ctns) AS cur_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.cur_qty) AS cur_qty'),
                DB::raw('SUM(daily_inv_rpt.cur_plt) AS cur_plt_qty'),

            ]);

        //$query->where(DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.inv_dt), '%Y-%m-%d')"), '=',
        //    DB::raw("DATE_FORMAT(now(), '%Y-%m-%d')"));
        $query->where('daily_inv_rpt.inv_dt', $curDate);

        $query->where('daily_inv_rpt.whs_id', $whs_id);
        $query->where('daily_inv_rpt.cus_id', $cus_id);
        $query->where('daily_inv_rpt.is_rack', 1);

        $query->groupBy('daily_inv_rpt.item_id');

        $query->orderBy('in_qty', 'DESC');
        $query->orderBy('out_qty', 'DESC');
        $query->orderBy('other_qty', 'DESC');

        $row = $query->get();

        return $row;
    }

    //Subtotal RM Kayu for Daily Inventory On Rack
    public function subTotalForDailyInventoryOnRack(
        $whs_id,
        $cus_id,
        $curDate,
        $attributes = [],
        $with = [],
        $limit = PAGING_LIMIT
    ) {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('daily_inv_rpt')
            ->select([
                'daily_inv_rpt.whs_id',
                'daily_inv_rpt.cus_id',
                'daily_inv_rpt.item_id',
                'daily_inv_rpt.description',
                'daily_inv_rpt.sku',
                'daily_inv_rpt.size',
                'daily_inv_rpt.color',
                'daily_inv_rpt.pack',
                'daily_inv_rpt.uom',
                'daily_inv_rpt.lot',
                'daily_inv_rpt.is_rack',
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.created_at), '%Y-%m-%d') AS created_at"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.updated_at), '%Y-%m-%d') AS updated_at"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.inv_dt), '%Y-%m-%d') AS inv_dt"),
                DB::raw('SUM(daily_inv_rpt.init_ctns) AS init_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.init_qty) AS init_qty'),
                DB::raw('SUM(daily_inv_rpt.init_plt) AS init_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.in_ctns) AS in_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.in_qty) AS in_qty'),
                DB::raw('SUM(daily_inv_rpt.in_plt) AS in_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.out_ctns) AS out_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.out_qty) AS out_qty'),
                DB::raw('SUM(daily_inv_rpt.out_plt) AS out_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.other_ctns) AS other_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.other_qty) AS other_qty'),
                DB::raw('SUM(daily_inv_rpt.other_plt) AS other_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.cur_ctns) AS cur_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.cur_qty) AS cur_qty'),
                DB::raw('SUM(daily_inv_rpt.cur_plt) AS cur_plt_qty'),

            ]);

        //$query->where(DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.inv_dt), '%Y-%m-%d')"), '=',
        //    DB::raw("DATE_FORMAT(now(), '%Y-%m-%d')"));
        $query->where('daily_inv_rpt.inv_dt', $curDate);

        $query->where('daily_inv_rpt.whs_id', $whs_id);
        $query->where('daily_inv_rpt.cus_id', $cus_id);
        $query->where('daily_inv_rpt.is_rack', 1);

        $query->groupBy('daily_inv_rpt.cus_id');

        $row = $query->first();

        return $row;
    }

    //Daily Inventory Block Stack
    public function dailyInventoryBlockStackInfos($whs_id, $cus_id, $curDate, $attributes = [], $with = [], $limit = PAGING_LIMIT)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('daily_inv_rpt')
            ->select([
                'daily_inv_rpt.whs_id',
                'daily_inv_rpt.cus_id',
                'daily_inv_rpt.item_id',
                'daily_inv_rpt.description',
                'daily_inv_rpt.sku',
                'daily_inv_rpt.size',
                'daily_inv_rpt.color',
                'daily_inv_rpt.pack',
                'daily_inv_rpt.uom',
                'daily_inv_rpt.lot',
                'daily_inv_rpt.is_rack',
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.created_at), '%Y-%m-%d') AS created_at"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.updated_at), '%Y-%m-%d') AS updated_at"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.inv_dt), '%Y-%m-%d') AS inv_dt"),
                DB::raw('SUM(daily_inv_rpt.init_ctns) AS init_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.init_qty) AS init_qty'),
                DB::raw('SUM(daily_inv_rpt.init_plt) AS init_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.in_ctns) AS in_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.in_qty) AS in_qty'),
                DB::raw('SUM(daily_inv_rpt.in_plt) AS in_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.out_ctns) AS out_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.out_qty) AS out_qty'),
                DB::raw('SUM(daily_inv_rpt.out_plt) AS out_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.other_ctns) AS other_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.other_qty) AS other_qty'),
                DB::raw('SUM(daily_inv_rpt.other_plt) AS other_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.cur_ctns) AS cur_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.cur_qty) AS cur_qty'),
                DB::raw('SUM(daily_inv_rpt.cur_plt) AS cur_plt_qty'),

            ]);

        //$query->where(DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.inv_dt), '%Y-%m-%d')"), '=',
        //    DB::raw("DATE_FORMAT(now(), '%Y-%m-%d')"));
        $query->where('daily_inv_rpt.inv_dt', $curDate);

        $query->where('daily_inv_rpt.whs_id', $whs_id);
        $query->where('daily_inv_rpt.cus_id', $cus_id);
        $query->where('daily_inv_rpt.is_rack', 0);

        $query->groupBy('daily_inv_rpt.item_id');

        $query->orderBy('in_qty', 'DESC');
        $query->orderBy('out_qty', 'DESC');
        $query->orderBy('other_qty', 'DESC');

        $row = $query->get();

        return $row;
    }

    //Subtotal Block Stack for Daily Inventory
    public function subtotalBlockStackForDailyInventory(
        $whs_id,
        $cus_id,
        $curDate,
        $attributes = [],
        $with = [],
        $limit =
        PAGING_LIMIT
    ) {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('daily_inv_rpt')
            ->select([
                'daily_inv_rpt.whs_id',
                'daily_inv_rpt.cus_id',
                'daily_inv_rpt.item_id',
                'daily_inv_rpt.description',
                'daily_inv_rpt.sku',
                'daily_inv_rpt.size',
                'daily_inv_rpt.color',
                'daily_inv_rpt.pack',
                'daily_inv_rpt.uom',
                'daily_inv_rpt.lot',
                'daily_inv_rpt.is_rack',
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.created_at), '%Y-%m-%d') AS created_at"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.updated_at), '%Y-%m-%d') AS updated_at"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.inv_dt), '%Y-%m-%d') AS inv_dt"),
                DB::raw('SUM(daily_inv_rpt.init_ctns) AS init_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.init_qty) AS init_qty'),
                DB::raw('SUM(daily_inv_rpt.init_plt) AS init_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.in_ctns) AS in_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.in_qty) AS in_qty'),
                DB::raw('SUM(daily_inv_rpt.in_plt) AS in_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.out_ctns) AS out_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.out_qty) AS out_qty'),
                DB::raw('SUM(daily_inv_rpt.out_plt) AS out_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.other_ctns) AS other_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.other_qty) AS other_qty'),
                DB::raw('SUM(daily_inv_rpt.other_plt) AS other_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.cur_ctns) AS cur_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.cur_qty) AS cur_qty'),
                DB::raw('SUM(daily_inv_rpt.cur_plt) AS cur_plt_qty'),

            ]);

        //$query->where(DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.inv_dt), '%Y-%m-%d')"), '=',
        //    DB::raw("DATE_FORMAT(now(), '%Y-%m-%d')"));
        $query->where('daily_inv_rpt.inv_dt', $curDate);

        $query->where('daily_inv_rpt.whs_id', $whs_id);
        $query->where('daily_inv_rpt.cus_id', $cus_id);
        $query->where('daily_inv_rpt.is_rack', 0);

        $query->groupBy('daily_inv_rpt.cus_id');

        $row = $query->first();

        return $row;
    }

    //Total Daily Inventory
    public function totalForDailyInventory(
        $whs_id,
        $cus_id,
        $curDate,
        $attributes = [],
        $with = [],
        $limit =
        PAGING_LIMIT
    ) {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('daily_inv_rpt')
            ->select([
                'daily_inv_rpt.whs_id',
                'daily_inv_rpt.cus_id',
                'daily_inv_rpt.item_id',
                'daily_inv_rpt.description',
                'daily_inv_rpt.sku',
                'daily_inv_rpt.size',
                'daily_inv_rpt.color',
                'daily_inv_rpt.pack',
                'daily_inv_rpt.uom',
                'daily_inv_rpt.lot',
                'daily_inv_rpt.is_rack',
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.created_at), '%Y-%m-%d') AS created_at"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.updated_at), '%Y-%m-%d') AS updated_at"),
                //DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.inv_dt), '%Y-%m-%d') AS inv_dt"),
                DB::raw('SUM(daily_inv_rpt.init_ctns) AS init_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.init_qty) AS init_qty'),
                DB::raw('SUM(daily_inv_rpt.init_plt) AS init_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.in_ctns) AS in_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.in_qty) AS in_qty'),
                DB::raw('SUM(daily_inv_rpt.in_plt) AS in_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.out_ctns) AS out_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.out_qty) AS out_qty'),
                DB::raw('SUM(daily_inv_rpt.out_plt) AS out_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.other_ctns) AS other_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.other_qty) AS other_qty'),
                DB::raw('SUM(daily_inv_rpt.other_plt) AS other_plt_qty'),
                DB::raw('SUM(daily_inv_rpt.cur_ctns) AS cur_ctns_qty'),
                DB::raw('SUM(daily_inv_rpt.cur_qty) AS cur_qty'),
                DB::raw('SUM(daily_inv_rpt.cur_plt) AS cur_plt_qty'),

            ]);

        //$query->where(DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.inv_dt), '%Y-%m-%d')"), '=',
        //    DB::raw("DATE_FORMAT(now() - INTERVAL 3 DAY, '%Y-%m-%d')"));
        //$query->where(DB::raw("DATE_FORMAT(FROM_UNIXTIME(daily_inv_rpt.inv_dt), '%Y-%m-%d')"), '=',
        //    DB::raw("DATE_FORMAT(now(), '%Y-%m-%d')"));
        $query->where('daily_inv_rpt.inv_dt', $curDate);

        $query->where('daily_inv_rpt.whs_id', $whs_id);
        $query->where('daily_inv_rpt.cus_id', $cus_id);

        $query->groupBy('daily_inv_rpt.cus_id');

        $row = $query->first();

        return $row;
    }

    /**
     * [getDailyInventoryReport description]
     * @param  [type] $whs_id     [description]
     * @param  [type] $cus_id     [description]
     * @param  array  $curDate    [description]
     * @param  array  $attributes [description]
     * @param  array  $with       [description]
     * @param  int $limit      [description]
     * @param array $isRack    [description]
     */
    public function getDailyInventoryReport($whs_id, $cus_id, $curDate, $attributes = [], $with = [], $limit = PAGING_LIMIT, array $isRack)
    {
        return $this->model
            ->select([
                'daily_inv_rpt.whs_id',
                'daily_inv_rpt.cus_id',
                'daily_inv_rpt.item_id',
                'daily_inv_rpt.description',
                'daily_inv_rpt.sku',
                'daily_inv_rpt.size',
                'daily_inv_rpt.color',
                'daily_inv_rpt.pack',
                'daily_inv_rpt.uom',
                'daily_inv_rpt.lot',
                'daily_inv_rpt.is_rack',
                'daily_inv_rpt.updated_at',

                'daily_inv_rpt.init_ctns AS init_ctns_qty',
                'daily_inv_rpt.init_qty AS init_qty',
                'daily_inv_rpt.init_plt AS init_plt_qty',
                'daily_inv_rpt.init_volume AS init_volume',

                'daily_inv_rpt.in_ctns AS in_ctns_qty',
                'daily_inv_rpt.in_qty AS in_qty',
                'daily_inv_rpt.in_plt AS in_plt_qty',
                'daily_inv_rpt.in_volume AS in_volume',

                'daily_inv_rpt.out_ctns AS out_ctns_qty',
                'daily_inv_rpt.out_qty AS out_qty',
                'daily_inv_rpt.out_plt AS out_plt_qty',
                'daily_inv_rpt.out_volume AS out_volume',

                'daily_inv_rpt.other_ctns AS other_ctns_qty',
                'daily_inv_rpt.other_qty AS other_qty',
                'daily_inv_rpt.other_plt AS other_plt_qty',
                'daily_inv_rpt.other_volume AS other_volume',

                'daily_inv_rpt.cur_ctns AS cur_ctns_qty',
                'daily_inv_rpt.cur_qty AS cur_qty',
                'daily_inv_rpt.cur_plt AS cur_plt_qty',
                'daily_inv_rpt.cur_volume AS cur_volume',

                'i.length',
                'i.width',
                'i.height',
                'i.weight',

            ])
            ->join('item as i', 'daily_inv_rpt.item_id', '=', 'i.item_id')
            ->where('daily_inv_rpt.inv_dt', $curDate)
            ->where('daily_inv_rpt.whs_id', $whs_id)
            ->where('daily_inv_rpt.cus_id', $cus_id)
            ->orderBy('in_qty', 'DESC')
            ->orderBy('out_qty', 'DESC')
            ->orderBy('other_qty', 'DESC')
            ->orderBy('cur_qty', 'DESC')
            ->latest('updated_at')
            ->get();
    }

    public function getForecastActualData($dateArr, $data) {
        $dateBetweenString = implode(' AND ',$dateArr);

        return $this->model
            ->select([
                DB::raw('IFNULL(SUM(daily_inv_rpt.in_ctns), 0) AS in_ctn_qty'),
                DB::raw('IFNULL(SUM(daily_inv_rpt.in_qty), 0) AS in_piece_qty'),
                DB::raw('IFNULL(SUM(daily_inv_rpt.in_plt), 0) AS in_plt_qty'),
                DB::raw('IFNULL(SUM(daily_inv_rpt.in_ctns * item.cube), 0) AS in_cube_qty'),

                DB::raw('IFNULL(SUM(daily_inv_rpt.out_ctns), 0) AS out_ctn_qty'),
                DB::raw('IFNULL(SUM(daily_inv_rpt.out_qty), 0) AS out_piece_qty'),
                DB::raw('IFNULL(SUM(daily_inv_rpt.out_plt), 0) AS out_plt_qty'),
                DB::raw('IFNULL(SUM(daily_inv_rpt.out_ctns * item.cube), 0) AS out_cube_qty'),
            ])
            ->join('item', 'daily_inv_rpt.item_id', '=', 'item.item_id')
            ->whereBetween('daily_inv_rpt.inv_dt', $dateArr)
            ->where('daily_inv_rpt.whs_id', $data['whs_id'])
            ->where('daily_inv_rpt.cus_id', $data['cus_id'])
            ->first();
    }

    public function getStorageData($data, $storageDate) {
        date_default_timezone_set(config('constants.timezone_utc_zero'));
        $storageDateTime = new \DateTime($storageDate);

        return $this->model
            ->select([
                DB::raw("IFNULL(SUM(daily_inv_rpt.cur_ctns),0) AS cur_ctn_qty"),
                DB::raw("IFNULL(SUM(daily_inv_rpt.cur_qty),0) AS cur_piece_qty"),
                DB::raw("IFNULL(SUM(daily_inv_rpt.cur_plt),0) AS cur_plt_qty"),
                DB::raw("IFNULL(SUM(daily_inv_rpt.cur_ctns * item.cube),0) AS cur_cube_qty"),
            ])
            ->join('item', 'daily_inv_rpt.item_id', '=', 'item.item_id')
            ->where('daily_inv_rpt.whs_id', $data['whs_id'])
            ->where('daily_inv_rpt.cus_id', $data['cus_id'])
            ->where('daily_inv_rpt.inv_dt', strtotime($storageDate . ' UCT'))
            ->first();
    }

    public function getForecastActualStorageLocation($data, $storageDate) {
        $dateArr = [
            'fromDate' => strtotime('first day of this month'),
            'toDate'   => strtotime($storageDate)
        ];
        $dateArrString = implode(' AND ', $dateArr);
        $dateInterval = ceil(($dateArr['toDate'] - $dateArr['fromDate'] ) / (24 * 60 * 60));

        $data = $this->model
                ->select([
                    DB::raw("IFNULL(SUM(daily_inv_rpt.init_plt) / {$dateInterval},0) AS act_storage_loc")
                ])
                ->where('daily_inv_rpt.whs_id', $data['whs_id'])
                ->where('daily_inv_rpt.cus_id', $data['cus_id'])
                ->whereRaw("daily_inv_rpt.inv_dt BETWEEN {$dateArrString} ")
                ->first();

        return $data['act_storage_loc'];
    }

    public function getLocationByDate($data, $storageDate){
        date_default_timezone_set(config('constants.timezone_utc_zero'));
        $storageTime = strtotime($storageDate);
        $data = $this->model
                ->select([
                    DB::raw("IFNULL(SUM(daily_inv_rpt.init_plt), 0) AS location")
                ])
                ->where('daily_inv_rpt.whs_id', $data['whs_id'])
                ->where('daily_inv_rpt.cus_id', $data['cus_id'])
                ->where('daily_inv_rpt.inv_dt', $storageTime)
                ->first();

        return $data['location'];
    }
}
