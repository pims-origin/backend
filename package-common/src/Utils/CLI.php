<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 10-Jan-17
 * Time: 10:44
 */

namespace Seldat\Wms2\Utils;

/**
 * Class CLI Command Line Color
 *
 * @package Seldat\Wms2\Utils
 */
class CLI
{
    const BLACK = 'black';
    const DARK_GRAY_FOR_TEXT = 'dark_gray';
    const BLUE = 'blue';
    const LIGHT_BLUE_FOR_TEXT = 'light_blue';
    const GREEN = 'green';
    const LIGHT_GREEN_FOR_TEXT = 'light_green';
    const CYAN = 'cyan';
    const LIGHT_CYAN_FOR_TEXT = 'light_cyan';
    const RED = 'red';
    const LIGHT_RED_FOR_TEXT = 'light_red';
    const PURPLE_FOR_TEXT = 'purple';
    const LIGHT_PURPLE_FOR_TEXT = 'light_purple';
    const BROWN_FOR_TEXT = 'brown';
    const YELLOW = 'yellow';
    const LIGHT_GRAY = 'light_gray';
    const WHITE_FOR_TEXT = 'white';
    const MAGENTA_FOR_BG = 'magenta';

    protected static $txt_colors = [
        self::BLACK                 => '0;30',
        self::DARK_GRAY_FOR_TEXT    => '1;30',
        self::BLUE                  => '0;34',
        self::LIGHT_BLUE_FOR_TEXT   => '1;34',
        self::GREEN                 => '0;32',
        self::LIGHT_GREEN_FOR_TEXT  => '1;32',
        self::CYAN                  => '0;36',
        self::LIGHT_CYAN_FOR_TEXT   => '1;36',
        self::RED                   => '0;31',
        self::LIGHT_RED_FOR_TEXT    => '1;31',
        self::PURPLE_FOR_TEXT       => '0;35',
        self::LIGHT_PURPLE_FOR_TEXT => '1;35',
        self::BROWN_FOR_TEXT        => '0;33',
        self::YELLOW                => '1;33',
        self::LIGHT_GRAY            => '0;37',
        self::WHITE_FOR_TEXT        => '1;37',
    ];

    protected static $bg_colors = [
        self::BLACK          => '40',
        self::RED            => '41',
        self::GREEN          => '42',
        self::YELLOW         => '43',
        self::BLUE           => '44',
        self::MAGENTA_FOR_BG => '45',
        self::CYAN           => '46',
        self::LIGHT_GRAY     => '47',
    ];


    // Returns colored string
    public static function setTextColor($text, $txtColor = null, $bgColor = null)
    {
        $output = "";

        // Background Color
        if (!empty(self::$bg_colors[$bgColor])) {
            $output .= "\033[" . self::$bg_colors[$bgColor] . "m";
        }

        // Text Color
        if (!empty(self::$txt_colors[$txtColor])) {
            $output .= "\033[" . self::$txt_colors[$txtColor] . "m";
        }

        $output .= "$text\033[0m";

        return $output;
    }

    /**
     * List All Text Color
     *
     * @return array
     */
    public static function listTxtColor()
    {
        return array_keys(self::$txt_colors);
    }

    /**
     * List All Background Color
     *
     * @return array
     */
    public static function listBGColor()
    {
        return array_keys(self::$bg_colors);
    }

    public static function showExample()
    {
        foreach (self::$bg_colors as $bgName => $bgCode) {
            foreach (self::$txt_colors as $txtName => $txtCode) {
                echo self::setTextColor("BG Color: $bgName - TXT Color: $txtName", $txtName, $bgName) . "\n";
            }
            echo PHP_EOL;
        }
    }
}
