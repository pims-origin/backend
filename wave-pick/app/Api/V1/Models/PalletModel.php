<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Pallet;
use Wms2\UserInfo\Data;

class PalletModel extends AbstractModel
{

    protected $model;

    /**
     * PalletModel constructor.
     *
     * @param Pallet|null $model
     */
    public function __construct(Pallet $model = null)
    {
        $this->model = ($model) ?: new Pallet();
    }

    /**
     * @param $loc_id
     * @deprecated  DONT USE IT
     * @return mixed
     */
    public function updatePallet($loc_id)
    {
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);

        // Change request: Update table pallet - update column storage_duration - 24-11-2016
        // Get Pallet info
        $pltInfo = $this->model
            ->where('loc_id', '=', $loc_id)
            ->where('whs_id', '=', $currentWH)
            ->first();

        $created_at = array_get($pltInfo, 'created_at', 0);
        $zeroDt = time();

        // Calculate storage_duration
        $date1 = date("Y-m-d", is_int($created_at) ?: $created_at->timestamp);
        $date2 = date("Y-m-d");

        $dateDiff = (int)round(abs(strtotime($date1) - strtotime($date2)) / 86400);
        if($dateDiff == 0)
        {
            $storageDuration = 1;
        }   else {
            $storageDuration = $dateDiff;
        }

        return $this->model
            ->where('loc_id', '=', $loc_id)
            ->where('whs_id', '=', $currentWH)
            ->update([
                'loc_id'           => null,
                'loc_code'         => null,
                'loc_name'         => null,
                'zero_date'        => $zeroDt,
                'storage_duration' => $storageDuration
            ]);
    }

    public function updatePalletCtnTtl($locIds)
    {
        foreach (array_chunk($locIds, 200) as $chunkLocIds) {
            DB::table("rpt_pallet")
                ->whereIn('loc_id', $chunkLocIds)
                ->update([
                    'ctn_ttl' => DB::raw("(SELECT COUNT(c.ctn_id) FROM cartons c WHERE rpt_pallet.plt_id = c.plt_id AND ctn_sts = 'AC' AND deleted = 0 AND deleted_at = 915148800)")
                ]);
        }
        foreach (array_chunk($locIds, 200) as $chunkLocIds) {
            $this->model
                ->whereIn('loc_id', $chunkLocIds)
                ->update([
                    'ctn_ttl' => DB::raw("(SELECT COUNT(c.ctn_id) FROM cartons c WHERE pallet.plt_id = c.plt_id AND ctn_sts = 'AC' AND deleted = 0 AND deleted_at = 915148800)")
                ]);
        }
    }



    public function updateZeroPallet($locIds)
    {
        //$strSQL = sprintf("IF(DATEDIFF(NOW(), FROM_UNIXTIME(created_at)) > 0 , DATEDIFF('%s', FROM_UNIXTIME
        //(created_at)), 1)", date('Y-m-d'));
        foreach (array_chunk($locIds, 200) as $chunkLocIds) {
            DB::table("rpt_pallet")
                ->whereIn('loc_id', $chunkLocIds)
                ->where('ctn_ttl', 0)
                ->update([
                    'loc_id' => null,
                    'loc_code' => null,
                    'loc_name' => null,
                    'zero_date' => time(),
                    'storage_duration' => CartonModel::getCalculateStorageDurationRaw()
                ]);
        }
        foreach (array_chunk($locIds, 200) as $chunkLocIds) {
            $this->model
                ->whereIn('loc_id', $chunkLocIds)
                ->where('ctn_ttl', 0)
                ->update([
                    'loc_id' => null,
                    'loc_code' => null,
                    'loc_name' => null,
                    'zero_date' => time(),
                    'storage_duration' => CartonModel::getCalculateStorageDurationRaw()
                ]);
        }
    }

    public function updatePalletZeroDateAndDurationDays($plt_id) {
        $pltInfo = $this->model
            ->where('plt_id', '=', $plt_id)
            ->first();

        $created_at = array_get($pltInfo, 'created_at', 0);
        $zeroDt = time();

        // Calculate storage_duration
        $date1 = date("Y-m-d", is_int($created_at) ?: $created_at->timestamp);
        $date2 = date("Y-m-d");

        $dateDiff = (int)round(abs(strtotime($date1) - strtotime($date2)) / 86400);
        if($dateDiff == 0)
        {
            $storageDuration = 1;
        }   else {
            $storageDuration = $dateDiff;
        }

        return $this->model
            ->where('plt_id', '=', $plt_id)
            ->update([
                'loc_id'           => null,
                'loc_code'         => null,
                'loc_name'         => null,
                'zero_date'        => $zeroDt,
                'storage_duration' => $storageDuration
            ]);
    }

    public function updatePalletZeroDateAndDurationDaysAuto()
    {
        $this->model
            ->whereNull('pallet.zero_date')
            ->where(DB::raw("(SELECT COUNT(cartons.ctn_id) FROM cartons WHERE cartons.plt_id = pallet.plt_id)"), 0)
            ->update([
                'pallet.zero_date'       =>
                    DB::raw("(SELECT plt.updated_at) FROM pallet as plt WHERE plt.plt_id = pallet.plt_id)"),
                'pallet.ctn_ttl'         => 0,
                'pallet.loc_id'          => null,
                'pallet.loc_code'        => null,
                'pallet.loc_name'        => null,
                'pallet.storage_duration'=>
                    DB::raw("(IF(DATEDIFF(FROM_UNIXTIME(pallet.updated_at), FROM_UNIXTIME(pallet.created_at)), 
                        DATEDIFF(FROM_UNIXTIME(pallet.updated_at),FROM_UNIXTIME(pallet.created_at)), 1))")
            ]);

        $this->model
            ->whereNull('pallet.zero_date')
            ->where(DB::raw("(SELECT COUNT(cartons.ctn_id) FROM cartons WHERE cartons.plt_id = pallet.plt_id)"),'>', 0)
            ->update([
                'pallet.ctn_ttl' =>
                    DB::raw("(SELECT COUNT(cartons.ctn_id) FROM cartons WHERE cartons.plt_id = pallet.plt_id)")
            ]);
    }
}
