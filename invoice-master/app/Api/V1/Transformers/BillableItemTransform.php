<?php

namespace App\Api\V1\Transformers;

use App\BillableDtl;
use App\CustomerMeta;
use App\VasHdr;
use League\Fractal\TransformerAbstract;

class BillableItemTransform extends TransformerAbstract
{

    /**
     * ps:Show WorkOrder
     *
     * @param VasHdr $workOrderHdr
     *
     * @return array
     */
    public function transform(BillableDtl $billableDtl)
    {
        $currency = $this->getCurrencyViaCustomerMeta($billableDtl->cus_id, $billableDtl->whs_id);
        return [
            'ba_dtl_id'       => $billableDtl->ba_dtl_id,
            'ba_id'           => $billableDtl->ba_id,
            'ref_num'         => $billableDtl->ref_num,
            'sac'             => $billableDtl->sac,
            'sac_id'          => $billableDtl->sac_id,
            'sac_name'        => $billableDtl->sac_name,
            'sac_description' => $billableDtl->sac_description,
            'unit_uom'        => $billableDtl->sac_unit_uom,
            'uom'             => $billableDtl->sac_unit_uom, //save in invoice detail
            'unit_uom_name'   => $billableDtl->sac_unit_uom_name,
            'unit_price'      => $billableDtl->unit_price,
            'currency'        => $currency ?: 'USD',
            'qty'             => $billableDtl->qty,
            'discount'        => $billableDtl->discount,
            'sac_unique'      => $billableDtl->sac_unique,
            'item'            => $billableDtl->item,
            'description'     => $billableDtl->description,
            'is_charge'       => 1,
            'type'            => $billableDtl->ba_type 
        ];
    }

    private function getCurrencyViaCustomerMeta($cusId, $whsId, $qualifier = 'CUR')
    {
        $customerMeta = CustomerMeta::where('cus_id', $cusId)
            ->where('qualifier', $qualifier)->first();

        if (!$customerMeta) {
            return 'USD';
        }

        $option = json_decode($customerMeta->value, true);

        if (!is_array($option) || !array_key_exists($whsId, $option)) {
            return 'USD';
        }

        if (!array_key_exists('currency', $option[$whsId])) {
            return 'USD';
        }

        return $option[$whsId]['currency'] ?? 'USD';
    }
}
