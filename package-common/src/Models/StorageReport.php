<?php

namespace Seldat\Wms2\Models;

class StorageReport extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'strg_rpt';

    /**
     * @var array
     */
    protected $fillable = [
        'whs_id',
        'cus_id',
        'date',
        'rack',
        'bin',
        'shelf',
        'created_at'
    ];
}