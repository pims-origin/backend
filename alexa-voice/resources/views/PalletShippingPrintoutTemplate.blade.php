﻿<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta charset="utf-8"/>
    <title>Pallet Print Order</title>
    <style type="text/css">

        .container{
            margin-bottom: 10px;
            padding-bottom: 10px;
            border-bottom: 1px dashed #000;
			padding-left: 40px;
			padding-right: 40px;
        }
        h4 {
            margin-bottom: 5px;
        }

        h1 {
            color: #504e4f;
            font-size: 32px;
            font-weight: 100;
            margin: 0;
        }

        h2 {
            color: #364150;
            text-transform: uppercase;
            margin: 5px 0;
            font-weight: 500;
        }

        body {
            background-color: #fff;
            color: #000;
            font-size: 14px;
            font-family: "Calibri";
            width: 21cm;
            height: 29.7cm;
            margin: 0 auto;
            border: 0px solid #ddd;
            padding: 20px;
            margin: 0 auto;

        }


        .table-style thead tr th {
            font-weight: 400;
            text-transform: uppercase;
            text-align: left;
            color: #a9a9a9;
            font-size: 19px;
        }

        .table-style tbody tr td {
            padding-top: 5px;
            vertical-align: top;
            padding-bottom: 20px;
        }

        .table-style tbody tr td b {
            font-size: 20px;
            font-weight: 600;
            color: #282828;
        }
        .table-style1{
            width: 100%;
            border: 1px solid #ddd;
            line-height: 20px;
        }
        .table-style1 thead tr th{
            color: #333;
            padding: 0 5px;
            border-bottom: 1px dashed #d2d2d2;
            font-size: 16px;
            text-transform: uppercase;
            font-weight: bold;
            text-align: left;
            border-right: 1px solid #e3f1f2;
            line-height: 24px;
        }
        .table-style1 tbody tr td{
            border-bottom: 1px dashed #ddd;
            padding: 0 5px;
            color: #595959;
        }
        .table-style1 tbody tr:last-child td{
            border-bottom: none;
        }
        .table-style1 tbody tr td.title{
            border-right: 1px solid #ddd;
            background-color: #e3f1f2;
            width: 20%;
        }
        .table-style2 {
            border: 1px solid #ddd;
            line-height: 20px;
        }

        .table-style2 thead tr th {
            font-weight: 600;
            border-bottom: 1px solid #c0c0c0;
            text-align: left;
            background-color: #e7e7e7;
            padding-left: 5px;
            border-right: 1px solid #c1c1c1;
            color: #595959;
            text-transform: capitalize;
            line-height: 24px;
        }

        .table-style2 tbody tr td {
            padding-left: 5px;
            border-right: 1px dashed #d5d5d5;
            border-bottom: 1px dashed #d5d5d5;
            color: #595959;
            line-height: 20px;
        }

        .table-style2 tbody tr td:last-child {
            border-right: none;
        }
        .table-style2 tbody tr td.title{
            font-weight: 600;
            color: #364150;
        }
        .table-style2 tbody tr:nth-child(2n) td {
            background-color: #e3f1f2;
        }

        .table-style2 tbody tr:last-child td {
            border-bottom: none
        }

        .text-right {
            text-align: right !important;
        }
        .table-style3 thead tr th{
            font-weight: 600;
            text-align: left;
            color: #364150;
            text-transform: capitalize;
        }
        .table-style3 tr td {
            line-height: 20px;
            font-size: 14px;
            color: #595959;
            padding-right: 5px;
        }

        .table-style3 tr td b {
            color: #1c1c1c;
        }

        .table-style4{
            width: 100%;
            border: 1px solid #ddd;
            line-height: 24px;
        }
        .table-style4 thead tr th{
            color: #36c6d3;
            padding: 0 5px;
            border-bottom: 1px dashed #a9a9a9;
            font-size: 16px;
            text-transform: uppercase;
            font-weight: bold;
            text-align: left;
            border-right: 1px solid #e3f1f2;
        }
        .table-style4 tbody tr td{
            padding: 5px;
            color: #595959;
            line-height: 20px;
            border-bottom: 1px solid #ddd;

        }
        .table-style4 tbody tr td:last-child{
            border-right: none;
        }
        .table-style4 tbody tr:last-child td{
            border-bottom: none;
        }
        .table-style4 tbody tr td.title{
            white-space:nowrap;
            width: 15%;
            font-weight: 600;
            color: #333;
        }
    </style>
</head>
<body>
<?php 
$i=1;
$ctns_ttl=0;
?>
@foreach ($orderHdrs as $orderHdr)
<div class="container">
    <table class="table-style1" cellspacing="0" cellpadding="0">
        <thead>
        <tr>
            <th width="50%">FROM</th>
            <th width="50%">SHIP TO</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{array_get($orderHdr, 'whs_name', '')}}<br>
                {{array_get($orderHdr, 'ship_from_addr_1', '')}} <br>
                {{array_get($orderHdr, 'ship_from_city', '')}},{{array_get($orderHdr, 'ship_from_state', '')}},{{array_get($orderHdr, 'ship_from_zip', '')}}.

            </td>
            <td>
                {{array_get($orderHdr, 'ship_to_name', '')}}<br>
                {{array_get($orderHdr, 'ship_to_add_1', '')}}<br>
                {{array_get($orderHdr, 'ship_to_city', '')}}, {{array_get($orderHdr, 'ship_to_state', '')}} {{array_get($orderHdr, 'ship_to_zip', '')}}
            </td>
        </tr>
        </tbody>
    </table>
    <table class="table-style2" cellpadding="0" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th width="50%">Ship to Postal Code</th>
            <th>Carrier</th>
        </tr>
        </thead>
        <tbody>
        <tr style="vertical-align: middle">
            <td style="padding: 5px; text-align: center">
                <barcode height="1.5" size="0.9" code="{{array_get($orderHdr, 'ship_to_zip', '')}}" type="C128A"/>
                    <br>
                    {{array_get($orderHdr, 'ship_to_zip', '')}}
            </td>
            <td>{{array_get($orderHdr, 'carrier_name', '')}}</td>
        </tr>
        </tbody>
    </table>
    <table class="table-style4" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td class="title">Cus Name:</td>
            <td style="white-space: nowrap">{{array_get($orderHdr, 'cus_name', '')}}</td>
            <td class="title">CTNS:</td>
            <td>{{$ctn_Total}}</td>
            <td class="title">Pallet:</td>
            <td>{{$i++}} of {{!empty($plt_Total) ? $plt_Total: 0}}</td>
        </tr>
        <tr>
            <td class="title">CUS ORD#:</td>
            <td colspan="5">{{array_get($orderHdr, 'cus_odr_num', '')}}</td>
        </tr>
        <tr>
            <td class="title">PO #:</td>
            <td>{{array_get($orderHdr, 'cus_po', '')}}</td>
            <td class="title" style="border-left:1px solid #ddd">Lot:</td>
            <td>{{array_get($orderHdr, 'lot', '')}}</td>
            <td colspan="2" rowspan="2" style="text-align: center; border-left:1px solid #ddd">
                <barcode height="1.5" size="0.9" code="{{!empty(array_get($orderHdr, 'cus_upc', '')) ? array_get($orderHdr, 'cus_upc', 0): array_get($orderHdr, 'item_id', 0)}}" type="C128A"/>
                    <br>
                    {{!empty(array_get($orderHdr, 'cus_upc', '')) ? array_get($orderHdr, 'cus_upc', 0): array_get($orderHdr, 'item_id', 0)}}
            </td>
        </tr>
        <tr>
            <td class="title">Item #:</td>
            <td>{{array_get($orderHdr, 'item_id', '')}}</td>
            <td class="title" style="border-left:1px solid #ddd">UPC:</td>
            <td>{{array_get($orderHdr, 'cus_upc', '')}}</td>
        </tr>
        <tr>

        </tr>
        <tr>

        </tr>
        <tr>
            <td class="title">SKU:</td>
            <td>{{array_get($orderHdr, 'sku', '')}}</td>
            <td class="title">Size:</td>
            <td>{{array_get($orderHdr, 'size', '')}}</td>
            <td class="title">Color:</td>
            <td>{{array_get($orderHdr, 'color', '')}}</td>
        </tr>
        </tbody>
    </table>
    <table class="table-style1" cellspacing="0" cellpadding="0">
        <!--<thead>
        <tr>
            <th>SSCC</th>
        </tr>
        </thead>-->
        <tbody>
        <tr>
            <td style="text-align: center; padding: 5px">
                 <barcode height="1.5" size="0.9" code="{{array_get($orderHdr, 'plt_num', '')}}" type="C128A"/>
                    <br>
                    {{array_get($orderHdr, 'plt_num', '')}}
            </td>
        </tr>
        </tbody>
    </table>
</div>
@endforeach
</body>
</html>
