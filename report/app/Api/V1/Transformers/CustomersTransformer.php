<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\Customer;
use League\Fractal\TransformerAbstract;

class CustomersTransformer extends TransformerAbstract
{
    public function transform(Customer $customers)
    {
        return [
            'cus_name' => $customers->cus_name,
            'cus_status' => object_get($customers, 'customerStatus.cus_sts_name', ''),
            'cus_code' => $customers->cus_code,
            'cus_country' => object_get($customers, 'systemCountry.sys_country_name', ''),
            'cus_state' => object_get($customers, 'systemState.sys_state_name', ''),
            'cus_city_name' => $customers->cus_city_name,
            'cus_postal_code' => $customers->cus_postal_code,
            'cus_billing_account' => $customers->cus_billing_account,
            'cus_des' => $customers->cus_des,
            'net_terms' => $customers->net_terms,
            'cus_ctt_department' => object_get($customers, 'customerDefaultContact.cus_ctt_department', ''),
            'cus_ctt_position' => object_get($customers, 'customerDefaultContact.cus_ctt_position', ''),
            'cus_ctt_fname' => object_get($customers, 'customerDefaultContact.cus_ctt_fname', ''),
            'cus_ctt_lname' => object_get($customers, 'customerDefaultContact.cus_ctt_lname', ''),
            'cus_ctt_email' => object_get($customers, 'customerDefaultContact.cus_ctt_email', ''),
            'cus_ctt_phone' => object_get($customers, 'customerDefaultContact.cus_ctt_phone', ''),
            'cus_ctt_ext' => object_get($customers, 'customerDefaultContact.cus_ctt_ext', ''),
            'cus_ctt_mobile' => object_get($customers, 'customerDefaultContact.cus_ctt_mobile', ''),
            'cus_ware_city'=>object_get($customers,'customerwarehouse.warehouse.whs_city_name',''),
            'cus_bill_ship'=>object_get($customers,'customeraddress.cus_add_type',''),
            'cus_add_line_1'=>object_get($customers,'customeraddress.cus_add_line_1',''),
            'cus_add_line_2'=>object_get($customers,'customeraddress.cus_add_line_2',''),
            'cus_add_city_name'=>object_get($customers,'customeraddress.cus_add_city_name',''),
            'cus_add_state'=>object_get($customers,'customeraddress.systemstate.sys_state_name',''),          
            'cus_add_postal_code'=>object_get($customers,'customeraddress.cus_add_postal_code',''),
            'cus_add_country'=>object_get($customers,'customeraddress.systemcountry.sys_country_name','')
           
        ];
    }

    /*
    ****************************************************************************
    */

}
