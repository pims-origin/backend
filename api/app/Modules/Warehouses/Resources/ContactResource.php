<?php

namespace App\Modules\Warehouses\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContactResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'type' => 'contacts',
            'id' => (int)$this->whs_con_id,
            'attributes' => [
                'first_name' => $this->whs_con_fname,
                'last_name' => $this->whs_con_lname,
                'position' => $this->whs_con_position,
                'email' => $this->whs_con_email,
                'phone' => $this->whs_con_phone,
                'extension' => $this->whs_con_ext,
                'mobile' => $this->whs_con_mobile,
            ]
        ];
    }
}


