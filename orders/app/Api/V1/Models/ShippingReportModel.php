<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\ReportShipping;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

/**
 * Class ShippingReportModel
 *
 * @package App\Api\V1\Models
 */
class ShippingReportModel extends AbstractModel
{
    /**
     * @var ShippingReport
     */
    protected $model;

    /**
     * ShipmentModel constructor.
     *
     * @param ShippingReport|null $model
     */
    public function __construct(ReportShipping $model = null)
    {
        $this->model = ($model) ?: new ReportShipping();
    }


}
