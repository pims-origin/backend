<?php

namespace App\Console\Commands;

use App\Api\V1\Models\ForecastModel;
use App\Api\V1\Traits\ForecastControllerTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AwmsForecastsMonthly extends AbstractCommand
{
    use ForecastControllerTrait;
    protected $signature = 'awms-forecast-monthly';
    protected $description = 'Awms Forecast Monthly';
    protected $forecastModel;

    protected $mapping;
    protected $customerIds = '';

    public function __construct()
    {
        parent::__construct();
        $this->forecastModel = new ForecastModel();
        $this->mapping = new AwmsMapping();
    }

    public function handle()
    {
        $dateFrom = date('Y-m-01');
        $dateTo = date('Y-m-t');

        $warehouses = array_filter($this->mapping->whsMap());
        if (empty ($warehouses)) {
            return false;
        }
        $warehouses = array_flip($warehouses);

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        try {

            DB::beginTransaction();

            $year = date('Y');
            $month = date('m');
            $lastYear = date('Y-m-d', strtotime('-1 year', strtotime($dateFrom)));

            foreach ($warehouses as $whsId => $AWMS_WhsId) {

                if (!$AWMS_WhsId || !$whsId) {
                    continue;
                }

                $this->customerIds = $this->mapping->customerMapping($whsId, 'AWMS');
                if (empty($this->customerIds)) {
                    continue;
                }
                // Delete Old data
                /*DB::table('forecast')->where('whs_id', $whsId)
                    ->whereIn('cus_id', $this->customerIds)
                    ->where('fc_month', $month)
                    ->where('fc_year', $year)
                    ->delete();*/

                $fc_indicators = DB::table('fc_indicator')->where('deleted', 0)->get();
                $arrInserts = [];
                foreach ($fc_indicators as $fc_indicator) {

                    $fc_ind_code = $fc_indicator['fc_ind_code'];

                    if (!$fc_ind_code) {
                        continue;
                    }

                    $data = [];
                    switch ($fc_ind_code) {
                        case 'CTNR':
                            $data = DB::connection('awms')->table('inventory_containers AS co')
                                ->select([
                                    DB::raw('v.id as awms_cus_id'),
                                    //DB::raw('v.vendorName as cus_name'),
                                    //DB::raw('MONTH(ca.created_at) AS fc_month'),
                                    //DB::raw('YEAR(ca.created_at) AS fc_year'),
                                    DB::raw("(COUNT(DISTINCT IF(ca.cp_at >= $lastYear, co.recNum, NULL)) / 12) AS fc_ib"),
                                    DB::raw("COUNT(DISTINCT IF(YEAR(ca.cp_at) = $year AND MONTH(ca.cp_at) = $month, co.recNum, NULL)) AS fc_ib_act"),
                                    DB::raw('COUNT(DISTINCT co.recNum) AS fc_ib'),
                                    DB::raw('0 AS fc_ob'),
                                    DB::raw('0 AS fc_ob_act'),
                                    DB::raw('COUNT(DISTINCT co.recNum) AS fc_st'),
                                    DB::raw('COUNT(DISTINCT IF(ca.`statusID` IN (29, 9, 11, 10), co.recNum, NULL)) AS fc_st_act')
                                ])
                                ->join('inventory_batches AS b', 'b.recNum', '=', 'co.recNum')
                                ->join('inventory_cartons AS ca', 'ca.batchID', '=', 'b.id')
                                ->join('vendors AS v', 'v.id', '=', 'co.vendorID')
                                ->where('v.warehouseID', $AWMS_WhsId)
                                ->where("ca.isSplit", '=', 0)
                                ->where("ca.unSplit", '=', 0)
                                ->where('ca.cp_at', '<=', $dateTo)
                                //->whereBetween('ca.created_at', [$dateFrom, $dateTo])
                                ->groupBy('v.id'/*, DB::raw('YEAR(ca.`created_at`)'), DB::raw('MONTH(ca.`created_at`)')*/)
                                ->get();

                            break;
                        case 'PL':
                            $data = DB::connection('awms')->table('inventory_containers AS co')
                                ->select([
                                    DB::raw('v.id as awms_cus_id'),
                                    //DB::raw('v.vendorName as cus_name'),
                                    //DB::raw('MONTH(ca.created_at) AS fc_month'),
                                    //DB::raw('YEAR(ca.created_at) AS fc_year'),
                                    DB::raw("(COUNT(DISTINCT IF(ca.cp_at >= $lastYear, ca.plate, NULL)) / 12) AS fc_ib"),
                                    DB::raw("COUNT(DISTINCT IF(YEAR(ca.cp_at) = $year AND MONTH(ca.cp_at) = $month, ca.plate, NULL)) AS fc_ib_act"),
                                    DB::raw('(COUNT(DISTINCT IF(ca.statusID = 12, ca.plate, NULL)) / 12) AS fc_ob'),
                                    DB::raw("COUNT(DISTINCT IF(YEAR(ca.cp_at) = $year AND MONTH(ca.cp_at) = $month AND ca.statusID = 12, ca.plate, NULL)) AS fc_ob_act"),
                                    DB::raw('COUNT(DISTINCT IF(ca.`statusID` IN (29, 9, 11, 10), ca.plate, NULL)) AS fc_st'),
                                    DB::raw('COUNT(DISTINCT IF(ca.`statusID` IN (29, 9, 11, 10), ca.plate, NULL)) AS fc_st_act')
                                ])
                                ->join('inventory_batches AS b', 'b.recNum', '=', 'co.recNum')
                                ->join('inventory_cartons AS ca', 'ca.batchID', '=', 'b.id')
                                ->join('vendors AS v', 'v.id', '=', 'co.vendorID')
                                ->where('v.warehouseID', $AWMS_WhsId)
                                ->where("ca.isSplit", '=', 0)
                                ->where("ca.unSplit", '=', 0)
                                ->where('ca.cp_at', '<=', $dateTo)
                                //->whereBetween('ca.created_at', [$dateFrom, $dateTo])
                                ->groupBy('v.id'/*, DB::raw('YEAR(ca.`created_at`)'), DB::raw('MONTH(ca.`created_at`)')*/)
                                ->get();

                            if (count($data)) {

                                foreach (array_chunk($data, 300) as $newData) {

                                    $awms_cus_ids = array_pluck($newData, 'awms_cus_id', 'awms_cus_id');

                                    if (!empty($awms_cus_ids)) {

                                        $obData = DB::connection('awms')->table('inventory_containers AS co')
                                            ->select([
                                                DB::raw('v.id as awms_cus_id'),
                                                DB::raw("(SUM(IF(tr.cp_at >= $lastYear, tr.plateCount, NULL)) / 12) AS fc_ib"),
                                                DB::raw("SUM(IF(YEAR(tr.cp_at) = $year AND MONTH(tr.cp_at) = $month, tr.plateCount, 0)) AS fc_ib_act"),
                                            ])
                                            ->join('tallies AS t', 't.recNum', '=', 'co.recNum')
                                            ->join('tally_rows AS tr', 'tr.tallyID', '=', 't.id')
                                            ->join('vendors AS v', 'v.id', '=', 'co.vendorID')
                                            ->where('v.warehouseID', $AWMS_WhsId)
                                            ->where('tr.cp_at', '<=', $dateTo)
                                            ->whereIn('v.id', $awms_cus_ids)
                                            ->groupBy('v.id')
                                            ->get();

                                        $awms_cus_ids = [];
                                        if (!empty($obData)) {
                                            foreach ($data as $key => $dataOld) {
                                                foreach ($obData as $item) {
                                                    if ($dataOld['awms_cus_id'] == $item['awms_cus_id']) {
                                                        $data[$key]['fc_ib'] = $item['fc_ib'];
                                                        $data[$key]['fc_ib_act'] = $item['fc_ib_act'];
                                                    }
                                                }
                                            }
                                        }

                                        $obData = false;
                                    }
                                }
                            }

                            break;

                        case 'CTN':
                            $data = DB::connection('awms')->table('inventory_containers AS co')
                                ->select([
                                    DB::raw('v.id as awms_cus_id'),
                                    //DB::raw('v.vendorName as cus_name'),
                                    //DB::raw('MONTH(ca.created_at) AS fc_month'),
                                    //DB::raw('YEAR(ca.created_at) AS fc_year'),
                                    DB::raw("(COUNT(DISTINCT IF(ca.cp_at >= $lastYear, ca.id, NULL)) / 12) AS fc_ib"),
                                    // DB::raw('COUNT(DISTINCT ca.`id`) AS fc_ib_act'),
                                    DB::raw("SUM(IF(YEAR(ca.cp_at) = $year AND MONTH(ca.cp_at) = $month, 1, 0)) AS fc_ib_act"),
                                    DB::raw('SUM(IF(ca.`statusID` = 12, 1, 0)) AS fc_ob'),
                                    //DB::raw('SUM(IF(ca.`statusID` = 12, 1, 0)) AS fc_ob_act'),
                                    DB::raw("SUM(IF(YEAR(ca.cp_at) = $year AND MONTH(ca.cp_at) = $month AND ca.statusID = 12, 1, 0)) AS fc_ob_act"),
                                    DB::raw('SUM(IF(ca.`statusID` IN (29, 9, 11, 10), 1, 0)) AS fc_st'),
                                    DB::raw('SUM(IF(ca.`statusID` IN (29, 9, 11, 10), 1, 0)) AS fc_st_act')
                                ])
                                ->join('inventory_batches AS b', 'b.recNum', '=', 'co.recNum')
                                ->join('inventory_cartons AS ca', 'ca.batchID', '=', 'b.id')
                                ->join('vendors AS v', 'v.id', '=', 'co.vendorID')
                                ->where('v.warehouseID', $AWMS_WhsId)
                                ->where("ca.isSplit", '=', 0)
                                ->where("ca.unSplit", '=', 0)
                                ->where('ca.cp_at', '<=', $dateTo)
                                //->whereBetween('ca.created_at', [$dateFrom, $dateTo])
                                ->groupBy('v.id'/*, DB::raw('YEAR(ca.`created_at`)'), DB::raw('MONTH(ca.`created_at`)')*/)
                                ->get();

                            break;

                        case 'CUB':
                            $data = DB::connection('awms')->table('inventory_containers AS co')
                                ->select([
                                    DB::raw('v.id as awms_cus_id'),
//                                    DB::raw('v.vendorName as cus_name'),
//                                    DB::raw('MONTH(ca.created_at) AS fc_month'),
//                                    DB::raw('YEAR(ca.created_at) AS fc_year'),
                                    DB::raw("(SUM(IF(ca.cp_at >= $lastYear, (b.`length` * b.`width` * b.`height`)/1728, 0)) / 12) AS fc_ib"),
                                    //DB::raw('SUM((b.`length` * b.`weight` * b.`height`)/1728) AS fc_ib_act'),
                                    DB::raw("SUM(IF(YEAR(ca.cp_at) = $year AND MONTH(ca.cp_at) = $month, (b.`length` * b.`width` * b.`height`)/1728, 0)) AS fc_ib_act"),

                                    DB::raw("(SUM(IF(ca.cp_at >= $lastYear AND ca.`statusID` = 12, (b.`length` * b.`width` * b.`height`)/1728, 0)) / 12) AS fc_ob"),
                                    //DB::raw('SUM(IF(ca.`statusID` = 12, (b.`length` * b.`weight` * b.`height`)/1728, 0)) AS fc_ob_act'),
                                    DB::raw("SUM(IF(YEAR(ca.cp_at) = $year AND MONTH(ca.cp_at) = $month AND ca.`statusID` = 12, (b.`length` * b.`width` * b.`height`)/1728, 0)) AS fc_ob_act"),

                                    DB::raw('SUM(IF(ca.`statusID` IN (29, 9, 11, 10), (b.`length` * b.`width` * b.`height`)/1728, 0)) AS fc_st'),
                                    DB::raw('SUM(IF(ca.`statusID` IN (29, 9, 11, 10), (b.`length` * b.`width` * b.`height`)/1728, 0)) AS fc_st_act')
                                ])
                                ->join('inventory_batches AS b', 'b.recNum', '=', 'co.recNum')
                                ->join('inventory_cartons AS ca', 'ca.batchID', '=', 'b.id')
                                ->join('vendors AS v', 'v.id', '=', 'co.vendorID')
                                ->where('v.warehouseID', $AWMS_WhsId)
                                ->where("ca.isSplit", '=', 0)
                                ->where("ca.unSplit", '=', 0)
                                ->where('ca.cp_at', '<=', $dateTo)
                                //->whereBetween('ca.created_at', [$dateFrom, $dateTo])
                                ->groupBy('v.id'/*, DB::raw('YEAR(ca.`created_at`)'), DB::raw('MONTH(ca.`created_at`)')*/)
                                ->get();

                            break;

                        case 'UN':
                            $data = DB::connection('awms')->table('inventory_containers AS co')
                                ->select([
                                    DB::raw('v.id as awms_cus_id'),
                                    /*DB::raw('v.vendorName as cus_name'),
                                    DB::raw('MONTH(ca.created_at) AS fc_month'),
                                    DB::raw('YEAR(ca.created_at) AS fc_year'),*/
                                    DB::raw("(SUM(IF(ca.cp_at >= $lastYear, ca.uom, 0)) / 12) AS fc_ib"),
                                    //DB::raw('SUM(ca.`uom`) AS fc_ib_act'),
                                    DB::raw("SUM(IF(YEAR(ca.cp_at) = $year AND MONTH(ca.cp_at) = $month, ca.`uom`, 0)) AS fc_ib_act"),
                                    DB::raw("(SUM(IF(ca.cp_at >= $lastYear AND ca.`statusID` = 12, ca.uom, 0)) / 12) AS fc_ob"),
                                    //DB::raw('SUM(IF(ca.`statusID` = 12, ca.`uom`, 0)) AS fc_ob_act'),
                                    DB::raw("SUM(IF(YEAR(ca.cp_at) = $year AND MONTH(ca.cp_at) = $month AND ca.`statusID` = 12, ca.`uom`, 0)) AS fc_ob_act"),

                                    DB::raw('SUM(IF(ca.`statusID` IN (29, 9, 11, 10), ca.`uom`, 0)) AS fc_st'),
                                    DB::raw('SUM(IF(ca.`statusID` IN (29, 9, 11, 10), ca.`uom`, 0)) AS fc_st_act')
                                ])
                                ->join('inventory_batches AS b', 'b.recNum', '=', 'co.recNum')
                                ->join('inventory_cartons AS ca', 'ca.batchID', '=', 'b.id')
                                ->join('vendors AS v', 'v.id', '=', 'co.vendorID')
                                ->where('v.warehouseID', $AWMS_WhsId)
                                ->where("ca.isSplit", '=', 0)
                                ->where("ca.unSplit", '=', 0)
                                ->where('ca.cp_at', '<=', $dateTo)
                                //->whereBetween('ca.created_at', [$dateFrom, $dateTo])
                                ->groupBy('v.id'/*, DB::raw('YEAR(ca.`created_at`)'), DB::raw('MONTH(ca.`created_at`)')*/)
                                ->get();

                            break;

                        case /*'SHP'*/ 'ORD':
                            $data = DB::connection('awms')->table('neworder AS co')
                                ->select([
                                    DB::raw('v.id as awms_cus_id'),
                                    /*DB::raw('v.vendorName as cus_name'),
                                    DB::raw('MONTH(ob.dateCreated) AS fc_month'),
                                    DB::raw('YEAR(ob.dateCreated) AS fc_year'),*/
                                    DB::raw('0 AS fc_ib'),
                                    DB::raw('0 AS fc_ib_act'),
                                    DB::raw("(COUNT(DISTINCT IF(co.cp_at >= $lastYear, co.ID, NULL)) / 12) AS fc_ob"),
                                    DB::raw('COUNT(DISTINCT co.ID) AS fc_ob_act'),
                                    DB::raw('0 AS fc_st'),
                                    DB::raw('0 AS fc_st_act')
                                ])
                                ->join('order_batches AS ob', 'ob.id', '=', 'co.order_batch')
                                ->join('vendors AS v', 'v.id', '=', 'ob.vendorID')
                                ->where('v.warehouseID', $AWMS_WhsId)
                                ->where("co.statusID", 23)
                                ->whereBetween('co.readyshipdate', [$dateFrom, $dateTo])
                                ->groupBy('v.id'/*, DB::raw('YEAR(ob.readyshipdate)'), DB::raw('MONTH(ob.readyshipdate)')*/)
                                ->get();

                            break;

                        /*case 'ORD':
                            $data = DB::connection('awms')->table('neworder AS co')
                                ->select([
                                    DB::raw('v.id as awms_cus_id'),
                                    DB::raw('v.vendorName as cus_name'),
                                    DB::raw('MONTH(ob.dateCreated) AS fc_month'),
                                    DB::raw('YEAR(ob.dateCreated) AS fc_year'),
                                    DB::raw('0 AS fc_ib'),
                                    DB::raw('0 AS fc_ib_act'),
                                    DB::raw('COUNT(DISTINCT IF(co.statusID = 23, co.ID, NULL)) AS fc_ob'),
                                    //DB::raw('COUNT(DISTINCT IF(co.statusID = 23, co.ID, NULL)) AS fc_ob_act'),
                                    DB::raw("COUNT(IF(YEAR(ob.dateCreated) = $year AND MONTH(ob.dateCreated) = $month AND co.statusID = 23, co.ID, 0)) AS fc_ob_act"),
                                    DB::raw('COUNT(DISTINCT IF(co.statusID <> 23, co.ID, NULL)) AS fc_st'),
                                    DB::raw('COUNT(DISTINCT IF(co.statusID <> 23, co.ID, NULL)) AS fc_st_act')
                                ])
                                ->join('order_batches AS ob', 'ob.id', '=', 'co.order_batch')
                                ->join('vendors AS v', 'v.id', '=', 'ob.vendorID')
                                ->where('v.warehouseID', $AWMS_WhsId)
                                ->where('ob.dateCreated', '<=', $dateTo)
                                //->whereBetween('ob.dateCreated', [$dateFrom, $dateTo])
                                ->groupBy('v.id'/*, DB::raw('YEAR(ob.dateCreated)'), DB::raw('MONTH(ob.dateCreated)'))
                                ->get();

                            break;*/

                        case 'LOC':
                            $data = DB::connection('awms')->table('locations AS l')
                                ->select([
                                    DB::raw('v.id as awms_cus_id'),
                                    DB::raw('v.vendorName as cus_name'),
                                    DB::raw('MONTH(ca.created_at) AS fc_month'),
                                    DB::raw('YEAR(ca.created_at) AS fc_year'),
                                    DB::raw('COUNT(l.id) AS fc_ib'),
                                    DB::raw('COUNT(l.id) AS fc_ib_act'),
                                    DB::raw('0 AS fc_ob'),
                                    DB::raw('0 AS fc_ob_act'),
                                    DB::raw('COUNT(l.id) AS fc_st'),
                                    DB::raw('COUNT(l.id) AS fc_st_act')
                                ])
                                ->join('inventory_cartons AS ca', 'ca.locID', '=', 'l.id')
                                ->join('inventory_batches AS b', 'b.id', '=', 'ca.batchID')
                                ->join('inventory_containers AS co', 'co.recNum', '=', 'b.recNum')
                                ->join('vendors AS v', 'v.id', '=', 'co.vendorID')
                                ->where('v.warehouseID', $AWMS_WhsId)
                                ->whereBetween('ca.cp_at', [$dateFrom, $dateTo])
                                ->groupBy('v.id'/*, DB::raw('YEAR(ca.created_at)'), DB::raw('MONTH(ca.created_at)')*/)
                                ->get();

                            break;
                    }

                    $this->proccess($fc_indicator['fc_ind_id'], $data, $whsId, $arrInserts, $month, $year);
                }

                if (!empty($arrInserts)) {
                    /*foreach (array_chunk($arrInserts, 300) as $data) {
                        DB::table('forecast')->insert($data);
                    }*/
                    foreach ($arrInserts as $data) {

                        $forecast = DB::table('forecast')->where([
                                'fc_ind_id' => $data['fc_ind_id'],
                                'cus_id' => $data['cus_id'],
                                'whs_id' => $data['whs_id'],
                                'fc_month' => $data['fc_month'],
                                'fc_year' => $data['fc_year']
                            ])->first();

                        if ($forecast) {

                            $arrUpdate = [];

                            if(isset($data['fc_ib_act'])) {
                                $arrUpdate['fc_ib_act'] = $data['fc_ib_act'];
                            }
                            if(isset($data['fc_ob_act'])) {
                                $arrUpdate['fc_ob_act'] = $data['fc_ob_act'];
                            }
                            if(isset($data['fc_st_act'])) {
                                $arrUpdate['fc_st_act'] = $data['fc_st_act'];
                            }

                            if (!empty($arrUpdate)) {
                                DB::table('forecast')->where([
                                    'fc_ind_id' => $data['fc_ind_id'],
                                    'cus_id' => $data['cus_id'],
                                    'whs_id' => $data['whs_id'],
                                    'fc_month' => $data['fc_month'],
                                    'fc_year' => $data['fc_year']
                                ])
                                    ->update($arrUpdate);
                            }

                        } else {
                            DB::table('forecast')->insert($data);
                        }
                    }
                }

                /*$dataDelete = DB::table('forecast')
                    ->select(DB::raw('SUM(fc_st_act) AS fc_st_act'), 'cus_id')
                    ->whereIn('cus_id', $this->customerIds)
                    ->groupBy('cus_id')
                    ->having('fc_st_act', 0)
                    ->get();
                if (!empty($dataDelete)) {
                    $cus_ids = array_pluck($dataDelete, 'cus_id', 'cus_id');
                    DB::table('forecast')
                        ->whereIn('cus_id', $cus_ids)
                        ->delete();
                }*/
            }

            DB::commit();

            $this->logSuccess("AWMS Update Weekly Forecast report successfully in ". date('Y-m'));

        } catch (\PDOException $e) {

            DB::rollback();
            $this->logPdo($e->getMessage());

        } catch (\Exception $e) {

            DB::rollback();
            $this->logPdo($e->getMessage());

        }

    }

    public function proccess($fc_ind_id, $data, $whsId, &$arrInserts, $month, $year)
    {
        foreach ($this->customerIds as  $awmsId => $cusId) {
            $key = $whsId . $cusId . $fc_ind_id;
            $arrInserts[$key] = [
                'whs_id' => $whsId,
                'cus_id' => $cusId,
                'fc_ind_id' => $fc_ind_id,
                'fc_ib' => 0,
                'fc_ib_act' => 0,
                'fc_ob' => 0,
                'fc_ob_act' => 0,
                'fc_st' => 0,
                'fc_st_act' => 0,
                'fc_month' => $month,
                'fc_year' => $year,
                'created_at' => time(),
                'created_by' => 1,
                'updated_at' => time(),
                'updated_by' => 1,
                'deleted_at' => 915148800,
                'deleted' => 0,
            ];

            foreach ($data as $item) {
                if ($awmsId == $item['awms_cus_id']) {
                    $arrInserts[$key]['fc_ib'] = $item['fc_ib_act'];
                    $arrInserts[$key]['fc_ob'] = $item['fc_ob_act'];
                    $arrInserts[$key]['fc_st'] = $item['fc_st_act'];

                    $arrInserts[$key]['fc_ib_act'] = $item['fc_ib_act'];
                    $arrInserts[$key]['fc_ob_act'] = $item['fc_ob_act'];
                    $arrInserts[$key]['fc_st_act'] = $item['fc_st_act'];

                }
            }
        }
    }
}
