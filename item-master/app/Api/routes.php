<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = ['trimInput'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
    $middleware[] = 'authorize';
    $middleware[] = 'setWarehouseTimezone';
}

// Handle application Route
$api->version('v1', function ($api) use ($middleware) {
    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) use ($middleware) {
        $api->group(['middleware' => $middleware], function($api) {
            // Item
            $api->get('/items', ['action' => "viewItem", 'uses' => 'ItemController@search']);
            $api->get('/items/{itemId}', ['action' => "viewItem", 'uses' => 'ItemController@show']);
            $api->get('/items/{itemId}/detail', ['action' => "viewItem", 'uses' => 'ItemController@getItemById']);
            $api->post('/items', ['action' => "createItem", 'uses' => 'ItemController@store']);
            $api->post('/items/{itemId}/approve', ['action' => "createItem", 'uses' => 'ItemController@approve']);
            $api->post('/items/approve-items', ['action' => "createItem", 'uses' => 'ItemController@approveItems']);

            $api->post('/items/{itemId}/upload-img', ['action' => "viewItem", 'uses' => 'ImageController@uploadItemImage']);

            $api->post('/items/check-items', ['action' => "viewItem", 'uses' => 'ItemController@check']);
            $api->put('/items/{itemId}', ['action' => "viewItem", 'uses' => 'ItemController@update']);
            $api->put('/items/update-description/{itemId}', ['action' => "viewItem", 'uses' => 'ItemController@updateDescription']);
            $api->delete('/items/{itemId}', ['action' => "deleteItem", 'uses' => 'ItemController@destroy']);
            $api->delete('/items', ['action' => "viewItem", 'uses' => 'ItemController@deleteMass']);
            $api->post('/items/import', ['action' => "createItem", 'uses' => 'ItemController@import']);
            $api->put('/items/{itemId}/delete', ['action' => "softDeleteItem", 'uses' => 'ItemController@deleteItem']);

            $api->get('/items/cus/{cusId:[0-9]+}', ['action' => "viewItem", 'uses' => 'ItemController@validateCusAndSku']);

            $api->get('/item-uom', ['action' => "viewItem", 'uses' => 'ItemController@getItemUom']);
            $api->get('/item-level', ['action' => "viewItem", 'uses' => 'ItemController@getItemLevel']);

            //item packing
            $api->post('/item-packing', ['action' => "createItem", 'uses' => 'ItemPackingController@create']);
            $api->get('/item-packing/{itemId:[0-9]+}', ['action' => "createItem", 'uses' => 'ItemPackingController@show']);
            $api->get('/item-export-excel', [
                'action' => "viewItem",
                'uses' => 'ItemController@exportToExcel'
            ]);


            $api->get('/item-charge-by', ['action' => 'viewItem', 'uses' => 'ItemController@getItemChargeBy']);

            $api->get('/sku-generate/{cusId:[0-9]+}', ['action' => "createItem", 'uses' => 'ItemController@generateSKU']);

            $api->get('/print-sku', ['action' => "viewItem", 'uses' => 'ItemController@printSKU']);

        });

        // Except Middleware
        $api->get('/view/{imgKey}', ['action' => "viewItem", 'uses' => 'ImageController@readItemImage']);
    });
});
