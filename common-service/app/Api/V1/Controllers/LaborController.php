<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 08-Aug-16
 * Time: 14:21
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\LaborModel;
use App\Api\V1\Validators\LaborValidator;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as IRequest;
use Seldat\Wms2\Models\Labor;
use Seldat\Wms2\Utils\Message;
use Psr\Http\Message\ServerRequestInterface as Request;
use Wms2\UserInfo\Data;

/**
 * Class SystemUomController
 *
 * @package App\Api\V1\Controllers
 */
class LaborController extends AbstractController
{

    protected $laborModel;

    /**
     * UserDepartmentController constructor.
     *
     * @param Request $request
     */
    public function __construct(IRequest $request, LaborModel $laborModel)
    {
        //parent::__construct($request, new AuthenticationService($request));

        $this->laborModel = $laborModel;
    }


    /**
     * @param $refId
     * @param $category
     * @param $whsId
     * @param $cusId
     * @param Request $request
     * @param LaborValidator $laborValidator
     *
     * @return array|void
     */
    public function store($refId, $category, $whsId, $cusId, IRequest $request, LaborValidator $laborValidator)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        $laborValidator->validate($input);
        try {
            if (!in_array($category, ['wo', 'op', 'rcv'])) {
                throw new \Exception($category . 'is not valid category!');
            }
            DB::beginTransaction();
            foreach ($input as $labor) {
                $expectedAmountInput = array_get($labor, 'expected_amount', null);
                $laborActualInput = array_get($labor, 'actual_amount', null);
                if (!empty($expectedAmountInput) && !empty($laborActualInput)) {
                    $laborExpected = $this->laborModel->firstOrNew([
                        'category' => $category,
                        'ref_id'   => $refId,
                        'type'     => 'expected',
                        'whs_id'   => $whsId,
                        'cus_id'   => $cusId,
                        'urgency'  => array_get($labor, 'urgency')
                    ]);

                    $laborExpected->amount = array_get($labor, 'expected_amount', null);

                    $laborExpected->save();

                    $laborActual = $this->laborModel->firstOrNew([
                        'category' => $category,
                        'ref_id'   => $refId,
                        'type'     => 'actual',
                        'whs_id'   => $whsId,
                        'cus_id'   => $cusId,
                        'urgency'  => array_get($labor, 'urgency')
                    ]);

                    $laborActual->amount = array_get($labor, 'actual_amount', null);
                    $laborActual->save();
                }
            }
            DB::commit();

            return ['data' => "Save labor succesfull!"];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $userDepartmentId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function show($refId, $category, $whsId, $cusId, Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {

            $laborByRef = $this->laborModel->findWhere([
                'category' => $category,
                'ref_id'   => $refId,
                'whs_id'   => $whsId,
                'cus_id'   => $cusId,
            ]);

            $result = [];
            if (count($laborByRef)) {
                $resLabor = ['urgency' => 'labor'];
                $resOvertime = ['urgency' => 'overtime'];
                $resRush = ['urgency' => 'rush'];
                foreach ($laborByRef as $labor) {
                    switch ($labor->urgency) {
                        case  'overtime' :
                            $resOvertime[$labor->type . '_amount'] = object_get($labor, 'amount', null);
                            break;
                        case  'rush' :
                            $resRush[$labor->type . '_amount'] = object_get($labor, 'amount', null);
                            break;
                        case  'labor' :
                            $resLabor[$labor->type . '_amount'] = object_get($labor, 'amount', null);
                            break;
                        default:
                            break;
                    }
                }
                $result[] = $resLabor;
                $result[] = $resOvertime;
                $result[] = $resRush;
            }

            return ["data" => $result];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function getLaborTracking($refId, $category, $whsId, $cusId, IRequest $request)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        try {
            $data = [];
            $laborTracking = [
                'expected_amount' => '',
                'actual_amount' => '',
                'urgency' => 'labor'
            ];
            $order = DB::table('odr_hdr')
                ->where('deleted', 0)
                ->where('whs_id', $whsId)
                ->where('cus_id', $cusId)
                ->where('odr_id', $refId)
                ->select(['odr_num', 'odr_sts', 'whs_id', 'cus_id'])
                ->first();

            if (! empty($order)) {
                $laborOrder = DB::table('rpt_labor_tracking')
                    ->where('cus_id', $order['cus_id'])
                    ->where('whs_id', $order['whs_id'])
                    ->where('owner', $order['odr_num'])
                    ->where('trans_num', $order['odr_num'])
                    ->whereNotNull('trans_dtl_id')
                    ->whereNotNull('start_time')
                    ->whereNotNull('end_time')
                    ->where('lt_type', 'OB')
                    ->first();

                if (! empty($laborOrder)) {
                    $laborTracking['actual_amount'] = number_format($laborOrder['trans_dtl_id'] / 3600, 1);
                }
            }
            $data[] = $laborTracking;
            return ['data' => $data];
        } catch (\Exception $e) {
            return $this->response()->errorBadRequest($e->getMessage());
        }
    }

    public function laborTracking($refId, $category, $whsId, $cusId, IRequest $request)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $input = $request->getParsedBody();
        try {
            $order = DB::table('odr_hdr')
                ->where('deleted', 0)
                ->where('whs_id', $whsId)
                ->where('cus_id', $cusId)
                ->where('odr_id', $refId)
                ->select(['odr_num', 'odr_sts', 'whs_id', 'cus_id'])
                ->first();

            if (empty($order)) {
                return ['data' => null];
            }
            if ($order['odr_sts'] == 'SH') {
                return $this->response()->errorBadRequest(sprintf('Cannot update. Order %s has been shipped', $order['odr_num']));
            }

            DB::beginTransaction();
            $endTime = time();
            foreach ($input as $labor) {
                if (array_get($labor, 'urgency', '') == 'labor') {
                    $trackingTime = array_get($labor, 'actual_amount', 0);
                    if ($trackingTime <= 0) {
                        return $this->response()->errorBadRequest('The labor time (hour) must be greater than zero');
                    }
                    $trackingTime = $this->roundUp($trackingTime);
                    $startTime = $endTime - ($trackingTime * 3600);
                    $workingTime = $endTime - $startTime;
                    $laborOrder = DB::table('rpt_labor_tracking')
                        ->where('cus_id', $order['cus_id'])
                        ->where('whs_id', $order['whs_id'])
                        ->where('owner', $order['odr_num'])
                        ->where('trans_num', $order['odr_num'])
                        ->whereNotNull('trans_dtl_id')
                        ->whereNotNull('start_time')
                        ->whereNotNull('end_time')
                        ->where('lt_type', 'OB')
                        ->first();

                    if (! empty($laborOrder)) {
                        DB::table('rpt_labor_tracking')
                            ->where('lt_id', $laborOrder['lt_id'])
                            ->update(['trans_dtl_id' => $workingTime, 'start_time' => $startTime, 'end_time' => $endTime]);
                    } else {
                        $laborTrackingData = [
                            'user_id' => Data::getCurrentUserId(),
                            'cus_id' => $order['cus_id'],
                            'whs_id' => $order['whs_id'],
                            'owner' => $order['odr_num'],
                            'trans_num' => $order['odr_num'],
                            'trans_dtl_id' => $workingTime,
                            'start_time' => $startTime,
                            'end_time' => $endTime,
                            'lt_type' => 'OB'
                        ];
                        DB::table('rpt_labor_tracking')->insert($laborTrackingData);
                    }
                }
            }
            DB::commit();
            return ['data' => "Save labor successfully!"];
        } catch (\PDOException $e) {
            DB::rollback();
            return $this->response()->errorBadRequest(SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response()->errorBadRequest($e->getMessage());
        }
    }

    private function roundUp($number)
    {
        $whole = floor(floatval($number));
        $fraction = $number - $whole;
        if ($fraction == 0) {
            return number_format($whole, 1);
        }
        if ($fraction <= 0.5) {
            $whole += 0.5;
        } else {
            $whole += 1;
        }
        return number_format($whole, 1);
    }
}
