<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Dingo\Api\Exception\UnknownVersionException;
use Seldat\Wms2\Utils\Message;

/**
 * Class OrderHdrModel
 *
 * @package App\Api\V1\Models
 */
class OrderHdrModel extends AbstractModel
{
    /**
     * OrderHdrModel constructor.
     *
     * @param OrderHdr|null $model
     */
    public function __construct(OrderHdr $model = null)
    {
        $this->model = ($model) ?: new OrderHdr();
    }

    /**
     * @param $orderHdrId
     *
     * @return mixed
     */
    public function deleteOrderHdr($orderHdrId)
    {
        return $this->model
            ->where('ord_hdr_id', $orderHdrId)
            ->delete();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $arrEqual = ['cus_id', 'odr_sts', 'odr_type'];

        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === "csr") {
                    $query->whereHas("csrUser", function ($q) use ($attributes) {
                        $q->where("first_name", 'like', "%" . SelStr::escapeLike($attributes['csr']) . "%");
                        $q->orWhere("last_name", 'like', "%" . SelStr::escapeLike($attributes['csr']) . "%");
                        $q->orWhere(
                            DB::raw("concat(first_name, ' ', last_name)"),
                            'like',
                            "%" . SelStr::escapeLike(str_replace("  ", " ", $attributes['csr'])) . "%"
                        );
                    });
                    continue;
                }
                if ($key === "odr_num") {
                    $query->where($key, "like", "%" . SelStr::escapeLike($value) . "%");
                    continue;
                }
                if (in_array($key, $arrEqual)) {
                    $query->where($key, SelStr::escapeLike($value));
                }
            }
        }

        $this->sortBuilder($query, $attributes);

        $this->model->filterData($query, true);

        $models = $query->paginate($limit);

        return $models;
    }

    public function getOrderNum()
    {
        $order = $this->model
            ->where('odr_num', 'like', 'ORD-' . (date("ym", time())) . "-%")
            ->orderBy('odr_id', 'desc')
            ->first();
        $index = 0;
        $orderNum = object_get($order, "odr_num", null);

        if ($orderNum) {
            $temp = explode("-", $orderNum);
            $index = (int)end($temp);
        }

        return 'ORD-' . (date("ym", time())) . "-" . str_pad(++$index, 5, "0", STR_PAD_LEFT);
    }

    /**
     * @param $orderIds
     *
     * @return mixed
     */
    public function checkWhereIn($orderIds)
    {
        return $this->model
            ->whereIn('odr_id', $orderIds)
            ->count();
    }

    /**
     * @param $order_ids
     *
     * @return mixed
     */
    public function getOrderById($order_ids)
    {
        $order_ids = is_array($order_ids) ? $order_ids : [$order_ids];

        $rows = $this->model
            ->whereIn('odr_id', $order_ids)
            ->get();

        return $rows;
    }

    /**
     * @param $orderIds
     *
     * @return mixed
     */
    public function findWhereIn(array $orderIds, $with = [])
    {
        $query = $this->make($with);
        $model = $query->whereIn('odr_id', $orderIds)->get();
        return $model;
    }

    /**
     * @param $odr_hdr_id
     */
    public function undoOdrHdrStatusToPicked($odr_hdr_id)
    {
        return $this->model->where('odr_id', $odr_hdr_id)->update(['odr_sts' => "PD"]);
    }
}
