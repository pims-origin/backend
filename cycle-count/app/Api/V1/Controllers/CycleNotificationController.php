<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CycleCountNotificationModel;
use App\Api\V1\Traits\CycleHdrControllerTrait;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Api\V1\Validators\CycleCountNotificationValidator;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Api\V1\Transformers\CycleCountNotificationTransformer;
use App\Api\V1\Transformers\CompleteLocationTransformer;
use App\Api\V1\Transformers\AddCCNLocationAutoComTransformer;
use Seldat\Wms2\Utils\SystemBug;
use Illuminate\Support\Collection;
use App\Api\V1\Transformers\CCNStatusTransformer;
use App\Api\V1\Validators\CycleHdrValidator;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\CycleHdrModel;
use App\Api\V1\Models\CartonModel;
use App\Api\V1\Transformers\CycleHdrTransformer;
use Illuminate\Http\Response as IlluminateResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CycleNotificationController extends AbstractController
{

    use CycleHdrControllerTrait;

    /**
     * @var CycleCountNotificationModel
     */
    protected $model;

    protected $cartonModel;

    /**
     * @var CycleCountNotificationValidator
     */
    protected $validator;

    /**
     * @var $transform
     */
    protected $transform;

    /**
     * @var CycleCountNotificationValidator
     */
    protected $cycleCountNotificationTransformer;

    /**
     * CycleNotificationController constructor.
     *
     * @param CycleCountNotificationModel $model
     * @param CycleCountNotificationValidator $validator
     * @param CycleCountNotificationTransformer $cycleCountNotificationTransformer
     * @param CartonModel $cartonModel
     */
    public function __construct(
        CycleCountNotificationModel $model,
        CycleCountNotificationValidator $validator,
        CycleCountNotificationTransformer $cycleCountNotificationTransformer,
        CartonModel $cartonModel
    ) {
        $this->model = $model;
        $this->validator = $validator;
        $this->transform = new CycleCountNotificationTransformer();
        $this->cycleCountNotificationTransformer = $cycleCountNotificationTransformer;
        $this->cartonModel = $cartonModel;
    }

    /**
     * @param Request $request
     * @param null $ccnID
     * @param $whsId
     *
     * @return \Dingo\Api\Http\Response|void
     * @throws \Exception
     */
    public function store(Request $request, $ccnID = null, $whsId)
    {
        $input = $request->getParsedBody();

        if ($ccnID) {
            $this->validator->validate($input);
            $locCodeArr = [$input['loc_code']];
            $inputArr = [$input];

            $model = $this->model->getFirstBy('ccn_id', $ccnID);
            if (!$model) {
                throw new \Exception("Cycle Count Notification is not existed.");
            }
            if ($model->cc_ntf_sts != 'NW') {
                throw new \Exception("Cycle Count Notification status is not New");
            }
            $locCode_up = $model->loc_code;
            $sku_up = $model->sku;
        } else {
            foreach ($input as $key => $value) {
                $value['cc_ntf_sts'] = CycleCountNotificationModel::CCN_STATUS_NEW;
                $input[$key] = $value;
                $this->validator->validate($value);
            }
            $locCodeArr = array_column($input, 'loc_code');
            $inputArr = $input;
            $locCode_up = null;
            $sku_up = null;
        }

        if (true !== ($locNotBelong = $this->model->checkWhsLocationBelong($whsId, $locCodeArr))) {
            $locStr = implode(', ', $locNotBelong);

            throw new HttpException(500, "Location {$locStr} does not belong to the current warehouse.");
        }

        // //Check create CCN with SKU not existed in that location
        // $dupLoc = '';
        // $dupSku = '';
        // foreach ($inputArr as $data) {
        //     if ($data['loc_code'] != $locCode_up || $data['sku'] != $sku_up) {
        //         $locHaveSku = $this->cartonModel->checkLocationNotSku($whsId, $data['loc_code'], $data['sku'], $ccnID,
        //             $locCode_up, $sku_up);
        //         if ($locHaveSku) {
        //             $dupLoc .= '  ' . $data['loc_code'];
        //             $dupSku .= '  ' . $data['sku'];
        //         }
        //     }
        // }

        // if ($dupLoc) {
        //     throw new HttpException(500,
        //         "The locations: {$dupLoc} are existed with respective SKUs: $dupSku.");
        // }

        //check duplicate location
        $duplicateInfo = '';
        foreach ($inputArr as $data) {
            $duplicateLocInfo = $this->model->checkCcIdDuplication($whsId, $data, $ccnID);
            if ($duplicateLocInfo) {
                $duplicateInfo .= '  ' . $data['loc_code'] . ':' . $data['item_id'] . ':' . $data['lot'] . ':' .
                    $data['pack'] . ':' . $data['remain_qty'];
            }
        }

        $CCNStatus=array_get($input, 'cc_ntf_sts', '');
        if ($duplicateInfo && strtoupper($CCNStatus) != "CE") {
            throw new HttpException(500,
                "Cycle Count Notification duplicated, (Location:Item:Lot:Pack:RemainQty): {$duplicateInfo}.");
        }
        //if (false !== ($ccnExist = $this->model->checkCCNDuplication($whsId, $locCodeArr, $ccnID))) {
        //    $ccnArr = $ccnExist->pluck('loc_code')->all();
        //    $listDup = implode(', ', array_unique($ccnArr));
        //
        //    throw new HttpException(500, "Cycle Count Notification duplicated, locations: {$listDup}.");
        //}

        //check duplicated inputting data
        $this->checkInputtingDataDup($inputArr);

        try {
            DB::beginTransaction();

            $data = $this->model->processCCN($whsId, $input, $ccnID);

            DB::commit();

            return $this->response->item($data, new CycleCountNotificationTransformer());
        } catch (\PDOException $e) {
            DB::rollBack();

            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, "Store_CCN", __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('There is some thing wrong, please contact admin !');
    }

    /**
     * Check duplication loc_code
     *
     * @param type $input
     *
     * @throws \Exception
     */
    public function checkDupLocCode($input)
    {
        $locCodeArr = array_column($input, 'loc_code');
        $locCodeArr2 = array_flip($locCodeArr);
        if (count($locCodeArr) != count($locCodeArr2)) {
            $count = array_count_values($locCodeArr);
            $dupArr = [];
            foreach ($count as $locCode => $num) {
                if ($num > 1) {
                    $dupArr[] = $locCode;
                }
            }
            $dupString = implode(', ', $dupArr);

            throw new \Exception("Duplicate loc_code: {$dupString}");

        }
    }

    /**
     * @param $input
     *
     * @throws \Exception
     */
    public function checkInputtingDataDup($input)
    {
        $checkArray = [];
        foreach ($input as $data) {
            $string = $data['loc_code'] . ':' . $data['item_id'] . ':' . $data['lot'] . ':' . $data['pack'] . ':' .
                $data['remain_qty'];
            array_push($checkArray, $string);

        }
        $locCodeArr = $checkArray;
        $locCodeArr2 = array_flip($checkArray);

        if (count($locCodeArr) != count($locCodeArr2)) {
            $count = array_count_values($locCodeArr);
            $dupArr = [];
            foreach ($count as $locCode => $num) {
                if ($num > 1) {
                    $dupArr[] = $locCode;
                }
            }
            $dupString = implode(', ', $dupArr);

            throw new \Exception("Your inputting duplicated (Location:Item:Lot:Pack:RemainQty): {$dupString}");

        }
    }

    /**
     * @return mixed
     */
    public function index_bk(Request $request, $whsId)
    {
        $input = $request->getQueryParams();

        try {
            $limit = array_get($input, 'limit', 10);
            $input['cc-ntf-sts'] = array_get($input, 'cc-ntf-sts', 'NW');
            if ($input['cc-ntf-sts'] == 'ALL') {
                unset($input['cc-ntf-sts']);
            }
            $complete = array_get($input, 'com-loc', null);
            $data = $this->model->search($whsId, $input, [], $limit, $complete);

            $transformer = new CycleCountNotificationTransformer();
            if ($complete) {
                $transformer = new CompleteLocationTransformer();
            }

            return $this->response->paginator($data, $transformer);
        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }

    /**
     * @param $whs_id
     * @param Request $request
     * @param CycleCountNotificationTransformer $cycleCountNotificationTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function getAutocompleteLocations(
        $whs_id,
        Request $request,
        CycleCountNotificationTransformer $cycleCountNotificationTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();
                
        try {
            $limit = array_get($input, 'limit', 20);
            $cycleCountNotificationInfo = $this->model->getAutocompleteLocations($whs_id, $input, [], $limit);            

            return $this->response->paginator($cycleCountNotificationInfo, $cycleCountNotificationTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function index(
        $whs_id,
        Request $request,
        CycleCountNotificationTransformer $cycleCountNotificationTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        if (!empty($input['export']) && $input['export'] == 1) {
            $this->export($whs_id, $input);
            die;
        }
        //$input['gr_sts'] = 'RE';
        try {
            $limit = array_get($input, 'limit', 20);
            $cycleCountNotificationInfo = $this->model->search($whs_id, $input, [], $limit);            

            return $this->response->paginator($cycleCountNotificationInfo, $cycleCountNotificationTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $ccnId
     *
     * @return string
     */
    public function show($ccnId)
    {
        try {
            $data = $this->model->findDetail($ccnId);

            return $this->response->item($data, new CycleCountNotificationTransformer());
        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }

    /**
     * Auto complete on add Cycle Count Notification
     *
     * @param Request $request
     * @param Integer $whsId
     *
     * @return Response
     */
    public function completeLocAddCCN(Request $request, $whsId)
    {
        $input = $request->getQueryParams();
        try {
            $keyword = array_get($input, 'keyword');
            $limit = array_get($input, 'limit', 20);
            $data = $this->model->completeLocationAddCCN($whsId, $keyword, $limit);

            //return $this->response->collection($data, new AddCCNLocationAutoComTransformer());
            return $this->response->paginator($data, new AddCCNLocationAutoComTransformer());
        } catch (Exception $ex) {

            return $this->response->errorBadRequest($ex->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param $whsId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function autoCompleteLocationAddCCN(Request $request, $whsId)
    {
        $input = $request->getQueryParams();
        try {
            $limit = array_get($input, 'limit', 20);
            $data = $this->model->autoCompleteLocationAddCCN($whsId, $input, $limit);

            //return $this->response->paginator($data, new AddCCNLocationAutoComTransformer());
            return ['data' => $data];

        } catch (Exception $ex) {

            return $this->response->errorBadRequest($ex->getMessage());
        }
    }

    /**
     * Delete on or multi Cycle Count Notification
     *
     * @param Request $request
     * @param int $whsId
     * @param int $ccnID
     *
     * @return Response
     */
    public function destroy(Request $request, $whsId, $ccnID)
    {
        try {
            $item = $this->model->findDetail($ccnID);
            if (!$item) {
                return $this->response->errorBadRequest("Not found Cycle Count Notification {$ccnID}");
            }
            $result = $this->model->deleteCycleCountNotification([$ccnID]);
            if (!$result) {
                return $this->response->errorBadRequest("Delete Cycle Count Notification {$ccnID} failed");
            }

            return $this->response->item($item, new CycleCountNotificationTransformer());
        } catch (Exception $ex) {

        }
    }

    /**
     * Return status
     *
     * @return response
     */
    public function status()
    {

        try {
            $coll = new Collection();
            $status = $this->model->getStatusList();
            foreach ($status as $code => $value) {
                $obj = new \stdClass();
                $obj->code = $code;
                $obj->name = $value;
                $coll->push($obj);
            }

            return $this->response->collection($coll, new CCNStatusTransformer());
        } catch (Exception $ex) {

            return $this->response->errorBadRequest($ex->getMessage());
        }
    }

    /**
     * Create Cycle Count By Array Location
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addListCycleCount(Request $request)
    {
        /**
         * {
         * "whs_id":"1",
         * "cycle_name":"TestWap",
         * "cycle_assign_to":"19",
         * "cycle_due_date":"05/15/2017",
         * "cycle_des":"",
         * "cycle_type":"LC",
         * "cycle_detail":"001-A-L1-01, 001-A-L1-03",
         * "cycle_has_color_size":true,
         * "cycle_count_by":"carton",
         * "cycle_method":"paper"
         * }
         */
        try {
            $input = $request->getParsedBody();

            $cycleHdrValidate = new CycleHdrValidator();
            $cycleHdrValidate->validate($input);

            //generate event code
            $evtTracking = new EventTrackingModel();
            $cycleNumAndSeq = $evtTracking->generateEventTrackingNum();
            $input['cycle_num'] = $cycleNumAndSeq['cycle_num'];

            DB::beginTransaction();
            //Create Cycle_hdr and Cycle_dtl
            $cycleHdr = new CycleHdrModel();
            $data = $cycleHdr->processCreateCycleHdrForListLocation($input,$fromCcn = true);

            if (!$data) {
                $errorMsg = implode($cycleHdr->errors);
                DB::rollback();

                return $this->response->error($errorMsg, IlluminateResponse::HTTP_NOT_ACCEPTABLE);
            }

            //change status CCN to CC
            $this->model->updateCCNStatusByCCID($input['ccn_ids'],
                CycleCountNotificationModel::CCN_STATUS_CYCLE_COUNT);

            //event tracking
            $trans_num = str_replace(CYCLE_PREFIX, EventTrackingModel::CYCLE_CREATED_ASSIGNED,
                $cycleNumAndSeq['cycle_num']);
            $evtTracking->create([
                'whs_id'    => $data['whs_id'],
                'cus_id'    => ucfirst($input['cycle_type']) == 'CS' ? $input['cycle_detail'] : null,
                'owner'     => $cycleNumAndSeq['cycle_num'],
                'evt_code'  => EventTrackingModel::CYCLE_CREATED_ASSIGNED,
                'trans_num' => $trans_num,
                'info'      => $cycleNumAndSeq['cycle_num'] . ' ' .
                    EventTrackingModel::$arrEvtCycle[EventTrackingModel::CYCLE_CREATED_ASSIGNED]
            ]);

            DB::commit();

            return $this->response->item($data, new CycleHdrTransformer());
        } catch (Exception $ex) {
            DB::rollback();

            return $this->response->errorBadRequest($ex->getMessage());
        }
    }

    /**
     * @param $whs_id
     * @param Request $request
     *
     * @return array|void
     */
    public function skuCcnInventoryComplete(
        $whs_id,
        Request $request
    ) {
        $input = $request->getQueryParams();
        try {
            $skuCcnInventorys = $this->model->skuCcnInventoryComplete($whs_id, $input, array_get($input, 'limit'));

            return ['data' => $skuCcnInventorys];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    /**
     * @param Request $request
     *
     * @return array|void
     */
    public function getReasonDropDown(
        Request $request
    ) {
        $input = $request->getQueryParams();
        try {
            $reasonDropDownInfo = $this->model->getReasonDropDown($input, array_get($input, 'limit'));

            return ['data' => $reasonDropDownInfo];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

}
