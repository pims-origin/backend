<?php

namespace App\Api\V2\Outbound\WavepickWithoutRfid\UpdateWavepick\Models;


use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Utils\SelStr;


class EventTrackingModel extends AbstractModel
{
    protected $model;

    /**
     * @param EventTracking $model
     */
    public function __construct(EventTracking $model)
    {
        $this->model = $model;
    }
}
