<?php

namespace App\Api\Shipping\Controllers;

use App\Api\Shipping\Models\LocationModel;
use App\Api\Shipping\Models\OrderHdrModel;
use App\Api\V1\Models\PackHdrModel;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;

use Seldat\Wms2\Models\OrderHdr;


/**
 * Class SugMoreLocShippingLaneController
 *
 * @package App\Api\V1\Controllers
 */
class SugMoreLocShippingLaneController extends AbstractController
{

    /**
     * @var locationModel
     */
    protected $locationModel;

    /**
     * SugMoreLocShippingLaneController constructor.
     */
    public function __construct()
    {
        $this->locationModel = new LocationModel();
    }

    public function moreSuggestShippingLocation($whsId, Request $request)
    {
        $input = $request->getQueryParams();

        $odrNum = $input['odr_num'] ?? null;

        if (empty($odrNum)) {
            $this->response->errorBadRequest("Order Number is required!");
        }

        $odrHdr = OrderHdr::where('odr_num', $odrNum)->first();

        if (empty($odrHdr)) {
            $this->response->errorBadRequest("Order is not existed");
        }

        $odrType = $odrHdr->odr_type;

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        try {

            $queryRaw = "not exists (select * from pallet where pallet.loc_id = location.loc_id)";

            $query = $this->locationModel->getModel()
                ->join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
                ->where('loc_type.loc_type_code', 'SHP')
                ->where('location.loc_sts_code', 'AC')
                ->where('location.loc_whs_id', $whsId)
                ->whereNotNull('location.loc_zone_id')
                ->select([
                    'location.loc_id',
                    'location.loc_code',
                ])
                ->limit(1);

            if ($odrType == 'PKU') {
                $query->whereRaw("
                    SUBSTRING_INDEX(location.loc_code, '-', -2) LIKE '01-%'
                ");
            } else {
                $query->whereRaw("
                    SUBSTRING_INDEX(location.loc_code, '-', -2) NOT LIKE '01-%'
                ")
                    ->whereRaw($queryRaw)
                ;
            }

            return ['data' => $query->get()];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function suggestShippingLocation($whsId, Request $request)
    {
        $input = $request->getQueryParams();
        $out_side_box = $input['out_side_box'] ?? null;
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        //when have outside box in table will apply code under
        $odrNum = '';
        if($out_side_box != null) {
            $packHdrModel = new PackHdrModel();
            $dataPack = $packHdrModel->getModel()->where('out_side_box',$out_side_box)->first();
            $odrHdrId = array_get($dataPack, 'odr_hdr_id', null);
            if($odrHdrId != null){
                $odrHdrModel = new OrderHdrModel();
                $dataOdrHdr = $odrHdrModel->getModel()->where('odr_id',$odrHdrId)->first();
                $odrNum = array_get($dataOdrHdr, 'odr_num', null);
            }
        }
        if (empty($odrNum)) {
            $this->response->errorBadRequest("Order Number is required!");
        }

        $odrHdr = OrderHdr::where('odr_num', $odrNum)->first();

        if (empty($odrHdr)) {
            $this->response->errorBadRequest("Order is not existed");
        }

        $odrType = $odrHdr->odr_type;
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        try {

            $queryRaw = "not exists (select * from pallet where pallet.loc_id = location.loc_id)";

            $query = $this->locationModel->getModel()
                ->join('loc_type', 'location.loc_type_id', '=', 'loc_type.loc_type_id')
                ->where('loc_type.loc_type_code', 'SHP')
                ->where('location.loc_sts_code', 'AC')
                ->where('location.loc_whs_id', $whsId)
                ->whereNotNull('location.loc_zone_id')
                ->select([
                    'location.loc_id',
                    'location.loc_code',
                ])
                ->limit(1);

            if ($odrType == 'PKU') {
                $query->whereRaw("
                    SUBSTRING_INDEX(location.loc_code, '-', -2) LIKE '01-%'
                ");
            } else {
                $query->whereRaw("
                    SUBSTRING_INDEX(location.loc_code, '-', -2) NOT LIKE '01-%'
                ")
                    ->whereRaw($queryRaw)
                ;
            }

            return ['data' => $query->get()];

        } catch (\PDOException $e) {

            return $this->response->errorBadRequest($e->getMessage());

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }
}

