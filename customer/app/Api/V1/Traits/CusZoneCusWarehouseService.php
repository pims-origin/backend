<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 8/10/2016
 * Time: 3:35 PM
 */

namespace App\Api\V1\Traits;

trait CusZoneCusWarehouseService
{
    protected function _update(array $data, $getBy = 'whs_id')
    {
        $model = $this->model->firstOrNew([
            'cus_id' => $data['cus_id'],
            $getBy   => $data[$getBy],
        ]);


        $model->fill($data);

        if ($model->save()) {
            return $model;
        }

        return $this->getCustomerWarehouse($data['cus_id'], $data['whs_id']);
    }

    public function getByCustomerId($customerId)
    {
        return $this->allBy('cus_id', $customerId);
    }
}