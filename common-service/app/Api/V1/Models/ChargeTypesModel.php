<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\ChargeType;

class ChargeTypesModel extends AbstractModel
{
    public function __construct(ChargeType $model = null)
    {
        $this->model = ($model) ?: new ChargeType();
    }

    /*
    ****************************************************************************
    */

}
