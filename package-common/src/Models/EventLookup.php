<?php

namespace Seldat\Wms2\Models;

class EventLookup extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'evt_lookup';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'evt_code';

    public $incrementing = false;

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function indicator()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Indicator', 'trans', 'ind_trans');
    }
    public function event() {
        return $this->hasMany(__NAMESPACE__ . '\EventTracking', 'evt_code', 'evt_code');
    }



}
