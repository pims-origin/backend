<?php

namespace App\Api\V2\BillOfLadings\PrintBOL\Controllers;

use Laravel\Lumen\Routing\Controller;
use Dingo\Api\Routing\Helpers;
use Wms2\UserInfo\Data;

/**
 * Class BaseController
 * @package App\Api\V1\Controllers
 *
 * @SWG\Swagger(
 *     schemes={"http","https"},
 *     host="api.domain",
 *     basePath="/",
 *     definitions={},
 *     @SWG\Info(
 *         version="WMS 2.0.0",
 *         title="Your API Functional Name",
 *         description="Description for APIs",
 *         termsOfService="",
 *         @SWG\Contact(
 *             email="your@email.address"
 *         ),
 *         @SWG\License(
 *             name="WMS2.0",
 *             url="seldatinc.com"
 *         )
 *     ),
 *     @SWG\Definition(
 *         definition="data",
 *         type="object",
 *         properties={
 *             @SWG\Property(property="menu_group_id", type="integer"),
 *             @SWG\Property(property="name", type="string"),
 *             @SWG\Property(property="description", type="string"),
 *         },
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="",
 *         url=""
 *     )
 * )
 */
abstract class AbstractController extends Controller
{
    use Helpers;

    protected final function chkWhsAndCus($whsId, $cusId)
    {
        if (!Data::isAccessWhsAndCus($cusId, $whsId)) {
            $msg = sprintf("You cannot access Warehouse %d and Customer %d! Please contact with admin",
                $cusId, $cusId);
            return $this->response->errorUnauthorized($msg);
        }
    }
}
