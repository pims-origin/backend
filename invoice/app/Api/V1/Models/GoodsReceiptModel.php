<?php

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;

use Seldat\Wms2\Models\GoodsReceipt;

use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class GoodsReceiptModel extends AbstractModel
{
    /**
    * @var
    */
    
     protected $container;
    /**
     * Constructor.
    */

    public function __construct()
    {
        $this->model = new GoodsReceipt();
    }
    
     /**
     * @recNums array
     * return mixed
    */

    public function getContainerReceivedDate($params)
    {
        $recNums = $params;
        
        if (getDefault($params['items'])) {
            $recNums = explode(',', $params['items']);
        }

        $results = $this->model
                        ->join('evt_tracking AS et', 'et.trans_num', '=', 'gr_hdr.gr_hdr_num')
                        ->join('asn_hdr AS ah', 'ah.asn_hdr_id', '=', 'gr_hdr.asn_hdr_id')
                        ->join('customer AS cs', 'gr_hdr.cus_id', '=', 'cs.cus_id')
                        ->select('gr_hdr.gr_hdr_id', 
                                  'cus_name',
                                  'gr_hdr.ctnr_id',
                                  'gr_hdr.ctnr_num', 
                                  'asn_hdr_ref', 
                                  'sys_mea_code',
                                  DB::raw('FROM_UNIXTIME(et.created_at, "%Y-%m-%d") AS created')
                                )
                        ->where('evt_code', config('constants.event.GR-COMPLETE'))
                        ->whereIn('gr_hdr.gr_hdr_id', $recNums)
                        ->groupBy('gr_hdr.gr_hdr_id');
    
        return $results;
        
    }
    
    /**
     * @attributes with array
     * return mixed
    */
    
    public function viewDetail($attributes = [], $limit = PAGING_LIMIT)
    {
        $query = $this->getContainerReceivedDate($attributes);

        $searchAttr = SelArr::removeNullOrEmptyString($attributes);

        $searchLike = ['ctnr_num'];
        $searchEqual = [];
        
        foreach ($searchAttr as $key => $value) {
            if (in_array($key, $searchLike)) {
                $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                continue;
            } 
            if (in_array($key, $searchEqual)) {
                $query->where($$key, $value);
            }
        }

        $this->sortBuilder($query, $searchAttr);

        $models = $query->paginate($limit);

        return $models;
   }
   
   /**
     * @recNum array
     * return mixed
    */

    public function getContainerData($recNum)
    {
        $return = [];
        
        $result = $this->model
                        ->join('asn_hdr AS ah', 'ah.asn_hdr_id', '=', 'gr_hdr.asn_hdr_id')
                        ->where('gr_hdr_id', $recNum)
                        ->select('ctnr_num AS containerName', 'ah.asn_hdr_ref AS referenceCode')
                        ->get();
        foreach ($result as $row) {
            $return['containerName'] = $row->containerName;
            $return['referenceCode'] = $row->referenceCode;
        }  
        
        return $return;
    }
}

