<?php
namespace backend\controllers;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Yii;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\UnauthorizedHttpException;

class AuthorizeController extends Controller
{

    /**
     * @return array
     * @throws UnauthorizedHttpException
     */
    public function actionCheck()
    {
        $input = Yii::$app->request->post();
        $permission = isset($input['permission']) ? $input['permission'] : '';
        $data = isset($input['data']) ? $input['data'] : [];
        $params['obj'] = $data;

        $userId = $this->getUserIdByToken();

        //check Supper Admin user ID = 1
        if($userId == 1){
            return [
                'data' => [
                    'permission' => true,
                    'user' => $userId
                ]
            ];
        }

        if (! $userId) {
            throw new UnauthorizedHttpException('Can not found this user');
        }

        $result = ($userId && $permission) ? Yii::$app->authManager->checkAccess($userId, $permission, $params) : false;
        return [
            'data' => [
                'permission' => $result,
                'user' => $userId
            ]
        ];
    }

    /**
     * Get UserID from token
     *
     * @return bool
     */
    protected function getUserIdByToken()
    {
        $url = Yii::$app->params['authentication_api'] . 'users/token';

        $requestHeader = Yii::$app->request->headers;

        $token = $requestHeader->get('authorization');

        $option = [
            'headers' => [
                'Authorization' => $token,
            ]
        ];

        $response = Yii::$app->httpclient->post($url, [], $option, false);

        if ($response->getStatusCode() == 200){
            return Yii::$app->httpclient->formatResponse($response)['data']['user_id'];
        }

        return false;
    }

    /**
     * @return array
     */
    public function actionMenu()
    {
        $userId = $this->getUserIdByToken();
        if (! $userId) {
            return [
                'data' => [
                    'permission' => $userId
                ]
            ];
        }
        $menuByUser = Yii::$app->authManager->getMenuPermissionsByUser($userId);
        $result = array_keys($menuByUser);
        return ['data' => $result];
    }

    /**
     * @return array
     */
    public function actionAssignRoleToUser()
    {
        $auth = Yii::$app->authManager;
        $input = Yii::$app->request->post();
        $role = $auth->getRole($input['role']);

        if ( $auth->getAssignment($input['role'], $input['user_id']) ) {
            throw new BadRequestHttpException('Role has assigned to user');
        }

        Yii::$app->authManager->assign($role, $input['user_id']);
        return ['data' => ['message' => 'OK']];
    }
}
