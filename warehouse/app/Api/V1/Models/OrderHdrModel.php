<?php
/**
 * Created by PhpStorm.
 *
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\OrderHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;
use App\Api\V1\Models\WarehouseMetaModel;


/**
 * Class OrderHdrModel
 *
 * @package App\Api\V1\Models
 */
class OrderHdrModel extends AbstractModel
{

    const DAS_LIMIT = 0;
    const REP_LIMIT = 1;

    protected $statusType = [
        'csr'               => ['NW'],
        'allocate'          => ['NW', 'AL', 'PAL'],
        'wave-pick'         => ['PK', 'PPK', 'PD', 'PIP', 'AL', 'PAL'],
        'packing'           => ['PD', 'PIP', 'PN', 'PPA', 'PA', 'PAP'],
        'pallet-assignment' => ['PA', 'PAP'],
        'shipping'          => ['SH', 'ST', 'PSH'],
        'cancel-order'      => ['CC']
    ];

    /**
     * @param OrderHdr $model
     */
    public function __construct(OrderHdr $model = null)
    {
        $this->model = ($model) ?: new OrderHdr();
        $this->warehouseMetaModel = new WarehouseMetaModel();

    }

    /**
     * @param $warehouseId
     * @param $attributes
     * @param array $with
     * @param $limit
     *
     * @return mixed
     */
    public function shippingReport($warehouseId, $attributes, array $with, $limit)
    {
        //  Limit
        $limitDay = (!empty($attributes['day']) && is_numeric($attributes['day']))
            ? $attributes['day'] : self::REP_LIMIT;

        $query = $this->make($with);

        $query->where('whs_id', $warehouseId);
        $query->where('created_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limitDay . ' DAY)'));

        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);

    }

    /**
     * @param $warehouseId
     * @param $attributes
     * @param array $with
     * @param $limit
     *
     * @return mixed
     */
    public function orderReport($warehouseId, $attributes, array $with, $limit)
    {
        //  Limit
        $limitDay = (!empty($attributes['day']) && is_numeric($attributes['day']))
            ? $attributes['day'] : self::REP_LIMIT;

        $query = $this->make($with);

        $query->where('whs_id', $warehouseId);

        if(!empty($attributes['odr_sts'])){
            $query->where('odr_sts', $attributes['odr_sts']);
        }

        if(!empty($attributes['cus_id'])){
            $query->where('cus_id', $attributes['cus_id']);
        }

        $query->where('created_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limitDay . ' DAY)'));

        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);

    }


    /**
     * @return mixed
     */
    public function orderStatus()
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $resuls = DB::table('odr_hdr')
            ->select('odr_sts')
            ->distinct('odr_sts')
            ->get();

        $rows = [];

        foreach ($resuls as $resul){
            $rows[] = $resul['odr_sts'];
        }

        return $rows;
    }

    /**
     * @return mixed
     */
    public function odrDashboard($input = [])
    {
        $odrSts = [
            'NW'  => [Status::getByValue("New", "Order-status")],
            'IP'  => [
                Status::getByValue("Allocated", "Order-status"),
                Status::getByValue("Partial Allocated", "Order-status"),
                Status::getByValue("Picking", "Order-status"),
                Status::getByValue("Partial Picking", "Order-status"),
                Status::getByValue("Picked", "Order-status"),
                Status::getByValue("Partial Picked", "Order-status"),
                Status::getByValue("Packing", "Order-status"),
                Status::getByValue("Partial Packing", "Order-status"),
                Status::getByValue("Packed", "Order-status"),
                Status::getByValue("Partial Packed", "Order-status")
            ],
            'OS'  => [
                Status::getByValue("Staging", "Order-status"),
                Status::getByValue("Partial Staging", "Order-status")
            ],
            'BAC' => [Status::getByValue("BackOrder", "Order-type")],
            'FN'  => [Status::getByValue("Final", "SHIP-STATUS")]
        ];

        foreach ($odrSts as $key => $item) {
            $item = is_array($item) ? $item : [$item];
            $odrSts[$key] = "'" . implode("', '", $item) . "'";
        }

        //  Limit
        $limit = (!empty($input['day']) && is_numeric($input['day']))
            ? $input['day'] : self::DAS_LIMIT;

        $query = DB::table('odr_hdr as odr')
            ->select(DB::raw("
                COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['NW']}) THEN 1 ELSE 0 END), 0) AS newOrders,
	            COALESCE( SUM(CASE WHEN odr.odr_sts IN ({$odrSts['IP']}) THEN 1 
		            WHEN (odr.odr_sts IN ({$odrSts['OS']}) 
		                AND shipment.`ship_sts` NOT IN ({$odrSts['FN']})) THEN 1 
		            ELSE 0 END), 0) AS inprocessOrders,
	            COALESCE( SUM(CASE WHEN (odr.odr_sts IN ({$odrSts['OS']}) 
	                    AND shipment.`ship_sts` IN ({$odrSts['FN']})) THEN 1 
	                ELSE 0 END), 0) AS orderReadyToShip,
	            COALESCE( SUM(CASE WHEN odr.odr_type IN ({$odrSts['BAC']}) 
	 	            THEN 1 ELSE 0 END), 0) AS backOrders
            "))
            ->leftJoin('shipment', 'odr.ship_id', '=', 'shipment.ship_id')
            ->where('odr.deleted', 0)
            ->where('odr.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'));

        $predis = new Data();
        $userInfo = $predis->getUserInfo();

        $currentWH = array_get($userInfo, 'current_whs', 0);
        $query->where('odr.whs_id', $currentWH);

        $cus_ids = $predis->getCustomersByWhs($currentWH);
        $query->whereIn('odr.cus_id', $cus_ids);

        $userId =  array_get($userInfo, 'user_id', 0);
        $query->where(function ($query) use ($userId) {
            $query->where('odr.created_by', $userId)
                ->orWhere('odr.updated_by', $userId);
        });

        $model = $query->get();

        return $model;
    }

    /**
     * @return mixed
     */
    public function inprocessDashboard($input = [])
    {
        $odrSts = [
            'AL' => [
                Status::getByValue("Allocated", "Order-status"),
                Status::getByValue("Partial Allocated", "Order-status")
            ],
            'PK' => [
                Status::getByValue("Picked", "Order-status"),
                Status::getByValue("Partial Picked", "Order-status"),
                Status::getByValue("Picking", "Order-status"),
                Status::getByValue("Partial Picking", "Order-status")
            ],
            'PN' => [
                Status::getByValue("Packing", "Order-status"),
                Status::getByValue("Partial Packing", "Order-status"),
                Status::getByValue("Packed", "Order-status"),
                Status::getByValue("Partial Packed", "Order-status")
            ],
            'ST' => [
                Status::getByValue("Staging", "Order-status"),
                Status::getByValue("Partial Staging", "Order-status")
            ],
            'FN' => [Status::getByValue("Final", "SHIP-STATUS")]
        ];

        foreach ($odrSts as $key => $item) {
            $item = is_array($item) ? $item : [$item];
            $odrSts[$key] = "'" . implode("', '", $item) . "'";
        }

        //  Limit
        $limit = (!empty($input['day']) && is_numeric($input['day']))
            ? $input['day'] : self::DAS_LIMIT;

        $query = DB::table('odr_hdr as odr')
            ->select(DB::raw("
                COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['AL']}) 
                    THEN 1 ELSE 0 END), 0) AS allocated,
	            COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['PK']}) 
	                THEN 1 ELSE 0 END), 0) AS picked,
	            COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['PN']}) 
	                THEN 1 ELSE 0 END), 0) AS packing,
	            COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['ST']}) 
	                    AND shipment.`ship_sts` NOT IN ({$odrSts['FN']}) 
	                THEN 1 ELSE 0 END), 0) AS staging
            "))
            ->leftJoin('shipment', 'odr.ship_id', '=', 'shipment.ship_id')
            ->where('odr.deleted', 0)
            ->where('odr.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(NOW() - INTERVAL ' . $limit . ' DAY)'));

        $predis = new Data();
        $userInfo = $predis->getUserInfo();

        $currentWH = array_get($userInfo, 'current_whs', 0);
        $query->where('odr.whs_id', $currentWH);

        $cus_ids = $predis->getCustomersByWhs($currentWH);
        $query->whereIn('odr.cus_id', $cus_ids);

        $userId =  array_get($userInfo, 'user_id', 0);
        $query->where(function ($query) use ($userId) {
            $query->where('odr.created_by', $userId)
                ->orWhere('odr.updated_by', $userId);
        });

        $model = $query->get();

        return $model;
    }

    /**
     * @return mixed
     */
    public function odrAdminDashboard($input = [])
    {
        $odrSts = [
            'NW'  => [Status::getByValue("New", "Order-status")],
            'IP'  => [
                Status::getByValue("Allocated", "Order-status"),
                Status::getByValue("Partial Allocated", "Order-status"),
                Status::getByValue("Picking", "Order-status"),
                Status::getByValue("Partial Picking", "Order-status"),
                Status::getByValue("Picked", "Order-status"),
                Status::getByValue("Partial Picked", "Order-status"),
                Status::getByValue("Packing", "Order-status"),
                Status::getByValue("Partial Packing", "Order-status"),
                Status::getByValue("Packed", "Order-status"),
                Status::getByValue("Partial Packed", "Order-status"),
                Status::getByValue("Staging", "Order-status"),
                Status::getByValue("Partial Staging", "Order-status"),
            ],
            'OS'  => [
                Status::getByValue("Staging", "Order-status"),
                Status::getByValue("Partial Staging", "Order-status"),
            ],
            'BAC' => [Status::getByValue("BackOrder", "Order-type")],
            'FN'  => [Status::getByValue("Final", "SHIP-STATUS")],
            'SS'  => [Status::getByValue("Scheduled to Ship", "Order-status")],
            'RS'  => [
                Status::getByValue("Ready to Ship", "Order-status")
            ]
        ];

        foreach ($odrSts as $key => $item) {
            $item = is_array($item) ? $item : [$item];
            $odrSts[$key] = "'" . implode("', '", $item) . "'";
        }

        //  Limit
        $limit = (!empty($input['day']) && is_numeric($input['day']))
            ? $input['day'] : self::DAS_LIMIT;

        $query = DB::table('odr_hdr as odr')
            ->select(DB::raw("
                COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['NW']}) THEN 1 ELSE 0 END), 0) AS newOrders,
	            COALESCE( SUM(CASE WHEN odr.odr_sts IN ({$odrSts['IP']} ) 
		                AND ( shipment.`ship_sts` NOT IN ({$odrSts['FN']}) OR shipment.`ship_sts` IS NULL) THEN 1 
		            ELSE 0 END), 0) AS inprocessOrders,
	            COALESCE( SUM(CASE WHEN (odr.odr_sts IN ({$odrSts['RS']}) 
	                    AND shipment.`ship_sts` IN ({$odrSts['FN']})) THEN 1 
	                ELSE 0 END), 0) AS orderReadyToShip,
                COALESCE( SUM(CASE WHEN (odr.odr_sts IN ({$odrSts['SS']}) 
                    AND shipment.`ship_sts` IN ({$odrSts['FN']})) THEN 1 
	                ELSE 0 END), 0) AS orderScheduledToShip,
	            COALESCE( SUM(CASE WHEN odr.odr_type IN ({$odrSts['BAC']}) and odr.odr_sts not IN ('CC', 'SH')
	 	            THEN 1 ELSE 0 END), 0) AS backOrders,
	 	        COALESCE( SUM(CASE WHEN odr.odr_sts = 'SH' and odr.odr_type <> 'EC' and shipping_odr.type <> 'EC'
	 	            THEN 1 ELSE 0 END), 0) AS shippedOrders
            "))
            ->leftJoin('shipment', 'odr.ship_id', '=', 'shipment.ship_id')
            ->leftJoin('shipping_odr', 'odr.so_id', '=', 'shipping_odr.so_id')
            ->where('odr.deleted', 0)
            ->where('odr.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . $limit . ' DAY)'))
            ->where('odr.odr_type', '!=', Status::getByValue("Ecomm", "Order-Type"));

        $predis = new Data();
        $userInfo = $predis->getUserInfo();

        $currentWH = array_get($userInfo, 'current_whs', 0);
        $query->where('odr.whs_id', $currentWH);

        $cus_ids = $predis->getCurrentCusIds();
        $query->whereIn('odr.cus_id', $cus_ids);

        $model = $query->get();

        return $model;
    }

    /**
     * @return mixed
     */
    public function inprocessAdminDashboard($input = [])
    {
        $odrSts = [
            'AL' => [
                Status::getByValue("Allocated", "Order-status"),
                Status::getByValue("Partial Allocated", "Order-status")
            ],
            'PK' => [
                Status::getByValue("Picking", "Order-status"),
                Status::getByValue("Partial Picking", "Order-status")
            ],
            'PD' => [
                Status::getByValue("Picked", "Order-status"),
                Status::getByValue("Partial Picked", "Order-status")
            ],
            'PN' => [
                Status::getByValue("Packing", "Order-status"),
                Status::getByValue("Partial Packing", "Order-status")
            ],
            'PA' => [
                Status::getByValue("Packed", "Order-status"),
                Status::getByValue("Partial Packed", "Order-status")
            ],
            'ST' => [
                Status::getByValue("Staging", "Order-status"),
                Status::getByValue("Partial Staging", "Order-status")
            ],
            'FN' => [Status::getByValue("Final", "SHIP-STATUS")]
        ];

        foreach ($odrSts as $key => $item) {
            $item = is_array($item) ? $item : [$item];
            $odrSts[$key] = "'" . implode("', '", $item) . "'";
        }

        //  Limit
        $limit = (!empty($input['day']) && is_numeric($input['day']))
            ? $input['day'] : self::DAS_LIMIT;

        $query = DB::table('odr_hdr as odr')
            ->select(DB::raw("
                COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['AL']}) 
                    THEN 1 ELSE 0 END), 0) AS allocated,
	            COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['PK']}) 
	                THEN 1 ELSE 0 END), 0) AS picking,
	             COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['PD']}) 
	                THEN 1 ELSE 0 END), 0) AS picked,
	            COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['PN']}) 
	                THEN 1 ELSE 0 END), 0) AS packing,
	             COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['PA']}) 
	                THEN 1 ELSE 0 END), 0) AS packed,    
	            COALESCE(SUM(CASE WHEN odr.odr_sts IN ({$odrSts['ST']}) 
	                    AND (shipment.`ship_sts` NOT IN ({$odrSts['FN']}) OR shipment.`ship_sts` IS NULL ) 
	                THEN 1 ELSE 0 END), 0) AS staging
            "))
            ->leftJoin('shipment', 'odr.ship_id', '=', 'shipment.ship_id')
            ->where('odr.deleted', 0)
            ->where('odr.updated_at', '>=', DB::raw('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . $limit . ' DAY)'))
            ->where('odr.odr_type', '!=', Status::getByValue("Ecomm", "Order-Type"));

        $predis = new Data();
        $userInfo = $predis->getUserInfo();

        $currentWH = array_get($userInfo, 'current_whs', 0);
        $query->where('odr.whs_id', $currentWH);

        $cus_ids = $predis->getCurrentCusIds();
        $query->whereIn('odr.cus_id', $cus_ids);

        $model = $query->get();

        return $model;
    }

    public function getItemBO($cus_ids, $whsId){
        $item_bo = $this->model
            ->join('odr_dtl', 'odr_dtl.odr_id', '=', 'odr_hdr.odr_id')
            ->where('odr_dtl.deleted', 0)
            ->whereIn('odr_dtl.cus_id', $cus_ids)
            ->where('odr_dtl.whs_id', $whsId)
            ->where('odr_hdr.odr_type', 'BAC')
            ->whereNotIn('odr_hdr.odr_sts', ['SH', 'CC'])
            ->whereRaw('DATE(FROM_UNIXTIME(odr_hdr.ship_by_dt)) < DATE(NOW())')
            ->where('odr_hdr.is_ecom', 0)
            ->whereHas('shippingOrder', function ($sh) {
                $sh->where('type', '!=', 'EC');
            })
            ->select([
                'odr_dtl.item_id',
                'odr_dtl.sku'
            ])->groupBy('odr_dtl.item_id')
            ->get();

        return count($item_bo);
    }

    public function getOrdersLate($cus_ids, $whsId){

        $result = $this->model
            ->where('deleted', 0)
            ->whereIn('cus_id', $cus_ids)
            ->where('whs_id', $whsId)
            ->whereRaw('DATE(FROM_UNIXTIME(ship_by_dt)) < DATE(NOW())')
            ->whereNotIn('odr_sts', ['SH', 'CC'])
            ->where('is_ecom', 0)
            ->whereHas('shippingOrder', function ($sh) {
                $sh->where('type', '!=', 'EC');
            })->count();

        return $result;
    }

    public function getNonInteractiveOrders($cus_ids, $whsId){
        $whsConfig = $this->warehouseMetaModel->getWarehouseMeta($whsId, 'wno');
        if(!is_null($whsConfig)) {
            $nonInteractiveDay = array_get($whsConfig, 'whs_meta_value', 0) * 86400;
            $result = $this->model
                ->where('deleted', 0)
                ->whereIn('cus_id', $cus_ids)
                ->where('whs_id', $whsId)
                ->where('odr_sts', 'NW')
                ->where('is_ecom', 0)
                ->whereIn('odr_type', ['BU', 'RTL', 'BAC', 'XDK', 'TK'])
                ->where('updated_at', '<', time() + $nonInteractiveDay)
                ->count();
        }else{
            return 0;
        }
        return $result;
    }
}
