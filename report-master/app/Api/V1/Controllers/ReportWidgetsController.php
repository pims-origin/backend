<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ReportWidgetsModel;
use App\Api\V1\Validators\ReportWidgetsValidator;
use App\Api\V1\Transformers\ReportListTransformer;
use App\Api\V1\Transformers\ReportWidgetsTransformer;
use App\Api\V1\Models\ReportConfigsModel;
use Seldat\Wms2\Models\ReportConfigs;
use Seldat\Wms2\Models\ReportWidgets;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\Container;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\SelExport;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Dingo\Api\Http\Response;

use Seldat\Wms2\Utils\Message;


class ReportWidgetsController extends AbstractController
{
    protected $reportWidgetsValidator;
    protected $reportWidgetsModel;

    /**
     * ReportWidgetsController constructor.
     *
     * @param ReportWidgetsValidator $reportWidgetsValidator
     * @param ReportWidgetsModel $reportWidgetsModel
     */
    public function __construct(
        ReportWidgetsValidator $reportWidgetsValidator,
        ReportWidgetsModel $reportWidgetsModel
    ) {
        $this->reportWidgetsValidator = $reportWidgetsValidator;
        $this->reportWidgetsModel = $reportWidgetsModel;
    }

    /**
     * @param Request $request
     * @param ReportListTransformer $reportListTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function getReportList(ReportConfigsModel $reportConfigsModel,
        Request $request,
        ReportListTransformer $reportListTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();
        try {
            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $data = ReportWidgets::select(DB::raw('JSON_EXTRACT(data, "$**.id")'))
                ->get();
            $idsArr = [];

            foreach ($data as $key =>$element) {
                $ids = array_get($data, $key, "");
                $rs = json_decode($ids, true);
                foreach ($rs as $a) {
                    $idsArr = array_merge($idsArr, json_decode($a, true));
                }
            }
            $dataResuls = ReportConfigs::select('*')->where('level', 1);
            if(is_array($idsArr)) {
                $dataResuls->whereNotIn('qualifier', $idsArr);
            }
            $dataResuls  = $dataResuls->get();
            return $this->response->collection($dataResuls, new $reportListTransformer)
                ->setStatusCode(IlluminateResponse::HTTP_CREATED);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return array|void
     */
    public function store(ReportWidgetsModel $reportWidgetsModel, Request $request, ReportWidgetsTransformer $reportWidgetsTransformer)
    {
        // get data from HTTP
        $input = $request->getParsedBody();
        // validation
        $this->reportWidgetsValidator->validate($input);

        $valueArrJson = array_get($input, 'data', '');
        $valueJson = json_encode($valueArrJson);

        $params = [
            'data' => $valueJson,
            'des' => array_get($input, 'des', ''),
            'name' => array_get($input, 'name', ''),
            'max_report' => array_get($input, 'max_report', ''),
            'count_report' => array_get($input, 'count_report', ''),
            'ordinal' => array_get($input, 'ordinal', ''),
            'created_at' => time(),
            'updated_at' => time(),
        ];
        try {
            if ($reportWidgetsModel->getFirstWhere(['id' => $input['id']]) && isset($input['id'])) {
                 $reportWidgetsModel->updateWhere($params, ['id' => $input['id']]);
                $data = $reportWidgetsModel->getFirstWhere(['id' => $input['id']]);
            }
            else {
                $data = $reportWidgetsModel->create($params);
                }
            return $this->response->item($data, new $reportWidgetsTransformer)
                ->setStatusCode(IlluminateResponse::HTTP_CREATED);
        }
        catch
            (\PDOException $e) {
                return $this->response->errorBadRequest($e->getMessage());
            } catch (\Exception $e) {

                return $this->response->errorBadRequest($e->getMessage());
            }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    public function delete(Request $request, $id, ReportWidgetsModel $reportWidgetsModel) {
        try {
            if (empty($reportWidgetsModel->getFirstWhere(['id' => $id]))) {
                return $this->response->errorBadRequest(sprintf("The ID %u is not existed!", $id));
            }
            DB::beginTransaction();
                $reportWidgetsModel->deleteById($id);
            DB::commit();
            return $this->response->noContent()
                ->setContent(['data' => ['message' => sprintf("The ID %u is deleted!", $id)]])
                ->setStatusCode(Response::HTTP_CREATED);
        }
        catch
        (\PDOException $e) {
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {

            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }
    /**
     * @param Request $request
     * @param $reportWidgetsId
     * @param ReportWidgetsTransformer $reportWidgetsTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function viewReportWidgets(
        Request $request,
        $reportWidgetsId,
        ReportWidgetsTransformer $reportWidgetsTransformer
    ) {
        // get data from HTTP
        $input = $request->getQueryParams();

        try {
            DB::setFetchMode(\PDO::FETCH_ASSOC);
            $reportWidgets = $this->reportWidgetsModel->getFirstWhere([
                'id'    => $reportWidgetsId
            ]);

            return $this->response->item($reportWidgets, $reportWidgetsTransformer);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_WAREHOUSE, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function  getReportWidgets(Request $request, ReportWidgetsTransformer
    $reportWidgetsTransformer) {
        $input = $request->getQueryParams();
        $reportWidgetsList = ReportWidgets::orderBy('ordinal','asc')->get();
        if (empty($reportWidgetsList)) {
            throw new \Exception("No results");
        }
        return $this->response->collection($reportWidgetsList, new $reportWidgetsTransformer)
            ->setStatusCode(IlluminateResponse::HTTP_CREATED);
    }


}
