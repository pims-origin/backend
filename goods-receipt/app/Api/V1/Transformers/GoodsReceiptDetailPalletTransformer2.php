<?php


namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Utils\Status;

class GoodsReceiptDetailPalletTransformer2 extends TransformerAbstract
{
    //return result
    public function transform(Carton $ct)
    {

        return [
            'plt_id'           => null,
            'gr_dtl_id'        => object_get($ct, 'gr_dtl_id'),
            'plt_num'          => object_get($ct, 'lpn_carton'),
            'item_id'          => object_get($ct, 'item_id'),
            'item_status_code' => object_get($ct, 'item_status_code'),
            'item_status'      => object_get($ct, 'item_status'),
            'sku'              => object_get($ct, 'sku'),
            'size'             => object_get($ct, 'size'),
            'color'            => object_get($ct, 'color'),
            'lot'              => object_get($ct, 'lot'),
            'des'              => object_get($ct, 'des'),
            'plt_sts'          => "",
            'plt_sts_code'     => "",
            'dmg_ttl'          => 0,
            'expired_date'     =>  null,
            'act_loc_code'     => null,
            'init_piece_ttl'   => object_get($ct, 'init_qty'),
            'init_ctns'        => object_get($ct, 'init_ctns'),
            'putaway_location' => object_get($ct, 'act_loc_code'),
            'uom_code'         => object_get($ct, 'uom_code'),
            'uom_name'         => object_get($ct, 'uom_name'),
            'weight'           => number_format(object_get($ct, 'weight', 0), 2, '.', ','),
            'pack'             => object_get($ct, 'pack'),
            // 'pallet_uom'       => object_get($pallet, 'uom'),
            // 'pallet_pack'      => object_get($pallet, 'pallet_pack'),
            'ctn_ttl'          => 0,
            'cur_qty'          => 0,
            'rfid'             => object_get($ct, 'rfid', ''),
        ];
    }
}
