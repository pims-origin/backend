<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\ReturnDtl;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;
use Wms2\UserInfo\Data;

class ReturnOrderDtlModel extends AbstractModel
{

    protected $model;

    /**
     * @param ReturnDtl $model
     */
    public function __construct(ReturnDtl $model = null)
    {
        $this->model = ($model) ?: new ReturnDtl();
    }



}
