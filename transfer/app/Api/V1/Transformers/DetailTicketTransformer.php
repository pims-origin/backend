<?php

namespace App\Api\V1\Transformers;

use Illuminate\Support\Facades\DB;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\XferDtl;
use Seldat\Wms2\Utils\Status;

class DetailTicketTransformer extends TransformerAbstract
{
    /**
     * turn array
     */
    public function transform(XferDtl $xferDtl)
    {
        $cus_info = DB::table('customer')->where('cus_id', '=', $xferDtl->cus_id)->select('cus_name')->first();

        $ctn_info = DB::table('cartons')
            ->where('loc_id', '=', $xferDtl->act_req_loc_id ?: $xferDtl->req_loc_id)
            ->where('item_id', '=', $xferDtl->item_id)
            ->where('whs_id', '=', $xferDtl->whs_id)
            ->where('cus_id', $xferDtl->cus_id)
            ->where(function ($q) {
                $q->where('is_damaged', 0);
                $q->orWhereNull('is_damaged');
            })
            ->where('ctn_sts', 'AC')
            ->where('deleted', 0)
            ->groupBy('item_id');

        $ttl_piece = $ctn_info->where(function ($query) {
            $query->whereNull('is_ecom');
            $query->orWhere('is_ecom', 0);
        })->sum('piece_remain');

        return [
            'xfer_hdr_id'          => $xferDtl->xfer_hdr_id,
            'xfer_hdr_num'         => object_get($xferDtl, 'xferHdr.xfer_hdr_num', null),
            'xfer_hdr_sts'         => object_get($xferDtl, 'xferHdr.xfer_sts', null),
            'xfer_hdr_sts_name'    => Status::getByKey("XFER-STATUS", object_get($xferDtl, 'xferHdr.xfer_sts', null)),
            'piece_qty'            => object_get($xferDtl, 'xferHdr.piece_qty', null),
            'cus_id'               => $xferDtl->cus_id,
            'cus_name'             => array_get($cus_info, 'cus_name', null),
            'sku'                  => $xferDtl->sku,
            'item_id'              => $xferDtl->item_id,
            'cus_upc'              => $xferDtl->cus_upc,
            'size'                 => $xferDtl->size,
            'color'                => $xferDtl->color,
            'lot'                  => $xferDtl->lot,
            'xfer_dtl_id'          => $xferDtl->xfer_dtl_id,
            'allocated_qty'        => $xferDtl->allocated_qty,
            'act_piece_qty'        => $xferDtl->act_piece_qty,
            'putaway_qty'          => $xferDtl->putaway_qty,
            'req_loc'              => $xferDtl->req_loc,
            'req_loc_id'           => $xferDtl->req_loc_id,
            'mez_loc'              => $xferDtl->mez_loc,
            'mez_loc_id'           => $xferDtl->mez_loc_id,
            'act_req_loc_id'       => $xferDtl->act_req_loc_id,
            'act_req_loc'          => $xferDtl->act_req_loc,
            'act_mez_loc'          => $xferDtl->act_mez_loc,
            'act_mez_loc_id'       => $xferDtl->act_mez_loc_id,
            'whs_id'               => $xferDtl->whs_id,
            'xfer_dtl_sts'         => $xferDtl->xfer_dtl_sts,
            'xfer_dtl_sts_name'    => Status::getByKey("XFER-STATUS", $xferDtl->xfer_dtl_sts),
            'xfer_ticket_id'       => $xferDtl->xfer_ticket_id,
            'xfer_ticket_num'      => object_get($xferDtl, 'xferTicket.xfer_ticket_num', null),
            'xfer_ttl'             => object_get($xferDtl, 'xferTicket.xfer_ttl', null),
            'xfer_ticket_sts'      => object_get($xferDtl, 'xferTicket.xfer_ticket_sts', null),
            'xfer_ticket_sts_name' => Status::getByKey("XFER-STATUS",
                object_get($xferDtl, 'xferTicket.xfer_ticket_sts', null)),
            'piece_remain'         => $ttl_piece ? $ttl_piece : 0
        ];

    }
}
