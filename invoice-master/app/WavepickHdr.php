<?php
namespace App;

/**
 * Class WavepickHdr
 *
 * @property mixed wv_id
 * @property mixed whs_id
 * @property mixed wv_num
 * @property mixed wv_sts
 * @property mixed seq
 *
 */
class WavepickHdr extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wv_hdr';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'wv_id';
    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'whs_id',
        'wv_num',
        'wv_sts',
        'seq',
        'picker',
        'is_ecom'
    ];

    public $odrTtl ;
    public $SkuTtl ;

    public function orderHdr()
    {
        return $this->hasMany(__NAMESPACE__ . '\OrderHdr', 'wv_id', 'wv_id');
    }

    public function userCreated()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'created_by');
    }

    public function pickerUser()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'picker');
    }

    public function details()
    {
        return $this->hasMany(__NAMESPACE__ . '\WavepickDtl', 'wv_id', 'wv_id');
    }
}
