<?php

namespace App\Api\V2\Inbound\Location\DropPallet\Controllers;

use App\Api\V3\Inbound\Location\Putaway\Models\AsnDtlModel;
use App\Api\V3\Inbound\Location\Putaway\Models\CartonModel;
use App\Api\V3\Inbound\Location\Putaway\Models\EventTrackingModel;
use App\Api\V3\Inbound\Location\Putaway\Models\GoodsReceiptDetailModel;
use App\Api\V3\Inbound\Location\Putaway\Models\GoodsReceiptModel;
use App\Api\V3\Inbound\Location\Putaway\Models\LocationModel;
use App\Api\V3\Inbound\Location\Putaway\Models\PalletModel;
use App\Api\V3\Inbound\Location\Putaway\Models\PalletSuggestLocationModel;
use App\Api\V3\Inbound\Location\Putaway\Models\VirtualCartonModel;
use App\Api\V3\Inbound\Location\Putaway\Models\Log;
use App\libraries\RFIDValidate;
use App\MessageCode;
use App\Utils\JWTUtil;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\CycleCountNotification;
use Seldat\Wms2\Models\Reason;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\Status;
use Seldat\Wms2\Utils\SystemBug;
use Wms2\UserInfo\Data;
use App\MyHelper;

class PutAwayController extends AbstractController
{
    /**
     * @var PalletModel
     */
    protected $palletModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var VirtualCartonModel
     */
    protected $virtualCartonModel;

    protected $palletSuggestLocationModel;

    protected $eventTrackingModel;

    /**
     * PutAwayController constructor.
     *
     * @param PalletModel $palletModel
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     * @param VirtualCartonModel $virtualCartonModel
     */
    public function __construct(
        PalletModel $palletModel,
        LocationModel $locationModel,
        VirtualCartonModel $virtualCartonModel
    ) {
        $this->palletModel = $palletModel;
        $this->cartonModel = new CartonModel();
        $this->locationModel = $locationModel;
        $this->virtualCartonModel = $virtualCartonModel;
        $this->eventTrackingModel = new EventTrackingModel();
        $this->palletSuggestLocationModel = new PalletSuggestLocationModel();
    }

    /**
     * @param $whsId
     * @param Request $request
     *
     * @return Response|\Symfony\Component\HttpFoundation\Response|void
     */
    public function dropPalletFromLocation($whsId, Request $request)
    {
        $url = "/v2/whs/$whsId/location/rack/drop-pallet";
        $owner = "";
        $transaction = "";
        Log::info($request, $whsId, [
            'evt_code'     => 'DPR',
            'owner'        => $owner,
            'transaction'  => $transaction,
            'url_endpoint' => $url,
            'message'      => 'Drop pallet from RACK'
        ]);

        $input = $request->getParsedBody();
        $type = array_get($input, 'type');
        if (!in_array($type, [1, 2])) {
            $msg = "Type is required. Just including 1(RFID) / 2(Barcode)";
            return $this->_responseErrorMessage($msg);
        }
        if ('' == trim(array_get($input, 'pallet_rfid')) || array_get($input, 'pallet_rfid') == null) {
            $msg = 'pallet_rfid is required!';
            return $this->_responseErrorMessage($msg);
        }

        if ('' == trim(array_get($input,'location')) || array_get($input,'location') == null) {
            $msg = 'location field is required!';
            return $this->_responseErrorMessage($msg);
        }

        //validate pallet RFID
        $pltRFIDValid = new RFIDValidate($input['pallet_rfid'], RFIDValidate::TYPE_PALLET, $whsId);
        if (!$pltRFIDValid->validate()) {
            return $this->_responseErrorMessage($pltRFIDValid->error);
        }

        //validate location RFID
        if ($type == 1) {
            $locRFIDValid = new RFIDValidate($input['location'], RFIDValidate::TYPE_LOCATION, $whsId);
            if (!$locRFIDValid->validate()) {
                return $this->_responseErrorMessage($locRFIDValid->error);
            }
        }

        // Get Pallet by Rfid
        $pallet = $this->palletModel->getModel()->where([
            'rfid'    => $input['pallet_rfid'],
            'whs_id'  => $whsId,
            // 'plt_sts' => 'AC',
        ])->where('ctn_ttl', '>', 0)->first();

        if (!$pallet) {
            $msg = sprintf("The pallet RFID %s doesn't exist, Please try to pass through the inbound gateway again!", $input['pallet_rfid']);
            return $this->_responseErrorMessage($msg);
        }

        if ($pallet && $pallet->loc_id == null) {
            $msg = sprintf("The pallet %s isn't on any location.", $input['pallet_rfid']);
            return $this->_responseErrorMessage($msg);
        }

        $cusId = object_get($pallet, 'cus_id', '');

        //Check existed for The loc RFID
        $locationInfo = '';
        if ($type == 1) {
            $locationInfo = $this->locationModel->getRackLocationByLocRFID($whsId, $input['location']);
        } else {
            $locationInfo = $this->locationModel->getFirstWhere([
                                'loc_whs_id' => $whsId,
                                'loc_code'   => $input['location']]);
        }
        if (empty($locationInfo)) {
            $msg = sprintf("The location %s doesn't exist!", $input['location']);
            return $this->_responseErrorMessage($msg);
        }

        $location = $this->locationModel->getRackLocationByCustomerById($whsId, $locationInfo->loc_id, $cusId);
        $user = (new Data())->getUserInfo();

        $grHdrObj = (new GoodsReceiptModel)->getModel()->where('gr_hdr_id', $pallet->gr_hdr_id)->first();
        $grStatus = object_get($grHdrObj, 'gr_sts');

        if ($grStatus == Status::getByKey('GR_STATUS', 'RECEIVED')) {
            $msg = sprintf("The Goods receipt %s is already RECEIVED.", object_get($grHdrObj, 'gr_hdr_num'));
            return $this->_responseErrorMessage($msg);
        }

        if (empty($location)) {
            $msg = sprintf("The location %s does not belong to this customer.", $input['location']);
            return $this->_responseErrorMessage($msg);
        } elseif ($location->loc_sts_code != 'AC') {
            $msg = sprintf("Location %s is not active. Current Status %s",
                $location->loc_code,
                $location->loc_sts_code);
            return $this->_responseErrorMessage($msg);
        }

        if ($location->loc_id != $pallet->loc_id) {
            $msg = sprintf("The pallet %s doesn't belong to location %s", $input['pallet_rfid'], $input['location']);
            return $this->_responseErrorMessage($msg);
        }

        $chkPlt = $this->palletModel->getModel()->where([
            'loc_id' => $location->loc_id,
            'whs_id' => $whsId
        ])
            // ->where('rfid', '<>', $input['pallet_rfid'])
//                ->where('ctn_ttl', '>', 0)
//                ->where('plt_sts', 'AC')
            ->first();

        if ($chkPlt && ($chkPlt->rfid <> $input['pallet_rfid'] || $chkPlt->rfid == null)) {
            $msg = sprintf("The pallet %s doesn't belong to location %s", $input['pallet_rfid'], $input['location']);
            return $this->_responseErrorMessage($msg);
        }

        try {
            DB::beginTransaction();

            $returnData = [];

            // Update Pallet
            $this->palletModel->refreshModel();
            $this->palletModel->updateWhere([
                    'loc_id'   => null,
                    'loc_code' => null,
                    'loc_name' => null
                ],
                [
                    'rfid'             => $input['pallet_rfid'],
                    // 'plt_sts'          => 'AC',
                    'storage_duration' => 0
                ]);

            // Update Carton
            $data = [
                'loc_id'        => null,
                'loc_code'      => null,
                'loc_name'      => null,
                'loc_type_code' => null,
                'plt_id'        => $pallet->plt_id
            ];

            $this->cartonModel->updateCartonWithPltID($data);

            // turn off putaway when enough pallets have put on rack
            $this->_turnOffPutAway($pallet->gr_hdr_id);

            //unlock this location
            $this->locationModel->updateWhere(
                [
                    'loc_sts_code' => Status::getByKey('LOCATION_STATUS', 'ACTIVE'),
                ],
                [
                    'loc_id' => $location->loc_id
                ]
            );

            //remove from table pal_sug_loc
            $this->removePalletSugLoc($pallet, $location);

            $returnData = $this->cartonModel->getCartonsInfoByPalletRFID($whsId, $pallet->plt_id);

            //write even tracking
            $vtlObj = $this->virtualCartonModel->getASNByVtlCtn($input['pallet_rfid']);
            $dataEvt = [
                'whs_id'    => object_get($vtlObj, 'whs_id', null),
                'cus_id'    => object_get($vtlObj, 'cus_id', null),
                'owner'     => object_get($vtlObj, 'gr_hdr_num', ''),
                'evt_code'  => 'PUT',
                'trans_num' => object_get($vtlObj, 'ctnr_num', ''),
                'info'      => sprintf('Remove %s from %s', $input['pallet_rfid'], $location->loc_code)
            ];

            //call Event tracking
            $this->eventTrackingModel->refreshModel();
            $this->eventTrackingModel->create($dataEvt);

            DB::commit();
            $msg = sprintf("Pallet %s has been removed from location %s", $input['pallet_rfid'], $location->loc_code);

            return [
                'status' => true,
                'iat'    => time(),
                'messages' => [
                    [
                        'status_code' => 1,
                        'msg'         => $msg
                    ]
                ],
                'data' => [
                    [
                        'skus' => $returnData,
                    ]
                ]
            ];
        } catch (\PDOException $e) {
            DB::rollBack();
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * checking enough pallets have put on rack then turn on putaway within gr_hdr table
     *
     * @param $grHdrId
     *
     * @throws \Exception
     */
    private function _turnOnPutAway($grHdrId)
    {
        try {
            $grHdrObj = (new GoodsReceiptModel)->getModel()->where('gr_hdr_id', $grHdrId)->first();
            if ($grHdrObj) {
                $grStatus = object_get($grHdrObj, 'gr_sts');
                if ($grStatus == Status::getByKey('GR_STATUS', 'RECEIVED')) {
                    // // WMS2 4616 - [WAP-API][Putaway] Set gr_hdr.putaway=2 when putting
                    // if ($grHdrObj && $grHdrObj->putaway != 2) {
                    //     $grHdrObj->putaway = 2;
                    //     $grHdrObj->save();
                    // }
                    return;
                }
                $totalPallet = (new GoodsReceiptDetailModel())->palletTotalOfGrHdr($grHdrId);
                $countPallet = $this->palletModel->countPalletOnRackByGRHdrId($grHdrId);

                // WMS2 4616 - [WAP-API][Putaway] Set gr_hdr.putaway=2 when putting
                $grHdrObj->putaway = 2;
                if ($totalPallet == $countPallet) {
                    $grHdrObj->putaway = 1;
                }
                $grHdrObj->save();
            }

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * checking enough pallets have put on rack then turn on putaway within gr_hdr table
     *
     * @param $grHdrId
     *
     * @throws \Exception
     */
    private function _turnOffPutAway($grHdrId)
    {
        try {
            $grHdrObj = (new GoodsReceiptModel)->getModel()->where('gr_hdr_id', $grHdrId)->first();
            $grStatus = object_get($grHdrObj, 'gr_sts');
            if ($grStatus != Status::getByKey('GR_STATUS', 'RECEIVED')) {
                // WMS2 4616 - [WAP-API][Putaway] Set gr_hdr.putaway=2 when putting
                if ($grHdrObj && $grHdrObj->putaway != 2) {
                    $grHdrObj->putaway = 2;
                    $grHdrObj->save();
                }
                return;
            }

            $totalPallet = (new GoodsReceiptDetailModel())->palletTotalOfGrHdr($grHdrId);
            $countPallet = $this->palletModel->countPalletOnRackByGRHdrId($grHdrId);

            // WMS2 4616 - [WAP-API][Putaway] Set gr_hdr.putaway=2 when putting
            $grHdrObj->putaway = 2;
            if ($totalPallet == 0) {
                $grHdrObj->putaway = 0;
            }
            $grHdrObj->save();

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Input last pallet to RAC and input data into table pal_sug_log
     *
     * @param $pallet
     * @param $location
     * @param $grObj
     *
     * @throws \Exception
     */
    private function initPalletSugLoc($pallet, $location, $grObj)
    {
        try {
            $userId = JWTUtil::getPayloadValue('jti');

            if ($palSugLoc = $this->_checkPalletRFIDExistedOnSuggestLocation($pallet->plt_id)) {
                $palSugLoc->act_loc_id   = $location->loc_id;
                $palSugLoc->act_loc_code = $location->loc_code;
                $palSugLoc->save();
                return;
            }

            $suPltLocData = [
                'plt_id'       => $pallet->plt_id,
                'loc_id'       => $location->loc_id,
                'data'         => $location->loc_code,
                'ctn_ttl'      => $pallet->ctn_ttl,
                'item_id'      => object_get($grObj, 'item_id', null),
                'sku'          => object_get($grObj, 'sku', 0),
                'size'         => object_get($grObj, 'size', 'NA'),
                'color'        => object_get($grObj, 'color', 'NA'),
                'lot'          => object_get($grObj, 'lot', 'NA'),
                'putter'       => $userId,
                'gr_hdr_id'    => $pallet->gr_hdr_id,
                'gr_dtl_id'    => $pallet->gr_dtl_id,
                'gr_hdr_num'   => object_get($grObj, 'gr_hdr_num', ''),
                'whs_id'       => $pallet->whs_id,
                'put_sts'      => "CO",
                'act_loc_id'   => $location->loc_id,
                'act_loc_code' => $location->loc_code,
            ];

            $this->palletSuggestLocationModel->refreshModel();
            $this->palletSuggestLocationModel->create($suPltLocData);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Input last pallet to RAC and input data into table pal_sug_log
     *
     * @param $pallet
     * @param $location
     * @param $grObj
     *
     * @throws \Exception
     */
    private function removePalletSugLoc($pallet, $location)
    {
        try {
            if ($palSugLoc = $this->_checkPalletRFIDExistedOnSuggestLocation($pallet->plt_id)) {
                $palSugLoc->act_loc_id   = null;
                $palSugLoc->act_loc_code = null;
                $palSugLoc->save();
                return;
            }

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    private function _checkPalletRFIDExistedOnSuggestLocation($pltId)
    {
        $result = $this->palletSuggestLocationModel->getFirstWhere(
            [
                'plt_id' => $pltId,
            ]
        );

        return $result;
    }

    private function _responseErrorMessage($msg, $data = [])
    {
        return [
            'status'  => false,
            'iat'     => time(),
            'data'    => $data,
            'messages' => [[
                'status_code' => -1,
                'msg'         => $msg,
            ]]
        ];
    }
}
