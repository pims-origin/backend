<?php

namespace App\Modules\Customers\Eloquents;

use Illuminate\Database\Eloquent\Model;

class UserWhs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_whs';

    protected $fillable = [
        'user_id',
        'whs_id'
    ];
}
