<?php

use App\Helpers\SettingMailHelper;

$configs = SettingMailHelper::setting('EC');
$ea = SettingMailHelper::setting('EA');

return [
    "driver" => "smtp",
    "host" => !empty($configs['host']) ? $configs['host'] : '',
    "port" => !empty($configs['port']) ? $configs['port'] : '',
    "from" => array(
        "address" => !empty($ea) ? $ea : env('MAIL_FROM_EMAIL', 'seldat.noreplpy@seldatinc.com'),
        "name" => !empty($configs['name']) ? $configs['name'] : ''
    ),
    'encryption' => !empty($configs['encryption']) ? $configs['encryption'] : '',
    "username" => !empty($configs['username']) ? $configs['username'] : '',
    "password" => !empty($configs['password']) ? $configs['password'] : '',
    'markdown' => [
        'theme' => 'default',
        'paths' => [
            resource_path('views/vendor/mail'),
        ],
    ],
];
