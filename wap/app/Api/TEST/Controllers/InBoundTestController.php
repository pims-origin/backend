<?php

namespace App\Api\TEST\Controllers;

use App\Api\OUTBOUND\Models\LogsModel;
use App\Api\V1\Models\AsnHdrModel;
use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\Log;
use App\Api\V1\Models\PalletSuggestLocationModel;
use App\MyHelper;
use Dingo\Api\Http\Response;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Laravel\Lumen\Routing\Controller;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Utils\Status;

class InBoundTestController extends Controller
{

    public function __construct()
    {
        if (env('APP_ENV') != 'testing') {
            return "Access denied!";
        }
    }

    public function index()
    {
        return "Unit test API.";
    }

    public function scanCarton()
    {
        $asnDtlId = 541;
        /*
         * 1. Scan carton
         * 2. scan pallet
         * 3. call this function to complete
         */
        $asnDtl = AsnDtl::where('asn_dtl.asn_dtl_id', $asnDtlId)->first()->toArray();
    }

    public function scanPallet()
    {
        $asnDtlId = 541;
        /*
         * 1. Scan carton
         * 2. scan pallet
         * 3. call this function to complete
         */
        $asnDtl = AsnDtl::where('asn_dtl.asn_dtl_id', $asnDtlId)->first()->toArray();
    }

    public function completeAsnDtl()
    {
        $asnDtlId = 597;
        /*
         * 1. Scan carton
         * 2. scan pallet
         * 3. call this function to complete
         */

        $grHdrModel = new GoodsReceiptModel();

        $asnDtl = AsnDtl::where('asn_dtl.asn_dtl_id', $asnDtlId)->first()->toArray();

        DB::beginTransaction();
        $asnDtl['whs_id'] = 1;
        $asnDtl['cus_id'] = 1;
        $result = $grHdrModel->createGRWhenScanAllCartons($asnDtl);
        DB::commit();

        dd($result);
    }

    public function logTest(Request $request)
    {

        DB::beginTransaction();
        $result = Log::error($request, 1, [
                    'evt_code' => 'ODR-0001-00101',
                    'owner' => Status::getByKey('EVENT', 'GR-RECEIVING'),
                    'transaction' => 'ODR-0001-00101',
                    'url_endpoint' => '/log-test',
                    'message' => sprintf(Status::getByKey('EVENT-INFO', 'GR-RECEIVING'), 'ODR-0001-00101'),
                    'response_data' => true
        ]);

        DB::commit();
    }

    public function testCtnRFID()
    {
        $ctnRFID = 'AAAAFFFFEEEETTT123456O_@';
        return MyHelper::isHex($ctnRFID);
    }

    public function generateData($num)
    {
        $arr = [];
        for ($i = 0; $i < $num; $i++) {
            $rand = uniqid() . uniqid();
            $randStr = strtoupper(substr($rand, 0, 25));
            $arr[] = "\"{$i}\":\"{$randStr}\"";
            $arr2[] = $randStr;
        }
        $str = "[\n" . implode(",\n", $arr) . "\n]";
        $str2 = json_encode($arr2);
        return (new Response($str))->header('Content-Type', 'text/plain');
    }

}
