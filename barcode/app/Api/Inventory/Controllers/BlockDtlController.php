<?php

namespace App\Api\Inventory\Controllers;

use App\Api\Inventory\Models\BlockDtlModel;
use App\Api\Inventory\Models\CycleDtlModel;
use App\Api\Inventory\Models\CycleHdrModel;
use App\Api\Inventory\Models\LocationModel;
use App\Api\Inventory\Transformers\BlockDtlTransformer;
use App\Api\Inventory\Transformers\CycleDtlTransformer;
use App\Api\Inventory\Validators\CycleDtlValidator;
use App\Api\Inventory\Validators\UpdateActualCycleDtlValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Api\Inventory\Validators\CycleHdrValidator;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\CycleHdr;
use App\Api\Inventory\Transformers\CycleHdrTransformer;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

class BlockDtlController extends AbstractController
{
    protected $model;
    protected $blockDtlTransform;
    protected $request;
    protected $error;

    /**
     * BlockDtlController constructor.
     * @param BlockDtlModel $model
     * @param Request $request
     */
    public function __construct(BlockDtlModel $model, Request $request)
    {
        $this->model = $model;
        $this->blockDtlTransform = new BlockDtlTransformer();
        $this->request = $request;
    }

    /**
     * @param $blockHdrId
     * @return \Dingo\Api\Http\Response|string
     */
    public function listDtl($blockHdrId)
    {
        $input = $this->request->getQueryParams();

        try {
            $data = $this->model->search($blockHdrId, null, $input, [], array_get($input, 'limit', 20));

            return $this->response->paginator($data, $this->blockDtlTransform);

        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }

    /**
     * @param $id
     */
    public function unblockItem($id)
    {
        $input = $this->request->getParsedBody();
    }

    /**
     * @param $blockHdrId
     * @param $blockDtlId
     * @return \Dingo\Api\Http\Response|string
     */
    public function view($blockHdrId, $blockDtlId)
    {
        $input = $this->request->getQueryParams();

        try {

            $data = $this->model->search($blockHdrId, $blockDtlId, $input);

            return $this->response->paginator($data, $this->blockDtlTransform);

        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }
}
