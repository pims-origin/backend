<?php

namespace Seldat\Wms2\Models;

class BlockHdr extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'block_hdr';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'block_hdr_id';

    /**
     * @var array
     */
    protected $fillable = [
        'block_rsn_id',
        'block_num',
        'whs_id',
        'block_type',
        'block_detail',
        'block_sts',
        'sts'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'whs_id', 'whs_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function blockDtl()
    {
        return $this->hasMany(BlockDtl::class, 'block_hdr_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function blockReason()
    {
        return $this->belongsTo(BlockRsn::class, 'block_rsn_id', 'block_rsn_id');
    }
}
