<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/30/2016
 * Time: 5:07 PM
 */

namespace backend\models;

use backend\models\Form;
use yii\rbac\Role;
use common\models\Authority;

class RoleSearchForm extends Form
{
    const ALLOW_SELECT_FIELDS = [
        'name',
        'code',
        'description',
        'type',
        'rule_name',
        'data',
        'created_at',
        'updated_at',
    ];

    const SORT_STYLE = [
        'desc' => SORT_DESC,
        'asc'  => SORT_ASC,
    ];

    public $name;
    public $code;
    public $description;
    public $type;
    public $rule_name;
    public $data;
    public $limit;
    public $page;
    public $sort;

    public function rules()
    {
        return [
            [['name', 'code', 'type', 'description', 'rule_name', 'data'], 'safe'],
            [
                ['type', 'page', 'limit'],
                'integer'
            ],
            [
                'sort',
                'validateSorting'
            ]
        ];
    }

    public function validateSorting($attribute, $params)
    {
        if (! is_array($this->sort)) {
            $this->addError($attribute, 'Sort param is invalid.');
        }
        foreach ($this->sort as $field => $sort) {
            if (! in_array($field, self::ALLOW_SELECT_FIELDS, true)) {
                $this->addError($attribute, 'Sort param is invalid.');
                return $this;
            }
        }
    }

    public function makeUserCriteria()
    {
        if (! $this->validate()) {
            return false;
        }
        $data = [];
        if (! empty($this->name)) {
            $data['name'] = [
                'opr'   => 'like',
                'value' => $this->name
            ];
        }
        if (! empty($this->code)) {
            $data['code'] = [
                'opr'   => 'like',
                'value' => $this->code
            ];
        }
        if (! empty($this->type)) {
            $data['type'] = [
                'opr'   => '=',
                'value' => $this->type
            ];
        }
        if (! empty($this->description)) {
            $data['description'] = [
                'opr'   => 'like',
                'value' => $this->description
            ];
        }
        if (! empty($this->data)) {
            $data['data'] = [
                'opr'   => 'like',
                'value' => $this->data
            ];
        }
        if (! empty($this->rule_name)) {
            $data['last_name'] = [
                'opr'   => 'like',
                'value' => $this->rule_name
            ];
        }
        return $data;
    }

    public function makeOrder()
    {
        if (! $this->sort) {
            return false;
        }
        foreach ($this->sort as &$sort) {
            if ($sort == 'desc') {
                $sort = SORT_DESC;
            } else {
                $sort = SORT_ASC;
            }
        }
        return $this;
    }
}