<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 1/10/2019
 * Time: 2:20 PM
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrModel;
use Illuminate\Support\Facades\DB;
use Wms2\UserInfo\Data;
use Psr\Http\Message\ServerRequestInterface as Request;


class KittingController extends AbstractController
{
   protected $odrHdrModel;
   protected $odrDtlModel;

   public function __construct(
       OrderHdrModel $orderHdrModel,
       OrderDtlModel $orderDtlModel
   )
   {
       $this->odrHdrModel = $orderHdrModel;
       $this->odrDtlModel = $orderDtlModel;
   }

   public function allocateOrder(Request $request, $whsId) {
       try {
            $input = $request->getParsedBody();
            $input['whs_id'] = $whsId;

            $this->validateAllocate($input);
           $result = $this->odrHdrModel->AllocateOrderKit($input);
           if(!empty($result['is_error']) && $result['is_error'] == 1) {
               return $this->response->errorBadRequest($result['message']);
           }
           return [
               "data" => [
                   "item"       => $result,
                   "message"    => "Successful!"
               ]
           ];
       } catch (\Exception $e) {
           return $this->response->errorBadRequest($e->getMessage());
       }
   }

   protected function validateAllocate(&$attribute) {
       if(empty($attribute['odr_id'])) {
           return $this->response->errorBadRequest("Field Order id is required!");
       }

       $order = $this->odrHdrModel->getFirstWhere(['odr_id' => $attribute['odr_id']]);
       if(empty($order)) {
           return $this->response->errorBadRequest("Order does not exist!");
       }
       if($order->odr_sts != "NW") {
           return $this->response->errorBadRequest("Only New Order can Allocate!");
       }
       $attribute['odr_hdr'] = $order;

   }
}