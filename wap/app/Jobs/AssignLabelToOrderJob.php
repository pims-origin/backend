<?php

namespace App\Jobs;

use DB;
use GuzzleHttp\Client;
use App\Api\V1\Models\Log;
use Wms2\UserInfo\Data;

class AssignLabelToOrderJob extends Job
{
    const LABEL_UNIT_UOM     = "LAB";
    const REF_TYPE_ORDER     = "Order";
    const LABEL_DESCRIPTION  = "Label";
    const TYPE_WORK_ORDER    = "WO";
    const WORK_ORDER_STS_NEW = "New";
    const CURRENCY_DEFAULT   = "USD";

    protected $whsId;
    protected $odrNum;
    protected $label;
    protected $qty;
    protected $request;
    protected $delFlag;
    protected $url;
    protected $data;
    /**
     * AutoPackJob constructor.
     */
    public function __construct($whsId, $input, $request, $delFlag = false)
    {
        $this->whsId   = $whsId;
        $this->odrNum  = $input['odr_num'];
        $this->label   = $input['label'];
        $this->qty     = $input['qty'];
        $this->request = $request;
        $this->delFlag = $delFlag;
    }

    public function handle()
    {
        $this->url = "AssignLabelJob";
        try {
            $odrObj = DB::table('odr_hdr')
                ->where('odr_num', $this->odrNum)
                ->where('whs_id', $this->whsId)
                ->where('deleted', 0)
                ->first();
            $odrId = array_get($odrObj, 'odr_id');
            $cusId = array_get($odrObj, 'cus_id');
            $cusSac = DB::table('cus_sac')
                ->where('whs_id', $this->whsId)
                ->where('cus_id', $cusId)
                ->where('unit_uom', self::LABEL_UNIT_UOM)
                ->where('type', self::TYPE_WORK_ORDER)
                ->where('deleted', 0)
                ->first();
            $unitPrice = array_get($cusSac, 'price');
            $discount  = array_get($cusSac, 'discount');
            $amount = $this->qty * $unitPrice * (1 - $discount/100);
            $currency = $this->_getCurrencyByWhsCus($this->whsId, $cusId);
            if (!$currency) {
                $msg = sprintf("Not found currency of customer %s and warehouse %s", $cusId, $this->whsId);
                throw new \Exception($msg);
            }
            $data = [
                'whs_id' => $this->whsId,
                'cus_id' => array_get($odrObj, 'cus_id'),
                'work_order_num' => '',
                'sts' => self::WORK_ORDER_STS_NEW,
                'note' => '',
                'tax' => '',
                'sub_total' => $amount,
                'total' => $amount,
                'is_order' => true,
                'odr_id' => $odrId,
                'odr_num' => $this->odrNum,
                'is_rush' => false,
            ];

            $items = [[
                'checked'            => false,
                'ref_type'           => self::REF_TYPE_ORDER,
                'ref_num'            => $this->odrNum,
                'charge_item'        => $this->label,
                'charge_description' => self::LABEL_DESCRIPTION,
                'cus_sac_id'         => array_get($cusSac, 'cus_sac_id'),
                'sac_code'           => array_get($cusSac, 'sac'),
                'sac_description'    => array_get($cusSac, 'description'),
                'unit_uom'           => array_get($cusSac, 'unit_uom'),
                'qty'                => $this->qty,
                'unit_price'         => $unitPrice,
                'currency'           => $currency,
                'discount'           => $discount,
                'amount'             => $amount,
            ]];

            $this->data = $data['items'] = $items;

            $checkWorkOrder = DB::table('vas_hdr')
                ->where('whs_id', $this->whsId)
                ->where('cus_id', $cusId)
                ->where('odr_hdr_id', $odrId)
                ->where('deleted', 0)
                ->first();
            $vasId = array_get($checkWorkOrder, 'vas_id');
            $client = new Client();
            $authorization = $this->request->getHeader('Authorization');

            // if ($this->whsId != Data::getCurrentWhsId())
            // {
                $this->url = $url = 'user-metas/wid';
                $response = $client->request('PUT', env("API_AUTHENTICATION"). $url,
                    [
                        'headers'     => ['Authorization' => $authorization],
                        'form_params' => ["value" => $this->whsId],
                    ]
                );
            // }

            if (!count($checkWorkOrder))
            {
                // insert
                $msg = "Unable assign label when Work Order has not created yet.";
                throw new Exception($msg);
                // $url = '/v1/work-order';

                // $response = $client->request('POST', env("API_INVOICE_MASTER"). $url,
                //     [
                //         'headers'     => ['Authorization' => $authorization],
                //         'form_params' => $data,
                //     ]
                // );
            } else {
                $data['work_order_num'] = array_get($checkWorkOrder, 'vas_num');
                $data['note'] = array_get($checkWorkOrder, 'vas_note');
                $data['tax'] = array_get($checkWorkOrder, 'tax');
                $data['sub_total'] = array_get($checkWorkOrder, 'subtotal');
                $data['total'] = array_get($checkWorkOrder, 'amount_ttl');

                // update
                $vasDtlArrs = null;
                if (!$this->delFlag) {
                    // insert more
                    $vasDtlArrs = DB::table('vas_dtl')
                        ->where('vas_id', $vasId)
                        // ->whereNotNull('vas_item')
                        ->where('deleted', 0)
                        ->get();
                } else {
                    // delete
                    $items = [];
                    $vasDtlArrs = DB::table('vas_dtl')
                        ->where('vas_id', $vasId)
                        ->where('vas_item', '!=', $this->label)
                        ->where('deleted', 0)
                        ->get();
                }

                foreach ($vasDtlArrs as $vasDtlArr)
                {
                    $items[] = [
                        'checked'            => false,
                        'ref_type'           => self::REF_TYPE_ORDER,
                        'ref_num'            => array_get($vasDtlArr, 'ref_num'),
                        'charge_item'        => array_get($vasDtlArr, 'vas_item'),
                        'charge_description' => array_get($vasDtlArr, 'vas_description'),
                        'cus_sac_id'         => array_get($vasDtlArr, 'cus_sac_id'),
                        'sac_code'           => array_get($cusSac, 'sac'),
                        'sac_description'    => array_get($cusSac, 'description'),
                        'unit_uom'           => array_get($cusSac, 'unit_uom'),
                        'qty'                => array_get($vasDtlArr, 'qty'),
                        'unit_price'         => array_get($vasDtlArr, 'unit_price'),
                        'currency'           => array_get($vasDtlArr, 'currency'),
                        'discount'           => array_get($vasDtlArr, 'discount'),
                        'amount'             => array_get($vasDtlArr, 'amount'),
                    ];
                }
                $this->data = $data['items'] = $items;
                $this->url = $url = '/v1/work-order/'. $vasId;

                $response = $client->request('PUT', env("API_INVOICE_MASTER"). $url,
                    [
                        'headers'     => ['Authorization' => $authorization],
                        'form_params' => $data,
                    ]
                );
            }

            $owner = $transaction = "";
            Log:: info($this->request, $this->whsId, [
                'evt_code'     => 'OLL',
                'owner'        => $owner,
                'transaction'  => $transaction,
                'url_endpoint' => $url,
                'message'      => 'Assign Label To Order by AssignLabelToOrderJob. '. json_encode($data)
            ]);

        }catch (\Exception $exception) {

            $dataError = [
                'date'     => time(),
                'api_name' => "WAP - Assign Label to Order",
                'error'    => $exception->getMessage(),
            ];

            DB::table('sys_bugs')->insert($dataError);

            $owner = $transaction = "";
            Log:: info($this->request, $this->whsId, [
                'evt_code'     => 'ERR',
                'owner'        => $owner,
                'transaction'  => $transaction,
                'url_endpoint' => $this->url,
                'message'      => 'Assign Label To Order by AssignLabelToOrderJob. '. json_encode($this->data)
            ]);
        }
    }

    private function _getCurrencyByWhsCus($whsId, $cusId)
    {
        $qualifier = 'CUR';
        $cusMeta = DB::table('cus_meta')
            ->where('cus_id', $cusId)
            ->where('qualifier', $qualifier)
            ->where('deleted', 0)
            ->first();
        $data = array_get($cusMeta, 'value');
        if (!count($data)) { return self::CURRENCY_DEFAULT;}

        $data = \GuzzleHttp\json_decode($data, true);

        foreach ($data as $warehouse)
        {
            if (array_get($warehouse, 'whs_id') == $whsId)
            {
                return array_get($warehouse, 'currency');
            }
        }

        return self::CURRENCY_DEFAULT;

    }
}
