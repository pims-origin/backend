<?php

namespace Seldat\Wms2\Models;

/**
 * Class ReturnHdr
 *
 * @package Seldat\Wms2\Models
 */
class ReturnHdr extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'return_hdr';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'return_id';

    /**
     * @var array
     */
    protected $fillable = [
        'return_num',
        'odr_hdr_num',
        'odr_hdr_id',
        'csr',
        'putter',
        'return_sts',
        'return_qty',
        'sts'
    ];

    public function odrHdr()
    {
        return $this->belongsTo(__NAMESPACE__ . '\OrderHdr', 'odr_hdr_id', 'odr_id');
    }


    public function csrUser()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'csr');
    }

    public function putterUser()
    {
        return $this->hasOne(__NAMESPACE__ . '\User', 'user_id', 'putter');
    }

    public function createdUser()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'created_by', 'user_id');
    }

    public function returnDetail()
    {
        return $this->hasMany(__NAMESPACE__ . '\ReturnDtl', 'return_id', 'return_id');
    }
}
