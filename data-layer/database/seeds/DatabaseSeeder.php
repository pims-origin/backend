<?php

use Illuminate\Database\Seeder;
use App\SystemCountry;
use App\SystemState;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = [
            ["sys_country_code" => "CA", "sys_country_name" => "Canada"],
            ["sys_country_code" => "US", "sys_country_name" => "USA"],
        ];

        $countryCanada = SystemCountry::create($countries[0]);
        $countryUs = SystemCountry::create($countries[1]);

        $stateUs = [
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'AL',
                'sys_state_name'       => 'Alabama',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'AK',
                'sys_state_name'       => 'Alaska',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'AZ',
                'sys_state_name'       => 'Arizona',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'AR',
                'sys_state_name'       => 'Arkansas',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'CA',
                'sys_state_name'       => 'California',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'CO',
                'sys_state_name'       => 'Colorado',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'CT',
                'sys_state_name'       => 'Connecticut',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'DE',
                'sys_state_name'       => 'Delaware',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'FL',
                'sys_state_name'       => 'Florida',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'GA',
                'sys_state_name'       => 'Georgia',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'HI',
                'sys_state_name'       => 'Hawaii',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'ID',
                'sys_state_name'       => 'Idaho',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'IL',
                'sys_state_name'       => 'Illinois',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'IN',
                'sys_state_name'       => 'Indiana',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'IA',
                'sys_state_name'       => 'Iowa',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'KS',
                'sys_state_name'       => 'Kansas',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'KY',
                'sys_state_name'       => 'Kentucky',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'LA',
                'sys_state_name'       => 'Louisiana',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'ME',
                'sys_state_name'       => 'Maine',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'MD',
                'sys_state_name'       => 'Maryland',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'MA',
                'sys_state_name'       => 'Massachusetts',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'MI',
                'sys_state_name'       => 'Michigan',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'MN',
                'sys_state_name'       => 'Minnesota',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'MS',
                'sys_state_name'       => 'Mississippi',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'MO',
                'sys_state_name'       => 'Missouri',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'MT',
                'sys_state_name'       => 'Montana',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'NE',
                'sys_state_name'       => 'Nebraska',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'NV',
                'sys_state_name'       => 'Nevada',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'NH',
                'sys_state_name'       => 'New Hampshire',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'NJ',
                'sys_state_name'       => 'New Jersey',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'NM',
                'sys_state_name'       => 'New Mexico',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'NY',
                'sys_state_name'       => 'New York',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'NC',
                'sys_state_name'       => 'North Carolina',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'ND',
                'sys_state_name'       => 'North Dakota',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'OH',
                'sys_state_name'       => 'Ohio',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'OK',
                'sys_state_name'       => 'Oklahoma',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'OR',
                'sys_state_name'       => 'Oregon',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'PA',
                'sys_state_name'       => 'Pennsylvania',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'RI',
                'sys_state_name'       => 'Rhode Island',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'SC',
                'sys_state_name'       => 'South Carolina',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'SD',
                'sys_state_name'       => 'South Dakota',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'TN',
                'sys_state_name'       => 'Tennessee',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'TX',
                'sys_state_name'       => 'Texas',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'UT',
                'sys_state_name'       => 'Utah',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'VT',
                'sys_state_name'       => 'Vermont',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'VA',
                'sys_state_name'       => 'Virginia',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'WA',
                'sys_state_name'       => 'Washington',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'WV',
                'sys_state_name'       => 'West Virginia',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'WI',
                'sys_state_name'       => 'Wisconsin',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryUs->sys_country_id,
                'sys_state_code'       => 'WY',
                'sys_state_name'       => 'Wyoming',
                'sys_state_desc'       => ''
            ],
        ];

        foreach ($stateUs as $state) {
            SystemState::create($state);
        }

        $stateCanada = [
            [
                'sys_state_country_id' => $countryCanada->sys_country_id,
                'sys_state_code'       => 'AB',
                'sys_state_name'       => 'Alberta',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryCanada->sys_country_id,
                'sys_state_code'       => 'BC',
                'sys_state_name'       => 'British Columbia',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryCanada->sys_country_id,
                'sys_state_code'       => 'MB',
                'sys_state_name'       => 'Manitoba',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryCanada->sys_country_id,
                'sys_state_code'       => 'NB',
                'sys_state_name'       => 'New Brunswick',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryCanada->sys_country_id,
                'sys_state_code'       => 'NL',
                'sys_state_name'       => 'Newfoundland and Labrador',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryCanada->sys_country_id,
                'sys_state_code'       => 'NS',
                'sys_state_name'       => 'Nova Scotia',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryCanada->sys_country_id,
                'sys_state_code'       => 'ON',
                'sys_state_name'       => 'Ontario',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryCanada->sys_country_id,
                'sys_state_code'       => 'PE',
                'sys_state_name'       => 'Prince Edward Island',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryCanada->sys_country_id,
                'sys_state_code'       => 'QC',
                'sys_state_name'       => 'Quebec',
                'sys_state_desc'       => ''
            ],
            [
                'sys_state_country_id' => $countryCanada->sys_country_id,
                'sys_state_code'       => 'SK',
                'sys_state_name'       => 'Saskatchewan',
                'sys_state_desc'       => ''
            ],


        ];

        foreach ($stateCanada as $state) {
            SystemState::create($state);
        }

        $this->command->info('Seed DB data successfully.');
    }
}
