<?php

return [
    'vas_hdr_sts'           => [
        'NEW'   => 'NW',
        'FINAL' => 'FN',
    ],
    'vas_ref_type'          => [
        'Goods Receipt' => 'GR',
        'Order'         => 'ORD',
        'Wavepick'      => 'WV',
        'Cycle Count'   => 'CYC',
        'Other'         => 'OTR',
    ],
    'BILL_ABLE_ITEM'        => [
        'CHARGE_CODE_TYPE' => 'PB',
        'BILL_ABLE_TYPE'   => 'PB',
        'BILL_ABLE_PERIOD' => 'DAY',
        'UNIT_UOM_CT'      => 'CTN',
        'UNIT_UOM_PL'      => 'PL',
        'UNIT_UOM_PC'      => 'PC',
        'UNIT_UOM_UNI'     => 'UNI',
        'CHARGE_TYPE_PB'   => 'PB',
    ],
    'charge_type_name'      => [
        'INB' => 'INBOUND',
        'OUB' => 'OUTBOUND',
        'STR' => 'STORAGE',
        'PB'  => 'PUTBACK',
        'WO'  => 'WORK-ORDER',
        'RET' => 'RETURNS',
        'TRA' => 'TRANSFER',
        'NA'  => 'NA',
    ],
    'unit_uom_name'         => [
        "CTNR" => "Container",
        "PL"   => "Pallet",
        "CTN"  => "Carton",
        "PC"   => "Piece",
        "UN"   => "Unit",
        "BG"   => "Bag",
        "TUB"  => "Tube",
        "CUB"  => "Cube",
        "PCK"  => "Pack",
        "KG"   => "Kg",
        "WK"   => "Week",
        "HR"   => "Hour",
        "DAY"  => "Day",
        "LAB"  => "Labor",
        "UNI"  => "Unit",
        "ORD"  => "Order",
        "BOL"  => "Bol",
        'MON' => 'Month',
        'VOLM' => 'Volumetric',
        'NA'   => 'NA'
    ],
    'PERIOD_UOM_NAME'       => [
        'QTY'  => 'Quantity',
        'PERI' => 'Period',
        'VOLM' => 'Volumetric',
        'LABO' => 'LABOR',
        'WEI' => 'WEIGHT',
        'SKU' => 'SKU',
        'KACF' => 'KACF',
        'KUCF' => 'KUCF',
        'KSCF' => 'KSCF',
        'ITCF' => 'ITCF',
        'IH'   => 'IH',  
    ],
    'INV_STATUS_NAME'       => [
        'NE' => 'New',
        'PE' => 'Pending',
        'RE' => 'Rejected',
        'AP' => 'Approved',
        'PA' => 'Paid',
        'OV' => 'Overdue',
    ],
    'INV_EMAIL_STATUS_NAME' => [
        '1' => 'Not sent',
        '2' => 'Sent',
    ],

    'evt_tracking_code' => [
        'NE' => "INN",
        'AP' => "INA",
        'RE' => "INR",
        'PE' => "INE",
        'PA' => "INP",
        'OV' => "INO",
        'ER' => "INF" // Invoice Error
    ],
    'evt_tracking_info' => [
        'NE' => "%s was created",
        'AP' => "%s was approved",
        'RE' => "%s was re-ject",
        'PE' => "%s was pending",
        'PA' => "%s was paid",
        'OV' => "%s was overdue",
        'ER' => "%s was not notified via mail due to %s",
    ],
];
