<?php
/**
 * Created by PhpStorm.
 * User: vinhpham(luan.pham)
 * Date: 8/4/16
 * Time: 4:42 PM
 */
return [
    'rolePermission' => [
        "viewItem"              => [
            ['viewItem'],
            ['viewASN'],
            ['editASN'],
            ['createASN'],
            ['viewGoodsReceipt'],
            ['editGoodsReceipt'],
            ['createGoodsReceipt'],
            ["viewCustomer"],
            ["createXFER"],
            ["editXFER"],
            ['editOrder'],
            ['createOrder'],
            ["addSKUCycleCount"],
            ["recountCycleCount"],
            ["approveCycleCount"],
            ["deleteCycleCount"]
        ],
        "createItem"            => ["createItem"],
        "editItem"              => ["editItem"],
        "deleteItem"            => ["deleteItem"],
        "viewUser"              => ["viewUser"],
        "createUser"            => ["createUser"],
        "editUser"              => ["editUser"],
        "deleteUser"            => ["deleteUser"],
        "viewRole"              => ["viewRole"],
        "createRole"            => ["createRole"],
        "editRole"              => ["editRole"],
        "deleteRole"            => ["deleteRole"],
        "viewASN"               => [
            ["viewASN"],
            ["createASN"],
            ["editASN"]
        ],
        "createASN"             => ["createASN"],
        "editASN"               => [
            ["editASN"],
            ["createASN"]
        ],
        "deleteASN"             => ["deleteASN"],
        "viewGoodsReceipt"      => [
            ["viewGoodsReceipt"],
            ["createGoodsReceipt"],
            ["editGoodsReceipt"],
            ["updateInventory"]

        ],
        "createGoodsReceipt"    => ["createGoodsReceipt"],
        "editGoodsReceipt"      => [
            ["editGoodsReceipt"],
            ["createGoodsReceipt"]
        ],
        "deleteGoodsReceipt"    => ["deleteGoodsReceipt"],
        "viewWarehouse"         => [
            ["viewWarehouse"],
            ["createCustomer"],
            ["editCustomer"],
            ["viewCustomer"],
            ['viewASN'],
            ['editASN'],
            ['createASN']
        ],
        "createWarehouse"       => ["createWarehouse"],
        "editWarehouse"         => ["editWarehouse"],
        "deleteWarehouse"       => ["deleteWarehouse"],
        "relocation"            => ["relocation"],
        "consolidate"           => [
            ["consolidate"],
            ["viewLocation"]
        ],
        "viewCustomer"          => [
            ["viewCustomer"],
            ['viewItem'],
            ['editItem'],
            ['createItem'],
            ['viewASN'],
            ['editASN'],
            ['createASN'],
            ['viewGoodsReceipt'],
            ['editGoodsReceipt'],
            ['createGoodsReceipt'],
            ['updateInventory'],
            ['viewPutaway'],
            ['viewOrder'],
            ['createOrder'],
            ['editOrder'],
            ['viewWorkOrder'],
            ["createXFER"],
            ['viewXFER'],
            ["createBOL"],
            ["editBOL"],
            ["viewBOL"],
            ["importOnlineOrder"],
            ['createWavePick'],
            ['palletAssignment'],
            ["viewPutbackReceipt"],
            ["createPutbackReceipt"],
            ["updatePutback"]
        ],
        "createCustomer"        => ["createCustomer"],
        "editCustomer"          => ["editCustomer"],
        "deleteCustomer"        => ["deleteCustomer"],
        "viewLocation"          => [
            ["viewLocation"],
            ["relocation"],
            ["consolidate"],
            ["editZone"],
            ["createZone"],
            ["editZone"],
            ["viewWarehouse"],
            ["updateInventory"],
            ["editCustomer"],
            ["createCustomer"],
            ["viewCustomer"],
            ["createXFER"],
            ["editXFER"],
            ["createCycleCount"],
            ["approveCycleCount"],
            ["updateOrderPicking"]

        ],
        "createLocation"        => [
            ["createLocation"],
            ["editZone"],
            ["createZone"]
        ],
        "editLocation"          => ["editLocation"],
        "deleteLocation"        => ["deleteLocation"],
        "changeLocation"        => ["changeLocation"],
        "importLocation"        => ["importLocation"],
        "changeLocationStatus"  => ["changeLocationStatus"],
        "viewZone"              => [
            ["viewZone"],
            ["viewLocation"],
            ["editLocation"],
            ["createLocation"],
            ["editCustomer"],
            ["createCustomer"],
            ["viewCustomer"],

        ],
        "createZone"            => ["createZone"],
        "editZone"              => ["editZone"],
        "deleteZone"            => ["deleteZone"],
        "viewZoneType"          => [
            ["viewZoneType"],
            ["editZone"],
            ["createZone"],
            ["editZone"],
        ],
        "createZoneType"        => ["createZoneType"],
        "editZoneType"          => ["editZoneType"],
        "deleteZoneType"        => ["deleteZoneType"],
        "viewPutaway"           => [
            ["viewPutaway"],
            ["putawayAssignment"],
            ["updateInventory"]
        ],
        "updateInventory"       => ["updateInventory"],
        "viewChargeType"        => [
            ["viewChargeType"],
            ['editZone'],
            ['createZone'],
            ['viewChargeCode'],
            ['createChargeCode'],
            ['editChargeCode'],
            ["createCustomer"],
            ["editCustomer"]
        ],
        "createChargeType"      => ["createChargeType"],
        "editChargeType"        => ["editChargeType"],
        "deleteChargeType"      => ["deleteChargeType"],
        "viewChargeCode"        => [
            ["viewChargeCode"],
            ["createCustomer"],
            ["editCustomer"]
        ],
        "createChargeCode"      => ["createChargeCode"],
        "editChargeCode"        => ["editChargeCode"],
        "deleteChargeCode"      => ["deleteChargeCode"],
        "viewDamageType"        => [
            ["viewDamageType"],
            ['editGoodsReceipt'],
            ['createGoodsReceipt']
        ],
        "createDamageType"      => ["createDamageType"],
        "editDamageType"        => ["editDamageType"],
        "deleteDamageType"      => ["deleteDamageType"],
        "viewUOM"               => [
            ["viewUOM"],
            ['viewItem'],
            ['editItem'],
            ['createItem'],
            ['viewChargeCode'],
            ['createChargeCode'],
            ['editChargeCode'],
            ['viewGoodsReceipt'],
            ['editGoodsReceipt'],
            ['createGoodsReceipt'],
            ["createCustomer"],
            ["editCustomer"],
            ['createOrder'],
            ['editOrder']
        ],
        "createUOM"             => ["createUOM"],
        "editUOM"               => ["editUOM"],
        "deleteUOM"             => ["deleteUOM"],
        "viewState"             => [
            ["viewState"],
            ['createWarehouse'],
            ['editWarehouse'],
            ['editCustomer'],
            ['createCustomer']
        ],
        "createState"           => ["createState"],
        "editState"             => ["editState"],
        "deleteState"           => ["deleteState"],
        "viewCountry"           => [
            ["viewCountry"],
            ['createWarehouse'],
            ['editWarehouse'],
            ['editCustomer'],
            ['createCustomer'],
            ["viewState"],
            ["createState"],
            ["editState"],
        ],
        "createCountry"         => ["createCountry"],
        "editCountry"           => ["editCountry"],
        "deleteCountry"         => ["deleteCountry"],
        "viewLocationType"      => [
            ["viewLocationType"],
            ["viewLocation"],
            ["editLocation"],
            ["createLocation"],
            ["changeLocation"]
        ],
        "createLocationType"    => ["createLocationType"],
        "editLocationType"      => ["editLocationType"],
        "deleteLocationType"    => ["deleteLocationType"],
        "viewPallet"            => [["relocation"], ["consolidate"]],
        "inboundEventTracking"  => [
            ['inboundEventTracking'],
            ["viewASN"],
            ["viewGoodsReceipt"],
            ['editOrder'],
            ['viewOrder'],
            ['viewXFER']
        ],
        "viewContainer"         => ['viewASN'],
        "saveAssignPallet"      => [['createGoodsReceipt'], ['editGoodsReceipt']],
        "viewAssignPallet"      => [['viewGoodsReceipt'], ['viewASN']],
        'viewOrder'             => [
            ['viewOrder'],
            ['editOrder'],
            ['createOrder'],
            ['assignCSR'],
            ['orderShipping'],
            ['palletAssignment'],
            ['orderPacking'],
            ['cancelOrder'],
            ['packingSlip'],
            ['importOnlineOrder'],
            ['allocateOrder'],
            ['createWavePick'],
            ['orderPacking'],
            ['createBOL'],
            ['editBOL'],
            ["viewPutbackReceipt"],
            ["createPutbackReceipt"],
            ["updatePutback"]

        ],
        'editOrder'             => ['editOrder'],
        "importOnlineOrder"     => ["importOnlineOrder"],
        'createOrder'           => [['createOrder'], ['editOrder']],
        'createWavePick'        => [['createWavePick'], ['updateOrderPicking'], ["wavepickAssignment"]],
        'assignCSR'             => [['assignCSR'], ['allocateOrder']],
        'updateOrderPicking'    => [['updateOrderPicking'], ["wavepickAssignment"]],
        'cancelOrder'           => [
            ['cancelOrder'],
            ['viewOrder'],
            ["viewPutbackReceipt"],
            ["createPutbackReceipt"],
            ["updatePutback"]
        ],
        'holdOrder'             => ['holdOrder'],
        'orderShipping'         => [['orderShipping'], ['editBOL'], ['createBOL']],
        'orderPacking'          => [['orderPacking'], ['viewOrder']],
        'palletAssignment'      => ['palletAssignment'],
        'allocateOrder'         => ['allocateOrder'],
        'outboundEventTracking' => [
            ['outboundEventTracking'],
            ["viewOrder"],
            ['editOrder'],
        ],
        'viewWorkOrder'         => [['viewWorkOrder'], ['editWorkOrder'], ['createWorkOrder']],
        'editWorkOrder'         => [['editWorkOrder'], ['createWorkOrder']],
        'createWorkOrder'       => [['createWorkOrder'], ['editWorkOrder']],

        "viewCycleCount"          => ["viewCycleCount"],
        "createCycleCount"        => ["createCycleCount"],
        "addSKUCycleCount"        => ["addSKUCycleCount"],
        "recountCycleCount"       => ["recountCycleCount"],
        "approveCycleCount"       => ["approveCycleCount"],
        "deleteCycleCount"        => ["deleteCycleCount"],
        "printCycleCount"         => ["printCycleCount"],
        "createXFER"              => ["createXFER"],
        "editXFER"                => ["editXFER"],
        "viewXFER"                => [
            ["viewXFER"],
            ["createXFER"],
            ["editXFER"]
        ],
        "packingSlip"             => [
            ['packingSlip']
        ],
        "createBOL"               => ["createBOL"],
        "editBOL"                 => ["editBOL"],
        "viewBOL"                 => [
            ["viewBOL"],
            ["createBOL"],
            ["editBOL"]
        ],
        "putawayAssignment"       => [
            ["putawayAssignment"]
        ],
        "wavepickAssignment"      => [
            ["wavepickAssignment"],
        ],
        "viewReport"              => ["viewReport"],
        "viewAvailableInventory"  => ["viewAvailableInventory"],
        "viewInventory"           => ["viewInventory"],
        "viewInventoryHistory"    => ["viewInventoryHistory"],
        "viewStyleLocations"      => ["viewStyleLocations"],
        "viewWarehouseLocations"  => [["viewWarehouseLocations"], ["updateOrderPicking"]],
        "viewPerformanceActivity" => ["viewPerformanceActivity"],
        "createPutbackReceipt"    => [
            ["createPutbackReceipt"]
        ],
        "assignPalletPutback"     => [
            ["assignPalletPutback"]
        ],
        "viewPutbackReceipt"      => [
            ["viewPutbackReceipt"],
            ["createPutbackReceipt"],
            ["updatePutback"]
        ],
        "printPutback"            => [
            ["printPutback"],
        ],
        "updatePutback"           => [
            ["updatePutback"]
        ],
        "viewCommodity"           => [
            ['viewCommodity'],
            ['createCommodity'],
            ['editCommodity']
        ],
        "createCommodity"         => [
            ["createCommodity"],
            ["editCommodity"],
            ["createBOL"]
        ],
        "editCommodity"           => [
            ["createCommodity"],
            ["editCommodity"],
            ["createBOL"]
        ],
        "viewCartonDimension"     => [
            ['viewCartonDimension'],
            ['createCartonDimension'],
            ['editCartonDimension']
        ],
        "createCartonDimension"   => [
            ["createCartonDimension"],
            ["editCartonDimension"],
            ["createBOL"],
            ['orderPacking']
        ],
        "editCartonDimension"     => [
            ["editCartonDimension"],
            ["createCartonDimension"],
            ["createBOL"],
            ['orderPacking']
        ],


    ]
];
