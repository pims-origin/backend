<?php
namespace App\Api\V1\Controllers;

use App\Api\V1\Models\AuthenticationService;
use App\Api\V1\Models\StyleLocationsModel;
use App\Api\V1\Transformers\StyleLocationsTransformer;
use Psr\Http\Message\ServerRequestInterface as Request;

class StyleLocationsController extends AbstractController
{

    /*
    ****************************************************************************
    */

    public function __construct(Request $request)
    {
        $object = new AuthenticationService($request);

        parent::__construct($request, $object);

        $this->model = new StyleLocationsModel();
        $this->transformer = new StyleLocationsTransformer();
    }

    /*
    ****************************************************************************
    */

}
