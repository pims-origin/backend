<?php


namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Carton;

class LpnACartonTransformer extends TransformerAbstract
{
    //return result
    public function transform(Carton $carton)
    {
        return [
            'ctn_id' => object_get($carton,'ctn_id'),
            'ctn_num' => object_get($carton,'ctn_num'),
            'loc_id' => object_get($carton,'is_lpn') == 1 ? object_get($carton,'lpn_carton') : object_get($carton,'loc_id'),
            'loc_name' => object_get($carton,'loc_code'),
            'lpn_carton' => object_get($carton,'lpn_carton'),
            'is_lpn'  => object_get($carton,'is_lpn'),
        ];
    }

}
