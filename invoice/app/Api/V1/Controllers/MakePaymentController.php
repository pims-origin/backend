<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\InvoiceHeaderModel;
use App\Api\V1\Transformers\MakePaymentTransformer;
use App\Api\V1\Validators\MakePaymentValidator;

use Psr\Http\Message\ServerRequestInterface as IRequest;
use App\Api\V1\Models\AuthenticationService;
use Seldat\Wms2\Utils\JWTUtil;
use Psr\Http\Message\ServerRequestInterface as Request;

class MakePaymentController extends AbstractController
{
    /**
    * @var
    */
    protected $invHdrModel;
    protected $makePaymentTransformer;
    protected $makePaymentValidator;

    /**
    *  Constructor
    */

    public function __construct(IRequest $request)
    {
//        $object = new AuthenticationService($request);
//
//        parent::__construct($request, $object);

        $this->invHdrModel = new InvoiceHeaderModel();
        $this->makePaymentTransformer = new MakePaymentTransformer();
        $this->makePaymentValidator = new MakePaymentValidator();
    }

    /**
     * Update invoice Payment
     *
     * @param IRequest $request
     *
     * @return array
     */

    public function receivePayment($inv_num, Request $request)
    {
        // cancel payment if no $paymentInfo is sent
        $paymentInfo = $request->getParsedBody();

        //get userID
        $userID = JWTUtil::getPayloadValue('jti') ?: 0;

        if (! $userID) {
            return;
        }

        $params = [
            'action' => $paymentInfo ? 'makePayment' : 'cancelPayment',
            'date' => getDefault($paymentInfo['inv_paid_dt'], NULL),
            'type' => getDefault($paymentInfo['inv_paid_typ'], NULL),
            'reference' => getDefault($paymentInfo['inv_paid_ref'], NULL),
            'invoice' => $inv_num,
            'userID' => $userID
        ];

        if ($params['action'] == 'makePayment') {
            $this->makePaymentValidator->validate($params);
        }

        $status = $params['action'] == 'makePayment' ? 1 : 0;
        $invoiceStatus = $params['action'] == 'makePayment' ? 'p' : 'i';

        $invID = $this->invHdrModel->getByInvoiceNumber($params['invoice']);

        $data = [
            'inv_id' => $invID,
            'inv_num' => $params['invoice'],
            'inv_sts' => $invoiceStatus,
            'inv_paid_dt' => $params['date'],
            'inv_paid_sts' => $status,
            'inv_paid_typ' => $params['type'],
            'inv_paid_ref' => $params['reference'],
            'sts' => 'u'
        ];

        //change later if we want to return something after update
        $return = (object) $data;

        //update data
        try {

            $results = $this->invHdrModel->updateInvoicePayment($data);

            if ($results) {
                return $this->response->item($return, $this->makePaymentTransformer);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

}