<?php
/**
 * Created by PhpStorm.
 * User: Long Do
 * Date: 07-14-2017
 * Time: 13:48
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\ItemHier;

class ItemHierModel extends AbstractModel
{

    /**
     * ItemModel constructor.
     *
     * @param Item|null $model
     */
    public function __construct(ItemHier $model = null)
    {
        $this->model = ($model) ?: new ItemHier();
    }

    public function deleteItem($itemId)
    {
        return $this->model
            ->where('item_id', $itemId)
            ->delete();
    }
}
