<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 03-Mar-2017
 * Time: 09:27
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Seldat\Wms2\Models\GoodsReceiptDetail;
use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Models\OrderDtl;
use Seldat\Wms2\Models\CycleHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Api\V1\Controllers\Paginator;

class GoodsReceiptDetailModel extends AbstractModel
{
    /**
     * GoodsReceiptDetailModel constructor.
     *
     * @param GoodsReceiptDetail|null $model
     */
    public function __construct(GoodsReceiptDetail $model = null)
    {
        $this->model = ($model) ?: new GoodsReceiptDetail();
    }


    /**
     * @param $whs_id
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($whs_id, $attributes = [], $with = [], $limit = null, $export = false)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        //$query = $this->make([])
        $query = (new GoodsReceiptDetail())->getModel()
            ->join('gr_hdr', 'gr_hdr.gr_hdr_id', '=', 'gr_dtl.gr_hdr_id')
            ->join('customer', 'customer.cus_id', '=', 'gr_hdr.cus_id')
            ->join('users as user', 'user.user_id', '=', 'gr_hdr.created_by')
            ->join('users as updatedBy', 'updatedBy.user_id', '=', 'gr_hdr.updated_by')
            ->join('asn_hdr', 'asn_hdr.asn_hdr_id', '=', 'gr_hdr.asn_hdr_id')
            ->join('item', 'item.item_id', '=', 'gr_dtl.item_id')

            ->select([
                //Gr hdr
                'gr_hdr.cus_id',
                'customer.cus_name',
                'customer.cus_code',
                'gr_hdr.gr_hdr_num',
                'gr_hdr.ref_code',
                //Gr Detail
                'gr_dtl.gr_hdr_id',
                'gr_dtl.ctnr_id',
                'gr_dtl.ctnr_num',
                'gr_dtl.gr_dtl_plt_ttl',
                'gr_hdr.gr_hdr_act_dt as updated_at',

                DB::RAW("CONCAT(user.first_name,' ',user.last_name) as created_by"),

                DB::RAW("CONCAT(updatedBy.first_name,' ',updatedBy.last_name) as updated_by"),

                'gr_dtl.created_at',
                'gr_dtl.item_id',
                'gr_dtl.sku',
                'gr_dtl.size',
                'gr_dtl.color',
                'gr_dtl.lot',
                'gr_dtl.upc',

                DB::RAW("(gr_dtl.gr_dtl_act_ctn_ttl * gr_dtl.pack) as gr_qty"),

                DB::RAW("(gr_dtl.crs_doc * gr_dtl.pack) as crs_doc_qty"),

                'gr_dtl.pack',
                DB::RAW("(gr_dtl.gr_dtl_dmg_ttl * gr_dtl.pack) as gr_dtl_dmg_qty"),
                'gr_dtl.gr_dtl_dmg_ttl',

                'gr_dtl.gr_dtl_ept_ctn_ttl',
                'gr_dtl.gr_dtl_act_ctn_ttl',

                'asn_hdr.asn_hdr_ept_dt',
                'asn_hdr.asn_hdr_act_dt',
                'item.length',
                'item.width',
                'item.height',
                'gr_dtl.weight',
                'gr_dtl.volume',
                DB::RAW("(round((gr_dtl.gr_dtl_act_ctn_ttl * (item.length*item.width*item.height))/1728, 2)) as cube"),

            ]);

        //$query->join('gr_hdr', 'gr_dtl.gr_hdr_id', '=', 'gr_hdr.gr_hdr_id');
        //$query->join('customer', 'customer.cus_id', '=', 'gr_hdr.cus_id');

        $query->where('gr_hdr.deleted', 0)
            ->where('customer.deleted', 0);

        $query->where('gr_hdr.whs_id', $whs_id);
        if (isset($attributes['gr_hdr_num'])) {
            $query->where('gr_hdr.gr_hdr_num', 'like', "%" . SelStr::escapeLike($attributes['gr_hdr_num']) . "%");
        }
        if (isset($attributes['gr_sts'])) {
            $query->where('gr_hdr.gr_sts', $attributes['gr_sts']);
        }


        // Search Customer
        if (isset($attributes['cus_id'])) {
            $query->where('gr_hdr.cus_id', $attributes['cus_id']);
        }
        if (isset($attributes['ref_code'])) {
            $query->where('gr_hdr.ref_code', 'like', "%" . SelStr::escapeLike($attributes['ref_code']) . "%");
        }


        if (isset($attributes['ctnr_num'])) {
            $query->where('gr_hdr.ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
        }

        if (isset($attributes['sku'])) {
            $query->where('gr_dtl.sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
        }

        //search more fields from Sku auto complete
        if (isset($attributes['item_id'])) {
            $query->where('gr_dtl.item_id', $attributes['item_id']);
        }

        if (isset($attributes['lot'])) {
            $query->where('gr_dtl.lot', $attributes['lot']);
        }

        //search according to from date to date
        if (!empty($attributes['updated_at_from'])) {

            $query->where("gr_hdr.gr_hdr_act_dt",">=",strtotime($attributes['updated_at_from']));

        }

        if (!empty($attributes['updated_at_to'])) {

            $query->where("gr_hdr.gr_hdr_act_dt","<",strtotime($attributes['updated_at_to']) + 84600);
        }

        //sort
        if (isset($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $val) {
                if ($key === 'cus_name') {
                    $attributes['sort']['customer.cus_name'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'gr_hdr_num') {
                    $attributes['sort']['gr_hdr.gr_hdr_num'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'ctnr_num') {
                    $attributes['sort']['gr_dtl.ctnr_num'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'ref_code') {
                    $attributes['sort']['gr_hdr.ref_code'] = $val;
                    unset($attributes['sort'][$key]);
                }

            }
        }

        // Get
        $this->sortBuilder($query, $attributes);
        if ($export) {
            return $models = $query->get();
        } else {
            //$models = $query->paginate($limit);

            $modelsFirsts = $query->get()->toArray();
            $arrItems = [];
            if ($modelsFirsts) {
                foreach ($modelsFirsts as $modelsFirsts) {
                    $arrItems[] = [
                        //'cus_name'      => array_get($modelsFirsts, 'cus_name', null),
                        //Gr hdr
                        'cus_id'         => array_get($modelsFirsts, 'cus_id', null),
                        'cus_name'       => array_get($modelsFirsts, 'cus_name', null),
                        'cus_code'       => array_get($modelsFirsts, 'cus_code', null),
                        'gr_hdr_num'     => array_get($modelsFirsts, 'gr_hdr_num', null),
                        'ref_code'       => array_get($modelsFirsts, 'ref_code', null),
                        //Gr Detail
                        'gr_hdr_id'      => array_get($modelsFirsts, 'gr_hdr_id', null),
                        'ctnr_id'        => array_get($modelsFirsts, 'ctnr_id', null),
                        'ctnr_num'       => array_get($modelsFirsts, 'ctnr_num', null),
                        //'updated_at'     => $this->dateFormat(array_get($modelsFirsts, 'gr_hdr_act_dt', '')),
                        'updated_at'     => array_get($modelsFirsts, 'updated_at', '') ?
                            date("m/d/Y", array_get($modelsFirsts, 'updated_at', '')) : '',
                        'gr_hdr_act_dt'     => array_get($modelsFirsts, 'updated_at', '') ?
                            date("m/d/Y", array_get($modelsFirsts, 'updated_at', '')) : '',
                        'created_by'     => trim(array_get($modelsFirsts, "created_by", null)),
                        'updated_by'     => trim(array_get($modelsFirsts, "updated_by", null)),
                        'item_id'        => array_get($modelsFirsts, 'item_id', null),
                        'sku'            => array_get($modelsFirsts, 'sku', null),
                        'size'           => array_get($modelsFirsts, 'size', null),
                        'color'          => array_get($modelsFirsts, 'color', null),
                        'lot'            => array_get($modelsFirsts, 'lot', null),
                        'upc'            => array_get($modelsFirsts, 'upc', null),
                        'gr_qty'         => array_get($modelsFirsts, 'gr_qty', null),
                        'crs_doc_qty'    => array_get($modelsFirsts, 'crs_doc_qty', 0),
                        'pack_size'      => array_get($modelsFirsts, 'pack', 0),
                        'gr_dtl_dmg_qty' => array_get($modelsFirsts, 'gr_dtl_dmg_qty', 0),
                        'gr_dtl_dmg_ttl' => array_get($modelsFirsts, 'gr_dtl_dmg_ttl', 0),

                        'gr_dtl_ept_ctn_ttl' => array_get($modelsFirsts, 'gr_dtl_ept_ctn_ttl', 0),
                        'gr_dtl_act_ctn_ttl' => array_get($modelsFirsts, 'gr_dtl_act_ctn_ttl', 0),
                        'gr_dtl_plt_ttl'     => array_get($modelsFirsts, 'gr_dtl_plt_ttl', 0),

                        //'gr_dtl_disc_ctn_ttl' => array_get($modelsFirsts, 'gr_dtl_disc_ctn_ttl', 0),  //pd
                        'gr_dtl_disc_ctn_ttl' => array_get($modelsFirsts, 'gr_dtl_act_ctn_ttl', 0) -
                            array_get($modelsFirsts, 'gr_dtl_ept_ctn_ttl', 0),  //pd

                        'asn_hdr_ept_dt'      => array_get($modelsFirsts, 'asn_hdr_ept_dt', 0) ?
                            date("m/d/Y", array_get($modelsFirsts, 'asn_hdr_ept_dt', 0)) : '',
                        'asn_hdr_act_dt'      => array_get($modelsFirsts, 'asn_hdr_act_dt', 0) ?
                            date("m/d/Y", array_get($modelsFirsts, 'asn_hdr_act_dt', 0)) : '',
                        'length'              => round(array_get($modelsFirsts, 'length', 0), 2),
                        'width'               => round(array_get($modelsFirsts, 'width', 0), 2),
                        'height'              => round(array_get($modelsFirsts, 'height', 0), 2),
                        'weight'              => round(array_get($modelsFirsts, 'weight', 0), 1),
                        'volume'              => round(array_get($modelsFirsts, 'volume', 0), 2),
                        'cube'              => round(array_get($modelsFirsts, 'cube', 0), 2),

                    ];
                }
            }

            $models = $arrItems;

            $page = array_get($attributes, 'page', 1);
            $paginate = array_get($attributes, 'limit', 20);

            $offSet = ($page * $paginate) - $paginate;
            $itemsForCurrentPage = array_slice($models, $offSet, $paginate, true);
            $data = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($models), $paginate,
                $page);

            $data = $data->toArray();

            $meta = [
                "pagination" => [
                    "total"         => (int)array_get($data, 'total', null),
                    "per_page"      => (int)array_get($data, 'per_page', null),
                    "count"         => (int)array_get($data, 'per_page', null),
                    "current_page"  => (int)array_get($data, 'current_page', null),
                    "total_pages"   => (int)array_get($data, 'last_page', null),
                    "next_page_url" => array_get($data, 'next_page_url', null),
                    "prev_page_url" => array_get($data, 'prev_page_url', null),
                    "from"          => array_get($data, 'from', null),
                    "to"            => array_get($data, 'to', null),
                    "last_page"     => (int)array_get($data, 'last_page', null),
                ]
            ];
            unset($data['total']);
            unset($data['per_page']);
            unset($data['current_page']);
            unset($data['total_pages']);
            unset($data['next_page_url']);
            unset($data['prev_page_url']);
            unset($data['from']);
            unset($data['to']);
            unset($data['last_page']);

            return [
                'data' => array_values($data['data']),
                'meta' => $meta
            ];
        }

        //return $models;
    }

    /**
     * @param $whs_id
     * @param $asn_id
     * @param array $attributes
     * @param array $with
     * @param null $limit
     * @param bool $export
     *
     * @return mixed
     */
    public function exportReceivingSlip($whs_id, $asn_id, $attributes = [], $with = [], $limit = null, $export = false)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $query->whereHas('goodsReceipt', function ($query) use ($whs_id, $asn_id, $attributes) {
            //$query->where('gr_sts', 'RE');
            //$query->where('gr_dtl_sts', 'RE');
            $query->where('asn_hdr_id', $asn_id);
            $query->where('whs_id', $whs_id);
            if (isset($attributes['gr_hdr_num'])) {
                $query->where('gr_hdr_num', 'like', "%" . SelStr::escapeLike($attributes['gr_hdr_num']) . "%");
            }
            if (isset($attributes['gr_sts'])) {
                $query->where('gr_sts', $attributes['gr_sts']);
            }
        });

        // Search Customer
        $query->whereHas('goodsReceipt.customer', function ($query) use ($whs_id, $attributes) {
            if (isset($attributes['cus_id'])) {
                $query->where('cus_id', $attributes['cus_id']);
            }
            if (isset($attributes['ref_code'])) {
                $query->where('ref_code', 'like', "%" . SelStr::escapeLike($attributes['ref_code']) . "%");
            }
        });

        if (isset($attributes['ctnr_num'])) {
            $query->where('ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
        }

        if (isset($attributes['sku'])) {
            $query->where('sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
        }

        //search more fields from Sku auto complete
        if (isset($attributes['item_id'])) {
            $query->where('item_id', $attributes['item_id']);
        }

        if (isset($attributes['lot'])) {
            $query->where('lot', $attributes['lot']);
        }

        //search according to from date to date
        if (!empty($attributes['updated_at_from'])) {
            $query->where(DB::raw("DATE_FORMAT(FROM_UNIXTIME(updated_at), '%m/%d/%Y')"), ">",
                date("m/d/Y H:i:s", strtotime($attributes['updated_at_from'] . "-1 day")));
        }

        if (!empty($attributes['updated_at_to'])) {
            $query->where(DB::raw("DATE_FORMAT(FROM_UNIXTIME(updated_at), '%Y/%m/%d/')"), "<", date("Y/m/d H:i:s",
                strtotime($attributes['updated_at_to'] . "+1 day")));
        }

        //sort
        if (isset($attributes['sort'])) {
            foreach ($attributes['sort'] as $key => $val) {
                if ($key === 'cus_name') {
                    $attributes['sort']['goodsReceipt.customer.cus_name'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'gr_hdr_num') {
                    $attributes['sort']['goodsReceipt.gr_hdr_num'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'ctnr_num') {
                    $attributes['sort']['goodsReceipt.container.ctnr_num'] = $val;
                    unset($attributes['sort'][$key]);
                }

                if ($key === 'ref_code') {
                    $attributes['sort']['goodsReceipt.ref_code'] = $val;
                    unset($attributes['sort'][$key]);
                }

            }
        }

        // Get
        $this->sortBuilder($query, $attributes);

        $models = $query->get();

        return $models;
    }

    /**
     * @param $whs_id
     * @param $asn_id
     * @param array $attributes
     * @param array $with
     * @param null $limit
     * @param bool $export
     *
     * @return mixed
     */
    public function exportReceivingSlip_newStatus(
        $whs_id,
        $asn_id,
        $attributes = [],
        $with = [],
        $limit = null,
        $export = false
    ) {
        $query = DB::Table('asn_dtl')
            ->join('asn_hdr', 'asn_hdr.asn_hdr_id', '=', 'asn_dtl.asn_hdr_id')
            ->join('customer', 'customer.cus_id', '=', 'asn_hdr.cus_id')
            ->where('asn_dtl.asn_hdr_id', $asn_id)
            ->where('asn_hdr.whs_id', $whs_id)
            ->select(
                'asn_dtl.*',
                'customer.cus_name',
                'asn_hdr.*',
                'asn_dtl.asn_dtl_des AS description',
                DB::raw("IF(asn_hdr.asn_hdr_id, '', '') AS actual_qty"),
                DB::raw("IF(asn_hdr.asn_hdr_id, '', '') AS gr_date"),
                DB::raw("IF(asn_hdr.asn_hdr_id, '', '') AS putaway_date"),
                DB::raw("IF(asn_hdr.asn_hdr_id, '', '') AS gr_by")
            );

        $models = $query->get();

        return $models;
    }

    /**
     * @param $whs_id
     * @param array $attributes
     * @param array $with
     * @param null $limit
     * @param bool $export
     *
     * @return mixed
     */
    public function skuTrackingSearch($whs_id, $attributes = [], $with = [], $limit = null, $export = false)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        //OutBound: Query Shipping orders
        $queryFirst = (new OrderDtl())->getModel()
        ->join('odr_hdr', 'odr_dtl.odr_id', '=', 'odr_hdr.odr_id')
        ->join('customer', 'odr_hdr.cus_id', '=', 'customer.cus_id')
        ->join('item', 'item.item_id', '=', 'odr_dtl.item_id')

        ->where('odr_hdr.whs_id', $whs_id)
        ->where('odr_hdr.odr_sts', 'SH')
        ->where('odr_dtl.picked_qty', '>', 0);

        if (isset($attributes['cus_id']) && !empty($attributes['cus_id'])) {
            $queryFirst->where('odr_hdr.cus_id', intval($attributes['cus_id']));
        }
        if (isset($attributes['trans_num']) && !empty($attributes['trans_num'])) {
            $queryFirst->where('odr_hdr.odr_num', 'like', "%" . SelStr::escapeLike($attributes['trans_num']) . "%");
        }
        if (isset($attributes['ref_cus_order']) && !empty($attributes['ref_cus_order'])) {
            $queryFirst->where('odr_hdr.cus_odr_num', 'like',
                "%" . SelStr::escapeLike($attributes['ref_cus_order']) . "%");
        }
        if (isset($attributes['po_ctnr']) && !empty($attributes['po_ctnr'])) {
            $queryFirst->where('odr_hdr.cus_po', 'like', "%" . SelStr::escapeLike($attributes['po_ctnr']) . "%");
        }
        if (isset($attributes['sku']) && !empty($attributes['sku'])) {
            $queryFirst->where('odr_dtl.sku', $attributes['sku']);
        }
        if (isset($attributes['item_id']) && !empty($attributes['item_id'])) {
            $queryFirst->where('odr_dtl.item_id', $attributes['item_id']);
        }
        if (isset($attributes['lot']) && !empty($attributes['lot'])) {
            $queryFirst->where('odr_dtl.lot', $attributes['lot']);
        }
        if (isset($attributes['color']) && !empty($attributes['color'])) {
            $queryFirst->where('odr_dtl.color', $attributes['color']);
        }

        //search according to from date to date
        if (!empty($attributes['actual_date_from'])) {
            $queryFirst->where('odr_hdr.shipped_dt', ">=", strtotime($attributes['actual_date_from']));
        }

        if (!empty($attributes['actual_date_to'])) {
            $queryFirst->where('odr_hdr.shipped_dt', "<", strtotime($attributes['actual_date_to'] . "+1 day"));
        }

        $queryFirst->select([
            'customer.cus_name',
            'customer.cus_code',
            DB::raw("'O' as type"),
            'odr_hdr.odr_num as trans_num',
            'odr_hdr.cus_odr_num as ref_cus_order',
            'odr_hdr.cus_po as po_ctnr',
            'odr_hdr.shipped_dt as actual_date',
            'odr_dtl.item_id',
            'odr_dtl.sku',
            'odr_dtl.size',
            'odr_dtl.color',
            'odr_dtl.lot',
            'odr_dtl.pack',
            DB::raw('(ROUND(SUM(odr_dtl.picked_qty/odr_dtl.pack)))*(-1) as ctns'),
            DB::raw('SUM(odr_dtl.picked_qty)*(-1) AS qty'),

            DB::raw('(ROUND( (SUM(odr_dtl.picked_qty)/odr_dtl.pack)*(-1) * (item.length*item.width*item.height)/1728, 2) ) as cube'),

        ]);

        $queryFirst->groupBy('odr_dtl.item_id','odr_dtl.odr_id');

        $queryFirst->orderBy('odr_hdr.shipped_dt', 'DESC');


        //Cycle Count:
        $queryCc = (new CycleHdr())->getModel()
            ->join('cycle_dtl AS d', 'cycle_hdr.cycle_hdr_id', '=', 'd.cycle_hdr_id')
            ->join('customer', 'd.cus_id', '=', 'customer.cus_id')
            ->join('item', 'item.item_id', '=', 'd.item_id')

            ->where('cycle_hdr.cycle_sts', 'CP')
            ->where('d.whs_id', $whs_id)
            ->whereRaw('d.sys_qty != d.act_qty');

        if (isset($attributes['cus_id']) && !empty($attributes['cus_id'])) {
            $queryCc->where('d.cus_id', intval($attributes['cus_id']));
        }
        if (isset($attributes['trans_num']) && !empty($attributes['trans_num'])) {
            $queryCc->where('cycle_hdr.cycle_num', 'like', "%" . SelStr::escapeLike($attributes['trans_num']) . "%");
        }
        if (isset($attributes['ref_cus_order']) && !empty($attributes['ref_cus_order'])) {
            $queryCc->where('cycle_hdr.cycle_name', 'like',
                "%" . SelStr::escapeLike($attributes['ref_cus_order']) . "%");
        }
        if (isset($attributes['po_ctnr']) && !empty($attributes['po_ctnr'])) {
            $queryCc->where('cycle_hdr.cycle_num', 'like', "%" . SelStr::escapeLike($attributes['po_ctnr']) . "%");
        }
        if (isset($attributes['sku']) && !empty($attributes['sku'])) {
            $queryCc->where('d.sku', $attributes['sku']);
        }
        if (isset($attributes['item_id']) && !empty($attributes['item_id'])) {
            $queryCc->where('d.item_id', $attributes['item_id']);
        }
        if (isset($attributes['lot']) && !empty($attributes['lot'])) {
            $queryCc->where('d.lot', $attributes['lot']);
        }
        if (isset($attributes['color']) && !empty($attributes['color'])) {
            $queryCc->where('d.color', $attributes['color']);
        }

        //search according to from date to date
        if (!empty($attributes['actual_date_from'])) {
            $queryCc->where('cycle_hdr.updated_at', ">=", strtotime($attributes['actual_date_from']));
        }

        if (!empty($attributes['actual_date_to'])) {
            $queryCc->where('cycle_hdr.updated_at', "<", strtotime($attributes['actual_date_to'] . "+1 day"));
        }

        $queryCc->select([
            'customer.cus_name',
            'customer.cus_code',
            DB::raw("'C' as type"),
            'cycle_hdr.cycle_num as trans_num',
            'cycle_hdr.cycle_name as ref_cus_order',
            'cycle_hdr.cycle_num as po_ctnr',
            'cycle_hdr.updated_at as actual_date',
            'd.item_id',
            'd.sku',
            'd.size',
            'd.color',
            'd.lot',
            'd.pack',

            DB::raw("ROUND(SUM(IF(cycle_hdr.count_by = 'EACH', 1, d.remain) * ( CAST(d.act_qty AS SIGNED) - CAST(d.sys_qty AS SIGNED)))/d.pack) as ctns"),
            DB::raw("SUM(IF(cycle_hdr.count_by = 'EACH', 1, d.remain) * ( CAST(d.act_qty AS SIGNED) - CAST(d.sys_qty AS SIGNED))) AS qty"),

            DB::raw("(ROUND( 
            ((SUM(IF(cycle_hdr.count_by = 'EACH', 1, d.remain) * ( CAST(d.act_qty AS SIGNED) - CAST(d.sys_qty AS SIGNED)))/d.pack) 
            * (item.length*item.width*item.height))/1728, 2) ) as cube"),

        ]);

        $queryCc->groupBy('cycle_hdr.cycle_hdr_id', 'd.item_id');


        //InBound: Query Goods Receipt
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $query = $this->model
            ->join('gr_hdr', 'gr_hdr.gr_hdr_id', '=', 'gr_dtl.gr_hdr_id')
            ->join('customer', 'gr_hdr.cus_id', '=', 'customer.cus_id')
            ->join('item', 'item.item_id', '=', 'gr_dtl.item_id')

            ->select([
                'customer.cus_name',
                'customer.cus_code',
                DB::raw("'G' as type"),
                'gr_hdr.gr_hdr_num as trans_num',
                'gr_hdr.ref_code as ref_cus_order',
                'gr_dtl.ctnr_num as po_ctnr',
                'gr_hdr.gr_hdr_act_dt as actual_date',
                'gr_dtl.item_id',
                'gr_dtl.sku',
                'gr_dtl.size',
                'gr_dtl.color',
                'gr_dtl.lot',
                'gr_dtl.pack',

                DB::raw("SUM(gr_dtl.gr_dtl_act_ctn_ttl) AS ctns"),
                DB::raw('SUM(gr_dtl.gr_dtl_act_ctn_ttl*gr_dtl.pack) AS qty'),

                DB::raw("(ROUND( 
                        ((SUM(gr_dtl.gr_dtl_act_ctn_ttl)) 
                        * (item.length*item.width*item.height))/1728, 2) ) as cube"),

            ]);

        $query->where('gr_hdr.whs_id', $whs_id);
        $query->where('gr_dtl.gr_dtl_sts', 'RE');
        if (isset($attributes['trans_num'])) {
            $query->where('gr_hdr.gr_hdr_num', 'like', "%" . SelStr::escapeLike($attributes['trans_num']) . "%");
        }
        if (isset($attributes['gr_sts'])) {
            $query->where('gr_hdr.gr_sts', $attributes['gr_sts']);
        }

        // Search Customer
        if (isset($attributes['cus_id'])) {
            $query->where('gr_hdr.cus_id', $attributes['cus_id']);
        }
        if (isset($attributes['ref_cus_order'])) {
            $query->where('gr_hdr.ref_code', 'like', "%" . SelStr::escapeLike($attributes['ref_cus_order']) . "%");
        }

        if (isset($attributes['po_ctnr'])) {
            $query->where('gr_dtl.ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['po_ctnr']) . "%");
        }

        if (isset($attributes['sku'])) {
            $query->where('gr_dtl.sku', 'like', "%" . SelStr::escapeLike($attributes['sku']) . "%");
        }

        //search more fields from Sku auto complete
        if (isset($attributes['item_id'])) {
            $query->where('gr_dtl.item_id', $attributes['item_id']);
        }
        if (isset($attributes['lot'])) {
            $query->where('gr_dtl.lot', $attributes['lot']);
        }

        //search according to from date to date
        if (!empty($attributes['actual_date_from'])) {
            $query->where('gr_hdr.gr_hdr_act_dt', ">=", strtotime($attributes['actual_date_from']));
        }

        if (!empty($attributes['actual_date_to'])) {
            $query->where('gr_hdr.gr_hdr_act_dt', "<", strtotime($attributes['actual_date_to'] ));
        }

        $query->groupBy('gr_dtl.item_id','gr_hdr.gr_hdr_id');
        $this->sortBuilder($query, $attributes);

        if ($export) {
            return $models = $query->union($queryCc)->union($queryFirst)->get();
        } else {
            $modelsFirsts = $query->union($queryCc)->union($queryFirst)->get()->toArray();
            $arrItems = [];
            if ($modelsFirsts) {
                foreach ($modelsFirsts as $modelsFirsts) {
                    $arrItems[] = [
                        'cus_name'      => array_get($modelsFirsts, 'cus_name', null),
                        'cus_code'      => array_get($modelsFirsts, 'cus_code', null),
                        'trans_num'     => array_get($modelsFirsts, 'trans_num', null),
                        "ref_cus_order" => array_get($modelsFirsts, "ref_cus_order", null),
                        "po_ctnr"       => array_get($modelsFirsts, "po_ctnr", null),
                        "actual_date"   => !empty(array_get($modelsFirsts, "actual_date", null))
                            ? date('m/d/Y', array_get($modelsFirsts, "actual_date", null)) : '',
                        'item_id'           => array_get($modelsFirsts, 'item_id', 'NA'),
                        'sku'           => array_get($modelsFirsts, 'sku', 'NA'),
                        'size'          => array_get($modelsFirsts, 'size', 'NA'),
                        'color'         => array_get($modelsFirsts, 'color', 'NA'),
                        'lot'           => array_get($modelsFirsts, 'lot', null),
                        'pack'          => array_get($modelsFirsts, 'pack', null),

                        'ctns'          => array_get($modelsFirsts, 'ctns', null),
                        'qty'           => array_get($modelsFirsts, 'qty', null),

                        'cube'           => round(array_get($modelsFirsts, 'cube', null)),
                        'type'          => array_get($modelsFirsts, 'type')
                    ];
                }
            }

            $models = $arrItems;

            //sort
            if (isset($attributes['sort'])) {
                foreach ($attributes['sort'] as $key => $val) {
                    if ($val == 'desc') {
                        $models = $this->array_msort($models, [$key => SORT_DESC]);
                    } else {
                        $models = $this->array_msort($models, [$key => SORT_ASC]);
                    }

                }
            }

            $page = array_get($attributes, 'page', 1);
            $paginate = array_get($attributes, 'limit', 20);

            $offSet = ($page * $paginate) - $paginate;
            $itemsForCurrentPage = array_slice($models, $offSet, $paginate, true);
            $data = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($models), $paginate,
                $page);

            $data = $data->toArray();

            $meta = [
                "pagination" => [
                    "total"         => (int)array_get($data, 'total', null),
                    "per_page"      => (int)array_get($data, 'per_page', null),
                    "count"         => (int)array_get($data, 'per_page', null),
                    "current_page"  => (int)array_get($data, 'current_page', null),
                    "total_pages"   => (int)array_get($data, 'last_page', null),
                    "next_page_url" => array_get($data, 'next_page_url', null),
                    "prev_page_url" => array_get($data, 'prev_page_url', null),
                    "from"          => array_get($data, 'from', null),
                    "to"            => array_get($data, 'to', null),
                    "last_page"     => (int)array_get($data, 'last_page', null),
                ]
            ];
            unset($data['total']);
            unset($data['per_page']);
            unset($data['current_page']);
            unset($data['total_pages']);
            unset($data['next_page_url']);
            unset($data['prev_page_url']);
            unset($data['from']);
            unset($data['to']);
            unset($data['last_page']);

            return [
                'data' => array_values($data['data']),
                'meta' => $meta
            ];
        }


    }

    /**
     * @param $array
     * @param $cols
     *
     * @return array
     */
    function array_msort($array, $cols)
    {
        $colarr = [];
        foreach ($cols as $col => $order) {
            $colarr[$col] = [];
            foreach ($array as $k => $row) {
                $colarr[$col]['_' . $k] = strtolower($row[$col]);
            }
        }
        $params = [];
        foreach ($cols as $col => $order) {
            $params[] =& $colarr[$col];
            $params = array_merge($params, (array)$order);
        }
        call_user_func_array('array_multisort', $params);
        $ret = [];
        $keys = [];
        $first = true;
        foreach ($colarr as $col => $arr) {
            foreach ($arr as $k => $v) {
                if ($first) {
                    $keys[$k] = substr($k, 1);
                }
                $k = $keys[$k];
                if (!isset($ret[$k])) {
                    $ret[$k] = $array[$k];
                }
                $ret[$k][$col] = $array[$k][$col];
            }
            $first = false;
        }

        return $ret;

    }

    /**
     * @param $whs_id
     * @param array $attributes
     * @param null $limit
     *
     * @return mixed
     */
    public function skuGrAutoComplete($whs_id, $attributes = [], $limit = null)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $limit = $limit > 0 ? $limit : self::LIMIT;

        // Search whs_id
        $query = DB::table('gr_dtl AS grDtl')
            ->select(['grDtl.sku', 'grDtl.size', 'grDtl.color', 'grDtl.lot', 'grDtl.item_id']);

        $query->leftJoin('gr_hdr AS grHdr', 'grHdr.gr_hdr_id', '=', 'grDtl.gr_hdr_id')
            ->where('grHdr.whs_id', $whs_id);

        if (isset($attributes['cus_id'])) {
            $query->where('grHdr.cus_id', $attributes['cus_id']);
        }

        if (isset($attributes['ctnr_num'])) {
            $query->where('grDtl.ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
        }

        if (isset($attributes['ref_code'])) {
            $query->where('grHdr.ref_code', 'like', "%" . SelStr::escapeLike($attributes['ref_code']) . "%");
        }

        if (isset($attributes['gr_hdr_num'])) {
            $query->where('grHdr.gr_hdr_num', 'like', "%" . SelStr::escapeLike($attributes['gr_hdr_num']) . "%");
        }

        // Search sku
        if (isset($attributes['sku'])) {
            $query->where('grDtl.sku', 'LIKE', "%{$attributes['sku']}%");
        }

        $result = $query->groupBy('grDtl.item_id', 'grDtl.lot')
            ->orderBy('grDtl.sku')->take($limit)->get();

        return $result;
    }

    /**
     * @param $whs_id
     * @param array $attributes
     * @param null $limit
     *
     * @return mixed
     */
    public function skuTrackingAutoComplete($whs_id, $attributes = [], $limit = null)
    {
        //sku Order Shipping complete
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $limit = $limit > 0 ? $limit : self::LIMIT;

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $queryFirst = DB::table('odr_cartons AS oc')
            ->select(['oc.sku', 'oc.size', 'oc.color', 'oc.lot', 'oc.item_id'])
            ->join('odr_hdr', 'oc.odr_hdr_id', '=', 'odr_hdr.odr_id')
            ->where('odr_hdr.whs_id', $whs_id)
            ->where('odr_hdr.odr_sts', 'SH');

        if (isset($attributes['cus_id'])) {
            $queryFirst->where('odr_hdr.cus_id', $attributes['cus_id']);
        }

        if (isset($attributes['trans_num'])) {
            $queryFirst->where('odr_hdr.odr_num', $attributes['trans_num']);
        }
        if (isset($attributes['ref_cus_order'])) {
            $queryFirst->where('odr_hdr.odr_num', $attributes['ref_cus_order']);
        }
        if (isset($attributes['po_ctnr'])) {
            $queryFirst->where('odr_hdr.odr_num', $attributes['po_ctnr']);
        }

        // Search Customer
        if (isset($attributes['sku'])) {
            $queryFirst->where('oc.sku', 'LIKE', "%{$attributes['sku']}%");
        }

        $queryFirst
            ->groupBy('oc.item_id', 'oc.lot')
            ->orderBy('oc.sku');

        //SKU - Goods Receipt complete
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('gr_dtl AS grDtl')
            ->select(['grDtl.sku', 'grDtl.size', 'grDtl.color', 'grDtl.lot', 'grDtl.item_id']);

        $query->leftJoin('gr_hdr AS grHdr', 'grHdr.gr_hdr_id', '=', 'grDtl.gr_hdr_id')
            ->where('grHdr.whs_id', $whs_id);

        if (isset($attributes['cus_id'])) {
            $query->where('grHdr.cus_id', $attributes['cus_id']);
        }

        if (isset($attributes['po_ctnr'])) {
            $query->where('grDtl.ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['po_ctnr']) . "%");
        }

        if (isset($attributes['ref_cus_order'])) {
            $query->where('grHdr.ref_code', 'like', "%" . SelStr::escapeLike($attributes['ref_cus_order']) . "%");
        }

        if (isset($attributes['trans_num'])) {
            $query->where('grHdr.gr_hdr_num', 'like', "%" . SelStr::escapeLike($attributes['trans_num']) . "%");
        }

        // Search sku
        if (isset($attributes['sku'])) {
            $query->where('grDtl.sku', 'LIKE', "%{$attributes['sku']}%");
        }

        $query->groupBy('grDtl.item_id', 'grDtl.lot')
            ->orderBy('grDtl.sku');

        $result = $query->union($queryFirst)->take($limit)->get();

        return $result;
    }


}
