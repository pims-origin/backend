<?php

namespace App\Api\Inventory\Models;


use Seldat\Wms2\Models\BlockHdr;
use Seldat\Wms2\Models\BlockRsn;
use Seldat\Wms2\Models\CycleHdr;
use Seldat\Wms2\Models\EventTracking;


class EventTrackingModel extends AbstractModel
{
    const BLOCK_PREFIX = 'BLK';
    const CYCLE_PREFIX = 'CYC';

    const CYCLE_CREATED_ASSIGNED = 'CRA';
    const CYCLE_CYCLED = 'CYD';
    const CYCLE_RECOUNT = 'CYR';
    const CYCLE_ADD_SKU = 'CYS';
    const CYCLE_COMPLETED = 'CYC';

    public static $arrEvtCycle = [
        self::CYCLE_CREATED_ASSIGNED => 'Created & Assigned',
        self::CYCLE_CYCLED => 'Cycled',
        self::CYCLE_RECOUNT => 'Recount',
        self::CYCLE_ADD_SKU => 'Add SKU',
        self::CYCLE_COMPLETED => 'Completed',
    ];

    /**
     * SampleModel constructor.
     */
    public function __construct($model = NULL)
    {
        $this->model = $model ? $model : new EventTracking();
    }

    public function search($attributes, array $with, $limit)
    {
        $query = $this->make($with);

        if (isset($attributes['evt_code'])) {
            $query->where('evt_code', 'like', "%" . escape_like($attributes['evt_code']) . "%");
        }

        if (isset($attributes['owner'])) {
            $query->where('owner', 'like', "%" . escape_like($attributes['owner']) . "%");
        }

        if (isset($attributes['trans_num'])) {
            $query->where('trans_num', 'like', "%" . escape_like($attributes['trans_num']) . "%");
        }

        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);

    }

    public function getLatestCycleNumber()
    {
        $result = CycleHdr::select('cycle_num')
            ->orderBy('cycle_num', 'desc')->first();
        return $result ? $result->cycle_num : null;
    }

    public function getCycleNumberByID($cycleHdrID)
    {
        $result = CycleHdr::find($cycleHdrID);
        return $result ? $result->cycle_num : null;
    }

    public function generateEventTrackingNum()
    {
        $cycleNum = $this->getLatestCycleNumber();

        $result = [
            'cycle_num' => '',
            'cycle_seq' => '',
        ];

        $currentYearMonth = date('ym');
        if ($cycleNum) {
            list(, $yymm, $result['cycle_seq']) = explode('-', $cycleNum);
            // whether reset asmNum
            $cycleNum = ($yymm != $currentYearMonth) ? '' : $cycleNum;
        }

        $result['cycle_seq'] = (!$cycleNum) ? 1 : ((int)$result['cycle_seq']) + 1;

        $result['cycle_num'] = sprintf('%s-%s-%s',
            self::CYCLE_PREFIX,
            $currentYearMonth,
            str_pad($result['cycle_seq'], 6, '0', STR_PAD_LEFT)
        );

        return $result;
    }

    public function generateEventTrackingTransNum($cycleHdrID, $evtCode)
    {
        $cycleNum = $this->getCycleNumberByID($cycleHdrID);

        $result = [
            'cycle_num' => '',
            'trans_num' => '',
            'trans_seq' => 1,
        ];
        if (!$cycleNum) {
            return false;
        }
        $result['cycle_num'] = $cycleNum;
        $result['trans_num'] = str_replace(self::CYCLE_PREFIX, $evtCode, $cycleNum);
        $transNum = EventTracking::select('trans_num')
            ->where('owner', $cycleNum)
            ->where('trans_num', 'like', $result['trans_num'] . '%')
            ->orderBy('trans_num', 'desc')->first();
        if ($transNum) {
            list(, , , $result['trans_seq']) = explode('-', $transNum->trans_num);
            $result['trans_seq'] = ((int) $result['trans_seq']) + 1;
        }

        $result['trans_num'] = sprintf('%s-%s',
            $result['trans_num'],
            str_pad($result['trans_seq'], 3, '0', STR_PAD_LEFT)
        );
        return $result;
    }

    /**
     * @return array
     */

    public function generateBlockNum()
    {
        $blockNum = $this->getLatestBlockNumber();

        $results = $this->generate($blockNum);

        return [
            'block_num' => $results['num'],
            'block_seq' =>$results['seq']
        ];
    }

    /**
     * @param $num
     * @return mixed
     */

    public function generate($num)
    {
        $eq = 0;

        $currentTime = date('ym');

        if ($num) {
            list(, $yymm, $eq) = explode('-', $num);

            $num = ($yymm != $currentTime) ? '' : $num;
        }

        $result['seq'] = ($num) ? ((int) $eq) + 1 : 1 ;

        $result['num'] = sprintf('%s-%s-%s',
            self::BLOCK_PREFIX,
            $currentTime,
            str_pad($result['seq'], 6, '0', STR_PAD_LEFT));

        return $result;

    }

    /**
     * @return null
     */

    public function getLatestBlockNumber()
    {
        $result = BlockHdr::select('block_num')
            ->orderBy('block_num', 'desc')->first();
        return $result ? $result->block_num : null;
    }

    /**
     * @return null
     */

    public function getLatestReasonNumber()
    {
        $result = BlockRsn::select('reason_num')
            ->orderBy('reason_num', 'desc')->first();
        return $result ? $result->block_num : null;
    }

    /**
     * @return array
     */

    public function generateReasonNum()
    {
        $blockNum = $this->getLatestReasonNumber();

        $results = $this->generate($blockNum);

        return [
            'reason_num' => $results['num'],
            'reason_seq' =>$results['seq']
        ];
    }

}
