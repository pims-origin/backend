<?php
namespace Seldat\Wms2\Models;/**
 * Class SKUTrackingReport
 *
 * @package Seldat\Wms2\Models
 */
class ReportSkuTracking extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rpt_sku_tracking';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'cus_id',
        'cus_name',
        'cus_code',
        'gr_hdr_id',
        'odr_id',
        'cycle_hdr_id',
        'trans_num',
        'whs_id',
        'ref_cus_order',
        'po_ctnr',
        'actual_date',

        'item_id',
        'sku',
        'size',
        'color',
        'lot',
        'pack',
        'ctns',
        'qty',
        'updated_by',
        'created_at',
        'updated_at',
        'created_by',
        'created_at',
        'deleted',
        'cube',
    ];




}