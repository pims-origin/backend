<?php

namespace console\controllers;

use common\models\Authority;
use yii\console\Controller;
use yii\rbac\Item;

class BaseRbacController extends Controller
{
    protected function permission($name, $des)
    {
        $permission = $this->auth->getPermission($name);

        if ($permission) {
            return $permission;
        }

        $obj = $this->auth->createPermission($name);
        $obj->description = $des;
        $this->auth->add($obj);

        return $obj;
    }

    protected function addGroup($item)
    {
        $group = $this->auth->getGroup($item->name);
        if ($group) {
            return $group;
        }

        $this->auth->add($item);

        return $item;
    }

    protected function role($name, $des)
    {
        $role = $this->auth->getRole($name);

        if ($role) {
            return $role;
        }

        $obj = $this->auth->createRole($name);
        $obj->description = $des;
        $this->auth->add($obj);

        return $obj;
    }

    protected function addChild($parent, $child)
    {
        $checker = $this->auth->hasChild($parent, $child);

        if ($checker) {
            return false;
        }

        $this->auth->addChild($parent, $child);
    }

    protected function addRule($ruleObject)
    {
        if (!$this->auth->getRule($ruleObject->name)) {
            $this->auth->add($ruleObject);
        }
    }

    /**
     * Add multiple children for parents
     *
     * @param array $parents
     * @param array $children
     *
     * @return bool
     */
    protected function addChildren($parents, $children)
    {
        foreach ($parents as $parent) {
            foreach ($children as $child) {
                $this->addChild($parent, $child);
            }
        }

        return true;
    }

    /**
     * Add children for permission by array
     *
     * @param $arr
     *
     * @return bool
     */
    protected function addChildrenPermissions($arr)
    {
        foreach ($arr as $parent => $children) {
            $parentPerm = $this->auth->getPermission($parent);
            $children = is_array($children) ? $children : [$children];
            foreach ($children as $child) {
                $childPerm = $this->auth->getPermission($child);
                $this->addChild($parentPerm, $childPerm);
            }
        }

        return true;
    }

    protected function createGrpPerm($perms, $addChildren, $groupName, $assignRoles = [], $order)
    {
        $grp = new Item();
        $grp->name = $groupName;
        $grp->description = str_pad($order, 3, "0", STR_PAD_LEFT);
        $grp->type = Authority::GROUP_TYPE;
        $this->addGroup($grp);

        $grpNrole[] = $grp;
        $return = [];
        foreach ($perms as $name => $desc) {
            $return[] = $this->permission($name, $desc);
        }

        //setup children permissions
        if ($addChildren) {
            $this->addChildrenPermissions($addChildren);
        }

        //add permission to group and user
        foreach ($assignRoles as $roleName) {
            $grpNrole[] = $this->auth->getRole($roleName);
        }
        $this->addChildren($grpNrole, $return);

        return $return;
    }


}