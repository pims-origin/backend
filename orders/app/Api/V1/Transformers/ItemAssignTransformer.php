<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\Carton;
use League\Fractal\TransformerAbstract;

/**
 * Class ItemAssignTransformer
 *
 * @package App\Api\V1\Transformers
 */
class ItemAssignTransformer extends TransformerAbstract
{
    /**
     * @param Carton $carton
     *
     * @return array
     */
    public function transform(Carton $carton)
    {
        return [
            'item_id'    => $carton->item_id,
            'sku'        => object_get($carton, "sku"),
            'size'       => object_get($carton, "size"),
            'color'      => object_get($carton, "color"),
            'lot'        => object_get($carton, "lot"),
            'pack'       => object_get($carton, "pack"),
            'upc'        => object_get($carton, "upc"),
            'uom_id'     => object_get($carton, "uom_id"),
            'uom_code'   => object_get($carton, "uom_code"),
            'pack_size'  => $carton->ctn_pack_size,
            'total_qty'  => $carton->piece_ttl,
            'odr_dtl_id' => $carton->odr_dtl_id,
            'odr_hdr_id' => $carton->odr_hdr_id
        ];
    }
}
