<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

// Initial Dingo API Route
$api = app('Dingo\Api\Routing\Router');

// Bypass middleware when testing
$middleware = ['trimInput', 'setWarehouseTimezone'];
if (env('APP_ENV') != 'testing') {
    $middleware[] = 'verifySecret';
    $middleware[] = 'authorize';
}

// Handle application Route
$api->version('v1', ['middleware' => $middleware], function ($api) {

    require(__DIR__ . '/V2/routes.php');

    $api->group(['prefix' => 'v1', 'namespace' => 'App\Api\V1\Controllers'], function ($api) {

        $api->get('/', function () use ($api) {
            return ['Seldat API V1'];
        });

        //Cycle Count Header
        $api->get('/cycle-count', ['action' => "viewCycleCount", 'uses' => 'CycleHdrController@index']);
        $api->get('/cycle-count/{id:[0-9]+}', ['action' => "viewCycleCount", 'uses' => 'CycleHdrController@view']);
        $api->post('/cycle-count', ['action' => "createCycleCount", 'uses' => 'CycleHdrController@store']);
        $api->put('/cycle-count/{id:[0-9]+}', ['action' => "createCycleCount", 'uses' => 'CycleHdrController@update']);
        $api->post('/cycle-count/delete', ['action' => "deleteCycleCount", 'uses' => 'CycleHdrController@destroy']);
        $api->get('/cycle-count/print-pdf/{id:[0-9]+}',
            ['action' => "printCycleCount", 'uses' => 'CycleHdrController@printPdf']);
        $api->post('/cycle-count/recount', ['action' => 'recountCycleCount', 'uses' => 'CycleHdrController@recount']);
        $api->post('/cycle-count/approve', ['action' => 'approveCycleCount', 'uses' => 'CycleHdrController@approve']);
        $api->post('/cycle-count/saveCycled/{id:[0-9]+}',
            ['action' => "viewCycleCount", 'uses' => 'CycleHdrController@saveCycled']);
        $api->post('/cycle-count/check-cus-in-user',
            ['action' => "viewCycleCount", 'uses' => 'CycleHdrController@checkCusInUser']);
        $api->get('/cycle-count/get-users-in-whs',
            ['action' => "viewCycleCount", 'uses' => 'CycleHdrController@getUserWhs']);
        $api->put('/cycle-count/{cycle_id:[0-9]+}/update-assignee/{assignee_id:[0-9]+}', [
            'action' => "createCycleCount", 'uses' => 'CycleHdrController@updateAssignee'
        ]);
        $api->post('/cycle-count/check-orders', [
            'action' => "createCycleCount", 'uses' => 'CycleHdrController@checkOdrSts'
        ]);

        $api->get('/cycle/type', ['action' => "viewCycleCount", 'uses' => 'CycleHdrController@cycleType']);
        $api->get('/cycle/status', ['action' => "viewCycleCount", 'uses' => 'CycleHdrController@cycleStatus']);

        $api->get('/cycle-count/auto-sku', ['action' => "createCycleCount", 'uses' => 'CycleHdrController@skuAutoComplete']);

        //Cycle Count Detail
        $api->get('/cycle-detail/{id:[0-9]+}', ['action' => "viewCycleCount", 'uses' => 'CycleDtlController@listDtl']);
        $api->get('/cycle-detail/{hdr_id:[0-9]+}/item/{dtl_id:[0-9]+}',
            ['action' => "viewCycleCount", 'uses' => 'CycleDtlController@view']);
        $api->get('/rfgun-cycle-detail/{hdr_id:[0-9]+}/item/{dtl_id:[0-9]+}', ['action' => "viewCycleCount", 'uses' =>
            'CycleDtlController@rfgunView']);
        $api->put('/cycle-detail/{id}', ['action' => "viewCycleCount", 'uses' => 'CycleDtlController@update']);

        //Location
        $api->get('/location/filter', ['action' => "viewCycleCount", 'uses' => 'LocationController@filter']);
        $api->post('/location/unset-cartons-rfid', ['action' => "viewCycleCount", 'uses' => 'LocationController@unsetCartonsRfid']);
        $api->put('/location/{locationId:[0-9]+}/unlock', ['action' => "unlockLocation", 'uses' => 'LocationController@unlockLocation']);

        //Add SKU
        $api->post('/cycle-count/{cycle_id:[0-9]+}/add-sku',
            ['action' => "addSKUCycleCount", 'uses' => 'AddSKUController@store']);

        $api->post('/cycle-count/{cycle_id:[0-9]+}/add-sku-cycle',
            ['action' => "addSKUCycleCount", 'uses' => 'AddSKUController@storeToCycle']);

        $api->get('/cycle/items', ['action' => "getItem", 'uses' => 'ItemController@search']);

        // Block stock
        // Create reason
        $api->post('/block-stock/add-reason/', ['action' => "createBlockStock", 'uses' => 'ReasonController@store']);

        // Cycle Count Reason
        $api->get('/cycle-count/reason-list/', ['action' => "createBlockStock", 'uses' => 'ReasonController@getCycleCountReasonList']);
        $api->post('/cycle-count/add-reason/', ['action' => "createBlockStock", 'uses' => 'ReasonController@storeCycleCountReason']);


        // get reason list
        $api->get('/block-stock/reason-list/',
            ['action' => "viewBlockStock", 'uses' => 'ReasonController@reasonList']);

        // Create block stock
        $api->post('/block-stock', ['action' => "createBlockStock", 'uses' => 'BlockHdrController@store']);

        // Get block stock list
        $api->get('/block-stock/', ['action' => "viewBlockStock", 'uses' => 'BlockHdrController@index']);

        // Unblock
        $api->post('/block-stock/unblock/{block_hdr_id:[0-9]+}', [
            'action' => 'createBlockStock',
            'uses'   => 'BlockHdrController@unblock'
        ]);

        //Block stock list
        $api->get('/block-stock/{id:[0-9]+}/list/',
            ['action' => "viewBlockStock", 'uses' => 'BlockDtlController@listDtl']);
        $api->get('/block-stock/{id:[0-9]+}', ['action' => "viewBlockStock", 'uses' => 'BlockHdrController@view']);
        $api->get('/block-stock/items', ['action' => "viewBlockStock", 'uses' => 'ItemController@listItems']);

        //get pallet RFID
        $api->get('/warehouses/{warehouseId:[0-9]+}/location/{locId:[0-9]+}/get-pallet', [
            'action' => 'viewLocation',
            'uses'   => 'LocationController@getPalletInfo'
        ]);

        //cycle-count-notification
        $api->get('/ccn/{whs_id:[0-9]+}/list', ['action' => "viewCycleCountNotification", 'uses' => 'CycleNotificationController@index']);
        $api->get('/ccn/{whs_id:[0-9]+}/complete-add-ccn',
            ['action' => "viewCycleCountNotification", 'uses' => 'CycleNotificationController@autoCompleteLocationAddCCN']);
        $api->post('/ccn/{whs_id:[0-9]+}/save',
            ['action' => "createCycleCountNotification", 'uses' => 'CycleNotificationController@store']);
        $api->put('/ccn/{whs_id:[0-9]+}/save/{ccnID:[0-9]+}',
            ['action' => "createCycleCountNotification", 'uses' => 'CycleNotificationController@store']);
        $api->get('/ccn/{whs_id:[0-9]+}/auto-sku',
            ['action' => "viewCycleCountNotification", 'uses' => 'CycleNotificationController@skuCcnInventoryComplete']);
        $api->get('/ccn/reason-drop-down',
            ['action' => "viewCycleCountNotification", 'uses' => 'CycleNotificationController@getReasonDropDown']);
        $api->get('/ccn/{whs_id:[0-9]+}/status',
            ['action' => "viewCycleCountNotification", 'uses' => 'CycleNotificationController@status']);
        $api->get('/ccn/{whs_id:[0-9]+}/show/{ccnId:[0-9]+}',
            ['action' => "viewCycleCountNotification", 'uses' => 'CycleNotificationController@show']);

        //create CycleCount
        $api->post('/ccn/{whs_id:[0-9]+}/add-list-cycle-count',
            ['action' => "createCycleCount", 'uses' => 'CycleNotificationController@addListCycleCount']);

        $api->get('/ccn/{whs_id:[0-9]+}/complete',
            ['action' => "viewCycleCountNotification", 'uses' => 'CycleNotificationController@getAutocompleteLocations']);
        $api->put('/ccn/{whs_id:[0-9]+}/remove/{ccnId:[0-9]+}',
            ['action' => "createCycleCountNotification", 'uses' => 'CycleNotificationController@destroy']);
    });
});
