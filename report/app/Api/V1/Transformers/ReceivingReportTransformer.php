<?php

namespace App\Api\V1\Transformers;

use Seldat\Wms2\Models\ReceivingReport;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\GoodsReceiptDetail;
use Seldat\Wms2\Models\User;

use League\Fractal\TransformerAbstract;


class ReceivingReportTransformer extends TransformerAbstract
{
    
    /*
    ***************************************************************************
    */
    
    public function transform(ReceivingReport $receivingReport)
    {
        $packSize = object_get($receivingReport, 'AsnDtl.asn_dtl_pack', 0);
        $grpack = object_get($receivingReport,'GoodsReceiptDetail.pack', 0 );
        $asnDate = object_get($receivingReport, 'AsnDtl.created_at', '');
        
        $grDate = object_get($receivingReport, 'GoodsReceiptDetail.created_at', '');

        $grHDRID = $receivingReport->gr_hdr_id;
        $gr_item_ttl = GoodsReceiptDetail::where('gr_hdr_id', $grHDRID)
                                    ->count('item_id');
        
        $gr_ctn_ttl = GoodsReceiptDetail::where('gr_hdr_id', $grHDRID)
                                    ->sum('gr_dtl.gr_dtl_act_ctn_ttl', '-', 'gr_dtl.gr_dtl_is_dmg');
      
        $asnHDRID = $receivingReport->asn_hdr_id;
        $asn_item_ttl = AsnDtl::where('asn_hdr_id', $asnHDRID)
                                    ->count('item_id');
        
        $asndtlid = object_get($receivingReport, 'AsnDtl.asn_dtl_id');
        $asn_ctn_ttl = AsnDtl::where('asn_dtl_id', $asndtlid)
                                    ->sum('asn_dtl_ctn_ttl');
        
        
        $asnUserFName= object_get($receivingReport, 'User.first_name');
        $asnUserLName= object_get($receivingReport, 'User.last_name');
        $asnFullName= $asnUserFName . ' ' . $asnUserLName;
        
        $grUserFName= object_get($receivingReport, 'User.first_name');
        $grUserLName= object_get($receivingReport, 'User.last_name');
        $grFullName= $grUserFName . ' ' . $grUserLName;
        
//date('m/d/Y', strtotime($receivingReport->created_at)),

        return [
            'cus_name' => object_get($receivingReport, 'customer.cus_name', ''),
            'ctnr_num' => $receivingReport->ctnr_num,
            'whs_name' => object_get($receivingReport, 'warehouse.whs_name', ''),
            'asn_number' => object_get($receivingReport, 'AsnHdr.asn_hdr_num'),
            'gr_hdr_num' => $receivingReport->gr_hdr_num,
            'asn_full_name' => $asnFullName,
            'asn_created_at' => $asnDate ? $asnDate->format('m/d/Y') : NULL, 
            'asn_ttl_sku'=>$asn_item_ttl,
            'asn_ttl_ctn'=> $asn_ctn_ttl,
            'asn_ttl_pcs' => $asn_ctn_ttl*$packSize,
            'gr_full_name' => $grFullName,
            'gr_created_at' => $grDate ? $grDate->format('m/d/Y') : NULL,
            'gr_ttl_sku' => $gr_item_ttl,
            'gr_ttl_ctn'=>$gr_ctn_ttl,
            'gr_ttl_pcs'=>$gr_ctn_ttl*$grpack,
            'gr_dtl_plt_ttl' => object_get($receivingReport, 'gr_hdr_plt_ttl', 0),

            'length'=> object_get($receivingReport, 'AsnDtl.Item.length', ''),
            'width'=> object_get($receivingReport, 'AsnDtl.Item.width', ''),
            'height'=> object_get($receivingReport, 'AsnDtl.Item.height', ''),
            'cube'=> round(($gr_ctn_ttl * (object_get($receivingReport, 'AsnDtl.Item.length', 0) *
                    object_get($receivingReport, 'AsnDtl.Item.width', 0) *
                    object_get($receivingReport, 'AsnDtl.Item.height', '')))/1728 , 2),
        ];
    }
    
    /*
    ***************************************************************************
    */
    
}