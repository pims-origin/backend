<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\OrderHdrMeta;

class OdrHdrMetaModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    protected  $odrHdrMeta;
    public function __construct(OrderHdrMeta $model = null)
    {
        $this->model = ($model) ?: new OrderHdrMeta();
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function insertOdrHdrMeta($data){
        return $this->model->create($data);
    }

    public function getValue($odrId, $qualifier)
    {
        return $this->model->where([
            'odr_id'    => $odrId,
            'qualifier' => $qualifier
        ])
            ->value('value');
    }
}
