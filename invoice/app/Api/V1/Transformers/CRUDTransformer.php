<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\SysBug;

class CRUDTransformer extends TransformerAbstract
{
    
    /**
     * @param SysBug $sysBug
     * @return array
     */
    public function transform(SysBug $sysBug)
    {
        return [
            'header' => $sysBug->id,
            'date' => $sysBug->date,
            'api' => $sysBug->api_name,
            'body' => $sysBug->error
        ];
    }
}
