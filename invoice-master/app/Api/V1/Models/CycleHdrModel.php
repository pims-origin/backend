<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use App\CycleDtl;
use App\CycleHdr;
use App\Wms2\UserInfo\Data;

/**
 * Class GoodsReceiptHdrModel
 *
 * @package App\Api\V1\Models
 */
class CycleHdrModel extends AbstractModel
{
    /**
     * OrderHdrModel constructor.
     *
     * @param CycleHdr|null $model
     */
    public function __construct(CycleHdr $model = null)
    {
        $this->model = ($model) ?: new CycleHdr();
    }
    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        if (isset($attributes['cycle_name'])) {
            $attributes['cycle_num'] = $attributes['cycle_name'];
        }
        $searchEqualFields = [

        ];

        $searchLikeFields = [
            'cycle_num'
        ];

        foreach ($attributes as $key => $value) {
            //search equal
            if (in_array($key, $searchEqualFields)) {
                if ($value != '') {
                    $query->where($this->getTable().'.' . $key, $value);
                }
            }
            //search like
            if (in_array($key, $searchLikeFields)) {
                if ($value != '') {
                    $query->where($this->getTable().'.' . $key, 'like', '%' . escape_like($value) . '%');
                }
            }
        }
        //filter Order that cycle_num not in ref_num_arr array
        if (!empty($attributes['ref_num_arr'])) {
            $query->whereNotIn($this->getTable().'.cycle_num', $attributes['ref_num_arr']);
        }

        //Filter whs_id and cus_id
        $predis = new Data();
        $userInfo = $predis->getUserInfo();
        $currentWH = array_get($userInfo, 'current_whs', 0);
        $query->where($this->getTable().'.whs_id', $currentWH);
        $cus_ids = $predis->getCurrentCusIds();
        $query->whereHas('cycleDtl', function ($query) use ($cus_ids, $attributes){
            $query->whereIn((new CycleDtl())->getTable().'.cus_id', $cus_ids);
            if (isset($attributes['cus_id']) && $attributes['cus_id'] != '') {
                $query->where((new CycleDtl())->getTable().'.cus_id', $attributes['cus_id']);
            }
        });

        $models = $query->paginate($limit);

        return $models;
    }
}
