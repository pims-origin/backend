<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\V3\Outbound\Models;

use Seldat\Wms2\Models\OutPallet;

class OutPalletModel extends AbstractModel
{
    /**
     * @param OutPallet $model
     */
    public function __construct()
    {
        $this->model = new OutPallet();
    }

    public function getOutPalletByPltNum($whsId, $pltNums)
    {
        $pltNums = is_array($pltNums) ? $pltNums : [$pltNums];
        $query = $this->model
            ->whereIn('plt_num', $pltNums)
            ->where('whs_id', $whsId)
        ;

        return $query->get();

    }

    public function updateOutPalletLocation($location, $pltNums)
    {
        $result = $this->model
            ->whereIn('plt_num', $pltNums)
            ->where('whs_id', $location->loc_whs_id)
            ->update([
                'loc_id' => $location->loc_id,
                'loc_code' => $location->loc_code,
                'loc_name' => $location->loc_alternative_name,
            ]);

        return $result;

    }

    public function getOdrsByPltNums($pltNums, $whsId)
    {
        $result = $this->model
            ->join('pack_hdr as ph', 'ph.out_plt_id', '=', 'out_pallet.plt_id')
            ->join('odr_hdr as oh', 'ph.odr_hdr_id', '=', 'oh.odr_id')
            ->whereIn('plt_num', $pltNums)
            ->where('oh.whs_id', $whsId)
            ->where('ph.deleted', 0)
            ->where('oh.deleted', 0)
            ->get();

        return $result;

    }

    public function checkOdrsByPltNums($pltNums, $whsId)
    {
        $result = $this->model
            ->join('pack_hdr as ph', 'ph.out_plt_id', '=', 'out_pallet.plt_id')
            ->join('odr_hdr as oh', 'ph.odr_hdr_id', '=', 'oh.odr_id')
            ->join('bol_odr_dtl as bo', 'oh.odr_id', '=', 'bo.odr_id')
            ->join('shipment as sm', 'sm.ship_id', '=', 'bo.ship_id')
            ->whereIn('plt_num', $pltNums)
            ->where('oh.whs_id', $whsId)
            ->where('ph.deleted', 0)
            ->where('oh.deleted', 0)
            ->get();

        return $result;

    }

}
