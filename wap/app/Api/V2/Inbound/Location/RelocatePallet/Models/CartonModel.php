<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V2\Inbound\Location\RelocatePallet\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Status;

class CartonModel extends AbstractModel
{
    /**
     * PalletModel constructor.
     */
    public function __construct(Carton $model = null)
    {
        $this->model = ($model) ?: new Carton();
    }

    /**
     * @param $ctn_ids
     *
     * @return bool
     */
    public function checkExistedCartons($ctn_ids)
    {
        $resultCount = $this->model->whereIn('ctn_id', $ctn_ids)->count();

        return $resultCount == count($ctn_ids) ? true : false;
    }

    /**
     * @param $arrCtnRfid
     * @param $whsId
     *
     * @return mixed
     */
    public function checkExistedCartonAccordingToRFID($arrCtnRfid, $whsId)
    {
        $result = $this->model
            ->where('rfid', $arrCtnRfid)
            ->where('whs_id', $whsId)
            ->first();

        return $result;
    }

    /**
     * @param $cus_ids
     *
     * @return mixed
     */
    public function countCartonsByCusIds($cus_ids)
    {
        $resultCount = $this->model
            ->select(
                'cus_id',
                DB::raw("(COUNT(cartons.ctn_id) ) AS numberOfCarton")
            )
            ->whereIn('cus_id', $cus_ids)
            ->groupBy('cus_id')
            ->get();

        return $resultCount;
    }

    /**
     * @param array $grDetail
     *
     * @return array
     */
    public function createCartonNumber($grDetail, $rfId)
    {
        $grDetailId = array_get($grDetail, 'gr_dtl_id', null);
        //get GR number
        $grNumber = array_get($grDetail, 'goods_receipt.gr_hdr_num', null);

        $cartons = [];
        //create CTN number by GR number
        $cartonCode = str_replace('GDR', 'CTN', $grNumber);
        if (!empty($grDetail) && is_array($grDetail)) {
            //get current carton number, count null is 0 so need add 1 for the first
            $countCTNObj = $this->model->where('gr_dtl_id', $grDetailId)->count() + 1;
            $cartonParams = [];
            $cartons[$countCTNObj] = $cartonCode . "-" . str_pad($countCTNObj, 4, "0", STR_PAD_LEFT);
            $cartonParams[] = $this->addTime([
                'asn_dtl_id'    => $grDetail['asn_dtl_id'],
                'gr_dtl_id'     => $grDetail['gr_dtl_id'],
                'item_id'       => array_get($grDetail, 'asn_detail.item_id', null),
                'whs_id'        => array_get($grDetail, 'goods_receipt.whs_id', null),
                'cus_id'        => array_get($grDetail, 'goods_receipt.cus_id', null),
                'ctn_num'       => $cartons[$countCTNObj],
                'rfid'          => $rfId,
                'ctn_sts'       => config('constants.ctn_status.ACTIVE'),
                'ctn_uom_id'    => array_get($grDetail, 'asn_detail.uom_id', null),
                'ctn_pack_size' => array_get($grDetail, 'asn_detail.asn_dtl_pack', null),
                'piece_remain'  => array_get($grDetail, 'asn_detail.asn_dtl_pack', null),
                'piece_ttl'     => array_get($grDetail, 'asn_detail.asn_dtl_pack', null),
            ]);

            $this->model->insert($cartonParams);
        }

        return $cartons;
    }

    /**
     * @param $ctnRfid
     *
     * @return mixed
     */
    public function getGrDtlIdByCtnRfid($ctnRfid)
    {
        $query = $this->model->select('gr_dtl_id')->where($this->model->getTable() . '.rfid', $ctnRfid)->first();

        return $query;
    }

    /**
     * @param $attributes
     *
     * @return mixed
     */
    public function assignedPalletToCarton($attributes)
    {
        return $this->model
            ->where('item_id', $attributes['item_id'])
            ->where('whs_id', $attributes['whs_id'])
            ->where('cus_id', $attributes['cus_id'])
            ->where('asn_dtl_id', $attributes['asn_dtl_id'])
            ->where(function ($sql) {
                $sql->orWhereNull('plt_id')
                    ->orWhere('plt_id', '0');
            })
            ->update(['plt_id' => $attributes['plt_id']]);
    }

    public function updateCartonWithPltID($attributes)
    {
        return $this->model
            ->where('plt_id', $attributes['plt_id'])
            ->update(
                [
                    'loc_id'        => $attributes['loc_id'],
                    'loc_code'      => $attributes['loc_code'],
                    'loc_name'      => $attributes['loc_name'],
                    'loc_type_code' => $attributes['loc_type_code'],
                ]);
    }

    /**
     * @param array $cartonParams
     *
     * @return array
     */
    public function addTime(array $cartonParams)
    {
        $userId = JWTUtil::getPayloadValue('jti') ?: 0;

        $deleted_at = getDefaultDatetimeDeletedAt();
        $created_at = time();
        $time = [
            'created_at' => $created_at,
            'updated_at' => $created_at,
            'deleted_at' => $deleted_at,
            'created_by' => $userId,
            'updated_by' => $userId,
            'deleted'    => 0
        ];

        return array_merge($cartonParams, $time);
    }

    public function saveRealCarton($data)
    {
        return $this->model->insert($data);
    }

    /**
     * @param $ctn_ids
     *
     * @return mixed
     */
    public function getCartonWithNewLocation($ctn_ids)
    {
        $ctn_ids = is_array($ctn_ids) ? $ctn_ids : [$ctn_ids];

        $rows = $this->model
            ->whereIn('ctn_id', $ctn_ids)
            ->get(['ctn_id', 'loc_id', 'loc_name', 'loc_code']);

        return $rows;
    }

    /**
     * @param $ids
     * @param array $with
     *
     * @return mixed
     */
    public function getPalletByAsn($ids, $with = [])
    {
        $query = $this->make($with);

        return $query
            ->whereIn('asn_dtl_id', $ids)
            ->groupBy('plt_id')
            ->get();
    }

    /**
     * @param $itemIds
     * @param array $with
     *
     * @return mixed
     */
    public function getItemLocation($itemIds, $with = [])
    {
        $query = $this->make($with);
        $query->select(['item_id', 'ctn_id', 'loc_id']);
        $query->whereIn('item_id', $itemIds);
        $query->where('loc_id', '>', 0);
        $query->groupBy('item_id');

        return $query->get();
    }

    /**
     * @param $asnDtlIds
     *
     * @return mixed
     */
    public function checkPalletAssigned($asnDtlIds)
    {
        return $this->model->whereIn('asn_dtl_id', $asnDtlIds)
            ->where('plt_id', '>', 0)
            ->first();
    }

    /**
     * @param $cartonId
     * @param $gr_dtl_id
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function getNextCtnId($cartonId, $gr_dtl_id)
    {
        $rs = $this->getFirstWhere(
            [
                "ctn_id" => ['ctn_id', '>', $cartonId],
                ['gr_dtl_id', '=', $gr_dtl_id]
            ],
            [],
            ['ctn_id' => "asc"]);

        return $rs;
    }

    /**
     * @param $locationId
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($locationId, array $with, $limit = null)
    {
        $query = $this->make($with);

        $query->where('loc_id', $locationId);

        $query->whereHas('pallet', function ($query) {
            // search whs id
            $query->whereNotNull('loc_id');
        });

        //filterData in getting Carton List By Location
        $this->model->filterData($query);

        return $query->paginate($limit);
    }

    /**
     * @param $palletId
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function searchPlt($palletId, array $with, $limit = null)
    {
        $query = $this->make($with);

        $query->where('plt_id', $palletId);

        $query->whereHas('pallet', function ($query) {
            // search whs id
            $query->whereNotNull('loc_id');
        });

        //filterData in getting Carton List By Pallet
        $this->model->filterData($query);

        return $query->paginate($limit);
    }

    /**
     * @param $locationId
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function searchItem($locationId, array $with, $limit = null)
    {
        $query = $this->make($with);
        $query->where('loc_id', $locationId);
        $query->groupBy('item_id');

        $query->whereHas('pallet', function ($query) {
            // search whs id
            $query->whereNotNull('loc_id');
        });

        //filterData in getting Items List for relocation
        $this->model->filterData($query);

        return $query->paginate($limit);
    }

    /**
     * @param array $param
     */
    public function removeWhereInLocation(array $param)
    {
        $this->model->whereIn('loc_id', $param)->update([
            'loc_id'   => null,
            'loc_name' => null,
            'loc_code' => null
        ]);
    }

    public function getDamageCarton($goodReceiptId)
    {
        $query = $this->make(['goodReceiptDtl.goodsReceipt']);

        $query->where('is_damaged', 1);

        $query->whereHas('goodReceiptDtl.goodsReceipt', function ($query) use ($goodReceiptId) {
            $query->where('gr_hdr_id', $goodReceiptId);
        });

        return $query->get()->transform(function ($item) {
            return [
                'ctn_num'  => object_get($item, 'ctn_num', ''),
                'dmg_type' => object_get($item, 'damageCarton.damageType.dmg_name', '')
            ];
        });

    }

    public function updateWhereIn(array $update, array $valueWhere, $columns)
    {
        return $this->model->whereIn($columns, $valueWhere)->update($update);
    }

    /**
     * @param $locationId
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function searchItemInLocation($locationId, array $with, $limit = null)
    {
        $query = $this->make($with);
        $query->where('loc_id', $locationId);
        $query->groupBy('item_id');

        $query->whereHas('pallet', function ($query) {
            // search whs id
            $query->whereNotNull('loc_id');
        });

        return $query->paginate($limit);
    }

    /**
     * @param $cartonIds
     * @param array $with
     *
     * @return mixed
     */
    public function getDmgQty($cartonIds, $with = [])
    {
        $query = $this->make($with)
            ->select([
                DB::raw("sum(ctn_pack_size) as dmg_qty"),
                "item_id",
                "cus_id",
                "whs_id"
            ])
            ->whereIn("ctn_id", $cartonIds)
            ->groupBy("item_id");

        return $query->get();
    }

    public function getGrDtlIdByPltRfid($pltId)
    {
        $query = $this->model->select('gr_dtl_id')->where($this->model->getTable() . '.plt_id', $pltId)->first();

        return $query;
    }

    /**
     * @param $listRfId
     *
     * @return mixed
     */
    public function getPltIdByRfId($listRfId)
    {
        return $this->model->select('plt_id')->whereIn('rfid', $listRfId)->get();
    }

    public function countCTNObj($grDetailId)
    {
        return $this->model->where('gr_dtl_id', $grDetailId)->count() + 1;
    }

    public function getCountCTNNum($grHdrNum)
    {
        return $this->model
            ->join('gr_dtl', 'gr_dtl.gr_dtl_id', '=', 'cartons.gr_dtl_id')
            ->join('gr_hdr', 'gr_hdr.gr_hdr_id', '=', 'gr_dtl.gr_hdr_id')
            ->where('gr_hdr.gr_hdr_num', $grHdrNum)
            ->count();
    }

    public function getMaxNum($grHdrId)
    {
        return $this->model
            ->where([
                'gr_hdr_id' => $grHdrId,
                'deleted'   => 0,
            ])
            ->max('ctn_num');
    }


    public function loadViewLayout($whsId, $with = [])
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = $this->make($with)
            ->select([
                'sku',
                'size',
                'color',
                'lot',
                'loc_id',
                DB::raw('sum(piece_ttl) as piece_ttl'),
                DB::raw('count(ctn_id) as ctn_ttl'),
                DB::raw('count(distinct item_id,lot) as sku_ttl')
            ])
            ->where('whs_id', $whsId)
            ->whereNotNull('loc_id')
            ->whereIn('ctn_sts', ['AC', 'LK'])
            ->groupBy('loc_id')
            ->get();

        return $query;
    }

    public function getCartonsByLocIds($whsId, $locIds)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = $this->make([])
            ->select([
                'sku',
                'size',
                'color',
                'lot',
                'loc_id',
                DB::raw('sum(piece_ttl) as piece_ttl'),
                DB::raw('count(ctn_id) as ctn_ttl'),
                DB::raw('count(distinct item_id,lot) as sku_ttl')
            ])
            ->where('whs_id', $whsId)
            ->whereIn('loc_id', $locIds)
            ->whereNotNull('loc_id')
            ->whereIn('ctn_sts', ['AC', 'LK'])
            ->groupBy('loc_id')
            ->get();

        return $query;
    }


    public function getCartonsByLocs($locIds, $limit = null)
    {
        $query = $this->model->select(['ctn_id', 'ctn_num', 'rfid as ctn_rfid'])
            ->whereIn('loc_id', $locIds)
            ->where('ctn_sts', "AC")
            ->where('loc_type_code', 'RAC')
            ->where(function ($q) {
                $q->orWhere('is_damaged', 0);
                $q->orWhereNull('is_damaged');
            })
            ->where('is_ecom', 0);

        if (!empty($limit)) {
            return $query->limit($limit)->get();
        } else {
            return $query->get();
        }
    }


    public function getCtnByLocationId($locId)
    {
        return $this->model
            ->where('loc_id', $locId)
            ->first();
    }


    public function countCtnByLocationId($locId)
    {
        return $this->model
            ->where('loc_id', $locId)
            ->count();
    }

    public function sumPieceOfCtnByLocationId($locId)
    {
        return $this->model->select(DB::raw('SUM(piece_remain) as pieces'))
            ->where('loc_id', $locId)
            ->first();
    }

    public function checkActiveCartonByCtnNum($ctnNum)
    {
        $ctnNums = is_array($ctnNum) ? $ctnNum : [$ctnNum];

        return $this->model->whereIn('ctn_num', $ctnNums)
            ->where('ctn_sts', Status::getByKey('CTN_STATUS', 'ACTIVE'))
            ->where('piece_remain', '>', 0)
            ->whereRaw('(is_damaged = 0 OR is_damaged is null)')
            ->whereRaw('(is_ecom = 0 OR is_ecom is null)')
            ->whereNotNull('loc_id')
            ->select('ctn_id', 'ctn_num')
            ->get();
    }

    public function getAllCartonByItemId($data)
    {
        return $this->model->whereIn('ctn_num', $data['ctnNums'])
            ->where('ctn_sts', Status::getByKey('CTN_STATUS', 'ACTIVE'))
            ->where('item_id', $data['itemId'])
            ->where('whs_id', $data['whsId'])
            ->where('cus_id', $data['cusId'])
            ->where('piece_remain', '>', 0)
            ->whereRaw('(is_damaged = 0 OR is_damaged is null)')
            ->whereRaw('(is_ecom = 0 OR is_ecom is null)')
            ->whereNotNull('loc_id')
            ->select('ctn_id', 'ctn_num', 'rfid as ctn_rfid', 'piece_remain', 'created_at', 'loc_id', 'loc_code',
                'plt_id')
            ->get();
    }

    public function getCartonByPltID($pltIDs)
    {
        return $this->model->whereIn('plt_id', $pltIDs)->first();
    }

    public function sumAvailQtyByAsnDtlId($asnDtlId)
    {
        $qty = (int)$this->model->where('asn_dtl_id', $asnDtlId)
            ->where('is_damaged', 0)
            ->select(DB::raw('SUM(piece_remain) as qty'))->value('qty');

        return $qty;
    }

    public function sumDamagedQtyByAsnDtlId($asnDtlId)
    {
        $qty = (int)$this->model->where('asn_dtl_id', $asnDtlId)
            ->where('is_damaged', 1)
            ->select(DB::raw('SUM(piece_remain) as qty'))->value('qty');

        return $qty;
    }

    public static function getCalculateStorageDurationRaw($startDate = 'created_at')
    {
        $strSQL = sprintf("IF(DATEDIFF('%s', FROM_UNIXTIME(`%s`)) > 0 , DATEDIFF('%s', FROM_UNIXTIME
        (`%s`)), 1)", date('Y-m-d'), $startDate, date('Y-m-d'), $startDate);

        return DB::raw($strSQL);
    }

    public function getCartonsInfoByPalletRFID($whsId, $pltID)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = $this->getModel()
            ->select([
                'sku',
                'size',
                'color',
                'lot',
                'loc_id',
                DB::raw('sum(piece_ttl) as piece_ttl'),
                DB::raw('count(ctn_id) as ctn_ttl')
            ])
            ->where('cartons.whs_id', $whsId)
            ->where('cartons.plt_id', $pltID)
            ->groupBy('item_id', 'lot');

        $res = $query->get();

        return $res;
    }

    /**
     * 5. Check Virtual/Real Cartons if existed and not in current pallet, delete  virtual and real cartons
     *
     * @param type $pltRfid
     * @param type $ctnsRfid
     *
     * @return type
     */
    public function deleteCartonExistInPallet($pltId, $ctnsRfid)
    {
        $result = $this->model
            ->where('plt_id', $pltId)
            ->whereNotIn('rfid', $ctnsRfid)
            ->delete();

        return $result;
    }


    public function getCartonRFIDs(array $rfids)
    {
        $res = $this->model
            ->whereIn('rfid', $rfids)
            ->select('rfid')
            ->get()
            ->toArray();

        return $res;
    }

    public function createCartons($params)
    {
        $userId = $this->getUserId();
        list ($pallet, $grHdr, $grDtl, $asnDetail, $vtlCtns, $palletExist) = array_values($params);
        $ctn_ttl = count($vtlCtns);
        if ($palletExist) {

            $rfids = array_pluck($vtlCtns, 'ctn_rfid');
            $ctns = $this->getCartonRFIDs($rfids);
            $ctns = array_pluck($ctns, 'rfid');

            $vtlCtns = array_filter($vtlCtns, function ($item) use ($ctns) {
                return !in_array($item['ctn_rfid'], $ctns);
            });
        }

        $grHdrNum = $grHdr->gr_hdr_num;
        $grHdrId = $grHdr->gr_hdr_id;

        $ctnData = [
            'plt_id'           => array_get($pallet, 'plt_id', null),
            'asn_dtl_id'       => object_get($grDtl, 'asn_dtl_id', null),
            'gr_dtl_id'        => object_get($grDtl, 'gr_dtl_id', null),
            'item_id'          => array_get($asnDetail, 'item_id', null),
            'whs_id'           => array_get($pallet, 'whs_id', null),
            'cus_id'           => array_get($pallet, 'cus_id', null),
            'loc_id'           => array_get($pallet, 'loc_id', null),
            'loc_code'         => array_get($pallet, 'loc_code', null),
            'loc_name'         => array_get($pallet, 'loc_name', null),
            'ctn_pack_size'    => array_get($asnDetail, 'asn_dtl_pack', 0),
            'piece_remain'     => array_get($asnDetail, 'asn_dtl_pack', 0),
            'piece_ttl'        => array_get($asnDetail, 'asn_dtl_pack', 0),
            'ctn_uom_id'       => array_get($asnDetail, 'uom_id', 0),
            'ctn_sts'          => Status::getByKey('CTN_STATUS', 'ACTIVE'),
            'gr_dt'            => time(),
            'gr_hdr_id'        => $grHdrId,
            'sku'              => array_get($asnDetail, 'asn_dtl_sku', 0),
            'size'             => array_get($asnDetail, 'asn_dtl_size', 'NA'),
            'color'            => array_get($asnDetail, 'asn_dtl_color', 'NA'),
            'lot'              => array_get($asnDetail, 'asn_dtl_lot', 'NA'),
            'po'               => array_get($asnDetail, 'asn_dtl_po', 'NA'),
            'uom_code'         => array_get($asnDetail, 'uom_code', 0),
            'uom_name'         => array_get($asnDetail, 'uom_name', 0),
            'upc'              => array_get($asnDetail, 'asn_dtl_cus_upc', 0),
            'ctnr_id'          => array_get($asnDetail, 'ctnr_id', 0),
            'ctnr_num'         => array_get($asnDetail, 'ctnr_num', 0),
            'is_ecom'          => 0,
            'picked_dt'        => 0,
            'storage_duration' => 0,
            'expired_dt'       => object_get($grDtl, 'expired_dt', 0),
            'return_id'        => null,
            'length'           => object_get($grDtl, 'length', 0),
            'width'            => object_get($grDtl, 'width', 0),
            'height'           => object_get($grDtl, 'height', 0),
            'weight'           => object_get($grDtl, 'weight', 0),
            'cube'             => object_get($grDtl, 'cube', 0),
            'volume'           => object_get($grDtl, 'volume', 0),
            'created_at'       => time(),
            'updated_at'       => time(),
            'created_by'       => $userId,
            'updated_by'       => $userId,
            'deleted_at'       => 915148800,
            'deleted'          => 0,
        ];

        $maxNum = null;
        $cartons = [];
        foreach ($vtlCtns as $vtlCtn) {
            $maxNum = $this->generateNum($grHdrNum, $grHdrId, $maxNum);
            $ctnData['rfid'] = array_get($vtlCtn, 'ctn_rfid', null);
            $ctnData['is_damaged'] = array_get($vtlCtn, 'is_damaged', 0);
            $ctnData['ctn_num'] = $maxNum;
            $cartons[] = $ctnData;
        }

        $this->model->insert($cartons);

        return $ctn_ttl;

    }


    public function generateNum($grHdrNum, $grHdrId, $maxNum = null)
    {

        $seq = 1;
        if (!$maxNum) {
            $maxNum = $this->getMaxNum($grHdrId);
        }

        // ctnNum
        $cartonCode = str_replace('GDR', 'CTN', $grHdrNum);

        if ($maxNum) {
            $parts = explode('-', $maxNum);
            $seq = array_pop($parts);
            $seq++;
        }

        $ctnNum = $cartonCode . "-" . str_pad($seq, 4, "0", STR_PAD_LEFT);

        return $ctnNum;
    }

    public function setDamagedCartons($vtlCtns)
    {
        $rfids = [];
        foreach ($vtlCtns as $ctn) {
            if ($ctn['is_damaged']) {
                $rfids[] = $ctn['ctn_rfid'];
            }
        }

        if (count($rfids) == 0) {
            return 0;
        }

        $sqlRfids = "'" . implode("','", $rfids) . "'";
        $sql = sprintf("REPLACE INTO damage_carton (ctn_id,created_at,created_by,updated_at,updated_by,deleted,deleted_at,
                    dmg_id,dmg_note)
                SELECT
                    ctn_id,created_at,created_by,updated_at,updated_by,deleted,deleted_at,1 AS dmg_id,' ' AS dmg_note
                FROM cartons
                WHERE rfid IN (%s)",
            $sqlRfids);

        if (DB::insert($sql)) {
            return count($rfids);
        }

        return 0;
    }

    /**
     * @param $pltId
     * @param $whsId
     * @param $arrCtnRfid
     *
     * @return mixed
     */
    public function updatePalletID($pltId, $whsId, $arrCtnRfid)
    {
        $result = $this->model
            ->where('whs_id', $whsId)
            ->whereIn('rfid', $arrCtnRfid)
            ->update([
                'plt_id' => $pltId
            ]);

        return $result;

    }

    /**
     * @param string $pltNum
     *
     * @return string
     */
    public function createCtnNumFromPltNum($pltObj, $seq = 0)
    {
        //create CTN number by Pallet number
        $cartonCode = str_replace('LPN', 'CTN', $pltObj->plt_num);
        if (!$seq) {
            //get current carton number, count null is 0 so need add 1 for the first
            $seq = $this->model->where('plt_id', $pltObj->plt_id)->count() + 1;
        }

        $ctnNum = $cartonCode . "-" . str_pad($seq, 4, "0", STR_PAD_LEFT);

        return $ctnNum;
    }

}
