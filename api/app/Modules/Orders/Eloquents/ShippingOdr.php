<?php

namespace App\Modules\Orders\Eloquents;

class ShippingOdr extends BaseSoftModel
{
    protected $table = 'shipping_odr';

    protected $primaryKey = 'so_id';

    protected $fillable = [
        'whs_id',
        'cus_id',
        'cus_odr_num',
        'po_total',
        'so_sts',
        'type',
    ];

    public function customer()
    {
        return $this->hasOne(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function warehouse()
    {
        return $this->hasOne(__NAMESPACE__ . '\Warehouse', 'whs_id', 'whs_id');
    }
}
