<?php

namespace App\Api\Ecom\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\PackHdr;

class PackTransformer extends TransformerAbstract
{
    public function transform(PackHdr $pack)
    {
        return [
            'pack_hdr_id'  => object_get($pack, 'pack_hdr_id'),
            'pack_hdr_num'  => object_get($pack, 'pack_hdr_num'),
            'tracking_number' => object_get($pack, 'tracking_number'),
            'ttl_sku' => count($pack->details)
        ];
    }
}
