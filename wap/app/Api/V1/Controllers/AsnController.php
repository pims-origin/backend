<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CartonModel;
use App\Api\V1\Models\ContainerModel;
use App\Api\V1\Models\CustomerModel;
use App\Api\V1\Models\EventTrackingModel;
use App\Api\V1\Models\GoodsReceiptDetailModel;
use App\Api\V1\Models\GoodsReceiptModel;
use App\Api\V1\Models\ItemModel;
use App\Api\V1\Models\AsnDtlModel;
use App\Api\V1\Models\AsnHdrModel;
use App\Api\V1\Models\SystemUomModel;
use App\Api\V1\Models\VirtualCartonModel;
use App\Api\V1\Models\VirtualCartonSumModel;
use App\Api\V1\Transformers\AsnDetailTransformer;
use App\Api\V1\Transformers\AsnListsCanSortTransformer;
use App\Api\V1\Transformers\AsnListsCanSortV1Transformer;
use App\Api\V1\Transformers\AsnListVirtualCartonTransformer;
use App\Api\V1\Transformers\GoodsReceiptTransformer;
use App\Api\V1\Transformers\SkuTransformer;
use App\Api\V1\Validators\AsnValidator;
use App\Api\V1\Validators\GoodsReceiptValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Item;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Validator;
use Dingo\Api\Http\Response;
use App\Api\V1\Models\Log;
use App\MyHelper;
use Seldat\Wms2\Models\CustomerWarehouse;

class AsnController extends AbstractController
{

    /**
     * @var AsnHdrModel
     */
    protected $asnHdrModel;
    /**
     * @var AsnDtlModel
     */
    protected $asnDtlModel;
    /**
     * @var ItemModel
     */
    protected $itemModel;
    /**
     * @var AsnValidator
     */
    protected $validator;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @var GoodsReceiptModel
     */
    protected $goodsReceiptModel;

    /**
     * @var GoodsReceiptTransformer
     */
    protected $goodsReceiptTransformer;

    /**
     * @var GoodsReceiptValidator
     */
    protected $goodsReceiptValidator;

    /**
     * @var GoodsReceiptDetailModel
     */
    protected $goodsReceiptDetailModel;

    /**
     * @var SystemUomModel
     */
    protected $systemUomModel;

    /**
     * @var CustomerModel
     */
    protected $customerModel;

    /**
     * @var VirtualCartonModel
     */
    protected $vtlCtnModel;

    /**
     * @var VirtualCartonSumModel
     */
    protected $vtlCtnSumModel;

    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var ContainerModel
     */
    protected $containerModel;

    /**
     * AsnController constructor.
     *
     * @param Item $itemModel
     * @param EventTrackingModel $eventTrackingModel
     */
    public function __construct(Item $itemModel, EventTrackingModel $eventTrackingModel)
    {
        $this->validator = new AsnValidator();
        $this->itemModel = new ItemModel();
        $this->eventTrackingModel = $eventTrackingModel;
        $this->goodsReceiptTransformer = new GoodsReceiptTransformer();
        $this->goodsReceiptValidator = new GoodsReceiptValidator();
        $this->asnHdrModel = new AsnHdrModel();
        $this->goodsReceiptModel = new GoodsReceiptModel();
        $this->asnDtlModel = new AsnDtlModel();
        $this->goodsReceiptDetailModel = new GoodsReceiptDetailModel();
        $this->systemUomModel = new SystemUomModel();
        $this->customerModel = new CustomerModel();
        $this->vtlCtnModel = new VirtualCartonModel();
        $this->vtlCtnSumModel = new VirtualCartonSumModel();
        $this->cartonModel = new CartonModel();
        $this->containerModel = new ContainerModel();
    }

    /**
     * @param Request $request
     * @param $whsId
     * @param AsnListsCanSortTransformer $asnListsCanSortTransformer
     * @return \Dingo\Api\Http\Response|void
     * @deprecated  DO NOT IN USE
     */
    public function searchCanSort(
        Request $request,
        $whsId,
        AsnListsCanSortTransformer $asnListsCanSortTransformer
    ) {
        //set_time_limit(0);
        $input = $request->getQueryParams();

        //get needed params
        $input['whs_id'] = $whsId;

        /*
         * start logs
         */

        $url = "/whs/$whsId/asns";
        $owner = $transaction = '';
        Log::info($request, $whsId, [
            'evt_code' => 'SCS',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Search can sort'
        ]);
        /*
         * end logs
         */

        try {
            // get list asn
            $asns = $this->asnHdrModel->search($input,
                ['asnDtl', 'asnStatus', 'customer', 'asnDtl.item', 'asnDtl.container', 'createdUser'],
                array_get($input, 'limit'));

            if (!empty($input['ctnr_id']) && !empty($asns)) {
                $ctnr = $this->containerModel->getFirstBy('ctnr_id', $input['ctnr_id']);
                if (!empty($ctnr)) {
                    foreach ($asns as $key => $asn) {
                        $asns[$key]['ctnr_num'] = $ctnr->ctnr_num;
                        $asns[$key]['ctnr_id'] = $ctnr->ctnr_id;
                    }
                }
            }

            return $this->response->paginator($asns, $asnListsCanSortTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function searchCanSortV1(
        Request $request,
        $whsId,
        AsnListsCanSortV1Transformer $asnListsCanSortTransformerV1
    ) {
        //set_time_limit(0);
        $input = $request->getQueryParams();
        //get needed params
        $input['whs_id'] = $whsId;

        /*
         * start logs
         */

        $url = "/whs/$whsId/asnsV1";
        $owner = $transaction = '';
        Log::info($request, $whsId, [
            'evt_code' => 'SCS',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get ASN List'
        ]);
        /*
         * end logs
         */

        try {
            $asnDtls = $this->asnDtlModel->getListASN(
                $input,
                array_get($input, 'limit')
            );

            $url = $_SERVER['SERVER_NAME'] . '/core/wap' . $_SERVER['REQUEST_URI'];
            $url = preg_replace('/\&page\=[0-9]+/', '', $url);  //fix bug page for wap
            $url = preg_replace('/\?page\=[0-9]+/', '', $url);  //fix bug page link for wms
            $asnDtls->setPath($url);

            foreach ($asnDtls as $asnDtl) {
                $items = null;
                if ((isset($input['sku']) && trim($input['sku']) != '')) {
                    $items = $this->asnDtlModel->getListItem($asnDtl->asn_hdr_id, $asnDtl->ctnr_id, $input['sku'], $input);
                } else {
                    $items = $this->asnDtlModel->getListItem($asnDtl->asn_hdr_id, $asnDtl->ctnr_id, null, $input);
                }

                $item_ = [];
                $totalCartons = 0;
                foreach ($items as $it) {
                    $it_ct['sku'] = $it['sku'];
                    $it_ct['size'] = $it['size'];
                    $it_ct['color'] = $it['color'];
                    $it_ct['pack'] = $it['pack'];
                    $it_ct['asn_dtl_ctn_ttl'] = $it['asn_dtl_ctn_ttl'];
                    $it_ct['asn_dtl_pack'] = $it['asn_dtl_pack'];
                    $it_ct['asn_dtl_sts'] = $this->asnDtlModel->formatStatus('default', $it['asn_dtl_sts']);
                    $it_ct['asn_sts_name'] = $it['asn_sts_name'];
                    $it_ct['item_id'] = $it['item_id'];
                    $it_ct['asn_dtl_id'] = $it['asn_dtl_id'];
                    $it_ct['asn_dtl_lot'] = $it['asn_dtl_lot'];
                    $it_ct['dtl_po'] = $it['asn_dtl_po'];

                    $actCtn = $this->vtlCtnModel->countCtnByASNDtlId($it['asn_dtl_id']);

                    $total_ = $it['asn_dtl_ctn_ttl'] * $it['asn_dtl_pack'];
                    $it_ct['total_pieces'] = '0/'. $total_;
                    $it_ct['act_carton']   = '0/'. $it['asn_dtl_ctn_ttl'];
                    $it_ct['current_ctn']  = $actCtn;
                    $it_ct['limit_ctn']    = $it['asn_dtl_ctn_ttl'];

                    if (!empty($actCtn) && $actCtn != 0) {
                        settype($actCtn, 'int');
                        $total_0 = $actCtn * $it['asn_dtl_pack'];
                        $it_ct['total_pieces'] = $total_0 . '/' . $total_;
                        $it_ct['act_carton']   = $actCtn . '/' . $it['asn_dtl_ctn_ttl'];
                        $totalCartons += $actCtn;
                    }
                    if ($actCtn == 0) {
                        $actCtn = $this->cartonModel->getModel()
                            ->where('asn_dtl_id', $it['asn_dtl_id'])->count();
                        if ($actCtn) {
                            settype($actCtn, 'int');
                            $total_0 = $actCtn * $it['asn_dtl_pack'];
                            $it_ct['total_pieces'] = $total_0 . '/' . $total_;
                            $it_ct['act_carton']   = $actCtn . '/' . $it['asn_dtl_ctn_ttl'];
                            $it_ct['current_ctn']  = $actCtn;
                            $totalCartons += $actCtn;
                        }
                    }

                    $item_[] = $it_ct;
                }
                $asnDtl->total_cartons = $totalCartons;
                $asnDtl->items = $item_;
                $asnDtl->total_sku = count($items);
            }

            return $this->response->paginator($asnDtls, $asnListsCanSortTransformerV1);
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whsId
     * @param Request $request
     * @param SkuTransformer $skuTransformer
     * @return \Dingo\Api\Http\Response|void
     */
    public function searchSku($whsId, Request $request, SkuTransformer $skuTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['whs_id'] = $whsId;

        /*
         * start logs
         */

        $url = "/whs/$whsId/skus";
        $owner = $transaction = '';
        Log::info($request, $whsId, [
            'evt_code' => 'LCL',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Load SKU list'
        ]);
        /*
         * end logs
         */

        try {
            $skus = $this->asnDtlModel->loadSkuList($input, [], array_get($input, 'limit'));
            // dd($skus);
            Log::respond($request, $whsId, [
                'evt_code' => 'LCL',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'response_data' => $this->response->collection($skus, $skuTransformer)
            ]);

            return $this->response->collection($skus, $skuTransformer);

        } catch (\PDOException $e) {
            $this->setMessage($e->getMessage());
            Log::error($request, $whsId, [
                'evt_code' => 'ERR',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => $e->getMessage(),
                'response_data' => $this->getResponseData()
            ]);
            return new Response($this->getResponseData(), 200, [], null);
        }
    }

    /**
     * @param Request $request
     * @param $whsId
     * @param AsnListsCanSortTransformer $asnListsCanSortTransformer
     * @return \Dingo\Api\Http\Response|void
     * Author:cuongnguyen
     */
    public function getReceivingASN(
        Request $request,
        $whsId,
        AsnListsCanSortTransformer $asnListsCanSortTransformer
    ) {
        $input = $request->getQueryParams();

        //get needed params
        $input['whs_id'] = $whsId;

        try {

            /*
             * start logs
             */

            $url = "/whs/$whsId/get-receiving-asn";
            $owner = $transaction = '';
            Log::info($request, $whsId, [
                'evt_code' => 'GRA',
                'owner' => $owner,
                'transaction' => $transaction,
                'url_endpoint' => $url,
                'message' => 'Get receive ASN'
            ]);
            /*
             * end logs
             */

            // get list asn
            $asns = $this->asnHdrModel->getASNReceiving($input,
                ['asnDtl', 'asnStatus', 'customer', 'asnDtl.item', 'asnDtl.container', 'createdUser'],
                array_get($input, 'limit'));

            if (!empty($input['ctnr_id']) && !empty($asns)) {
                $ctnr = $this->containerModel->getFirstBy('ctnr_id', $input['ctnr_id']);
                if (!empty($ctnr)) {
                    foreach ($asns as $key => $asn) {
                        $asns[$key]['ctnr_num'] = $ctnr->ctnr_num;
                        $asns[$key]['ctnr_id'] = $ctnr->ctnr_id;
                    }
                }
            }

            return $this->response->paginator($asns, $asnListsCanSortTransformer);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_GOOD_RECEIPT, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $whsId
     * @param $cusId
     * @param $asnID
     * @param $ctnID
     * @param AsnDetailTransformer $asnDetailTransformer
     * @return \Dingo\Api\Http\Response|void
     */
    public function loadASNDtl($whsId, $cusId, $asnID, $ctnID, AsnDetailTransformer $asnDetailTransformer, Request $request)
    {
        /*
         * start logs
         */
        $input = $request->getParsedBody();

        $url = "/whs/$whsId/cus/$cusId/asns/$asnID/containers/$ctnID";
        $owner = "";
        $transaction = "";
        Log::info($request, $whsId, [
            'evt_code' => 'LAD',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Load ASN Dtl'
        ]);
        /*
         * end logs
         */

        if (!$asnHrd = $this->asnHdrModel->getFirstWhere(['asn_hdr_id' => $asnID, 'whs_id' => $whsId, ['asn_sts', '<>', 'CO']])) {
            $msg = Message::get("BM017", "ASN");
            $asnHrd = $this->asnHdrModel->getFirstBy('asn_hdr_id', $asnID);
            if ($asnHrd) {
                $msg = "ASN doesn't belong to warehouse";
            }
            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];
            return new Response($data, 200, [], null);
        }

        $asnDtls = $this->asnDtlModel->findWhere(
            ['asn_hdr_id' => $asnID, 'ctnr_id' => $ctnID],
            ['container', 'grDtl', 'grDtl.goodsReceipt', 'item', 'systemUom', 'virtualCarton', 'virtualCartonSummary'],
            ['asn_dtl_id' => 'asc']
        );

        if (count($asnDtls) == 0) {
            $asnDtlsCheck = $this->asnDtlModel->getFirstWhere(['asn_hdr_id' => $asnID]);
            $msg = "Container doesn't exist or doesn't belong to ASN";
            $data = [
                'data' => null,
                'message' => $msg,
                'status' => false,
            ];
            return new Response($data, 200, [], null);
        }

        $details = [];
        if ($asnDtls) {
            foreach ($asnDtls as $asnDtl) {
                $details[] = [
                    'asn_dtl_id'             => $asnDtl->asn_dtl_id,
                    'asn_hdr_id'             => $asnDtl->asn_hdr_id,
                    'ctnr_id'                => $asnDtl->ctnr_id,
                    'ctnr_num'               => $asnDtl->container->ctnr_num,
                    'dtl_item_id'            => $asnDtl->item_id,
                    'asn_dtl_lot'            => $asnDtl->asn_dtl_lot,
                    'asn_dtl_cus_upc'        => $asnDtl->asn_dtl_cus_upc,
                    'dtl_sku'                => $asnDtl->item->sku,
                    'dtl_size'               => $asnDtl->item->size,
                    'dtl_color'              => $asnDtl->item->color,
                    'dtl_uom_id'             => $asnDtl->uom_id,
                    'dtl_uom_code'           => object_get($asnDtl, 'systemUom.sys_uom_code', ''),
                    'dtl_uom_name'           => object_get($asnDtl, 'systemUom.sys_uom_name', ''),
                    'dtl_po'                 => $asnDtl->asn_dtl_po,
                    'dtl_po_date'            => ($asnDtl->asn_dtl_po_dt) ? date('m/d/Y', $asnDtl->asn_dtl_po_dt) : '',
                    'dtl_ctn_ttl'            => $asnDtl->asn_dtl_ctn_ttl,
                    'gr_hdr_num'             => object_get($asnDtl, 'grDtl.goodsReceipt.gr_hdr_num', ''),
                    'dtl_gr_dtl_act_ctn_ttl' => object_get($asnDtl, 'grDtl.gr_dtl_act_ctn_ttl', 0),
                    'gr_dtl_plt_ttl'         => object_get($asnDtl, 'grDtl.gr_dtl_plt_ttl', 0),
                    'dtl_gr_dtl_is_dmg'      => object_get($asnDtl, 'grDtl.gr_dtl_is_dmg', 0),
                    'dtl_gr_dtl_disc'        => object_get($asnDtl, 'grDtl.gr_dtl_disc', 0),
                    'dtl_crs_doc'            => $asnDtl->asn_dtl_crs_doc,
                    'dtl_des'                => $asnDtl->asn_dtl_des,
                    'dtl_length'             => $asnDtl->asn_dtl_length,
                    'dtl_width'              => $asnDtl->asn_dtl_width,
                    'dtl_height'             => $asnDtl->asn_dtl_height,
                    'dtl_weight'             => $asnDtl->asn_dtl_weight,
                    'asn_dtl_pack'           => $asnDtl->asn_dtl_pack,
                    'dtl_lot'                => $asnDtl->asn_dtl_lot,
                    'dtl_ttl_piece'          => $asnDtl->asn_dtl_pack * $asnDtl->asn_dtl_ctn_ttl,
                    'scan_vir_ctn_ttl'       => $asnDtl->virtualCarton()->count(),
                    'asn_dtl_status'         => object_get($asnDtl, 'virtualCartonSummary.vtl_ctn_sum_sts', ''),
                    'gate_code'              => object_get($asnDtl, 'virtualCartonSummary.gate_code', ''),
                ];
            }
        }

        $asnHrd->details = $details;

        return $this->response->item($asnHrd, $asnDetailTransformer);
    }

    /**
     * @param $whsId
     * @param $cusId
     * @param $asnDtlId
     * @param Request $request
     * @param AsnListVirtualCartonTransformer $asnListVirtualCartonTransformer
     * @return \Dingo\Api\Http\Response
     */
    public function getAsnHistory($whsId, $cusId, $asnDtlId, Request $request,
        AsnListVirtualCartonTransformer $asnListVirtualCartonTransformer
    ) {
        $ansdtldt = $this->asnDtlModel->getFirstBy('asn_dtl_id', $asnDtlId);
        if(empty($ansdtldt)){
            $msg = sprintf('ASN detail %s is not existed.', $asnDtlId);
            //throw new \Exception($msg);
            $dataRequireFieldsValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataRequireFieldsValidate, 200, [], null);
        }

        //Fix WMS2-3961
        $this->checkCusBelongWhs($whsId, $cusId);
        $isBelong = $this->asnDtlModel->isBelongWhsAndCus($whsId, $cusId, $asnDtlId);
        if (!$isBelong) {
            //throw new \Exception("Asn detail doesn't belong to warehouse and customer");
            $msg = sprintf("Asn detail doesn't belong to warehouse and customer.");
            $dataRequireFieldsValidate = [
                'data'    => null,
                'message' => $msg,
                'status'  => false,
            ];

            return new Response($dataRequireFieldsValidate, 200, [], null);
        }

        $anslist = $this->asnDtlModel->findWhere([
            'asn_hdr_id' => $ansdtldt->asn_hdr_id
        ]);

        /*
         * start logs
         */
        $input = $request->getQueryParams();

        $url = "/whs/$whsId/cus/$cusId/asn-history/$asnDtlId";
        $owner = $transaction = "";
        Log::info($request, $whsId, [
            'evt_code' => 'GAH',
            'owner' => $owner,
            'transaction' => $transaction,
            'url_endpoint' => $url,
            'message' => 'Get ASN History'
        ]);
        /*
         * end logs
         */

        $ansHdr = $this->asnHdrModel->getFirstWhere([
            'asn_hdr_id' => $ansdtldt->asn_hdr_id
        ]);

        $anslist = $anslist->toArray();
        $data = []; $dt = [];
        foreach($anslist as $asn){
            $item = $this->itemModel->getFirstBy('item_id', $asn['item_id']);
            $dt['asn_dtl_id'] = $asn['asn_dtl_id'];
            $dt['item_id'] = $asn['item_id'];
            $dt['sku'] = $item->sku;
            $dt['size'] = $item->size;
            $dt['color'] = $item->color;
            $dt['asn_dtl_ctn_ttl'] = $asn['asn_dtl_ctn_ttl'];
            $dt['asn_dtl_sts'] = $asn['asn_dtl_sts'];
            $dt['asn_dtl_lot'] = $asn['asn_dtl_lot'];
            $count = $this->vtlCtnModel->countCtnByASNDtlId($asn['asn_dtl_id']);
            $dt['asn_ctn'] = $count . ' / ' . $dt['asn_dtl_ctn_ttl'];
            $dt['asn_num'] = object_get($ansHdr, 'asn_hdr_num', null);
            $data[] = $dt;
        }

        $asnDtl = $this->vtlCtnModel->getModel()
            ->where([
                'asn_dtl_id' => $asnDtlId,
                'whs_id'     => $whsId,
                'cus_id'     => $cusId
            ])
            ->orderBy('vtl_ctn_id', 'DESC')->get();

        $asnDtl = $this->formatArr($asnDtl->toArray(), true);
        $res = [];
        $res['history'] = $data;
        $res['carton'] = $asnDtl;

        return new Response($res, 200, [], null);
    }

    /**
     * Check customer is belong to warehouse
     *
     * @param integer $whsId
     * @param integer $cusId
     *
     * @throws \Exception
     */
    private function checkCusBelongWhs($whsId, $cusId) {
        $cusWhsModel = (new CustomerWarehouse())->where([
                    'whs_id' => $whsId,
                    'cus_id' => $cusId
                ])->first();
        if (!$cusWhsModel) {

            throw new \Exception("Customer doesn't belong to warehouse");
        }
    }
}
