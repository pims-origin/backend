<?php

namespace backend\forms;

use common\models\User;
use yii\helpers\VarDumper;

class EditProfileForm extends Form
{
    public $first_name;
    public $last_name;
    public $email;
    public $status;

    private $userStatuses = [User::STATUS_ACTIVE, User::STATUS_DELETED, User::STATUS_PENDING];

    public function rules()
    {
        return [
            ['email', 'email'],
            [['email'], 'string', 'max' => 64],
            [
                'email',
                'unique',
                'targetClass' => '\common\models\User',
                'message'     => 'This email has already been existed.'
            ],
            [['first_name', 'last_name'], 'string', 'max' => 35],
            ['status', 'validateStatus']
        ];
    }

    public function validateStatus($attribute, $params)
    {
        if (!in_array($this->status, $this->userStatuses, true)) {
            $this->addError($attribute, 'Status should be [' . implode(', ', $this->userStatuses) . ']');
        }
    }

    public function editProfile(User $user)
    {
        if ($this->email != null && $this->email == $user->email) {
            $this->email = null;
        }
        if (!$this->validate()) {
            return null;
        }
        if ($this->email != null) {
            $user->email = $this->email;
        }
        if ($this->first_name != null) {
            $user->first_name = $this->first_name;
        }
        if ($this->last_name != null) {
            $user->last_name = $this->last_name;
        }
        if ($this->status != null) {
            $user->status = $this->status;
        }
        $user->updated_at = time();
        $result = $user->save();

        return $result;
    }
}