<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Validators\BOLHdrValidator;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Helper;


/**
 * Class BOLController
 *
 * @package App\Api\V1\Controllers
 */
class MigrateController extends AbstractController
{

    CONST HG_ID =4 ;//1, 4
    CONST WHS_ID = 1;
    CONST LOC_ZONE = 6; // 3, 6
    CONST _TABLE = 'nj_vcny_loc_ctnr_rack_tmp'; // loc_ctnr_rack_tmp, nj_vcny_loc_ctnr_rack_tmp
    public function insertItem($data)
    {
        // Get info Items Exists
        $listItems = DB::table('item')
            ->select('item_id', 'sku', 'size', 'color')
            ->whereIn(DB::raw("CONCAT(sku,'-',size,'-',color)"), array_keys($data))->get();

        foreach ($listItems as $item) {
            $key = $item['sku'].'-'.$item['size'].'-'.$item['color'];
            if (!empty($data[$key])) {
                unset($data[$key]);
            }
        }
        $rs = [];
        foreach ($data as $item) {
            $skuSizeColor = sprintf('%s-%s-%s',
                array_get($item, 'sku', ''),
                array_get($item, 'size', ''),
                array_get($item, 'color', ''));
            $rs[$skuSizeColor] = [
                'sku'        => $item['sku'],
                'size'       => $item['size'],
                'color'      => $item['color'],
                'length'     => $item['length'],
                'width'      => $item['width'],
                'height'     => $item['height'],
                'weight'     => $item['weight'],
                'created_at' => time(),
                'created_by' => 1,
                'updated_at' => time(),
                'updated_by' => 1,
                'deleted_at' => 915148800,
                'deleted'    => 0,
                'volume'     => Helper::calculateVolume($item['length'], $item['width'], $item['height']),
                'cube'       => Helper::calculateCube($item['length'], $item['width'], $item['height']),
                'uom_name'   => 'EACH',
                'uom_code'   => 'EA',
                'uom_id'     => 1,
                'pack'       => $item['ctn_pack_size'],
                'cus_id'     => self::HG_ID,
                'status'     => 'AC',
            ];

        }
        try {
            DB::table('item')->insert($rs);

            //return DB::table('item')->where('cus_id', self::HG_ID)->take(count($rs))->skip(0)->get();
        } catch (\Exception $exception) {
            return $this->response->errorBadRequest($exception->getMessage());
        }

    }

    public function insertLocation($data)
    {
        try {
            $locations = DB::table('location')->select('loc_code')
                    ->whereIn('loc_code', array_keys($data))->get();
            foreach ($locations as $item) {
                if (!empty($data[$item['loc_code']])) {
                    unset($data[$item['loc_code']]);
                }
            }
            $locs = [];
            foreach ($data as $key => $location) {
                $locs[] = [
                    'loc_code'             => $key,
                    'loc_alternative_name' => $key,
                    'loc_whs_id'       => self::WHS_ID,

                    'loc_zone_id'      => self::LOC_ZONE,
                    'loc_type_id'      => 3,

                    'loc_sts_code'     => 'AC',
                    'loc_width'        => $location['width'],
                    'loc_length'       => $location['length'],
                    'loc_height'       => $location['height'],
                    'loc_max_weight'   => 0,
                    'loc_weight_capacity'   => 50,
                    'loc_min_count'   => 1,
                    'loc_max_count'   => 200,

                    'created_at'       => time(),
                    'updated_at'       => time(),
                    'updated_by'       => 1,
                    'created_by'       => 1,
                    'deleted'          => 0,
                    'deleted_at'       => 915148800

                ];
            }
            DB::table('location')->insert($locs);

        } catch (\Exception $exception) {
            return $this->response->errorBadRequest($exception->getMessage());
        }
    }

    public function insertContainers($data)
    {
        $containers = DB::table('container')->select('ctnr_num')
            ->whereIn('ctnr_num', $data)->get();

        foreach ($containers as $item) {
            if (!empty($data[$item['ctnr_num']])) {
                unset($data[$item['ctnr_num']]);
            }
        }
        try {
            if (!empty($data)) {
                $rs = [];
                foreach ($data as $name) {
                    $rs[] = [
                        'ctnr_num'   => $name,
                        'created_at' => time(),
                        'updated_at' => time(),
                        'created_by' => 1,
                        'updated_by' => 1,
                        'deleted_at' => 915148800,
                        'deleted'    => 0
                    ];
                }
                DB::table('container')->insert($rs);
            }
        } catch (\Exception $exception) {
            return $this->response->errorBadRequest($exception->getMessage());
        }
    }

    public function insertPallet($data)
    {
        try {
            $pallets = DB::table('pallet')->whereIn('loc_code', $data)->get();
            foreach ($pallets as $item) {
                if (!empty($data[$item['loc_code']])) {
                    unset($data[$item['loc_code']]);
                }
            }

            $locations = DB::table('location')->whereIn('loc_code', $data)->get();
            $pallets = [];
            foreach ($locations as $key => $location) {
                $pallets[] = [
                    'cus_id'           => self::HG_ID,
                    'whs_id'           => self::WHS_ID,
                    'plt_num'          => 'LPN-1612-' . str_pad($key + 1, 4, "0", STR_PAD_LEFT),
                    'ctn_ttl'          => 0,
                    'gr_hdr_id'        => null,
                    'gr_dtl_id'        => null,
                    'plt_sts'          => 'AC',
                    'created_at'       => time(),
                    'updated_at'       => time(),
                    'deleted'          => 0,
                    'deleted_at'       => 915148800,
                    'updated_by'       => 1,
                    'created_by'       => 1,
                    'loc_id'           => $location['loc_id'],
                    'loc_code'         => $location['loc_code'],
                    'loc_name'         => $location['loc_alternative_name'],
                    'storage_duration' => 0,
                    'is_movement'      => 0,

                ];
            }
            DB::table('pallet')->insert($pallets);

        } catch (\Exception $exception) {
            return $this->response->errorBadRequest($exception->getMessage());
        }

    }

    public function insertCartons($data, $locations, $items, $pallets, $ctnrs)
    {
        try {
            $cartonParams = [];

            $countItems = current(DB::table('cartons')->select(DB::raw('count(1)'))->where('ctn_num', 'like', 'CTN-1612-%')->first());
            $countItems = $countItems + 1;

            $arrError = [];
            foreach ($data as $key => $row) {
                $loc_id = $locations[$row['loc_code']];
                $item_id = $items[$row['sku'].'-'.$row['size'].'-'.$row['color']];
                $plt_id = $pallets[$row['loc_code']];
                if ($loc_id < 0) {
                    $arrError[$row['loc_code']] = $row['loc_code'];
                }
                if ($row['loc_code'] == 'B045-B-L4-02') {
                    if (empty($locations[$row['loc_code']])) {
                        die('Location');
                    }
                    if (empty($pallets[$row['loc_code']])) {
                        die('pallet');
                    }
                }

                $cartonParams[] = [
                    'asn_dtl_id'    => null,
                    'gr_dtl_id'     => null,
                    'gr_hdr_id'     => null,
                    'is_damaged'    => 0,
                    'item_id'       => $item_id,
                    'whs_id'        => self::WHS_ID,
                    'cus_id'        => self::HG_ID,
                    'ctn_num'       => 'CTN-1612-' . str_pad($countItems + $key, 4, "0", STR_PAD_LEFT),
                    'ctn_sts'       => 'AC',
                    'ctn_uom_id'    => 1,
                    'uom_code'      => 'EA',
                    'uom_name'      => 'EACH',
                    'loc_id'        => $loc_id,
                    'loc_code'      => $row['loc_code'],
                    'loc_name'      => $row['loc_name'],
                    'loc_type_code' => 'RAC',
                    'ctn_pack_size' => $row['ctn_pack_size'],
                    'piece_remain'  => $row['piece_remain'],
                    'piece_ttl'     => $row['piece_ttl'],
                    'gr_dt'         => strtotime($row['gr_dt']),
                    'sku'           => $row['sku'],
                    'size'          => $row['size'],
                    'color'         => $row['color'],
                    'lot'           => $row['lot'],
                    'po'            => $row['po'],
                    'upc'           => null,
                    'ctnr_id'       => $ctnrs[$row['containerName']],
                    'ctnr_num'      => $row['containerName'],
                    'length'        => $row['length'],
                    'width'         => $row['width'],
                    'height'        => $row['height'],
                    'weight'        => $row['weight'],
                    'volume'        => Helper::calculateVolume($row['length'], $row['width'], $row['height']),
                    'cube'          => Helper::calculateCube($row['length'], $row['width'], $row['height']),
                    'expired_dt'    => null,
                    'created_at'    => time(),
                    'updated_at'    => time(),
                    'deleted'       => 0,
                    'deleted_at'    => 915148800,
                    'updated_by'    => 1,
                    'created_by'    => 1,
                    'plt_id'        => $plt_id
                ];

            }
            if (!empty($arrError)) {
                dd(implode(', ', $arrError));
            }
            DB::table('cartons')->insert($cartonParams);

        } catch (\Exception $exception) {
            //dd($cartonParams[$key]);
            return $this->response->errorBadRequest($exception->getMessage());
        }
    }

    public function store(Request $request)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        //get params
        try {
            $jThis = $this;

            DB::table(self::_TABLE)->chunk(300, function($data) use ($jThis) {
            //DB::table('nj_vcny_loc_ctnr_rack_tmp')->chunk(300, function($data) use ($jThis) {
                DB::beginTransaction();

                // 1.2 create pallet
                $result = []; $locations = []; $arrItems = []; $containers = [];
                foreach ($data as $item) {
                    $loc_code = str_replace_first('B-', "B", $item['loc_code']);
                    $loc_code = str_replace('_', "-", $loc_code);
                    //$item['loc_code'] = str_replace('_', "-", $loc_code);
                    $result[$loc_code] = $loc_code;
                    $locations[$loc_code] = $item;

                    $key = $item['sku'].'-'.$item['size'].'-'.$item['color'];
                    $arrItems[$key] = $item;

                    $containers[$item['containerName']] = $item['containerName'];
                }

                // 1.1 insert item
                $jThis->insertItem($arrItems);

                // 1.1 insert item
                $jThis->insertContainers($containers);
                //dd($result);
                $jThis->insertPallet($result);

                $jThis->insertLocation($locations);

                DB::commit();
            });
            return 'Successfully Insert Data';

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function update(Request $request)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        //get params
        try {
            $jThis = $this;
            DB::table(self::_TABLE)->chunk(300, function($data) use ($jThis) {
            //DB::table('nj_vcny_loc_ctnr_rack_tmp')->chunk(300, function($data) use ($jThis) {
                DB::beginTransaction();

                $result = []; $locations = []; $arrItems = []; $containers = [];
                foreach ($data as $k => $item) {
                    $loc_code = str_replace_first('B-', "B", $item['loc_code']);
                    $loc_code = str_replace('_', "-", $loc_code);
                    //$item['loc_code'] = str_replace('_', "-", $loc_code);
                    $data[$k]['loc_code'] =  $loc_code;

                    $result[$loc_code] = $loc_code;

                    $key = $item['sku'].'-'.$item['size'].'-'.$item['color'];
                    $arrItems[$key] = $item;

                    $containers[$item['containerName']] = $item['containerName'];
                }

                $locations = DB::table('location')->select('loc_id', 'loc_code')->whereIn('loc_code', $result)->get();
                $locs = [];
                foreach ($locations as $loc) {
                    $locs[$loc['loc_code']] = $loc['loc_id'];
                }

                $pallets = DB::table('pallet')->select('plt_id', 'loc_code')->whereIn('loc_code', $result)->get();
                $plts = [];
                foreach ($pallets as $plt) {
                    $plts[$plt['loc_code']] = $plt['plt_id'];
                }
                $items = DB::table('item')
                    ->select('item_id', 'sku', 'size', 'color')
                    ->whereIn(DB::raw("CONCAT(sku,'-',size,'-',color)"), array_keys($arrItems))
                    ->get();
                $itms = [];
                foreach ($items as $itm) {
                    $key = $itm['sku'].'-'.$itm['size'].'-'.$itm['color'];
                    $itms[$key] = $itm['item_id'];
                }
                $ctnrs = DB::table('container')
                    ->select('ctnr_id', 'ctnr_num')
                    ->whereIn('ctnr_num', $containers)
                    ->get();

                $arrCtnrs = [];
                foreach ($ctnrs as $ctnr) {
                    $arrCtnrs[$ctnr['ctnr_num']] = $ctnr['ctnr_id'];
                }

                // 1.3 create cartons
                $jThis->insertCartons($data, $locs, $itms, $plts, $arrCtnrs);

                DB::commit();

            });

            return 'Successfully Update Cartons';

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    public function updateIvtSmr(Request $request)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        //get params
        try {
            DB::beginTransaction();
            // 1.4 invertory summay
            $cartons = DB::select("(select item_id,sku, cus_id, whs_id, size ,color, lot, sum(piece_remain) as ttl 
                    FROM cartons 
                    WHERE ctn_num like 'CTN-1612%'
                    AND cus_id = ".self::HG_ID." group by item_id, lot)");

            $rs = [];
            foreach ($cartons as $carton) {
                $rs[] = [
                    'item_id'       => $carton['item_id'],
                    'cus_id'        => $carton['cus_id'],
                    'whs_id'        => $carton['whs_id'],
                    'sku'           => $carton['sku'],
                    'size'          => $carton['size'],
                    'color'         => $carton['color'],
                    'lot'           => $carton['lot'],
                    'ttl'           => $carton['ttl'],
                    'avail'         => $carton['ttl'],
                    'picked_qty'    => 0,
                    'dmg_qty'       => 0,
                    'allocated_qty' => 0,
                    'back_qty'      => 0,
                    'ecom_qty'      => 0,
                    'created_at'    => time(),
                    'updated_at'    => time(),
                ];
            }
            // Insert IvtSmr
            DB::table('invt_smr')->insert($rs);

            $data = DB::table(self::_TABLE)->select('loc_code')->get();
            $locCodes = [];
            foreach ($data as $item) {
                $locCodes[$item['loc_code']] = $item['loc_code'];
            }
            $pallets = DB::table('pallet')->whereIn('loc_code', $locCodes)->get();
            foreach ($pallets as $pallet) {
                DB::table('pallet')
                    ->where('plt_id', $pallet['plt_id'])
                    ->update([
                        'ctn_ttl' => DB::raw("(SELECT COUNT(c.ctn_id) FROM cartons c WHERE pallet.plt_id = c.plt_id)")
                    ]);
            }
            DB::commit();
            return 'Successfully Update IvtSmr';

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

    }

    public function getLocId($locations, $loc_code)
    {
        foreach ($locations as $location) {
            if ($location['loc_code'] == $loc_code) {
                return $location['loc_id'];
            }
        }

        return -1;
    }

    public function getPalletId($pallets, $loc_code)
    {
        foreach ($pallets as $pallet) {
            if ($pallet['loc_code'] == $loc_code) {
                return $pallet['plt_id'];
            }
        }

        return -1;
    }

    public function getItemId($items, $row)
    {

        foreach ($items as $item) {
            if ($item['sku'] == $row['sku'] && $item['size'] == $row['size'] &&
                $item['color'] == $row['color']
            ) {
                return $item['item_id'];
            }
        }

        $rs = DB::table('item')->where('sku', $row['sku'])->where('size', $row['size'])->where('color', $row['color'])
            ->first();
        $item_id = array_get($rs, 'item_id', -1);

        return $item_id;
    }

}
