<?php
namespace App\Api\V2\Outbound\WavepickWithoutRfid\UpdateWavepick\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;

class OrderCartonModel extends AbstractModel
{

    /**
     * OrderCartonModel constructor.
     *
     * @param OrderCarton|null $model
     */
    public function __construct(OrderCarton $model = null)
    {
        $this->model = ($model) ?: new OrderCarton();
    }

    public function getPick($wvHdrId, $wvDtlId)
    {
        $query = $this->model
            ->select(['odr_hdr_id', 'odr_dtl_id', 'wv_hdr_id', 'wv_dtl_id', DB::raw("sum(piece_qty) as piece_ttl")])
            ->where('wv_hdr_id', $wvHdrId)
            ->where('wv_dtl_id', $wvDtlId)
            ->groupBy(['odr_hdr_id','odr_dtl_id'])
            ->get();

        return $query;
    }

    public function autoPack($wvHdrId)
    {
        $result = DB::table('odr_hdr as o')
            ->join('odr_hdr_meta as ohm', 'ohm.odr_id', '=', 'o.odr_id')
            ->join('odr_cartons as oc', 'o.odr_id', '=', 'oc.odr_hdr_id')
            ->join('cartons as c', 'c.ctn_id', '=', 'oc.ctn_id')
            ->where('ohm.value', 1)
            ->where('o.odr_sts', 'PD')
            ->where('o.wv_id', $wvHdrId)
            ->groupBy('c.ctn_id')
            ->get()
        ;

        return $result;
    }
}
