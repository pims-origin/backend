<?php

namespace Seldat\Wms2\Models;


class InvoiceHistoryMonth extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inv_his_month';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = [
        'cus_id',
        'whs_id',
        'type',
        'inv_id',
        'inv_num',
        'inv_date',
        'inv_sts'
    ];

}