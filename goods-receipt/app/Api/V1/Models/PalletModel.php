<?php
/**
 * Created by PhpStorm.
 * User: PhuongHong
 * Date: 22/July/16
 * Time: 11:43 AM
 */

namespace App\Api\V1\Models;

use Dingo\Api\Exception\UnknownVersionException;
use Dingo\Api\Exception\ValidationHttpException;
use App\Api\V1\Models\EventTrackingModel;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class PalletModel extends AbstractModel
{
    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var EventTrackingModel
     */
    protected $eventTrackingModel;

    /**
     * @param Pallet $model
     * @param CartonModel $cartonModel
     * @param LocationModel $locationModel
     * @param EventTrackingModel $eventTrackingModel
     */
    public function __construct(
        Pallet $model = null,
        CartonModel $cartonModel = null,
        LocationModel $locationModel = null,
        EventTrackingModel $eventTrackingModel = null

    ) {
        $this->model = ($model) ?: new Pallet();
        $this->cartonModel = ($cartonModel) ?: new CartonModel();
        $this->locationModel = ($locationModel) ?: new LocationModel();
        $this->eventTrackingModel = $eventTrackingModel;
    }

    public function findWhereGoodReceipt(array $attributes = [], array $with = [], $columns = ['*'])
    {
        $query = $this->make($with);

        $query->whereHas('carton.goodReceiptDtl', function ($query) use ($attributes) {
            $query->where('gr_hdr_id', $attributes['gr_hdr_id']);

        });
        $this->sortBuilder($query, $attributes);

        return $query->get($columns);

    }

    /**
     * @param $locationIds
     *
     * @return array
     */
    public function getPltIdsByLocIds($locationIds)
    {
        $locationIds = is_array($locationIds) ? $locationIds : [$locationIds];

        $rows = $this->model
            ->select('plt_id')
            ->whereIn('loc_id', $locationIds)
            ->get();

        $ids = [];
        if (!$rows->isEmpty()) {
            foreach ($rows as $row) {
                $ids[] = $row->plt_id;
            }
        }

        return $ids;
    }

    /**
     * @param $params
     * @param $grDetail
     *
     * @return bool
     */
    public function assignPallet($params, $grDetail)
    {
        $userId = JWTUtil::getPayloadValue('jti') ?: 1;
        $goodsReceipt = $grDetail[0]['goods_receipt'];

        //$itemIds = array_pluck($grDetail, 'asn_detail.item_id', 'asn_dtl_id');

        $palletCode = str_replace('GDR', 'LPN', $goodsReceipt['gr_hdr_num']);

        $index = 0;

        // For each Item
        $pallets = [];
        foreach ($params as $item) {
            // For each pallet in Item pallets.
            foreach ($item['pallets'] as $pallet) {
                $palletTotal = $pallet['pallet'];
                // Add each pallet.
                while ($palletTotal > 0) {
                    $index++;
                    // Add a pallet

                    $pallets[] = [
                        'cus_id'       => $goodsReceipt['cus_id'],
                        'whs_id'       => $goodsReceipt['whs_id'],
                        'plt_num'      => $palletCode . "-" . str_pad($index, 3, "0", STR_PAD_LEFT),
                        'ctn_ttl'      => $pallet['carton_per_pallet'],
                        'gr_hdr_id'    => $goodsReceipt['gr_hdr_id'],
                        'gr_dtl_id'    => $item['gr_dtl_id'],
                        'created_at'   => time(),
                        'updated_at'   => time(),
                        'deleted'      => 0,
                        'deleted_at'   => 915148800,
                        'created_by'   => $userId,
                        'updated_by'   => $userId,
                        'init_ctn_ttl' => $pallet['carton_per_pallet'],
                        'gr_dt'        => time(),
                    ];

                    $palletTotal--;
                }
            }

        }

        $cartons = DB::table('cartons')
            ->where('gr_hdr_id', $goodsReceipt['gr_hdr_id'])
            ->where('loc_id', null)
            ->select('ctn_id')
            ->get();
        $cartons = array_pluck($cartons, 'ctn_id');


        DB::table('pallet')->insert($pallets);

        $palletsNew = DB::table('pallet')
            ->whereNotExists(function($q) use($goodsReceipt)
            {
                $q->select(DB::raw(1))
                    ->from('cartons')
                    ->where('cartons.gr_hdr_id', '=', $goodsReceipt['gr_hdr_id'])
                    ->whereRaw('cartons.plt_id = pallet.plt_id');
            })
            ->where('pallet.gr_hdr_id', $goodsReceipt['gr_hdr_id'])->select('pallet.plt_id', 'pallet.ctn_ttl')
            ->orderBy('pallet.gr_dtl_id', 'ASC')->get();

        $queryUpate = '';
        $pos = 0;
        foreach (array_chunk($palletsNew, 1500) as $pls) {
            foreach ($pls as $pl) {
                $ctnIds = array_slice($cartons, $pos, $pl['ctn_ttl']);
                $pos += $pl['ctn_ttl'];
                $st_in = count($ctnIds) > 0 ? implode(',', $ctnIds) : '0';
                $queryUpate .= " UPDATE cartons SET `plt_id`=" . $pl['plt_id'] . ", `gr_dt`=" . time() . ' where ctn_id IN (' .
                    $st_in . ');';
            }
        }

        DB::connection()->getPdo()->exec($queryUpate);

        // DB::statement(DB::raw($queryUpate));
        // DB::raw($queryUpate);
        //
        //dd($pallets);
        //$param = [
        //    'plt_id'     => $palletResult->plt_id,
        //    'cus_id'     => $goodsReceipt['cus_id'],
        //    'whs_id'     => $goodsReceipt['whs_id'],
        //    'item_id'    => $item['item_id'],
        //    'asn_dtl_id' => $item['asn_dtl_id']
        //];
        //
        //
        //// Add carton to that pallet.
        //$this->cartonModel->assignedPalletToCarton($param, $pallet['carton_per_pallet']);
        //
        //$carton = $this->cartonModel->getFirstWhere(['plt_id' => $palletResult->plt_id]);
        //
        //$this->model->update(
        //    ['gr_dtl_id' => $carton->gr_dtl_id],
        //    ['plt_id' => $palletResult->plt_id]
        //);
        // update cartons

        return true;
    }


    public function suggestLocations($customerId, $warehouseId, $numOfPallet)
    {
        $locations = $this->locationModel->getSuggestedLocations($customerId, $warehouseId, $numOfPallet);

        return $locations->toArray();
    }

    public function suggestLocation($customerId, $warehouseId)
    {
        // Location not in table Pallet
        // Location in table pallet but ctn_total = 0 or null
        $exceptLocationIds = $this->model
            ->join('location', 'pallet.loc_id', '=', 'location.loc_id')
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->select(['location.loc_id'])
            ->where('location.loc_id', '>', 0)
            ->where('location.deleted', 0)
            ->where('loc_type.deleted', 0)
            ->where('loc_type.loc_type_code', '=', 'RAC')
            ->get()->toArray();

        $exceptLocationIds = array_pluck($exceptLocationIds, 'loc_id', 'loc_id');

        $location = $this->locationModel->loadSuggestedLocation($customerId, $warehouseId, $exceptLocationIds);

        return $location;
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === 'plt_num') {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
                if ($key === 'plt_sts') {
                    $query->where($key, SelStr::escapeLike($value));
                }
            }
        }

        //filterData in searching Pallet
        $this->model->filterData($query);

        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param array $param
     *
     * @return mixed
     */

    public function countPalletWhereInLocation(array $param)
    {
        $query = $this->model->whereIn('loc_id', $param);

        return $query->count();
    }

    /**
     * @param array $param
     */
    public function removeWhereInLocation(array $param)
    {
        return $this->model->whereIn('loc_id', $param)->update([
            'loc_id'           => null,
            'loc_name'         => null,
            'loc_code'         => null,
            'zero_date'        => time(),
            'storage_duration' => DB::raw('IF(DATEDIFF(FROM_UNIXTIME(updated_at), FROM_UNIXTIME(created_at)),
                        DATEDIFF(FROM_UNIXTIME(updated_at), FROM_UNIXTIME(created_at)), 1)'),
            'rfid'             => null,
            'deleted'          => 1,
            'deleted_at'       => time(),
        ]);
    }

    /**
     * Generate Asn number and sequence
     *
     * @return array
     */
    public function generateEventOwner($type)
    {

        $currentYearMonth = date('ym');

        $lastEvent = $this->eventTrackingModel->getFirstWhere(
            [['owner', 'like', '%' . $type . '%']],
            [],
            ['id' => 'desc']
        );
        $next = 1;
        if ($lastEvent) {
            list(, $yymm, $number) = explode('-', $lastEvent->owner);
            $next = ($yymm != $currentYearMonth) ? $next : intval($number) + 1;
        }

        $result = sprintf('%s-%s-%s',
            $type,
            $currentYearMonth,
            str_pad($next, 6, '0', STR_PAD_LEFT));

        return $result;
    }

    /**
     * @param $pallet_ids
     * @param null $limit
     *
     * @return mixed
     */
    public function getPalletById($pallet_ids, $limit = null)
    {
        $pallet_ids = is_array($pallet_ids) ? $pallet_ids : [$pallet_ids];
        $query = $this->make();
        $query->whereIn('plt_id', $pallet_ids);

        return $query->get();
    }

    public function updatePalletZeroDateAndDurationDays($pallet)
    {

        $created_at = object_get($pallet, 'created_at');
        // Calculate storage_duration
        $date1 = date("Y-m-d", is_int($created_at) ?: $created_at->timestamp);

        $date2 = date("Y-m-d");

        $dateDiff = (int)round(abs(strtotime($date1) - strtotime($date2)) / 86400);
        if ($dateDiff == 0) {
            $storageDuration = 1;
        } else {
            $storageDuration = $dateDiff;
        }

        $dataUpdate = [
            'loc_id'           => null,
            'loc_code'         => null,
            'loc_name'         => null,
            'storage_duration' => $storageDuration,

        ];
        $this->updateWhere($dataUpdate, [
            [
                "plt_id",
                "=",
                object_get($pallet, 'plt_id', 0)
            ]
        ]);

    }

    public function updatePalletZeroDateAndDurationDaysAuto()
    {
        $this->model
            ->whereNull('pallet.zero_date')
            ->where(DB::raw("(SELECT COUNT(cartons.ctn_id) FROM cartons WHERE cartons.plt_id = pallet.plt_id)"), 0)
            ->update([
                'pallet.zero_date'        => DB::raw('pallet.updated_at'),
                'pallet.ctn_ttl'          => 0,
                'pallet.loc_id'           => null,
                'pallet.loc_code'         => null,
                'pallet.loc_name'         => null,
                'pallet.storage_duration' =>
                    DB::raw("(IF(DATEDIFF(FROM_UNIXTIME(pallet.updated_at), FROM_UNIXTIME(pallet.created_at)),
					DATEDIFF(FROM_UNIXTIME(pallet.updated_at),FROM_UNIXTIME(pallet.created_at)), 1))")
            ]);

        $this->model
            ->whereNull('pallet.zero_date')
            ->where(DB::raw("(SELECT COUNT(cartons.ctn_id) FROM cartons WHERE cartons.plt_id = pallet.plt_id)"), '>', 0)
            ->update([
                'pallet.ctn_ttl' =>
                    DB::raw("(SELECT COUNT(cartons.ctn_id) FROM cartons WHERE cartons.plt_id = pallet.plt_id)")
            ]);
        DB::table('rpt_pallet')
            ->whereNull('rpt_pallet.zero_date')
            ->where(DB::raw("(SELECT COUNT(cartons.ctn_id) FROM cartons WHERE cartons.plt_id = rpt_pallet.plt_id)"), 0)
            ->update([
                'rpt_pallet.zero_date'        => DB::raw('rpt_pallet.updated_at'),
                'rpt_pallet.ctn_ttl'          => 0,
                'rpt_pallet.loc_id'           => null,
                'rpt_pallet.loc_code'         => null,
                'rpt_pallet.loc_name'         => null,
                'rpt_pallet.storage_duration' =>
                    DB::raw("(IF(DATEDIFF(FROM_UNIXTIME(rpt_pallet.updated_at), FROM_UNIXTIME(rpt_pallet.created_at)),
					DATEDIFF(FROM_UNIXTIME(rpt_pallet.updated_at),FROM_UNIXTIME(rpt_pallet.created_at)), 1))")
            ]);

        DB::table('rpt_pallet')
            ->whereNull('rpt_pallet.zero_date')
            ->where(DB::raw("(SELECT COUNT(cartons.ctn_id) FROM cartons WHERE cartons.plt_id = rpt_pallet.plt_id)"), '>', 0)
            ->update([
                'rpt_pallet.ctn_ttl' =>
                    DB::raw("(SELECT COUNT(cartons.ctn_id) FROM cartons WHERE cartons.plt_id = rpt_pallet.plt_id)")
            ]);

    }


    public function checkPltSameCustomer($whsId, $cusId, $pltId)
    {
        return $this->model
            ->where('plt_id', $pltId)
            ->where('whs_id', $whsId)
            ->where('cus_id', $cusId)
            ->first();
    }

    public function countPalletOnRackByGRHdrId($gHdrId)
    {
        return $this->model
            ->where([
                'gr_hdr_id' => $gHdrId
            ])
            ->whereNotNull('loc_id')
            ->count();
    }

    public function getPalletNotExistOnPalSugLoc($gHdrId)
    {
        return $this->model
            ->whereRaw(DB::raw(
                'plt_id NOT IN (SELECT plt_id FROM pal_sug_loc WHERE deleted = 0)'
            ))
            ->where('gr_hdr_id', $gHdrId)
            ->get();
    }

    /**
     * [getPalletByRFID WMS2-4529]
     *
     * @param  [type] $whsId [description]
     * @param  array $attr [description]
     * @param  array $with [description]
     * @param  [type] $limit [description]
     *
     * @return [type]        [description]
     */
    public function getPalletByRFID($whsId, $attr = [], array $with, $limit)
    {
        if (!$with) {
            $with = [];
        } elseif (!is_array($with)) {
            $with = [$with];
        }

        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attr);
        $query->where('ctn_ttl', '>', 0);
        $query->where('whs_id', $whsId);
        $query->where('plt_sts', 'AC');

        if (isset($attributes['rfid'])) {
            $query->where('pallet.rfid', 'like', "%" . SelStr::escapeLike($attributes['rfid']) . "%");
        }

        $this->sortBuilder($query, $attributes);

        return $query->paginate($limit);
    }

    /**
     * [checkPalletByPltId WMS2-4529]
     *
     * @param  [type] $whsId [description]
     * @param  [type] $pltId [description]
     *
     * @return [type]        [description]
     */
    public function checkPalletByPltId($whsId, $pltId)
    {
        return $this->model
            ->where('plt_id', $pltId)
            ->where('whs_id', $whsId)
            ->where('ctn_ttl', '>', 0)
            ->where('deleted', '=', 0)
            ->where('plt_sts', 'AC')
            ->first();
    }

    /**
     * [checkPalletByRFID WMS2-4529]
     *
     * @param  [type] $whsId [description]
     * @param  [type] $rfid  [description]
     *
     * @return [type]        [description]
     */
    public function checkPalletByRFID($whsId, $rfid)
    {
        return $this->model
            ->where('rfid', $rfid)
            ->where('whs_id', $whsId)
            ->where('ctn_ttl', '>', 0)
            ->where('deleted', '=', 0)
            //->whereIn('plt_sts', ['AC', 'LK', 'AL', 'RG', 'NW'])
            ->first();
    }

    /**
     * [getPalletNumber description]
     *
     * @param  [type] $rfid [description]
     *
     * @return [type]       [description]
     */
    public function getPalletNumber($rfid)
    {
        $palletNumInfo = $this->model
            ->select(DB::raw('plt_num'))
            ->orderBy('plt_num', 'desc')
            ->withTrashed()
            ->where('plt_num', 'like', $rfid . '%')
            ->first();

        $palletNum = object_get($palletNumInfo, 'plt_num', '');
        if ($palletNumInfo) {
            $palletNum++;
        } else {
            $palletNum = $rfid . "-00001";
        }

        return $palletNum;
    }

    /**
     * [createPalletNum description]
     *
     * @return [type] [description]
     */
    public function createPalletNum()
    {
        $today_ddmm = date('ym');
        $palletCode = "LPN-" . $today_ddmm;
        //$palletCode = "LPN-1706";

        $countPallet = \DB::table('pallet')->select('plt_num')
            ->where('plt_num', 'like', $palletCode . '%')
            ->orderBy('plt_num', 'DESC')
            ->first();
        $max = 0;
        $max2 = 0;
        if ($countPallet) {
            $pltCode = array_get($countPallet, 'plt_num', null);
            $arrMax = explode('-', $pltCode);
            $max = (int)end($arrMax);
            $max2 = $arrMax[2];
        }

        return $palletCode . "-" . str_pad($max2 + 1, 5, "0", STR_PAD_LEFT);
    }

    public function getPalletByGrHdrId($whsId, $grHdrId, $limit = 20)
    {
        $query = $this->model
            ->join('gr_dtl', 'gr_dtl.gr_dtl_id', '=', 'pallet.gr_dtl_id')
            ->join('cartons', 'cartons.plt_id', '=', 'pallet.plt_id')
            ->leftJoin('pal_sug_loc', function ($join) {
                // $join->on('gr_dtl.gr_dtl_id', '=', 'pal_sug_loc.gr_dtl_id');
                $join->on('pallet.plt_id', '=', 'pal_sug_loc.plt_id');
                $join->where('pal_sug_loc.deleted', '=',0);
                // $join->on('gr_dtl.item_id', '=', 'pal_sug_loc.item_id');
                // $join->on('gr_dtl.lot', '=', 'pal_sug_loc.lot');
            })
            ->where('gr_dtl.gr_hdr_id', $grHdrId)
            ->where('pallet.whs_id', $whsId)
            ->where('cartons.deleted', 0)
            ->whereIn('cartons.ctn_sts', ['RG', 'AC', 'LK'])
            //->whereNotNull('pallet.loc_id')
            //->whereIn('pallet.plt_sts', ['AC', 'NW'])
            ->orderBy('pallet.created_at')
            ->select([
                'pallet.plt_id',
                'gr_dtl.gr_dtl_id',
                'pallet.plt_num',
                'gr_dtl.item_id',
                'gr_dtl.sku',
                'gr_dtl.size',
                'gr_dtl.color',
                'gr_dtl.lot',
                'pallet.plt_sts',
                DB::raw('gr_dtl.gr_dtl_des AS des'),
                'pallet.ctn_ttl',
                'gr_dtl.pack',
                DB::raw('pallet.dmg_ttl AS dmg_ttl'),
                'gr_dtl.expired_dt as expired_date',
                'pallet.loc_code',
                'pallet.init_ctn_ttl as init_ctns',
                'pal_sug_loc.act_loc_code as putaway_location',
                'gr_dtl.uom_code',
                'gr_dtl.uom_name',
                'gr_dtl.weight',
                "gr_dtl.pack",
                "pallet.rfid",
                DB::raw("SUM(cartons.piece_ttl) AS init_qty"),
                DB::raw("SUM(cartons.piece_remain) AS cur_qty"),
            ])
            ->groupBy('pallet.plt_id')
            ->groupBy('pallet.item_id')
            ->groupBy('pallet.lot');

        return $query->paginate($limit);
    }

    public function getPalletSupportMixSku($whsId, $grHdrId, $rangeTime, $limit = 20)
    {
        $pallets = $this->model
//        ->join('gr_dtl', function($join){
//            $join->on('gr_dtl.gr_hdr_id', '=', 'pallet.gr_hdr_id');
//            $join->on('gr_dtl.gr_dtl_id', '=', 'pallet.gr_dtl_id');
//            $join->where('gr_dtl.deleted', '=', 0);
//        })

        ->join('cartons', 'cartons.plt_id', '=', 'pallet.plt_id')
        ->join('gr_dtl', 'gr_dtl.gr_dtl_id', '=', 'cartons.gr_dtl_id')
        ->join('item', 'item.item_id', '=', 'gr_dtl.item_id')
        ->join('item_status', 'item_status.item_sts_code', '=', 'item.status')
        ->where('gr_dtl.gr_hdr_id', $grHdrId)
        ->where('cartons.whs_id', $whsId)
        ->where('cartons.deleted', 0)
        ->where('cartons.ctn_sts', '<>', 'IA')
        ->where('cartons.ctn_sts', '<>', 'AJ')
        ->whereBetween('cartons.created_at', $rangeTime)
        ->select([
            'pallet.plt_id',
            'cartons.item_id',
            'cartons.gr_dtl_id',
            // 'pallet.plt_num',
            'cartons.lpn_carton',
            'cartons.sku',
            'cartons.size',
            'cartons.color',
            'cartons.lot',
            'pallet.plt_sts',
            'item.description AS des',
            'item.status AS item_status_code',
            'item_status.item_sts_name AS item_status',
            DB::raw('COUNT(DISTINCT(cartons.ctn_id)) as ctn_ttl'),
            'cartons.ctn_pack_size as pack',
            'pallet.dmg_ttl AS dmg_ttl',
            'gr_dtl.expired_dt as expired_date',
//            'pallet.loc_code',
            //DB::raw('COALESCE(cartons.loc_code, cartons.loc_name) as loc_code'),
            'cartons.loc_code as loc_code',
            'cartons.loc_name as loc_name',
            'pallet.init_ctn_ttl as init_ctns',
            'gr_dtl.uom_code',
            'gr_dtl.uom_name',
            'gr_dtl.weight',
            "pallet.rfid",
            DB::raw("SUM(cartons.piece_ttl) AS init_qty"),
            DB::raw("SUM(cartons.piece_remain) AS cur_qty"),
        ])
        ->groupBy('cartons.item_id')
        ->groupBy('cartons.lot')
        //->groupBy('cartons.plt_id')
        ->groupBy('cartons.lpn_carton')
        //->orderBy('cartons.item_id')
        ->orderBy('cartons.created_at')
        ->paginate($limit);

        $palSlugLocs = $this->model
        ->join('gr_dtl', function($join){
            $join->on('gr_dtl.gr_hdr_id', '=', 'pallet.gr_hdr_id');
            $join->on('gr_dtl.gr_dtl_id', '=', 'pallet.gr_dtl_id');
            $join->where('gr_dtl.deleted', '=', 0);
        })
        ->leftJoin('pal_sug_loc', function ($join) {
            $join->on('pallet.plt_id', '=', 'pal_sug_loc.plt_id');
            $join->where('pal_sug_loc.deleted', '=', 0);
        })
        ->where('gr_dtl.gr_hdr_id', $grHdrId)
        ->where('pallet.whs_id', $whsId)
        ->groupBy('pallet.plt_id')
        ->orderBy('pallet.created_at')
        ->paginate($limit);

        $this->mergerCollection($pallets, $palSlugLocs);

        return $pallets;
    }

    public function mergerCollection($container, $mergerCl){
        foreach($container as $pl){
            foreach($mergerCl as $psl){
                if($pl->plt_id == $psl['plt_id']){
                    $pl->putaway_location = $psl['act_loc_code'];
                }
            }
        }
        return $container;
    }

    public function generatePalletNumFromAvailiable($plt_num)
    {
        $today_ddmm = date('ym');
        $explodePltNum = explode('-', $plt_num);
        if (count($explodePltNum) >= 5) {
            $pltCode = $explodePltNum[0] . '-' . $today_ddmm . '-' . $explodePltNum[2] . '-' . $explodePltNum[3];
        } else {
            $pltCode = $explodePltNum[0] . '-' . $today_ddmm;
        }

        $pallet = $this->model
            ->withTrashed()
            ->where('plt_num', 'LIKE', $pltCode . '%')
            ->orderBy('plt_num', 'DESC')
            ->first();

        if (!$pallet) {
            return $pltCode . '-' . str_pad(1, 3, "0", STR_PAD_LEFT);
        }

        $pltNum = $pallet->plt_num;
        $explode = explode('-', $pltNum);
        $max = (int)end($explode);

        return $pltCode . "-" . str_pad($max + 1, 3, "0", STR_PAD_LEFT);
    }

    public function generatePltNum($prefix = "V") {
        $currentYearMonth = date('ym');
        $pallet = Pallet::where('plt_num', 'LIKE', '%-' . $currentYearMonth . '-%')
            ->orderBy('plt_id', 'DESC')
            ->first();

        if (!$pallet){
            return $prefix . '-' . $currentYearMonth . '-' . '000001';
        }

        $aNum = explode("-", $pallet['plt_num']);
        $aTemp = array_keys($aNum);
        $end = end($aTemp);
        $index = (int)$aNum[$end];
        $aNum[0] = $prefix;
        $aNum[$end] = str_pad(++$index, 6, "0", STR_PAD_LEFT);
        return implode("-", $aNum);
    }

}
