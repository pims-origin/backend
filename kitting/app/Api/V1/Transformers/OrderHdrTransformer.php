<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use App\OrderHdr;
use Seldat\Wms2\Utils\Status;

/**
 * Class OrderHdrTransformer
 *
 * @package App\Api\V1\Transformers
 */
class OrderHdrTransformer extends TransformerAbstract
{
    /**
     * @param OrderHdr $orderHdr
     *
     * @return array
     */
    public function transform(OrderHdr $orderHdr)
    {
        $createdAt = date("m/d/Y", strtotime($orderHdr->created_at));
        $statusName = Status::getByKey("Order-Status", $orderHdr->odr_sts);
        if ($orderHdr->back_odr) {
            $statusName = str_replace('Partial ', '', $statusName);
            $statusName = 'Partial ' . $statusName;
        }

        return [
            'odr_id'          => $orderHdr->odr_id,
            'odr_num'         => $orderHdr->odr_num,
            // Warehouse
            'whs_id'          => $orderHdr->whs_id,
            'whs_name'        => object_get($orderHdr, 'warehouse.whs_name', null),
            'whs_code'        => object_get($orderHdr, 'warehouse.whs_code', null),
            // Customer
            'cus_id'          => $orderHdr->cus_id,
            'cus_name'        => object_get($orderHdr, 'customer.cus_name', null),
            'cus_code'        => object_get($orderHdr, 'customer.cus_code', null),
            // Order Status
            'odr_sts'         => $orderHdr->odr_sts,
            'odr_sts_name'    => $statusName,
            // User
            'csr_id'          => $orderHdr->csr,
            // Order Type
            'ref_cod'         => $orderHdr->ref_cod,
            'alloc_qty'       => $orderHdr->alloc_qty,
            'created_at'      => $createdAt,
            'created_by'      => trim(object_get($orderHdr, "createdBy.first_name", null) . " " .
                object_get($orderHdr, "createdBy.last_name", null)),
        ];
    }

    /**
     * @param int $date
     *
     * @return bool|null|string
     */
    private function dateFormat($date = 0)
    {
        return $date && is_numeric($date) ? date("m/d/Y", $date) : null;
    }

    private function datetimeFormat($date = 0)
    {
        return $date && is_numeric($date) ? date("m/d/Y H:i:s", $date) : null;
    }
}
