<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\CycleDtlModel;
use App\Api\V1\Models\CycleHdrModel;
use App\Api\V1\Models\LocationModel;
use App\Api\V1\Models\CartonModel;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;
use Wms2\UserInfo\Data;

class LocationController extends AbstractController
{
    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    /**
     * @var Request
     */
    protected $request;

    /**
     * LocationController constructor.
     * @param LocationModel $model
     * @param Request $request
     */
    public function __construct(CartonModel $cartonModel, LocationModel $model, Request $request)
    {
        $this->cartonModel = ($cartonModel) ?: new CartonModel();
        $this->locationModel = $model;
        $this->request = $request;
    }

    /**
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    public function filter()
    {
        $input = $this->request->getQueryParams();

        try {
            $condition = [
                'loc_whs_id' => $input['whs_id'],
                'term' => $input['term'],

            ];
            $data = $this->locationModel->filterLocation($condition, 10);
            if ($data) {
                $result = [];
                foreach ($data as $location) {
                    $result[$location->loc_id] = $location->loc_code;
                }
                return response()->json($result);
            }
            return response(Message::get('BM067', 'Data'), 404);
        } catch (\Exception $e) {
            return SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__);
        }
    }

    /**
     * API get Pallet Info by loc id
     * @param $warehouseId
     * @param $locId
     * @param Request $request
     * @return null|void
     */
    public function getPalletInfo($warehouseId,$locId, Request $request)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['warehouseId'] = $warehouseId;
        $input['loc_id'] = $locId;

        try {
            $palletRFID = $this->locationModel->getPalletRFIDByLocID($input);
            $palletRFID['plt_rfid'] = $palletRFID['plt_rfid'] ? 'P-'.date('ym').'-'.$palletRFID['plt_rfid'] : null;

            $returnData = [
                'plt_rfid' => $palletRFID ?? null
            ];

            return $returnData;
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, 'CYCLE-COUNT', __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function unsetCartonsRfid(Request $request)
    {
        $input = array_get($request->getParsedBody(), 'data', '');
        if(!$input){
            throw new \Exception('The input data is empty.');
        }

        $locList = $this->locationModel->getPalletAndCartonsByLocation($input);
        if(!$locList){
            throw new \Exception('The input data does not match any location.');
        }

        try {
            foreach($locList as $loc){
                $pallet = array_get($loc, 'pallet', '');
                $cartons = array_get($pallet, 'carton', '');
                $this->cartonModel->unsetCartonsRfid($cartons);
            }
            return $this->response->accepted(null, ['message' => 'Unset Cartons Rfid Success', 'data' => 'success']);
        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, 'CYCLE-COUNT', __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function unlockLocation($locationId)
    {
        $whsId = Data::getCurrentWhsId();
        $location = $this->locationModel->getFirstWhere(['loc_whs_id' => $whsId, 'loc_id' => $locationId]);
        if (empty($location)) {
            return $this->response()->errorBadRequest('Location does not exists');
        }

        try {
            DB::beginTransaction();

            $byLocation = DB::table('cycle_hdr')
                ->where('cycle_hdr.deleted', 0)
                ->where('cycle_hdr.cycle_type', CycleHdrModel::CYCLE_TYPE_LOCATION)
                ->where('cycle_hdr.cycle_detail', $location->loc_id)
                ->where('cycle_hdr.whs_id', $location->loc_whs_id)
                ->where('cycle_hdr.cycle_sts', '<>', 'CP')
                ->pluck('cycle_hdr.cycle_num');

            $byCusAndSku = DB::table('cycle_hdr')
                ->join('cycle_dtl', 'cycle_dtl.cycle_hdr_id', '=', 'cycle_hdr.cycle_hdr_id')
                ->where('cycle_hdr.deleted', 0)
                ->where('cycle_dtl.deleted', 0)
                ->where('cycle_dtl.sys_loc_id', $location->loc_id)
                ->where('cycle_hdr.whs_id', $location->loc_whs_id)
                ->where('cycle_hdr.cycle_sts', '<>', 'CP')
                ->pluck('cycle_hdr.cycle_num');

            $cycleCount = array_unique(array_merge($byLocation, $byCusAndSku));

            if (! empty($cycleCount)) {
                $tranNums = implode(", ", $cycleCount);
                return $this->response()->errorBadRequest(sprintf('Location was locked by cycle count %s', $tranNums));
            }

            $location->loc_sts_code = 'AC';
            $location->updated_by = Data::getCurrentUserId();
            $location->updated_at = time();
            $location->save();

            DB::commit();

            return [
                'data' => [
                    'message' => sprintf('Unlock location %s successfully', $location->loc_code)
                ]
            ];
        } catch (\PDOException $e) {
            DB::rollback();
            return $this->response()->errorBadRequest(
                SystemBug::writeSysBugs($e, __CLASS__, __FUNCTION__)
            );
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response()->errorBadRequest($e->getMessage());
        }
    }
}
