<?php


namespace App\Api\Gate\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\GateDtl;

class GateDtlReportTransformer extends TransformerAbstract
{
    //return result
    public function transform(GateDtl $gateDtl)
    {
        return [
            'cus_id'            => object_get($gateDtl, 'cus_id', null),
            'cus_name'          => object_get($gateDtl, 'cus_name', null),
            'whs_id'            => object_get($gateDtl, 'whs_id', null),
            'ctnr_id'           => object_get($gateDtl, 'ctnr_id', null),
            'ctnr_num'          => object_get($gateDtl, 'ctnr_num', null),
            'open'              => object_get($gateDtl, 'open', null),
            'close'             => object_get($gateDtl, 'close', null),
            'duration'          => object_get($gateDtl, 'duration', null),
            'gate_dtl_sts'      => object_get($gateDtl, 'gate_dtl_sts', null),
            'gate_dtl_sts_name' => object_get($gateDtl, 'gate_dtl_sts', null),
            'by_id'             => object_get($gateDtl, 'by_id', null),
            'by_name'           => object_get($gateDtl, 'by_name', null)
        ];
    }

}
