<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2/12/2019
 * Time: 3:38 PM
 */

namespace App\Api\V1\Models;


use Seldat\Wms2\Models\Inventories;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\JWTUtil;
use Seldat\Wms2\Utils\SelStr;
use Wms2\UserInfo\Data;

class InventoryModel extends AbstractModel
{
    public function __construct()
    {
        $this->model = new Inventories();
    }
}