<?php
/**
 * Created by PhpStorm.
 * User: chicu
 * Date: 9/14/2016
 * Time: 4:08 PM
 */

namespace App\Api\V1\Models;


use Seldat\Wms2\Models\SysBug;

class SystemBugsModel extends AbstractModel
{
    public function __construct(SystemBugs $model = null)
    {
        $this->model = ($model) ?: new SysBug();
    }

    public function getSysBugs($option = null) {
        $curDate = array_get($option, 'current-date', 0);
        $apiName = array_get($option, 'api-name', '');
        $query = $query = $this->model;
        if ($curDate == 1) {
            $query = $this->model->where ('date', date('dmy'));
        }
        if ($apiName != '') {
            $query = $this->model->where ('api_name', $apiName);
        }
        return $query->get();
    }
}