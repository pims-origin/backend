<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\SystemCountry;

class SystemCountryModel extends AbstractModel
{

    /**
     * SystemCountryModel constructor.
     */
    public function __construct()
    {
        $this->model = new SystemCountry();
    }


}
