<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen Seldat
 * Date: 7/25/2016
 * Time: 5:36 PM
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\DamageCarton;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class DamageCartonModel extends AbstractModel
{
    /**
     * EloquentMenuGroup constructor.
     * @param DamageCarton $model
     */
    public function __construct(DamageCarton $model = null)
    {
        $this->model = ($model) ? : new DamageCarton();
    }

    public function checkExistedCartons($ctn_ids)
    {
        $resultCount = $this->model->whereIn('ctn_id', $ctn_ids)->count();

        return $resultCount > 0 ? true: false;
    }

    public function deleteDamagedCartonExistInPallet($pltId) {
        $res = DB::table('damage_carton')
            ->join('cartons', 'cartons.ctn_id', '=', 'damage_carton.ctn_id')
            ->where([
                'cartons.deleted' => 1,
                'cartons.plt_id' => $pltId,
            ])
            ->update([
              'damage_carton.deleted' => 1,
              'damage_carton.deleted_at' => time(),
              'damage_carton.updated_at' => time(),
              'damage_carton.updated_by' => $this->getUserId(),
            ]);
        return $res;
    }
}