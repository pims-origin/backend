<?php

namespace App\Http\Middleware;

use Closure;

class Application
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, $next)
    {
        $response = $next($request);
        if (empty($response->getContent())) {
            return $response;
        }

        try {
            $content = json_decode($response->getContent(), true);
        } catch (\Exception $e) {
            return $response;
        }
        if (isset($content['errors'])) {
            foreach ($content['errors'] as $key => $error) {
                if (!isset($error['title'])) {
                    return $response;
                }

                if (str_contains($error['title'], 'SQLSTATE')) {
                    $content['errors'][$key]['title'] = 'Data error, please contact Seldat support!';
                    $response->setContent(json_encode($content));
                }
            }
        }
        
        return $response->header('Content-Type', 'application/vnd.api+json');
    }
}
