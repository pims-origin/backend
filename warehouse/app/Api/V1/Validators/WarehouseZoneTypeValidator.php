<?php

namespace App\Api\V1\Validators;


class WarehouseZoneTypeValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'zone_type_name' => 'required',
            'zone_type_code' => 'required',
        ];
    }
}
