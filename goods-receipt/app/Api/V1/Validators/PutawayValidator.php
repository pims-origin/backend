<?php

namespace App\Api\V1\Validators;


class PutawayValidator extends AbstractValidator
{

    protected function rules()
    {

        return [
            '*.plt_id' => 'required|integer',
            '*.loc_id' => 'required|integer'
        ];

    }



}
