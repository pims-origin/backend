<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\Item;

class ItemModel extends AbstractModel
{
    /**
     * ItemModel constructor.
     * @param Item|null $model
     */
    public function __construct(Item $model = null)
    {
        $this->model = ($model) ?: new Item();
    }

    public function checkBySkuSizeColor($skuSizeColor)
    {
        if (count($skuSizeColor)==0) {
            return 0;
        }
        $query = $this->make();

        foreach ($skuSizeColor as $each) {
            $query->orWhere(function ($query) use ($each) {
                $query->where('sku', $each['sku']);
                $query->where('size', $each['size']);
                $query->where('color', $each['color']);
            });
        }
        return $query->count();
    }

    /**
     * @param $pallet_ids
     * @param null $limit
     *
     * @return mixed
     */
    public function getItemById($itemIds)
    {
        $itemIds = is_array($itemIds) ? $itemIds : [$itemIds];
        $query = $this->make();
        $query->whereIn('item_id', $itemIds);

        return $query->get();
    }

    /**
     * Search Item
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return object|null $models
     */
    public function getWhere($cus_id, $sku = [], $with = [])
    {
        $query = $this->make($with);
        $query->where('cus_id', $cus_id);
        $query->whereIn('sku', $sku);

        //ignored item is new sku
        $query->whereNull('is_new_sku');

        $this->model->filterDataIn($query, true, false);

        // Get
        $models = $query->get();

        return $models;
    }

    public function getItemsBelongToCustomer($cusId, array $arraySKUs)
    {
        return $this->model
            ->where('cus_id', $cusId)
            ->whereIn('sku', $arraySKUs)
            ->pluck('sku')
            ->toArray();
    }
}
