<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\V1\Models;


use Seldat\Wms2\Models\Location;

class LocationModel extends AbstractModel
{
    /**
     * LocationModel constructor.
     *
     * @param Location|null $model
     */
    public function __construct(Location $model = null)
    {
        $this->model = ($model) ?: new Location();
    }

    public function checkLocationByLocCode($locCode)
    {
        return $this->model->where('loc_code', $locCode)->count();
    }

    public function getLocationByLocCode($locCode, $whsId)
    {
        return $this->model
            ->select('location.loc_id', 'location.loc_code', 'location.loc_alternative_name')
            ->leftJoin('pallet', 'location.loc_id', '=', 'pallet.loc_id')
            ->where('location.loc_code', $locCode)
            ->where('location.loc_whs_id', $whsId)
            ->whereNull('pallet.loc_id')
            ->first();
    }

    public function getSuggest($locIds)
    {
        $query = $this->make(['cartons']);
        $query->whereHas('cartons', function ($q){
            $q->where('ctn_sts', 'AC');
        });
        return $query->select(['loc_code', 'loc_alternative_name', 'loc_id'])
            ->whereIn('loc_id', $locIds)->get();
    }

    /**
     * @param $locCode
     * @param $whsId
     *
     * @return mixed
     */
    public function getByLocCode($locCode, $whsId)
    {
        if (!is_array($locCode)) {
            $locCode = [$locCode];
        }

        return $this->model
            ->select(['loc_id', 'loc_code'])
            ->whereIn('loc_code', $locCode)
            ->where('loc_whs_id', $whsId)
            ->get();
    }
}
