<?php

namespace App\Api\V1\Transformers;


use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\WarehouseMeta;

class WarehouseMetaTransformer extends TransformerAbstract
{
    /**
     * @param WarehouseMeta $warehouseMeta
     *
     * @return array
     */
    public function transform(WarehouseMeta $warehouseMeta)
    {
        return [
            'whs_id'         => $warehouseMeta->whs_id,
            'whs_qualifier'  => $warehouseMeta->whs_qualifier,
            'whs_meta_value' => (boolean) $warehouseMeta->whs_meta_value,
        ];
    }
}
