<?php

namespace App\Api\V1\Validators;


class GetCartonByRFIDValidator extends AbstractValidator
{

    protected function rules()
    {
        return [
            'rfid' => 'required'
        ];
    }
}
