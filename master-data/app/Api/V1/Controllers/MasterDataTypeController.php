<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\MasterDataTypeModel;
use App\Api\V1\Transformers\MasterDataTypeTransformer;
use App\Api\V1\Validators\MasterDataTypeValidator;

use Psr\Http\Message\ServerRequestInterface as Request;

use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\JWTUtil;
use Illuminate\Support\Facades\DB;

use Dingo\Api\Http\Response;

class MasterDataTypeController extends AbstractController
{

    /**
     * @var object $masterDataTypeModel
     */
    protected $masterDataTypeModel;

    /**
     * @var object $masterDataTypeTransformer
     */
    protected $masterDataTypeTransformer;

    /**
     * @var object $masterDataTypeValidator
     */
    protected $masterDataTypeValidator;

    /**
     * MasterDataTypeController constructor.
     * @param MasterDataTypeModel $model
     * @param MasterDataTypeTransformer $transformer
     * @param MasterDataTypeValidator $validator
     */
    public function __construct()
    {
        $this->masterDataTypeModel       = new MasterDataTypeModel();
        $this->masterDataTypeTransformer = new MasterDataTypeTransformer();
        $this->masterDataTypeValidator   = new MasterDataTypeValidator();
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(Request $request)
    {
        $input = $request->getParsedBody();
        $limit = array_get($input, 'limit', PAGING_LIMIT);

        try {
            $masterDataType = $this->masterDataTypeModel->search($input, [
                                                                        'createdBy',
                                                                        'updatedBy'], $limit);

            return $this->response->paginator($masterDataType, $this->masterDataTypeTransformer);
        } catch (\Exception $e) {
            //catch errors
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $mdType
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function getInfo($mdType)
    {
        try {
            $masterDataType = $this->masterDataTypeModel->byType($mdType);

            return $this->response->item($masterDataType, $this->masterDataTypeTransformer);
        } catch (\Exception $e) {
            //catch errors
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function store(Request $request)
    {
        $input = $request->getParsedBody();

        $mdType = array_get($input, 'md_type');

        $checkExist = $this->masterDataTypeModel->byType($mdType);
        if ($checkExist) {
            return $this->response->errorBadRequest(Message::get("BM006", "Master Data Type"));
        }

        $params = [
            'md_type'  => $mdType,
            'mdt_name' => array_get($input, 'mdt_name'),
            'mdt_des'  => array_get($input, 'mdt_des'),
            'mdt_data' => array_get($input, 'mdt_data'),
            'md_sts'   => array_get($input, 'md_sts'),
            'created_at'  => time(),
            'updated_at'  => time(),
            'created_by'  => JWTUtil::getPayloadValue('jti') ?? 0,
            'updated_by'  => JWTUtil::getPayloadValue('jti') ?? 0,
            'deleted_at'  => getDefaultDatetimeDeletedAt(),
            'deleted'     => 0,
        ];

        // validate
        $this->masterDataTypeValidator->validate($params);

        try {
            DB::beginTransaction();

            $masterData = $this->masterDataTypeModel->createMasterDataType($params);

            DB::commit();

            return ["data" => "successful"];

        } catch (\PDOException $e) {
            DB::rollBack();
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param Request $request
     * @param $mdType
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function update(Request $request, $mdType)
    {
        $input = $request->getParsedBody();

        $params = [
            'md_type'  => $mdType,
            'mdt_name' => array_get($input, 'mdt_name'),
            'mdt_des'  => array_get($input, 'mdt_des'),
            'mdt_data' => array_get($input, 'mdt_data'),
            'md_sts'   => array_get($input, 'md_sts'),
            'updated_at' => time(),
            'updated_by' => JWTUtil::getPayloadValue('jti') ?? 0,
        ];

        // validate
        $this->masterDataTypeValidator->validate($params);

        try {
            DB::beginTransaction();

            $masterDataType = $this->masterDataTypeModel->updateMasterDataType($mdType, $params);

            DB::commit();

            return ["data" => "successful"];

        } catch (\PDOException $e) {
            DB::rollBack();
            return $this->response->errorBadRequest($e->getMessage());
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     *
     * @return array|void
     */
    public function mdtStatusDropDown()
    {
        try {
            $mdtStatus = config('constants.master_data_type.status');

            return ['data' => $mdtStatus];

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }
}
