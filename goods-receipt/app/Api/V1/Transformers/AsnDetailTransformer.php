<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\AsnHdr;

class AsnDetailTransformer extends TransformerAbstract
{

    /**
     * ps:Show ASN
     * @param $sample
     * @return array
     */
    public function transform(AsnHdr $asnHdr)
    {
        return [
            //asn_hdr
            'act_dt' => $asnHdr->act_dt,
            'asn_hdr_id' => $asnHdr->asn_hdr_id,
            'asn_hdr_num' => $asnHdr->asn_hdr_num,
            'asn_dtl_ept_dt' => $asnHdr->asn_dtl_ept_dt,//#5980 get expected date

            'asn_hdr_ref' => $asnHdr->asn_hdr_ref,
            'asn_hdr_ept_dt' => ($asnHdr->asn_hdr_ept_dt) ? date('m/d/Y', $asnHdr->asn_hdr_ept_dt) : '',
            'asn_hdr_act_dt' => ($asnHdr->asn_hdr_act_dt) ? date('m/d/Y', $asnHdr->asn_hdr_act_dt) : '',
            'asn_hdr_ctn_ttl' => $asnHdr->asn_hdr_ctn_ttl,
            'asn_hdr_itm_ttl' => $asnHdr->asnDtl()->count(),
            'sys_measurement_code' => $asnHdr->sys_mea_code,

            // ASN Status Info
            'asn_sts'      => $asnHdr->asn_sts,
            'asn_sts_name' => object_get($asnHdr, 'asnStatus.asn_sts_name', ''),
            'asn_sts_des'  => object_get($asnHdr, 'asnStatus.asn_sts_des', ''),

            // Warehouse Info
            'whs_id'   => $asnHdr->whs_id,
            'whs_code' => object_get($asnHdr, 'warehouse.whs_code', ''),
            'whs_name' => object_get($asnHdr, 'warehouse.whs_name', ''),

            // Customer Info
            'cus_id'   => $asnHdr->cus_id,
            'cus_code' => object_get($asnHdr, 'customer.cus_code', ''),
            'cus_name' => object_get($asnHdr, 'customer.cus_name', ''),

            'ctnr_sts'             => object_get($asnHdr, 'ctnr_sts', 0),
            // ASN Details
            'asn_details' => $asnHdr->details,
            'created_at' => $asnHdr->created_at,
            'updated_at' => $asnHdr->updated_at,

        ];
    }
}
