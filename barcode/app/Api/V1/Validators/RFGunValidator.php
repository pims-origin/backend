<?php

namespace App\Api\V1\Validators;


class RFGunValidator extends AbstractValidator
{

    protected function rules()
    {

        return [
            'whs_id'   => 'required|integer|exists:warehouse,whs_id'
        ];

    }


}
