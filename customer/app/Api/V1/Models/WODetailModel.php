<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\WorkOrderDtl;
use \Seldat\Wms2\Models\Zone;

class WODetailModel extends AbstractModel
{
    protected $model;

    public function __construct(WorkOrderDtl $model = null)
    {
        $this->model = ($model) ?: new WorkOrderDtl();
    }

    public function getWODetailByChargeCodeId($chgCodeId)
    {
        $query = $this->model
            ->where('chg_code_id', $chgCodeId)->count();
        return $query;
    }
}
