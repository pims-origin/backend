<?php

namespace App\Api\V1\Validators;


class ReasonValidator extends AbstractValidator
{
    public function rules()
    {
       return [
           'block_rsn_name' => 'required|max:50|unique:block_rsn'
       ];
    }
}