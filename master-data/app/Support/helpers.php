<?php

use Carbon\Carbon;

if ( ! function_exists('config_path'))
{
    /**
     * Get the configuration path.
     *
     * @param  string $path
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}

if ( ! function_exists('getDefaultDatetimeDeletedAt')) {
    /**
     * Get the time for deleted at.
     *
     * @return string
     */
    function getDefaultDatetimeDeletedAt()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', '1999-01-01 00:00:00', 'UTC')->timestamp;
    }
}