<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:26
 */

namespace App\Api\V2\Putback\AssignPallet\Transformers;

use Seldat\Wms2\Models\Item;
use League\Fractal\TransformerAbstract;

/**
 * Class ItemAssignTransformer
 *
 * @package App\Api\V1\Transformers
 */
class ItemAssignTransformer extends TransformerAbstract
{
    /**
     * @param Item $item
     *
     * @return array
     */
    public function transform(Item $item)
    {
        return [
            'item_id'    => object_get($item, "item_id"),
            'sku'        => object_get($item, "sku"),
            'size'       => object_get($item, "size"),
            'color'      => object_get($item, "color"),
            'lot'        => object_get($item, "lot"),
            'pack'       => object_get($item, "pack"),
            'upc'        => object_get($item, "upc"),
            'uom_id'     => object_get($item, "uom_id"),
            'uom_code'   => object_get($item, "uom_code"),
            'pack_size'  => object_get($item, "pack_size"),
        ];
    }
}
