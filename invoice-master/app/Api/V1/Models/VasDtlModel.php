<?php

namespace App\Api\V1\Models;


use App\VasDtl;

class VasDtlModel extends AbstractModel
{
    /**
     * Constructor.
    */

    public function __construct()
    {
        $this->model = new VasDtl();
    }
    /**
     * Get by (ref_num, vas_item, cus_sac_id)
     * @param array $input
     * @param int $workOrderId
     * @return bool
     * */
    public function checkDuplicateSet($input, $workOrderId = null) {
        $query = $this->make();
        $query->where('ref_num', $input['ref_num']);
        $query->where('vas_item', $input['charge_item']);
        $query->where('cus_sac_id', $input['cus_sac_id']);
        if (!empty($workOrderId)) {
            $query->where('vas_id', '!=', $workOrderId);
        }
        return !empty($query->first()) ? true : false;
    }
    /**
     * Delete WoDtl by vas_id
     * @param array $ids
     * */
    public function deleteByWorkOrderHdrIds($ids) {
        $query = $this->make();
        return $query->whereIn('vas_id', $ids)->delete();
    }

    public function checkRefNumExisted($refNumArr, $oldWorkOrderDtlIds) {
        $query = $this->make();
        $query->whereIn('ref_num', $refNumArr);
        $query->whereNotIn('vas_dtl_id', $oldWorkOrderDtlIds);
        $query->whereHas('vasHdr', function ($query){
            $query->where('is_order',1);
        });
        return $query->first();
    }

    /**
     * Delete WoDtl by vas_id
     * @param integer vas_id
     * */
    public function deleteByWorkOrderHdrId($vas_id)
    {
        $query = $this->model->where('vas_id', $vas_id)->pluck('vas_dtl_id')->toArray();

        return $this->model->destroy($query);
    }

    /**
     * Delete Multiple WorkOrders
     * @param array vas_ids
     */
    public function deleteMultiByWorkOrderHdrIds($vas_ids)
    {
        foreach ($vas_ids as $vas_id) {
            $vas_dtl_ids = $this->model->where('vas_id', $vas_id)->pluck('vas_dtl_id')->toArray();

            if (!empty($vas_dtl_ids))
            {
                $this->model->destroy($vas_dtl_ids);
            }
        }
    }

    /**
     * Delete WoDtl by vas_id
     * @param int $vasId
     * */
    public function forceDeleteByVasId($vasId) {
        $this->model->where('vas_id', $vasId)->forceDelete();
    }
}

