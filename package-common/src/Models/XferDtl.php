<?php
/**
 * Created by PhpStorm.
 * User: Quang Nguyen
 * Date: 12-Oct-16
 * Time: 09:34
 */

namespace Seldat\Wms2\Models;

class XferDtl extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'xfer_dtl';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'xfer_dtl_id';

    /**
     * @var array
     */
    protected $fillable = [
        'whs_id',
        'cus_id',
        'item_id',
        'sku',
        'size',
        'color',
        'lot',
        'cus_upc',
        'allocated_qty',
        'act_piece_qty',
        'req_loc_id',
        'req_loc',
        'act_req_loc_id',
        'act_req_loc',
        'mez_loc_id',
        'mez_loc',
        'act_mez_loc',
        'act_mez_loc_id',
        'xfer_dtl_sts',
        'sts',
        'xfer_hdr_id',
        'xfer_ticket_id'
    ];

    public function xferHdr()
    {
        return $this->hasOne(__NAMESPACE__ . '\XferHdr', 'xfer_hdr_id', 'xfer_hdr_id');
    }

    public function xferTicket()
    {
        return $this->hasOne(__NAMESPACE__ . '\XferTicket', 'xfer_ticket_id', 'xfer_ticket_id');
    }

    public function reqLocation()
    {
        return $this->hasOne(__NAMESPACE__ . '\Location', 'loc_id', 'req_loc_id');
    }
}
