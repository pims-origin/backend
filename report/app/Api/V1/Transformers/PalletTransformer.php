<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 15-June-2017
 * Time: 09:27
 */

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Utils\Status;


class PalletTransformer extends TransformerAbstract
{
    //return result
    public function transform(Pallet $pallet)
    {
        return [
            'plt_id'            => object_get($pallet, 'plt_id'),
            'plt_num'           => object_get($pallet, 'plt_num'),
            'cus_id'            => object_get($pallet, 'cus_id'),
            'cus_name'          => object_get($pallet, 'cus_name'),
            'cus_code'          => object_get($pallet, 'cus_code'),
            'loc_id'            => object_get($pallet, 'loc_id'),
            'loc_name'          => object_get($pallet, 'loc_name'),
            'loc_code'          => object_get($pallet, 'loc_code'),
            'sku'               => object_get($pallet, 'sku'),
            'size'              => object_get($pallet, 'size'),
            'color'             => object_get($pallet, 'color'),
            'ctn_pack_size'     => object_get($pallet, 'ctn_pack_size'),
            'item_id'           => object_get($pallet, 'item_id'),
            'lot'               => object_get($pallet, 'lot'),
            'des'               => object_get($pallet, 'des'),
            'upc'               => object_get($pallet, 'upc'),
            'init_ctn_ttl'      => object_get($pallet, 'init_ctn_ttl'),
            'init_piece_ttl'    => object_get($pallet, 'init_piece_ttl'),
            'dmg_ctns'          => object_get($pallet, 'dmg_ctn_ttl'),
            'current_ctns'      => object_get($pallet, 'current_ctn_ttl'),
            'current_pieces'    => object_get($pallet, 'current_piece_ttl'),
            'created_at'        => date("m/d/Y H:i:s", strtotime(object_get($pallet, 'created_at', null))),
            'plt_sts_code'      => object_get($pallet, 'plt_sts'),
            'plt_sts_name'      => object_get($pallet, 'plt_sts') ? Status::getByValue(object_get($pallet, 'plt_sts'), 'PALLET_STATUS') : null,
            'loc_sts_code'      => object_get($pallet, 'loc_sts_code'),
            'loc_sts_name'      => object_get($pallet, 'loc_sts_code') ? Status::getByValue(object_get($pallet, 'loc_sts_code'), 'LOCATION_STATUS') : null,
            'rfid'              => object_get($pallet, 'rfid', null),
            'row'               => object_get($pallet, 'row', null),
            'level'             => object_get($pallet, 'level', null),
            'aisle'             => object_get($pallet, 'aisle', null),
        ];
    }

}
