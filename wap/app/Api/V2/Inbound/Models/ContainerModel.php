<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 5/24/16
 * Time: 11:43 AM
 */

namespace App\Api\V2\Inbound\Models;

use Seldat\Wms2\Models\Container;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class ContainerModel extends AbstractModel
{
    /**
     * EloquentMenuGroup constructor.
     * @param Container $model
     */
    public function __construct(Container $model = null)
    {
        $this->model = ($model) ?: new Container();
    }

    /**
     * @param int $containerId
     * @return int
     */
    public function deleteContainer($containerId)
    {
        return $this->model
            ->where('ctnr_id', $containerId)
            ->delete();
    }

    /**
     * Search Container
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === 'ctnr_num') {
                    $query->where($key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
            }
        }

        $query->whereHas('asnDtl.asnHdr', function ($query) use ($attributes) {
            if (isset($attributes['whs_id'])) {
                $query->where('whs_id', SelStr::escapeLike($attributes['whs_id']));
            }
        });

        $this->sortBuilder($query, $attributes);
        // Get
        $models = $query->paginate($limit);
        return $models;
    }

    public function loadCTNRList($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $query->join('asn_dtl', 'asn_dtl.ctnr_id', '=', 'container.ctnr_id')
            ->join('asn_hdr', 'asn_hdr.asn_hdr_id', '=', 'asn_dtl.asn_hdr_id')
            ->whereIn('asn_hdr.asn_sts', ['NW', 'RG'])
            ->whereIn('asn_dtl.asn_dtl_sts', ['NW', 'RG'])
            ->where('asn_dtl.deleted', 0)
            ->where('asn_hdr.deleted', 0);

        if (isset($attributes['whs_id'])) {
            $query->where('asn_hdr.whs_id', SelStr::escapeLike($attributes['whs_id']));
        }

        if (isset($attributes['cus_name'])) {
            $query->join('customer', 'asn_hdr.cus_id', '=', 'customer.cus_id');
            $query->where('customer.cus_name', 'like', "%" . SelStr::escapeLike($attributes['cus_name']) . "%");
        }

        if (isset($attributes['ctnr_num'])) {
            $isLikeFromLeft = array_get($attributes, 'like_from_left', 0);
            if($isLikeFromLeft) {
                $query->where('container.ctnr_num', 'like', SelStr::escapeLike($attributes['ctnr_num']) . "%");
            } else {
                $query->where('container.ctnr_num', 'like', "%" . SelStr::escapeLike($attributes['ctnr_num']) . "%");
            }
        } else {
            $query->where('container.ctnr_id', -100);
        }

        $query->groupBy('container.ctnr_id');

        $query->orderBy('container.ctnr_num');

        $this->sortBuilder($query, $attributes);

        /*if (!$limit) {
            $limit = 20;
        }*/

        //$models = $query->paginate($limit);
        
        //[WMS2-4275][WAP API - OUTBOUND] order-skus api depends on limit param while other apis: get customers, container etc do not
        if ($limit !== null) {
            $query->limit($limit);
        }
        $models = $query->get();

        return $models;
    }

    public function getByNumber($number)
    {
        return $this->getFirstBy('ctnr_num', $number);
    }
}