<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:25
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\PackHdr;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

/**
 * Class PackHdrModel
 *
 * @package App\Api\V1\Models
 */
class PackHdrModel extends AbstractModel
{
    /**
     * PackHdrModel constructor.
     *
     * @param PackHdr|null $model
     */
    public function __construct(PackHdr $model = null)
    {
        $this->model = ($model) ?: new PackHdr();
    }

    /**
     * @param $packHdrId
     *
     * @return mixed
     */
    public function deletePackHdr($packHdrId)
    {
        return $this->model
            ->where('ord_hdr_id', $packHdrId)
            ->delete();
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $query->select([
            'pack_hdr.*',
            'cartons.rfid'
        ]);
        $query->leftJoin('cartons', 'pack_hdr.cnt_id', '=', 'cartons.ctn_id');
        $attributes = SelArr::removeNullOrEmptyString($attributes);
        $arrEqual = ['cus_id', 'odr_sts', 'odr_type', 'odr_hdr_id'];

        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if ($key === "csr") {
                    $query->whereHas("pack_hdr.csrUser", function ($q) use ($attributes) {
                        $q->where("first_name", 'like', "%" . SelStr::escapeLike($attributes['csr']) . "%");
                        $q->orWhere("last_name", 'like', "%" . SelStr::escapeLike($attributes['csr']) . "%");
                        $q->orWhere(
                            DB::raw("concat(first_name, ' ', last_name)"),
                            'like',
                            "%" . SelStr::escapeLike(str_replace("  ", " ", $attributes['csr'])) . "%"
                        );
                    });
                    continue;
                }
                if ($key === "odr_num") {
                    $query->where('pack_hdr.' . $key, "like", "%" . SelStr::escapeLike($value) . "%");
                    continue;
                }
                if (in_array($key, $arrEqual)) {
                    $query->where('pack_hdr.' . $key, SelStr::escapeLike($value));
                }
            }
        }

        $this->sortBuilder($query, $attributes);

        $this->model->filterData($query, true);

        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $orderIds
     *
     * @return mixed
     */
    public function getDistinctOutPltIdByOrderIds($orderIds)
    {
        $rows = $this->model
            ->distinct('out_plt_id')
            ->whereIn('odr_hdr_id', $orderIds)
            ->get(['out_plt_id']);

        return $rows;
    }

    /**
     * @param $order_ids
     *
     * @return mixed
     */
    public function whereIn($order_ids)
    {
        $order_ids = is_array($order_ids) ? $order_ids : [$order_ids];

        $rows = $this->model
            ->whereIn('odr_hdr_id', $order_ids)
            ->get();

        return $rows;
    }

    /**
     * @param $orderHdrIds
     *
     * @return mixed
     */
    public function countPalletForAllOriginAndPackOrder($orderHdrIds)
    {
        $palletCount = $this->model
            ->distinct('out_plt_id')
            ->whereIn('odr_hdr_id', $orderHdrIds)
            ->count(['out_plt_id']);

        return $palletCount;
    }

}
