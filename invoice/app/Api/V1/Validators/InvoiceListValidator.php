<?php

namespace App\Api\V1\Validators;

use Seldat\Wms2\Models\InvoiceList;

class InvoiceListValidator extends AbstractValidator
{
    protected function rules()
    {
        $table = new InvoiceList();

        return $this->getRules($table);
    }
}
