<?php
namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use App\Setting;

class SettingTransformer extends TransformerAbstract
{
    public function transform(Setting $setting)
    {
        return [
            'setting_id'    => (int)$setting->setting_id,
            'setting_key'   => $setting->setting_key,
            'setting_value' => json_decode($setting->setting_value, true)
        ];
    }
}
