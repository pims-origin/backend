<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 22-Jun-16
 * Time: 10:47
 */

namespace App\Api\Shipping\Models;


use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\Customer;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Models\Zone;
use Wms2\UserInfo\Data;

class LocationModel extends AbstractModel
{
    /**
     * LocationModel constructor.
     *
     * @param Location|null $model
     */
    public function __construct(Location $model = null)
    {
        $this->model = ($model) ?: new Location();
    }

    public function getLocationByKey($whsId, $key, $locCode)
    {
        return $this->model
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where($key, $locCode)
          //  ->where('location.loc_sts_code', 'AC')
            ->where('loc_type.loc_type_code', 'SHP')
            ->where('location.loc_whs_id', $whsId)
            ->first();
    }

    public function getLocationByKeyNoType($whsId, $key, $locCode)
    {
        return $this->model
            ->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where($key, $locCode)
          //  ->where('location.loc_sts_code', 'AC')
            ->where('location.loc_whs_id', $whsId)
            ->first();
    }

}
