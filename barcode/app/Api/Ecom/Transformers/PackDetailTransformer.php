<?php

namespace App\Api\Ecom\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\PackHdr;

class PackDetailTransformer extends TransformerAbstract
{
    public function transform(PackHdr $pack)
    {
        $items = [];
        if (!empty($pack->details)) {
            foreach ($pack->details as $packDtl) {
                $items[] = [
                    'item_id'     => $packDtl->item_id,
                    'sku'         => $packDtl->sku.'-'.$packDtl->size.'-'.$packDtl->color,
                    'lot'         => $packDtl->lot,
                    'pack_dtl_id' => $packDtl->pack_dtl_id,
                    'qty'         => $packDtl->piece_qty
                ];
            }
        }

        return [
            'pack_hdr_id'     => object_get($pack, 'pack_hdr_id'),
            'pack_hdr_num'    => object_get($pack, 'pack_hdr_num'),
            'tracking_number' => object_get($pack, 'tracking_number'),
            'items'           => $items
        ];
    }
}
