<?php

namespace App\Api\V2\Outbound\Wave\GetMoreLocation\Models;

use App\Utils\JWTUtil;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\OrderCarton;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
use Seldat\Wms2\Utils\Status;

class OrderCartonModel extends AbstractModel
{

    /**
     * OrderCartonModel constructor.
     *
     * @param OrderCarton|null $model
     */
    public function __construct(OrderCarton $model = null)
    {
        $this->model = ($model) ?: new OrderCarton();
    }

    public function autoPack($wvHdrId)
    {
        $result = DB::table('odr_hdr as o')
            ->join('odr_hdr_meta as ohm', 'ohm.odr_id', '=', 'o.odr_id')
            ->join('odr_cartons as oc', 'o.odr_id', '=', 'oc.odr_hdr_id')
            ->join('cartons as c', 'c.ctn_id', '=', 'oc.ctn_id')
            ->where('ohm.value', 1)
            ->where('o.odr_sts', 'PD')
            ->where('o.wv_id', $wvHdrId)
            ->groupBy('c.ctn_id')
            ->get();

        return $result;
    }

    public function sumPieceQtyOdrCtnByOdrDtlID($odrDtlId)
    {
        $sum = DB::table('odr_cartons')
            ->selectRaw('SUM(piece_qty) as piece_qty')
            ->where('odr_dtl_id', $odrDtlId)
            ->get();

        return $sum;
    }

    public function getOrderCT($odrDtlId)
    {

        $query = $this->model
            ->select([
                'odr_num',
                'wv_num',
                'odr_dtl_id',
                'odr_cartons.ctn_num',
                'odr_cartons.ctn_id',
                'rfid as ctn_rfid'
            ])
            ->join('cartons', 'odr_cartons.ctn_id', '=', 'cartons.ctn_id')
            ->where('odr_dtl_id', $odrDtlId)
            ->groupBy('odr_cartons.ctn_id');

        return $query->get();
    }

    /**
     * @param $wvDtlID
     *
     * @return mixed
     */
    public function countOrderCTByWvDtlId($wvDtlID)
    {
        $query = $this->model
            ->where('wv_dtl_id', $wvDtlID)
            ->count('ctn_num');

        return $query;
    }

    public function insertOrderCartons($ctnLists, $wvDtl)
    {
        $dataOrderCartons = [];
        $userId = JWTUtil::getPayloadValue('jti');;
        foreach ($ctnLists as $carton) {
            $isStorage = $carton['ctn_pack_size'] > $carton['piece_remain'] ? 1 : 0;
            $data = [
                'ctn_num'    => $carton['ctn_num'],
                'ctn_id'     => $carton['ctn_id'],
                'wv_hdr_id'  => array_get($wvDtl, 'wv_id', null),
                'wv_dtl_id'  => array_get($wvDtl, 'wv_dtl_id', null),
                'wv_num'     => array_get($wvDtl, 'wv_num', null),
                'piece_qty'  => array_get($carton, 'piece_remain', null),
                'ship_dt'    => 0,
                'ctn_rfid'   => $carton['rfid'],
                'ctn_sts'    => 'PD',
                'sts'        => 'i',
                'item_id'    => $carton['item_id'],
                'sku'        => $carton['sku'],
                'size'       => $carton['size'],
                'color'      => $carton['size'],
                'upc'        => $carton['upc'],
                'lot'        => $carton['lot'],
                'pack'       => $carton['ctn_pack_size'],
                'uom_id'     => $carton['ctn_uom_id'],
                'uom_code'   => $carton['uom_code'],
                'uom_name'   => $carton['uom_name'],
                'sts'        => 'i',
                'is_storage' => $isStorage,
                'created_at' => time(),
                'updated_at' => time(),
                'created_by' => ($userId) ? $userId : 0,
                'updated_by' => ($userId) ? $userId : 0,
                'deleted_at' => $carton['deleted_at'],
                'deleted'    => 0,

            ];
            $dataOrderCartons[] = $data;
        }

        return DB::table('odr_cartons')->insert($dataOrderCartons);


    }

    /**
     * @param $odrId
     *
     * @return mixed
     */
    public function countOdrHdr($odrId)
    {
        return $this->model->where('odr_hdr_id', '=', $odrId)->count();
    }

    /**
     * @param $CtnRfid
     *
     * @return mixed
     */
    public function updateOrderCarton($CtnRfid)
    {
        $result = $this->model
            ->where('ctn_rfid', $CtnRfid)
            ->update([
                'ctn_sts' => 'PB',
                'deleted' => 1,
                'deleted_at' => time(),
            ]);

        return $result;

    }

}
