<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 1-July-17
 * Time: 09:25
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\ReturnDtlModel;
use App\Api\V1\Traits\ReturnControllerTrait;
use App\Api\V1\Transformers\ReturnDtlTransformer;

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

use Psr\Http\Message\ServerRequestInterface as Request;

use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;

/**
 * Class ReturnDtlController
 *
 * @package App\Api\V1\Controllers
 */
class ReturnDtlController extends AbstractController
{
    //use ReturnControllerTrait;

    /**
     * @var ReturnDtlModel
     */
    protected $returnDtlModel;

    /**
     * ReturnDtlController constructor.
     *
     * @param ReturnDtlModel $returnDtlModel
     */
    public function __construct(ReturnDtlModel $returnDtlModel)
    {
        $this->returnDtlModel = $returnDtlModel;
    }

    /**
     * @param Request $request
     * @param ReturnDtlTransformer $returnDtlTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search(
        Request $request,
        ReturnDtlTransformer $returnDtlTransformer
    ) {
        $input = $request->getQueryParams();
        $limit = array_get($input, 'limit');

        if (!empty($input['export']) && $input['export'] == 1) {
            $this->export($input);
            die;
        }

        try {
            $putBackPerMonth = $this->returnDtlModel->search($input, [
                'returnHdr',
                'item',
                'item.customer',
                'createdBy',
                'odrHdr'
            ], $limit);

            return $this->response->paginator($putBackPerMonth, $returnDtlTransformer);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get('BM010'));
    }

    private function export($input)
    {
        try {
            $cycleDtlRev = $this->returnDtlModel->search($input, [
                'returnHdr',
                'item',
                'item.customer',
                'createdBy'
            ], null, true)->toArray();

            $title = [
                'item.customer.cus_code'                       => 'Customer Code',
                'item.customer.cus_name'                       => 'Customer Name',
                'return_hdr.return_num'                        => 'PB Receipt Num',
                'item_id'                                      => 'Item ID',
                'sku'                                          => 'SKU',
                'item.description'                             => 'Description',
                'size'                                         => 'Size',
                'color'                                        => 'Color',
                'lot'                                          => 'Lot',
                'item.cus_upc'                                 => 'UPC',
                'return_hdr.return_qty'                        => 'Put back QTY',
                'updated_at|format()|Y-m-d'                    => 'Created Date',
                'created_by.first_name|.|created_by.last_name' => 'Created By',
            ];

            // $filePath = storage_path() . "/Report_PBPM_{$warehouse->whs_name}.csv";
            $today = time();
            $filePath = "Report_PBPM_{$today}";
            $this->saveFile($title, $cycleDtlRev, $filePath);

            return $this->response->noContent()->setContent(['status' => 'OK', 'data' => []]);

        } catch (\PDOException $e) {
            return $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    private function saveFile(array $title, array $data, $filePath)
    {
        if (empty($data) || empty($title)) {
            return false;
        }

        $writer = WriterFactory::create(Type::CSV);
        // $writer->openToFile($filePath); // write data to a file or to a PHP stream
        $writer->openToBrowser($filePath); // stream data directly to the browser

        $dataSave[] = array_values($title);

        foreach ($data as $item) {
            $temp = [];
            foreach ($title as $key => $field) {
                $values = explode("|", $key);
                $value = "";
                if (count($values) == 1) {
                    $value = array_get($item, $values[0], null);
                } else if (!empty($values[2])) {
                    switch ($values[1]) {
                        case ".":
                            $value = trim(array_get($item, $values[0], null) . " " .
                                array_get($item, $values[2], null));
                            break;
                        case "format()":
                            $value = date($values[2], array_get($item, $values[0], null));
                            break;
                    }
                }
                $temp[] = $value;
            }
            $dataSave[] = $temp;
        }

        $writer->addRows($dataSave);
        $writer->close();
    }
}
