<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Api\V1\Models\CustomerModel;
use App\Api\V1\Models\CustomerAddressModel;
use Illuminate\Http\Response as IlluminateResponse;

class CustomerAddressTest extends TestCase
{
    use DatabaseTransactions;

    protected $customerId;
    protected $sysCountryId;

    protected $customerParams = [
        'cus_name'       => 'customer',
        'cus_status'     => 'IA',
        'cus_short_name' => 'phone'
    ];

    protected $customerAddressParams = [
        'cus_add_line_1'      => 'cus add line 1',
        'cus_add_line_2'      => 'cus add line 2',
        'cus_add_city_name'   =>  'Ho Chi Minh',
        'cus_add_state_id'    => '369',
        'cus_add_postal_code' => '12348',
        'cus_add_type'        => 'AT'
    ];

    /**
     * CustomerAddressControllerCest constructor.
     */
    public function setUp()
    {
        parent::setUp();

        $customer = factory(\Seldat\Wms2\Models\Customer::class)->create();
        $systemState = factory(\Seldat\Wms2\Models\SystemState::class)->create();
        $systemCountry = factory(\Seldat\Wms2\Models\SystemCountry::class)->create();
        $this->customerId = $customer->cus_id;
        $this->customerAddressParams['cus_add_cus_id'] = $this->customerId;


        $sysCountry = factory(\Seldat\Wms2\Models\SystemCountry::class)->create();
        $this->customerAddressParams['cus_add_country_id'] = $sysCountry->sys_country_id;
    }

    private function getEndPoint($customerAddressId = null)
    {
        return "/v1/customers/{$this->customerId}/addresses" . ($customerAddressId ? '/' . $customerAddressId : '');
    }

    public function testList_Ok()
    {
        // make data
        factory(\Seldat\Wms2\Models\CustomerAddress::class, 3)->create(['cus_add_cus_id' => $this->customerId]);

        $uri = $this->getEndPoint();
        $response = $this->call('GET', $uri);
        $responseData = json_decode($response->getContent(), true);

        // Case 1: Response is 200
        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        // Case 2: Data is not null.
        $this->assertNotEmpty($responseData['data']);
    }

    public function testShow_Ok()
    {
        // make data
        $address = factory(\Seldat\Wms2\Models\CustomerAddress::class)->create();
        $customerAddressId = $address->cus_add_id;

        $uri = $this->getEndPoint($customerAddressId);

        $this->call('GET', $uri);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);
    }

    public function testStore_Ok()
    {
        $response = $this->call('POST', $this->getEndPoint(), $this->customerAddressParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_CREATED);

        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData['data']);
    }

    public function testStore_NoParams_Fail()
    {
        $uri = $this->getEndPoint();

        $this->call('POST', $uri);

        $this->assertResponseStatus(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdate_Ok()
    {
        // make data
        $address = factory(\Seldat\Wms2\Models\CustomerAddress::class)->create();
        $customerAddressId = $address->cus_add_id;

        $uri = $this->getEndPoint($customerAddressId);

        $response = $this->call('PUT', $uri, $this->customerAddressParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_OK);

        $responseData = json_decode($response->getContent(), true);

        $this->assertNotEmpty($responseData['data']);
    }

    public function testUpdate_WithZero_Fail()
    {
        $uri = $this->getEndPoint(99999);

        $this->call('PUT', $uri, $this->customerAddressParams);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }

    public function testDelete_Ok()
    {
        // make data
        $address = factory(\Seldat\Wms2\Models\CustomerAddress::class)->create(['cus_add_cus_id' => $this->customerId]);
        $customerAddressId = $address->cus_add_id;

        $uri = $this->getEndPoint($customerAddressId);

        $this->call('DELETE', $uri);

        $this->assertResponseStatus(IlluminateResponse::HTTP_NO_CONTENT);

        $this->seeInDatabase((new \Seldat\Wms2\Models\CustomerAddress())->getTable(),
            ['cus_add_id' => $address->cus_add_id, 'deleted' => 1]);
    }

    public function testDelete_NotExit_Fail()
    {
        $uri = $this->getEndPoint(99999);

        $this->call('DELETE', $uri);

        $this->assertResponseStatus(IlluminateResponse::HTTP_BAD_REQUEST);
    }
}