<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V1\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Seldat\Wms2\Models\GoodsReceiptDetail;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;

class GoodsReceiptDetailModel extends AbstractModel
{
    /**
     * GoodsReceiptDetailModel constructor.
     *
     * @param GoodsReceiptDetail|null $model
     */
    public function __construct(GoodsReceiptDetail $model = null)
    {
        $this->model = ($model) ?: new GoodsReceiptDetail();
    }

    /**
     * @param int $goodsReceiptDetailId
     *
     * @return int
     */
    public function deleteGoodsReceiptDetail($goodsReceiptDetailId)
    {
        return $this->model
            ->where('gr_dtl_id', $goodsReceiptDetailId)
            ->delete();
    }

    /**
     * @param array $goodsReceiptDetailIds
     *
     * @return mixed
     */
    public function deleteMassGoodsReceiptDetail(array $goodsReceiptDetailIds)
    {
        return $this->model
            ->whereIn('gr_dtl_id', $goodsReceiptDetailIds)
            ->delete();
    }


    /**
     * Search GoodsReceipt
     *
     * @param array $attributes
     * @param array $with
     * @param int|null $limit
     *
     * @return mixed
     */
    public function search($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        if (isset($attributes['gr_dtl_is_dmg'])) {
            $query->where('gr_dtl_is_dmg', $attributes['gr_dtl_is_dmg']);
        }

        // Search Goods Receipt Number
        $query->whereHas('goodsReceipt', function ($query) use ($attributes) {

            if (isset($attributes['gr_hdr_id'])) {
                $query->where('gr_hdr_id', $attributes['gr_hdr_id']);
            }
        });

        $this->sortBuilder($query, $attributes);

        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    public function getTotalItemDamaged($item_id, $with = [])
    {
        $query = $this->make($with);
        $query->where('gr_dtl_is_dmg', 1);
        $query->whereHas('asnDetail', function ($query) use ($item_id) {
            $query->where('item_id', $item_id);
        });

        $result = $query->get()->transform(function ($item) {
            return [
                'itm_dmg_ttl' => -1 * object_get($item, 'gr_dtl_disc', 0) *
                    object_get($item, 'asnDetail.asn_dtl_pack', 0),
            ];
        });

        return $result->sum('itm_dmg_ttl');
    }

    /**
     * @param $grId
     *
     * @return mixed
     */
    public function updatePltTtl($grId){
       $res =  $this->updateWhere([
            'gr_dtl_plt_ttl' => DB::raw("(SELECT COUNT(1) FROM `pallet` WHERE pallet.gr_dtl_id = gr_dtl.gr_dtl_id)")
        ],['gr_hdr_id' =>$grId]);
        return $res;
    }

    /**
     * @param $grId
     *
     * @return mixed
     */
    public function getGoodsReceiptDetailByGrHdrId($grId){
        $result = $this->model
            ->where('gr_hdr_id', $grId)
            ->get();
        return $result;
    }


    public function updateGoodsReceiptDetailReceived($grId){
        $grDtls = $this->model->where('gr_hdr_id', $grId)
                            ->where('gr_dtl_sts', '!=', 'CC')
                            ->get();

        foreach($grDtls as $grDtl){
            $palletTotal = Pallet::where('gr_hdr_id', $grId)
                                ->where('gr_dtl_id', $grDtl->gr_dtl_id)
                                ->whereIn('plt_sts', ['RG'])
                                ->count();

            $grDtl->update([
                'gr_dtl_sts'        => 'RE',
                'putaway'           => 1,
                'gr_dtl_plt_ttl'    => $palletTotal
            ]);
        }
    }

    public function palletTotalOfGrHdr($grHdrId)
    {
        return $this->model->where('gr_hdr_id', $grHdrId)->sum('gr_dtl_plt_ttl');
    }

    public function computeGrDtlQty($grHdrId)
    {
        $query = DB::table('gr_dtl')
            ->where('deleted', 0)
            ->where('gr_hdr_id', $grHdrId)
            ->update([
                'gr_dtl_act_qty_ttl' => DB::raw('coalesce(gr_dtl_act_ctn_ttl, 0) * pack'),
                'gr_dtl_ept_qty_ttl' => DB::raw('coalesce(gr_dtl_ept_ctn_ttl, 0) * pack'),
                'gr_dtl_disc_qty'    => DB::raw('pack/(-1) * (-1) * coalesce(gr_dtl_disc, 0)'),
            ]);

        return $query;
    }
}
