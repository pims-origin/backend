<?php

namespace App\Modules\Orders\Eloquents;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'status';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'sts';

    /**
     * @var array
     */
    protected $fillable = [
        'table',
        'name',
        'des'
    ];
}
