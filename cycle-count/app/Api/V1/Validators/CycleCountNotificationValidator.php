<?php

namespace App\Api\V1\Validators;


class CycleCountNotificationValidator extends AbstractValidator
{
    /*
     * Define rules
     * 
     * @return array
     */
    protected function rules()
    {
        return [
            // 'loc_id'     => 'required|integer',
            'loc_code'   => 'required',
            'cc_ntf_sts' => 'required',
            //'cc_ntf_date'   => 'required',
            'color'      => 'required',
            'uom_name'   => 'required',
            'uom_code'   => 'required',
            'sku'        => 'required',
            'size'       => 'required',
            'remain_qty' => 'required|integer',
            'reason'     => 'required',
            'pack'       => 'required|integer',
            'lot'        => 'required',
            'item_id'    => 'required|integer',
            'cus_id'     => 'required|integer',

        ];
    }
}
