<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V4\Outbound\OutPallet\AssignCartons\Models;

use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Utils\Status;

class CartonModel extends AbstractModel
{
    /**
     * CartonModel constructor.
     *
     * @param Carton|null $model
     */
    public function __construct(Carton $model = null)
    {
        $this->model = ($model) ?: new Carton();
    }

    public function getActiveCartonByCtnRFID($ctnRFID)
    {
        $ctnRFID = is_array($ctnRFID) ? $ctnRFID : [$ctnRFID];

        return $this->model->whereIn('rfid', $ctnRFID)
            ->where('ctn_sts', Status::getByKey('CTN_STATUS', 'ACTIVE'))
            ->where('piece_remain', '>', 0)
            ->whereRaw('(is_damaged = 0 OR is_damaged is null)')
            ->whereRaw('(is_ecom = 0 OR is_ecom is null)')
            ->whereNotNull('loc_id')
            ->whereNotNull('plt_id')
            //->select('ctn_id', 'ctn_num', 'rfid')
            ->get();
    }

    /**
     * @param $ctnRFID
     *
     * @return mixed
     */
    public function getCartonByCtnRFID($ctnRFID)
    {
        return $this->model->where('rfid', $ctnRFID)
            ->orderBy('ctn_id', 'desc')
            ->first();
    }

    /**
     * @param $whsId
     * @param $ctnRFID_arr
     *
     * @return mixed
     */
    public function getCartonsByCtnRFIDs($whsId, $ctnRFID_arr)
    {
        return $this->model
            ->whereIn('rfid', $ctnRFID_arr)
            ->where('whs_id', $whsId)
            ->orderBy('rfid', 'asc')
            ->get();
    }

    /**
     * @param $whsId
     * @param $wvDtlId
     * @param $loc_ids
     * @param int $limit
     *
     * @return mixed
     */
    public function getMoreLocation($whsId, $wvDtlId, $loc_ids, $limit = 6)
    {
        // Loc ids
        $locIds = is_array($loc_ids) ? $loc_ids : [$loc_ids];

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('cartons AS ct')
            ->select([
                'ct.sku',
                'ct.size',
                'ct.color',
                'ct.lot',
                'ct.item_id',
                'location.loc_id',
                'location.loc_code',
                'location.rfid as loc_rfid',
                DB::raw('COUNT(1) AS cartons'),
                DB::raw('SUM(ct.piece_remain) AS avail_qty')
            ])
            ->join('wv_dtl', function ($join) {
                $join->on('wv_dtl.item_id', '=', 'ct.item_id')
                    ->on('wv_dtl.lot', '=', 'ct.lot');
            })
            ->join('location', 'ct.loc_id', '=', 'location.loc_id')
            //->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where('location.loc_whs_id', $whsId)
            ->where('wv_dtl.wv_dtl_id', $wvDtlId)
            ->where('ct.is_damaged', 0)
            ->where('ct.ctn_sts', 'AC')
            //->where('loc_type.loc_type_code', 'RAC')
            ->where('ct.loc_type_code', 'RAC')
            ->where('ct.is_ecom', 0)
            ->whereNotIn('location.loc_id', $locIds)
            ->where('location.loc_sts_code', 'AC')
            ->whereNotNull('location.rfid')
            ->groupBy('ct.loc_id')
            ->orderBy('ct.loc_id');

        return $query->take($limit)->get();
    }

    public function getSuggestLocationsByWvDtlNextSku($whsId, $locIds, $wvId, $wvDtlId, $limit = 6)
    {
        // Loc ids
        $locIds = is_array($locIds) ? $locIds : [$locIds];

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('cartons AS ct')
            ->select([
                'ct.sku',
                'ct.size',
                'ct.color',
                'ct.lot',
                'ct.item_id',
                'location.loc_id',
                'location.loc_code',
                //'location.rfid as loc_rfid',
                DB::raw('COUNT(1) AS ctn_ttl'),
                DB::raw('SUM(ct.piece_remain) AS avail_qty')
            ])
            ->join('wv_dtl', function ($join) {
                $join->on('wv_dtl.item_id', '=', 'ct.item_id')
                    ->on('wv_dtl.lot', '=', 'ct.lot');
            })
            ->join('location', 'ct.loc_id', '=', 'location.loc_id')
            //->join('loc_type', 'loc_type.loc_type_id', '=', 'location.loc_type_id')
            ->where('location.loc_whs_id', $whsId)
            ->where('wv_dtl.wv_dtl_id', '!=', $wvDtlId)
            ->where('wv_dtl.wv_id', $wvId)
            ->where('ct.is_damaged', 0)
            ->where('ct.ctn_sts', 'AC')
            //->where('loc_type.loc_type_code', 'RAC')
            ->where('ct.loc_type_code', 'RAC')
            ->where('ct.is_ecom', 0)
            ->whereNotIn('location.loc_id', $locIds)
            ->where('location.loc_sts_code', 'AC')
            ->whereNotNull('location.rfid')
            ->groupBy('ct.loc_id')
            ->orderBy('ct.loc_id');

        return $query->take($limit)->get();
    }

    public function getSuggestLocationsByWvDtl($whsId, $wvDtl, $limit = 6)
    {

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('cartons AS ct')
            ->select([
                'ct.sku',
                'ct.size',
                'ct.color',
                'ct.lot',
                'ct.loc_id',
                'ct.loc_code',
                DB::raw('COUNT(1) AS ctn_ttl'),
                DB::raw('SUM(ct.piece_remain) AS avail_qty')
            ])
            ->where('ct.whs_id', $whsId)
            ->where('ct.is_damaged', 0)
            ->where('ct.ctn_sts', 'AC')
            ->where('ct.item_id', $wvDtl->item_id)
            ->where('ct.lot', $wvDtl->lot)
            ->where('ct.loc_type_code', 'RAC')
            ->where('ct.is_ecom', 0)
            ->groupBy('ct.loc_id')
            ->orderBy('ct.loc_id');

        $result = $query->take($limit)->get();

        return $result;
    }

    public function getCtnByLocationId($locId)
    {
        return $this->model
            ->where('loc_id', $locId)
            ->first();
    }

    public function getCtnByLocIds($locIds)
    {
        return $this->model
            ->whereIn('loc_id', $locIds)
            ->pluck('loc_id');
    }


    public function countCtnByLocationId($locId)
    {
        return $this->model
            ->where('loc_id', $locId)
            ->count();
    }

    public function sumPieceOfCtnByLocationId($locId)
    {
        return $this->model->select(DB::raw('SUM(piece_remain) as pieces'))
            ->where('loc_id', $locId)
            ->first();
    }

    public static function getCalculateStorageDurationRaw($startDate = 'created_at')
    {
        $strSQL = sprintf("IF(DATEDIFF('%s', FROM_UNIXTIME(`%s`)) > 0 , DATEDIFF('%s', FROM_UNIXTIME
        (`%s`)), 1)", date('Y-m-d'), $startDate, date('Y-m-d'), $startDate);

        return DB::raw($strSQL);
    }

    public function updateCartonPutBackNewLoc(int $pltID, array $arrLoc)
    {
        return $this->model->where([
            'plt_id' => $pltID
        ])
            ->update(
                [
                    'loc_id'   => $arrLoc['loc_id'],
                    'loc_name' => $arrLoc['loc_alternative_name'],
                    'loc_code' => $arrLoc['loc_code'],
                ]
            );
    }

    public function updateWvDtlLoc($cartons)
    {
        $locs = $this->sortCartonByLocations($cartons);

        $wvDtlLoc = $this->calculateActualQtyByLocations($locs);

        return $wvDtlLoc;
    }

    public function sortCartonByLocations($cartons)
    {
        $locs = [];
        foreach ($cartons as $carton) {
            $locs[$carton['loc_id']][] = $carton;
        }

        return $locs;
    }

    public function calculateActualQtyByLocations($locs)
    {
        $wvDtlLoc = [];
        foreach ($locs as $locId => $cartons) {
            $carton = $cartons[0];
            $total = array_sum(array_column($cartons, 'piece_remain'));
            //[{"loc_id":106,"loc_code":"001-H-L6-02","act_loc":"001-H-L6-02","act_loc_id":null,"avail_qty":"25","picked_qty":5}]
            $avaiQty = $this->getAvailableQty($locId);
            $loc = [
                'loc_id'     => $locId,
                'loc_code'   => $carton['loc_code'],
                'act_loc'    => $carton['loc_code'],
                'act_loc_id' => $locId,
                'picked_qty' => $total,
                'avail_qty'  => $avaiQty,
            ];
            $wvDtlLoc[] = $loc;
        }

        return $wvDtlLoc;
    }

    public function getAvailableQty($locID)
    {
        $total = $this->getModel()->where([
            'is_damaged' => 0,
            'ctn_sts'    => 'AC',
            'loc_id'     => $locID
        ])->sum('piece_remain');

        return $total;
    }

    /**
     * @param array $locIds
     * @param int $itemId
     *
     * @return mixed
     */
    public function getMoreSugLocByWvDtl($whsId, $wvDtl, $locIds, $limit = 6)
    {
        $customerConfigModel = new CustomerConfigModel();


        $itemId = (int)$wvDtl->item_id;

        $cus_id = $wvDtl->cus_id;
        $algorithm = $customerConfigModel->getPickingAlgorithm($whsId, $cus_id);

        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $query = DB::table('cartons')
            ->select([
                'cartons.sku',
                'cartons.size',
                'cartons.color',
                'cartons.lot',
                'cartons.item_id',
                'location.loc_id',
                'location.loc_code',
                'location.rfid as loc_rfid',
                DB::raw('COUNT(1) AS cartons'),
                DB::raw('SUM(cartons.piece_remain) AS avail_qty')
            ])
            ->join('location', 'cartons.loc_id', '=', 'location.loc_id')
            ->where('cartons.is_damaged', 0)
            ->where('cartons.ctn_sts', 'AC')
            ->where('cartons.loc_type_code', 'RAC')
            ->where('cartons.item_id', (int)$itemId)
            ->where('cartons.is_ecom', 0)
            ->where('cartons.lot', $wvDtl->lot)
            ->where('cartons.deleted', 0)
            ->whereNotIn('location.loc_id', $locIds)
            ->whereNotNull('location.rfid')
            ->groupBy('location.loc_id');
        switch (strtoupper($algorithm)) {
            case "LIFO":
                $query->orderBy('cartons.created_at', 'DESC');
                break;
            case "FEFO":
                $query->orderBy('cartons.expired_dt', 'ASC');
                break;
            case "FIFO":
                $query->orderBy('cartons.created_at', 'ASC');
                break;
        }

        $query->orderBy('location.loc_code');

        return $query->take($limit)->get();
    }

    public function getCartonsByPltId($whsId, $pltId)
    {
        $total = $this->getModel()->where([
            'plt_id' => $pltId,
            'whs_id' => $whsId
        ])
            ->select(DB::raw('COUNT(1) as ctn_ttl'), 'sku', 'lot')
            ->groupBy('item_id', 'lot');

        return $total->get();
    }

    /**
     * @param $arrCtnRfid
     * @param $whsId
     *
     * @return mixed
     */
    public function checkExistedCartonAccordingToRFID($arrCtnRfid, $whsId)
    {
        $result = $this->model
            ->where('rfid', $arrCtnRfid)
            ->where('whs_id', $whsId)
            ->first();

        return $result;
    }

    /**
     * @param $arrCtnRfid
     * @param $whsId
     *
     * @return mixed
     */
    public function checkShippedCartonAccordingToRFID($arrCtnRfid, $whsId)
    {
        $query = $this->model
            ->where('rfid', $arrCtnRfid)
            ->where('whs_id', $whsId);
        $query->leftJoin('odr_cartons', 'odr_cartons.ctn_id', '=', 'cartons.ctn_id');
        $result = $query->first();

        return $result;
    }

    /**
     * @param $whsId
     * @param $pltId
     *
     * @return mixed
     */
    public function getAllCtnByPltId($whsId, $pltId)
    {
        $total = $this->getModel()->where([
            'plt_id' => $pltId,
            'whs_id' => $whsId
        ])
            ->select('rfid', 'ctn_num', 'ctn_sts', 'sku', 'cus_id')
            ->orderBy('rfid', 'asc');

        return $total->get();
    }

    /**
     * @param $whsId
     * @param $pltId
     *
     * @return mixed
     */
    public function getAllCtnByCtnRfids($whsId, $arrCtnRfid)
    {
        $total = $this->getModel()
            ->where([
                'whs_id' => $whsId
            ])
            ->whereIn('rfid', $arrCtnRfid)
            ->select('rfid', 'ctn_num', 'ctn_sts', 'sku', 'cus_id')
            ->orderBy('rfid', 'asc');

        return $total->get();
    }

    /**
     * @param $cus_ids
     *
     * @return mixed
     */
    public function countCartonsByCusIds($cus_ids)
    {
        $resultCount = $this->model
            ->select(
                'cus_id',
                DB::raw("(COUNT(cartons.ctn_id) ) AS numberOfCarton")
            )
            ->whereIn('cus_id', $cus_ids)
            ->groupBy('cus_id')
            ->get();

        return $resultCount;
    }

    /**
     * @param $palletId
     *
     * @return mixed
     */
    public function countCartonsByPalletId($palletId)
    {
        $resultCount = $this->model
            ->select(
                DB::raw("(COUNT(cartons.ctn_id) ) AS numberOfCarton")
            )
            ->where('plt_id', $palletId)
            ->first();

        return $resultCount;
    }

    /**
     * @param $pltId
     * @param $whsId
     * @param $arrCtnRfid
     *
     * @return mixed
     */
    public function updatePalletID($pltId, $whsId, $arrCtnRfid)
    {
        $result = $this->model
            ->where('whs_id', $whsId)
            ->whereIn('rfid', $arrCtnRfid)
            ->update([
                'plt_id'  => $pltId,
                'ctn_sts' => 'AC',
                //'loc_id'   => null,
                //'loc_code' => null,
                //'loc_name' => null
            ]);

        return $result;

    }

    /**
     * @param $pltId
     * @param $whsId
     *
     * @return mixed
     */
    public function removeAllLocForCtnInPallet($pltId, $whsId)
    {
        $result = $this->model
            ->where('whs_id', $whsId)
            ->where('plt_id', $pltId)
            ->update([
                'loc_id'   => null,
                'loc_code' => null,
                'loc_name' => null
            ]);

        return $result;

    }


}
