<?php

namespace App\Jobs;

class IMSJob extends Job
{
    public function login()
    {
        $endpoint = env('IMS_AUTHENTICATION');
        $username = env('IMS_USERNAME');
        $password = env('IMS_PASSWORD');

        // Check IMS Configuration
        if(empty($endpoint) || empty($username) || empty($password)) {
            $this->writeLogment('IMS Authentication', 'IMS Authentication is not define!');
            return false;
        }

        $data = [
            "username" => $username,
            "password" => $password
        ];

        try {
            // Call cURL to IMS
            $result = $this->call($endpoint, 'POST', json_encode($data));

            $token = json_decode($result)->data->attributes->token;

            if(!empty($token)) {
                $this->writeLogment('IMS Authentication', 'IMS Authentication logged in!');
            }
            return $token;

        } catch (\Exception $e) {
            $this->writeLogment('IMS Authentication', $e->getMessage());
        }
    }

    public function call($url, $method, $data, $header = null)
    {
        $curl = curl_init();

        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $data,
        );

        if(is_null($header)) {
            $options[CURLOPT_HTTPHEADER] = [
                "Content-Type: application/json"
            ];
        }else{
            $options[CURLOPT_HTTPHEADER] = $header;
        }

        curl_setopt_array($curl, $options);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->writeLogment('IMS Calling', "cURL Error #:" . $err);
        } else {
            return $response;
        }
    }
}
