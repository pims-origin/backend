<?php

namespace App\Api\V1\Validators;

class ExcelImportValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'warehouse_id' => 'required',
            'file'         => 'required',
        ];
    }
}
