<?php

namespace App\Api\Shipping\Controllers;

use App\Api\Shipping\Models\CartonModel;
use App\Api\Shipping\Models\LocationModel;
use App\Api\Shipping\Models\OrderHdrMetaModel;
use App\Api\Shipping\Models\OrderHdrModel;
use App\Api\V1\Models\PackHdrModel;
use Illuminate\Support\Facades\DB;
use Psr\Http\Message\ServerRequestInterface as Request;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Utils\JWTUtil;
use Wms2\UserInfo\Data;
use GuzzleHttp\Client;

/**
 * Class OrderToShippingLaneController
 *
 * @package App\Api\V1\Controllers
 */
class OrderToShippingLaneController extends AbstractController
{
    /**
     * @var CartonModel
     */
    protected $cartonModel;

    /**
     * @var OrderHdrModel
     */
    protected $orderHdrModel;

    protected $orderHdrMetaModel;

    /**
     * @var LocationModel
     */
    protected $locationModel;

    public function __construct(
        CartonModel $cartonModel,
        OrderHdrModel $orderHdrModel
    ) {
        $this->cartonModel = $cartonModel;
        $this->orderHdrModel = $orderHdrModel;
        $this->orderHdrMetaModel = new OrderHdrMetaModel();
        $this->locationModel = new LocationModel();
    }

    /**
     * url PUT: v1/1/order-shipping-lane
     * @param integer $whsId
     *
     * @return json object
     */
    public function assignShippingLane($whsId, Request $request)
    {
        \DB::setFetchMode(\PDO::FETCH_ASSOC);

        $input = $request->getParsedBody();

        $odrNum = array_get($input, 'odr_num');
        $locCode = array_get($input, 'loc_code');

        if (!$odrNum || !$locCode) {
            $msg = "Please check input params.";
            return $this->response->errorBadRequest($msg);
        }

        try {

            $keyLoc = 'loc_code';
            $location = $this->locationModel->getLocationByKey($whsId, $keyLoc, $locCode);
            if (!$location) {
                $location = $this->locationModel->getLocationByKeyNoType($whsId, $keyLoc, $locCode);
                $msg = (!$location) ? 'Location is not existed' : 'Location is not Shipping type';

                return $this->response->errorBadRequest($msg);
            }
            else {
                if(object_get($location, 'loc_sts_code') != 'AC') {
                    $msg = 'Location is not active';

                    return $this->response->errorBadRequest($msg);
                }
            }

            // Check order was shipped
            $odrObj = $this->orderHdrModel->getFirstWhere([
                        'odr_num' => $odrNum,
                        'whs_id'  => $whsId,
                    ]);
            if (!$odrObj) {
                $msg = sprintf('Order %s does not exist.', $odrNum);

                return $this->response->errorBadRequest($msg);
            }
            if(object_get($odrObj, 'odr_sts') != "ST") {
                $msg = 'Only Staging Orders can be assigned to Shipping Lane';

                return $this->response->errorBadRequest($msg);
            }

            // Check order was assigned to shipping lane
            $chkOdrShippingLane = $this->orderHdrMetaModel->getFirstWhere([
                                    'odr_id'    => $odrObj->odr_id,
                                    'qualifier' => 'SPL',
                                ]);
            if ($chkOdrShippingLane) {
                $msg = sprintf('Order %s was assigned to shipping lane.', $odrNum);

                return $this->response->errorBadRequest($msg);
            }

            // start transaction
            DB::beginTransaction();

            // 1. Create new pallet and assign all of cartons of order
            // 2. Update shipping lane location to each carton
            // 3. Insert to order header meta
            // 4. Insert item event tracking

            $locId   = object_get($location, 'loc_id');
            $locCode = object_get($location, 'loc_code');
            $locName = object_get($location, 'loc_alternative_name');
            $userId  = Data::getCurrentUserId();

            // check exist location in pallet
            /*$isExistLoc = Pallet::where('loc_id', $locId)->first();
            if ($odrObj->odr_type != 'PKU' && $isExistLoc) {
                $msg = sprintf('Location %s was assigned to another Order.', $locCode);
                return $this->response->errorBadRequest($msg);
            }*/

            /*if ($odrObj->odr_type == 'PKU' && $isExistLoc) {
                $palletId = $isExistLoc->plt_id;
            } else {*/
                // 1.Create new pallet and assign all of cartons of order
                $pltNumObj = Pallet::where('whs_id', $whsId)
                    ->orderBy('plt_id', 'DESC')
                    ->first();
                $plt = [
                    "cus_id"           => $odrObj->cus_id,
                    "whs_id"           => $odrObj->whs_id,
                    "plt_num"          => ++$pltNumObj->plt_num,
                    "gr_hdr_id"        => null,
                    "gr_dtl_id"        => null,
                    "ctn_ttl"          => 0,
                    "init_ctn_ttl"     => 0,
                    "dmg_ttl"          => 0,
                    "expired_date"     => 0,
                    "storage_duration" => 0,
                    "is_movement"      => 0,
                    "item_id"          => null,
                    "sku"              => null,
                    "size"             => null,
                    "color"            => null,
                    "pack"             => null,
                    "lot"              => 'NA',
                    "is_full"          => 0,
                    "uom"              => null,
                    "weight"           => null,
                    "init_piece_ttl"   => 0,
                    "created_at"       => time(),
                    "updated_at"       => time(),
                    "created_by"       => $userId,
                    "updated_by"       => $userId,
                    "deleted"          => 0,
                    "deleted_at"       => 915148800,
                    "spc_hdl_code"     => object_get($location, 'spc_hdl_code'),
                    //"cat_code"         => null,
                    "loc_id"           => $locId,
                    "loc_code"         => $locCode,
                    "loc_name"         => $locName,
                    "rfid"             => null,
                    "plt_sts"          => 'PD',
                ];

                $palletId = DB::table('pallet')->insertGetId($plt);
            //}


            $sqlRaw = sprintf('cartons.ctn_id in
                             (select ctn_id
                                from odr_cartons
                                where odr_cartons.deleted = 0
                                    and odr_cartons.odr_hdr_id = %s
                            )', $odrObj->odr_id);

            $this->cartonModel->getModel()
                ->where('cartons.whs_id', $whsId)
                ->whereRaw($sqlRaw)
                ->update([
                    'cartons.plt_id'        => $palletId,
                    'cartons.loc_id'        => $locId,
                    'cartons.loc_code'      => $locCode,
                    'cartons.loc_name'      => $locName,
                    'cartons.loc_type_code' => "SHP",
                ]);

            //update apllet
            $this->_updatePalletWithCarton($palletId, $whsId);

            $this->orderHdrMetaModel->updateShippingLocation($odrObj, $location);

            // NAB 92 - RFID Tracking Events
            $this->_writeRfidTrackingEvents($whsId, $odrObj, $locCode);

            DB::commit();

            return ['msg' => 'successful'];
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * url PUT: v1/1/shipped-order
     * @param integer $whsId
     *
     * @return json object
     */
    public function shippedOrder($whsId, Request $request)
    {
        \DB::setFetchMode(\PDO::FETCH_ASSOC);

        $input = $request->getParsedBody();

        $odrNum       = array_get($input, 'odr_num');
        $actShippedDt = array_get($input, 'act_shipped_date');
        $actShippedDt = date("Y-m-d", (int)$actShippedDt);

        if (!$odrNum) {
            $msg = "Please check input params.";
            return $this->response->errorBadRequest($msg);
        }

        try {
            // Check order was shipped
            $odrObj = $this->orderHdrModel->getFirstWhere([
                        'odr_num' => $odrNum,
                        'whs_id'  => $whsId,
                    ]);
            if (!$odrObj) {
                $msg = sprintf('Order %s does not exist.', $odrNum);

                return $this->response->errorBadRequest($msg);
            }

            if(object_get($odrObj, 'odr_sts') == "SH") {
                $msg = sprintf('Order %s already shipped.', $odrNum);

                return $this->response->errorBadRequest($msg);
            }

            $client = new Client();

            $url = sprintf('%sorder-shippings/shipped?order_id=%s&shipped_dt=%s', env('API_ORDER'), $odrObj->odr_id, $actShippedDt);
            $response = $client->request('GET', $url,
                [
                    'headers'     => ['Authorization' => $request->getHeader('Authorization')]
                ]
            );

            return ['msg' => 'successful'];
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    private function _writeRfidTrackingEvents($whsId, $odrObj, $locCode)
    {
        $cartons = \DB::table('odr_cartons')
            ->join('cartons', 'odr_cartons.ctn_id', '=', 'cartons.ctn_id')
            ->select([
                'cartons.rfid',
                'cartons.sku',
                'cartons.des',
                'odr_cartons.odr_hdr_id',
                'odr_cartons.odr_num',
            ])
            ->where('odr_cartons.odr_hdr_id', $odrObj->odr_id)
            ->where('odr_cartons.whs_id', $whsId)
            ->whereNotNull('cartons.rfid')
            ->where('cartons.deleted', 0)
            ->where('odr_cartons.deleted', 0)
            ->get();

        $infoShippingLane = 'GUN - Assigning Order to shipping lane';
        $constructRfidTrackings = [
            'evt_name'      => 'Staging',
            'source'        => 'ORD',
            'info'          => sprintf('Shipping lane: %s', $locCode),
            'created_at'    => time(),
            'created_by'    => Data::getCurrentUserId(),
        ];

        /*$dataRfidTrackings = [];
        foreach ($cartons as $carton) {
            $constructRfidTrackings['rfid']    = array_get($carton, 'rfid');
            $constructRfidTrackings['sku']     = array_get($carton, 'sku');
            $constructRfidTrackings['des']     = array_get($carton, 'des');
            $constructRfidTrackings['ref_id']  = array_get($carton, 'odr_hdr_id');
            $constructRfidTrackings['ref_num'] = array_get($carton, 'odr_num');

            $dataRfidTrackings[] = $constructRfidTrackings;
        }

        if (count($dataRfidTrackings) > 0) {
            \DB::table('item_evt')->insert($dataRfidTrackings);
        }*/
    }

    private function _updatePalletWithCarton($pltId, $whsId)
    {
        $pieceTtlRaw = 'sum(piece_remain) as piece_ttl';
        $ctnTtlRaw   = 'count(ctn_id) as ctn_ttl';
        $query = DB::table('cartons')
                ->select([
                    DB::raw($pieceTtlRaw),
                    DB::raw($ctnTtlRaw),
                    'item_id',
                    'sku',
                    'size',
                    'color',
                    'ctn_pack_size',
                    'lot',
                ])
                ->where('plt_id', $pltId)
                ->where('whs_id', $whsId)
                ->where('deleted', 0)
                ->get();

        $queryFirst = array_first($query);
        $pieceTtl = (int)array_get($queryFirst, 'piece_ttl', 0);
        $ctnTtl   = (int)array_get($queryFirst, 'ctn_ttl', 0);

        Pallet::where('plt_id', $pltId)
            ->update([
                'ctn_ttl'        => DB::raw("ctn_ttl + $ctnTtl"),
                'init_ctn_ttl'   => DB::raw("init_ctn_ttl + $ctnTtl"),
                'init_piece_ttl' => DB::raw("init_piece_ttl + $pieceTtl"),
                'item_id'        => array_get($queryFirst, 'item_id', null),
                'sku'            => array_get($queryFirst, 'sku', null),
                'size'           => array_get($queryFirst, 'size', null),
                'color'          => array_get($queryFirst, 'color', null),
                'pack'           => array_get($queryFirst, 'ctn_pack_size', null),
            ]);
    }

    function shippingLane($whsId, Request $request) {

        \DB::setFetchMode(\PDO::FETCH_ASSOC);

        $input = $request->getParsedBody();

        $out_side_box       = array_get($input, 'out_side_box');
        $locCode            = array_get($input, 'loc_code');
        $plt_num            = array_get($input, 'plt_num', "");

        $odrNum = '';
        if($out_side_box != null) {
            $packHdrModel = new PackHdrModel();
            $dataPack = $packHdrModel->getModel()->where('out_side_box',$out_side_box)->first();
            $odrHdrId = array_get($dataPack, 'odr_hdr_id', null);
            if($odrHdrId != null){
                $odrHdrModel = new OrderHdrModel();
                $dataOdrHdr = $odrHdrModel->getModel()->where('odr_id',$odrHdrId)->first();
                $odrNum = array_get($dataOdrHdr, 'odr_num', null);
            }
        }
        if (!$odrNum || !$locCode) {
            $msg = "Please check input params.";
            return $this->response->errorBadRequest($msg);
        }

        try {

            $keyLoc = 'loc_code';
            $location = $this->locationModel->getLocationByKey($whsId, $keyLoc, $locCode);
            if (!$location) {
                $location = $this->locationModel->getLocationByKeyNoType($whsId, $keyLoc, $locCode);
                $msg = (!$location) ? 'Location is not existed' : 'Location is not Shipping type';

                return $this->response->errorBadRequest($msg);
            }
            else {
                if(object_get($location, 'loc_sts_code') != 'AC') {
                    $msg = 'Location is not active';

                    return $this->response->errorBadRequest($msg);
                }
            }

            // Check order was shipped
            $odrObj = $this->orderHdrModel->getFirstWhere([
                'odr_num' => $odrNum,
                'whs_id'  => $whsId,
            ]);
            if (!$odrObj) {
                $msg = sprintf('Order %s does not exist.', $odrNum);

                return $this->response->errorBadRequest($msg);
            }
            if(object_get($odrObj, 'odr_sts') != "ST") {
                $msg = 'Only Staging Orders can be assigned to Shipping Lane';

                return $this->response->errorBadRequest($msg);
            }

            // Check order was assigned to shipping lane
            $chkOdrShippingLane = $this->orderHdrMetaModel->getFirstWhere([
                'odr_id'    => $odrObj->odr_id,
                'qualifier' => 'SPL',
            ]);
            if ($chkOdrShippingLane) {
                $msg = sprintf('Order %s was assigned to shipping lane.', $odrNum);

                return $this->response->errorBadRequest($msg);
            }

            // start transaction
            DB::beginTransaction();

            // 1. Create new pallet and assign all of cartons of order
            // 2. Update shipping lane location to each carton
            // 3. Insert to order header meta
            // 4. Insert item event tracking

            $locId   = object_get($location, 'loc_id');
            $locCode = object_get($location, 'loc_code');
            $locName = object_get($location, 'loc_alternative_name');
            $userId  = Data::getCurrentUserId();

            // check exist location in pallet
            /*$isExistLoc = Pallet::where('loc_id', $locId)->first();
            if ($odrObj->odr_type != 'PKU' && $isExistLoc) {
                $msg = sprintf('Location %s was assigned to another Order.', $locCode);
                return $this->response->errorBadRequest($msg);
            }*/

            /*if ($odrObj->odr_type == 'PKU' && $isExistLoc) {
                $palletId = $isExistLoc->plt_id;
            } else {*/
            // 1.Create new pallet and assign all of cartons of order
            $pltNumObj = Pallet::where('whs_id', $whsId)
                ->where('plt_num', $plt_num)
                ->first();

            $palletId = array_get($pltNumObj, 'plt_id', null);
            //}


            $sqlRaw = sprintf('cartons.ctn_id in
                             (select ctn_id
                                from odr_cartons
                                where odr_cartons.deleted = 0
                                    and odr_cartons.odr_hdr_id = %s
                            )', $odrObj->odr_id);

            $this->cartonModel->getModel()
                ->where('cartons.whs_id', $whsId)
                ->whereRaw($sqlRaw)
                ->update([
                    'cartons.plt_id'        => $palletId,
                    'cartons.loc_id'        => $locId,
                    'cartons.loc_code'      => $locCode,
                    'cartons.loc_name'      => $locName,
                    'cartons.loc_type_code' => "SHP",
                ]);

            //update apllet
            $this->_updatePalletWithCarton($palletId, $whsId);

            $this->orderHdrMetaModel->updateShippingLocation($odrObj, $location);

            // NAB 92 - RFID Tracking Events
            $this->_writeRfidTrackingEvents($whsId, $odrObj, $locCode);

            DB::commit();

            return ['msg' => 'successful'];
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}

