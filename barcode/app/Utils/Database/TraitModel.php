<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Utils\Database;

use Illuminate\Support\Facades\DB;

trait TraitModel
{
    public function orderByRaw($key, $value, &$query)
    {
        $buildKey = <<<EOF
CASE WHEN ASCII(SUBSTRING(`$key`,1)) BETWEEN 48 AND 57 THEN
	   CAST(`$key` AS UNSIGNED)
  WHEN ASCII(SUBSTRING(`$key`,2)) BETWEEN 48 AND 57 THEN
	   SUBSTRING(`$key`,1,1)
  WHEN ASCII(SUBSTRING(`$key`,3)) BETWEEN 48 AND 57 THEN
	   SUBSTRING(`$key`,1,2)
  WHEN ASCII(SUBSTRING(`$key`,4)) BETWEEN 48 AND 57 THEN
	   SUBSTRING(`$key`,1,3)
  WHEN ASCII(SUBSTRING(`$key`,5)) BETWEEN 48 AND 57 THEN
	   SUBSTRING(`$key`,1,4)
  WHEN ASCII(SUBSTRING(`$key`,6)) BETWEEN 48 AND 57 THEN
	   SUBSTRING(`$key`,1,5)
  WHEN ASCII(SUBSTRING(`$key`,7)) BETWEEN 48 AND 57 THEN
	   SUBSTRING(`$key`,1,6)
  WHEN ASCII(SUBSTRING(`$key`,8)) BETWEEN 48 AND 57 THEN
	   SUBSTRING(`$key`,1,7)
END,
CASE WHEN ASCII(SUBSTRING(`$key`,1)) BETWEEN 48 AND 57 THEN
	   CAST(SUBSTRING(`$key`,1) AS UNSIGNED)
  WHEN ASCII(SUBSTRING(`$key`,2)) BETWEEN 48 AND 57 THEN
	   CAST(SUBSTRING(`$key`,2) AS UNSIGNED)
  WHEN ASCII(SUBSTRING(`$key`,3)) BETWEEN 48 AND 57 THEN
	   CAST(SUBSTRING(`$key`,3) AS UNSIGNED)
  WHEN ASCII(SUBSTRING(`$key`,4)) BETWEEN 48 AND 57 THEN
	   CAST(SUBSTRING(`$key`,4) AS UNSIGNED)
  WHEN ASCII(SUBSTRING(`$key`,5)) BETWEEN 48 AND 57 THEN
	   CAST(SUBSTRING(`$key`,5) AS UNSIGNED)
  WHEN ASCII(SUBSTRING(`$key`,6)) BETWEEN 48 AND 57 THEN
	   CAST(SUBSTRING(`$key`,6) AS UNSIGNED)
  WHEN ASCII(SUBSTRING(`$key`,7)) BETWEEN 48 AND 57 THEN
	   CAST(SUBSTRING(`$key`,7) AS UNSIGNED)
  WHEN ASCII(SUBSTRING(`$key`,8)) BETWEEN 48 AND 57 THEN
	   CAST(SUBSTRING(`$key`,8) AS UNSIGNED)
END
$value
EOF;
        $query->orderByRaw(DB::raw($buildKey));
    }

}
