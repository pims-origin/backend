<?php

namespace App\Api\Inbound2\Traits;



use App\Api\V1\Models\PalletModel;
use App\Jobs\CreateWavePickXDock;
use Seldat\Wms2\Models\GoodsReceipt;
use Seldat\Wms2\Models\GoodsReceiptDetail;
use Seldat\Wms2\Models\InventorySummary;
use Psr\Http\Message\ServerRequestInterface as Request;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Models\AsnMeta;
use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\DamageCarton;
use Seldat\Wms2\Models\EventTracking;
use Seldat\Wms2\Models\Location;
use Seldat\Wms2\Models\Pallet;
use Seldat\Wms2\Models\OrderDtl as OrderDtlModel;
use Seldat\Wms2\Models\OrderHdr as OrderHdrModel;
use Seldat\Wms2\Models\VirtualCartonSummary;
use App\Api\Inbound2\Models\GoodsReceiptModel;
use Wms2\UserInfo\Data;

trait GoodsReceiptTrait
{
    /*
     *  X Dock clone API
     */

    /**
     * url: inbound/{whsId}/scan-carton/asn-dtl/{asnDtlId}
     * @throws \Exception
     * @internal param lpn string $code
     * @internal param integer $dmg_ttl
     * @internal param integer $act_ctn_ttl
     * @internal param decimal $weight
     * @internal param integer expired_date
     * @internal param string $lot
     *
     * @return mix
     */

    protected $completed;

//    public function __construct()
//    {
//        $this->completed = 0;
//    }

    public function doGRforXDock($whsId, $asnDtlId, Request $request)
    {
        $whsInfo = $this->getWhsInfo($whsId);
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $input = $request->getParsedBody();

        $ctnTtl = array_get($input, 'act_ctn_ttl', null); //this case is ttl kg, not null
        $weight = array_get($input, 'weight', null); //this case is ttl kg, null
        $dmgCtns = array_get($input, 'dmg_ctns', null);
        $isFull = array_get($input, 'is_full', 0);
        $isKg = false; // OUM is kg

        $dmgTtl = $dmgTtl2 = array_get($input, 'dmg_ttl', 0);
        // $dmgTtl = $dmgTtl2 = 0;
        // if ($dmgCtns) {
        //     foreach ($dmgCtns as $dmgCtn) {
        //         if (array_get($dmgCtn, 'dmg_id') > 0) {
        //             $dmgTtl++;
        //         }
        //     }
        // }
        // $dmgTtl2 = $dmgTtl;
        // $input['dmg_ttl'] = $dmgTtl;

        $rfid = array_get($input, 'lpn', null);
        $arrDmgCartonId = [];
        $expiredDate = array_get($input, 'expired_date', null);
        $lot = trim(array_get($input, 'lot', null));

        if ($expiredDate) {
            $expiredDate = strtotime(\DateTime::createFromFormat('ymd', $expiredDate)->format('Y-m-d'));
            if ($expiredDate < time()) {
                return $this->response->errorBadRequest("Expired date is less than today.");
            }
        }

        if ((int)$ctnTtl <= 0) {
            $msg = sprintf('Carton total must be greater than or equal one');

            return $this->response->errorBadRequest($msg);
        }

        if ((int)$ctnTtl >= self::MAX_ACTUAL_TTL) {
            $msg = sprintf('Actual total must be not greater than max total %d', self::MAX_ACTUAL_TTL);

            return $this->response->errorBadRequest($msg);
        }

        if ($dmgTtl > (int)$ctnTtl) {
            $msg = sprintf('Damaged total %d must be not greater than %d actual total', $dmgTtl, (int)$ctnTtl);

            return $this->response->errorBadRequest($msg);
        }

        $asnDtl = AsnDtl::where('asn_dtl_id', $asnDtlId)->first();
        if (empty($asnDtl)) {
            return $this->response->errorBadRequest('Asn dtl not existed');
        }

        if ($asnDtl->asn_dtl_sts == 'CC') {
            return $this->response->errorBadRequest('This item is cancelled');
        }

        //cuong, check UOM is KG
        if ($asnDtl->uom_code == 'KG') {
            $isKg = true;

            if ($asnDtl->asn_dtl_pack > 1) {
                return $this->response->errorBadRequest('UOM is Kg only process for pack size 1.');
            }

            if (empty($weight)) {
                return $this->response->errorBadRequest('UOM is Kg only process for weight greater than 0.');
            }
        } else {
            $ctnTtl = (int)$ctnTtl;
        }

        $asnHdr = AsnHdr::where('asn_hdr_id', $asnDtl->asn_hdr_id)->first();

        if(empty($asnHdr)){
            return $this->response->errorBadRequest('The ASN is not existed.');
        }
        //get gr hdr
        $grHdr = GoodsReceipt::where([
            'asn_hdr_id' => $asnHdr->asn_hdr_id,
            'ctnr_id'    => $asnDtl->ctnr_id
        ])->first();

        $grObj = new GoodsReceiptModel();
        //$grObj->validateLPNFormat($rfid, $whsInfo);
        $grObj->validateLPNPalFormat($rfid, $whsInfo);
        $nwPltNum = $grObj->validateRfidExisted($rfid, $grHdr->gr_hdr_id ?? -1,1);

        try {
            DB::beginTransaction();

            $userId = Data::getCurrentUserId();
            $expiredDate = !empty($expiredDate) ? $expiredDate : $asnDtl->expired_dt;
            if ($asnDtl->asn_dtl_sts == 'NW') {
                $asnDtl->asn_dtl_sts = 'RG';
                $asnDtl->save();

            }

            // Asn Meta By Qualifier
            $createdAt = AsnMeta::where('qualifier', 'GAD')
                ->where('asn_hdr_id', $asnHdr->asn_hdr_id)
                ->select('value')
                ->first();
            $createdAt = object_get($createdAt, 'value', time());

            if (!$grHdr) {
                $grSeq = GoodsReceipt::where('asn_hdr_id', $asnHdr->asn_hdr_id)->count();
                $grSeq++;
                $asnHdrNum = $asnHdr->asn_hdr_num;
                $grNum = str_replace(['ASN', 'RMA'], 'GDR', $asnHdrNum);
                $grNum = $grNum . '-' . str_pad($grSeq, 2, '0', STR_PAD_LEFT);


                $grHdr = new GoodsReceipt();
                $arr = [
                    "ctnr_id"       => $asnDtl->ctnr_id,
                    "asn_hdr_id"    => $asnDtl->asn_hdr_id,
                    "gr_hdr_seq"    => $grSeq,
                    "gr_hdr_ept_dt" => $asnHdr->asn_hdr_ept_dt,
                    "gr_hdr_num"    => $grNum,
                    "whs_id"        => $asnHdr->whs_id,
                    "cus_id"        => $asnHdr->cus_id,
                    "gr_in_note"    => "",
                    "gr_ex_note"    => "",
                    "ctnr_num"      => $asnDtl->ctnr_num,
                    "gr_sts"        => "RG",
                    "ref_code"      => $asnHdr->asn_hdr_ref,
                    "created_from"  => 'GUN',
                    "created_at"    => $createdAt,
                    "updated_at"    => $createdAt,
                    "created_by"    => $userId,
                    "updated_by"    => $userId,
                    "deleted"       => 0,
                    "deleted_at"    => 915148800
                ];
                $grHdr->fill($arr)->save();

                //event tracking
                $evtGRCreated = [
                    'whs_id'     => $whsId,
                    'cus_id'     => $asnHdr->cus_id,
                    'owner'      => object_get($grHdr, 'gr_hdr_num', null),
                    'evt_code'   => 'WGN',
                    'trans_num'  => $asnDtl->ctnr_num,
                    'info'       => sprintf('GUN - New Goods Receipt %s Created', $grHdr->gr_hdr_num),
                    'created_at' => time(),
                    'created_by' => $userId,
                ];

                EventTracking::insert($evtGRCreated);
            } else {
                $grNum = $grHdr->gr_hdr_num;

                if ($grHdr->created_from != 'GUN') {
                    return $this->response->errorBadRequest("This Goods Receipt can't process by GUN.");
                }

                if (!in_array($grHdr->gr_sts, ['NW', 'RG'])) {
                    return $this->response->errorBadRequest("This Goods Receipt was processed.");
                }

                $evtGRUpdate = [
                    'whs_id'     => $whsId,
                    'cus_id'     => $asnHdr->cus_id,
                    'owner'      => object_get($grHdr, 'gr_hdr_num', null),
                    'evt_code'   => 'WGU',
                    'trans_num'  => $asnDtl->ctnr_num,
                    'info'       => sprintf('GUN - Goods Receipt Update %d cartons, %d damaged cartons',
                        ($isKg) ? 1 : $ctnTtl,
                        $dmgTtl),
                    'created_at' => time(),
                    'created_by' => $userId,
                ];

                EventTracking::insert($evtGRUpdate);
            }

            if ($asnHdr->asn_sts == 'NW') {
                $asnHdr->asn_sts = 'RG';
                $asnHdr->save();

                $evtAsnCreated = [
                    'whs_id'     => $whsId,
                    'cus_id'     => $asnHdr->cus_id,
                    'owner'      => object_get($asnHdr, 'asn_hdr_num', null),
                    'evt_code'   => 'WAR',
                    'trans_num'  => $asnDtl->ctnr_num,
                    'info'       => sprintf('GUN - New Goods Receipt %s Created', $grHdr->gr_hdr_num),
                    'created_at' => time(),
                    'created_by' => $userId,
                ];

                EventTracking::insert($evtAsnCreated);
            }

            //create gr dtl
            $grDtl = GoodsReceiptDetail::where([
                'asn_dtl_id' => $asnDtlId,
                'gr_hdr_id'  => $grHdr->gr_hdr_id,
                'item_id'    => $asnDtl->item_id,
                'lot'        => $asnDtl->asn_dtl_lot,
            ])->first();
            if (!$grDtl) {
                $grDtl = new GoodsReceiptDetail();
                $arr = [
                    "asn_dtl_id"         => $asnDtlId,
                    "gr_hdr_id"          => $grHdr->gr_hdr_id,
                    "gr_dtl_ept_ctn_ttl" => $asnDtl->asn_dtl_ctn_ttl,
                    "gr_dtl_act_ctn_ttl" => $ctnTtl,
                    "gr_dtl_ept_qty_ttl" => ($isKg) ? $asnDtl->asn_dtl_qty_ttl :
                        $asnDtl->asn_dtl_ctn_ttl * $asnDtl->asn_dtl_pack, //new field

                    "gr_dtl_disc_qty" => ($isKg) ? $weight - $asnDtl->asn_dtl_qty_ttl : $ctnTtl *
                        $asnDtl->asn_dtl_pack - ($asnDtl->asn_dtl_ctn_ttl * $asnDtl->asn_dtl_pack), //new field

                    "gr_dtl_act_qty_ttl" => ($isKg) ? $weight : $ctnTtl * $asnDtl->asn_dtl_pack,//new field
                    "gr_dtl_des"         => $asnDtl->asn_dtl_des,
                    "gr_dtl_disc"        => $ctnTtl - $asnDtl->asn_dtl_ctn_ttl,
                    "gr_dtl_plt_ttl"     => 1,
                    "gr_dtl_is_dmg"      => $input['dmg_ttl'] > 0 ? 1 : 0,
                    "gr_dtl_dmg_ttl"     => $input['dmg_ttl'] > 0 ? $input['dmg_ttl'] : 0,
                    "cat_code"           => $asnDtl->cat_code,
                    "cat_name"           => $asnDtl->cat_name,
                    "gr_dtl_sts"         => "RG",
                    "item_id"            => $asnDtl->item_id,
                    "pack"               => $asnDtl->asn_dtl_pack,
                    "sku"                => $asnDtl->asn_dtl_sku,
                    "color"              => $asnDtl->asn_dtl_color,
                    "size"               => $asnDtl->asn_dtl_size,
                    "lot"                => $asnDtl->asn_dtl_lot,
                    "po"                 => $asnDtl->asn_dtl_po,
                    "uom_code"           => $asnDtl->uom_code,
                    "uom_name"           => $asnDtl->uom_name,
                    "uom_id"             => $asnDtl->uom_id,
                    "expired_dt"         => $expiredDate,
                    "upc"                => $asnDtl->asn_dtl_cus_upc,
                    "ctnr_id"            => $asnDtl->ctnr_id,
                    "ctnr_num"           => $asnDtl->ctnr_num,
                    "length"             => $asnDtl->asn_dtl_length,
                    "width"              => $asnDtl->asn_dtl_width,
                    "height"             => $asnDtl->asn_dtl_height,
                    "weight"             => ($isKg) ? $weight : $asnDtl->asn_dtl_weight,
                    "cube"               => $asnDtl->asn_dtl_cube,
                    "volume"             => $asnDtl->asn_dtl_volume,
                    "ucc128"             => $asnDtl->ucc128,
                    "crs_doc"            => 0, //consider
                    "spc_hdl_code"       => $asnDtl->spc_hdl_code,
                    "spc_hdl_name"       => $asnDtl->spc_hdl_name,
                    "created_at"         => $createdAt,
                    "updated_at"         => $createdAt,
                    "created_by"         => $userId,
                    "updated_by"         => $userId,
                    "deleted"            => 0,
                    "deleted_at"         => 915148800
                ];
                $grDtl->fill($arr)->save();

            } else {
                $grDtl->gr_dtl_act_ctn_ttl += $ctnTtl;
                if ($isKg) {
                    $grDtl->weight += $weight;
                    $grDtl->gr_dtl_act_qty_ttl += $weight;
                } else {
                    $grDtl->gr_dtl_act_qty_ttl += $ctnTtl * $asnDtl->asn_dtl_pack;
                }

                $cartons = DB::table("cartons")
                    ->join("pallet","pallet.plt_id","=","cartons.plt_id")
                    ->where("pallet.rfid",$rfid)
                    ->where("cartons.item_id",$asnDtl->item_id)
                    ->where("cartons.deleted",0)
                    ->count();
                if($cartons == 0) {
                    $grDtl->gr_dtl_plt_ttl += 1;
                }
                $grDtl->gr_dtl_is_dmg = $input['dmg_ttl'] > 0 ? 1 : $grDtl->gr_dtl_is_dmg;
                $grDtl->gr_dtl_dmg_ttl += $input['dmg_ttl'];
                $grDtl->gr_dtl_disc = $grDtl->gr_dtl_act_ctn_ttl - $asnDtl->asn_dtl_ctn_ttl;
                $grDtl->gr_dtl_disc_qty = $grDtl->gr_dtl_act_qty_ttl - $grDtl->gr_dtl_ept_qty_ttl;
                $grDtl->save();
            }

            $lot = empty($lot) ? $asnDtl->asn_dtl_lot : $lot;
            $palletID = null;
            if (is_object($nwPltNum)) {
                if ($isKg) {
                    return $this->response->errorBadRequest('Kg not allow mix sku');
                }

                //if ($grDtl->spc_hdl_code != $nwPltNum->spc_hdl_code) {
                //    return $this->response->errorBadRequest("Pallet's temperature don't match with item.");
                //}

                $palletID = $nwPltNum->plt_id;

                //process scan pallet many times
                $nwPltNum->ctn_ttl += $ctnTtl;
                $nwPltNum->init_ctn_ttl += $ctnTtl;
                $nwPltNum->init_piece_ttl += $grDtl->pack * $ctnTtl;
                $nwPltNum->dmg_ttl += $input['dmg_ttl'];
                if ($grDtl->item_id != $nwPltNum->item_id || $lot != $nwPltNum->lot) {
                    $nwPltNum->mixed_sku = 1;
                    $nwPltNum->sku = $nwPltNum->size = $nwPltNum->color = 'NA';
                }
                $nwPltNum->save();

            } else {
                //add plt_num
                $plt = [
                    "cus_id"           => $asnHdr->cus_id,
                    "whs_id"           => $asnHdr->whs_id,
                    "plt_num"          => !empty($cus_pid) ? $rfid . "-" . $cus_pid : ++$nwPltNum,
                    // "cus_pid"          => !empty($cus_pid) ? $cus_pid : null,
                    "rfid"             => $rfid,
                    "gr_hdr_id"        => $grHdr->gr_hdr_id,
                    "gr_dtl_id"        => $grDtl->gr_dtl_id,
                    "ctn_ttl"          => ($isKg) ? 1 : $input['act_ctn_ttl'],
                    "init_ctn_ttl"     => $ctnTtl,
                    "dmg_ttl"          => $input['dmg_ttl'],
                    "expired_date"     => !empty($expiredDate) ? $expiredDate : $asnDtl->expired_dt,
                    "storage_duration" => 0,
                    "plt_sts"          => 'NW',
                    "is_movement"      => 0,
                    "item_id"          => $grDtl->item_id,
                    "sku"              => $grDtl->sku,
                    "size"             => $grDtl->size,
                    "color"            => $grDtl->color,
                    "pack"             => ($isKg) ? $ctnTtl : $grDtl->pack,
                    "lot"              => $lot,
                    "is_full"          => $isFull,
                    "uom"              => ($isKg) ? 'PL' : null,
                    "weight"           => ($isKg) ? $weight : null,
                    "init_piece_ttl"   => ($isKg) ? $weight : $grDtl->pack * $ctnTtl,
                    "created_at"       => $createdAt,
                    "updated_at"       => $createdAt,
                    "created_by"       => $userId,
                    "updated_by"       => $userId,
                    "deleted"          => 0,
                    "deleted_at"       => 915148800,
                    "spc_hdl_code"     => $grDtl->spc_hdl_code
                ];

                $palletID = DB::table('pallet')->insertGetId($plt);
            }


            //create cartons
            $ctnNumPrefix = str_replace('GDR', 'CTN', $grHdr->gr_hdr_num) . "-";
            $maxCtnNum = DB::table('cartons')->where('ctn_num', 'LIKE', $ctnNumPrefix . '%')->max('ctn_num');
            $maxCtnNum = $maxCtnNum ?: $ctnNumPrefix . '0000';

            $cartonParams = [];
            $cartonDamages = [];

            $ctnSts = 'RG';
            if ($isKg) {
                $cartonParams = [
                    'asn_dtl_id'    => $asnDtlId,
                    'gr_dtl_id'     => $grDtl->gr_dtl_id,
                    'gr_hdr_id'     => $grDtl->gr_hdr_id,
                    'is_damaged'    => 0,
                    'item_id'       => $grDtl->item_id,
                    'whs_id'        => $whsId,
                    'cus_id'        => $grHdr->cus_id,
                    'ctn_num'       => ++$maxCtnNum,
                    'ctn_sts'       => $ctnSts,
                    'ctn_uom_id'    => $asnDtl->uom_id,
                    'uom_code'      => $asnDtl->uom_code,
                    'uom_name'      => $asnDtl->uom_name,
                    'ctn_pack_size' => 1,
                    // 'piece_remain'  => round($weight), // this case is weight
                    'piece_remain'  => $weight, // this case is weight
                    // 'piece_ttl'     => round($weight), // this case is weight
                    'piece_ttl'     => $weight, // this case is weight
                    'gr_dt'         => $createdAt,
                    'sku'           => $asnDtl->asn_dtl_sku,
                    'size'          => $asnDtl->asn_dtl_size,
                    'color'         => $asnDtl->asn_dtl_color,
                    'lot'           => $lot,
                    'po'            => $asnDtl->asn_dtl_po,
                    'upc'           => $asnDtl->asn_dtl_cus_upc,
                    'ctnr_id'       => $asnDtl->ctnr_id,
                    'ctnr_num'      => $asnDtl->ctnr_num,
                    'length'        => $asnDtl->asn_dtl_length,
                    'width'         => $asnDtl->asn_dtl_width,
                    'height'        => $asnDtl->asn_dtl_height,
                    'weight'        => $weight,
                    'volume'        => $asnDtl->asn_dtl_volume,
                    'cube'          => $asnDtl->asn_dtl_cube,
                    'expired_dt'    => $expiredDate,
                    'ucc128'        => $asnDtl->ucc128,
                    'des'           => $asnDtl->asn_dtl_des,
                    'cat_code'      => $asnDtl->cat_code,
                    'cat_name'      => $asnDtl->cat_name,
                    'spc_hdl_code'  => $asnDtl->spc_hdl_code,
                    'spc_hdl_name'  => $asnDtl->spc_hdl_name,
                    'plt_id'        => $palletID,
                    'inner_pack'    => $ctnTtl,
                    'loc_id'        => null,
                    'loc_code'      => null,
                    'loc_name'      => null,
                    "created_at"    => time(),
                    "updated_at"    => time(),
                    "created_by"    => $userId,
                    "updated_by"    => $userId,
                    "deleted"       => 0,
                    "deleted_at"    => 915148800
                ];
            } else {
                for ($i = 0; $i < $ctnTtl; $i++) {
                    $cartonParam = [
                        'asn_dtl_id'    => $asnDtlId,
                        'gr_dtl_id'     => $grDtl->gr_dtl_id,
                        'gr_hdr_id'     => $grDtl->gr_hdr_id,
                        'is_damaged'    => $dmgTtl-- > 0 ? 1 : 0,
                        'item_id'       => $grDtl->item_id,
                        'whs_id'        => $whsId,
                        'cus_id'        => $grHdr->cus_id,
                        'ctn_num'       => ++$maxCtnNum,
                        'ctn_sts'       => $ctnSts,
                        'ctn_uom_id'    => $asnDtl->uom_id,
                        'uom_code'      => $asnDtl->uom_code,
                        'uom_name'      => $asnDtl->uom_name,
                        'ctn_pack_size' => $asnDtl->asn_dtl_pack,
                        'piece_remain'  => $asnDtl->asn_dtl_pack,
                        'piece_ttl'     => $asnDtl->asn_dtl_pack,
                        'gr_dt'         => $createdAt,
                        'sku'           => $asnDtl->asn_dtl_sku,
                        'size'          => $asnDtl->asn_dtl_size,
                        'color'         => $asnDtl->asn_dtl_color,
                        'lot'           => $lot,
                        'po'            => $asnDtl->asn_dtl_po,
                        'upc'           => $asnDtl->asn_dtl_cus_upc,
                        'ctnr_id'       => $asnDtl->ctnr_id,
                        'ctnr_num'      => $asnDtl->ctnr_num,
                        'length'        => $asnDtl->asn_dtl_length,
                        'width'         => $asnDtl->asn_dtl_width,
                        'height'        => $asnDtl->asn_dtl_height,
                        'weight'        => $asnDtl->asn_dtl_weight,
                        'volume'        => $asnDtl->asn_dtl_volume,
                        'cube'          => $asnDtl->asn_dtl_cube,
                        'expired_dt'    => $expiredDate,
                        'ucc128'        => $asnDtl->ucc128,
                        'des'           => $asnDtl->asn_dtl_des,
                        'cat_code'      => $asnDtl->cat_code,
                        'cat_name'      => $asnDtl->cat_name,
                        'spc_hdl_code'  => $asnDtl->spc_hdl_code,
                        'spc_hdl_name'  => $asnDtl->spc_hdl_name,
                        'plt_id'        => $palletID,
                        'loc_id'        => null,
                        'loc_code'      => null,
                        'loc_name'      => null,
                        "created_at"    => time(),
                        "updated_at"    => time(),
                        "created_by"    => $userId,
                        "updated_by"    => $userId,
                        "deleted"       => 0,
                        "deleted_at"    => 915148800
                    ];

                    if ($cartonParam['is_damaged'] == 1) {
                        $cartonDamages[] = $cartonParam;
                    } else {
                        $cartonParams[] = $cartonParam;
                    }
                }
            }

            if (!empty($cartonParams)) {
                Carton::insert($cartonParams);
            }

            //add dmg cartons and dmg carton note
            if (!empty($cartonDamages)) {
                foreach ($cartonDamages as $idx => $cartonDamage) {
                    $cartonDamageId = Carton::insertGetId($cartonDamage);
                    $dmgData = [
                        "ctn_id"     => $cartonDamageId,
                        "dmg_id"     => array_get($dmgCtns, 'dmg_id') ?? 1,
                        "dmg_note"   => array_get($dmgCtns, 'note'),
                        "created_at" => 1493973026,
                        "updated_at" => 1493973026,
                        "created_by" => $userId,
                        "updated_by" => $userId,
                        "deleted"    => 0,
                        "deleted_at" => 915148800
                    ];
                    DB::table('damage_carton')->insert($dmgData);
                    $arrDmgCartonId[] = $cartonDamageId;

                }
            }

            //sprintf('GUN - %d  cartons  to pallet  %s', ($isKg)? 1 : $ctnTtl, $rfid)
            $info = sprintf('GUN - %d  cartons  to pallet  %s, order %s', $ctnTtl, $rfid, $input['odr_num']);
            if ($isKg) {
                $info = sprintf('GUN - %d carton and %s KG  to pallet  %s, order %s', $ctnTtl, $weight, $rfid, $input['odr_num']);
            }


            $evtCartonCreated = [
                'whs_id'     => $whsId,
                'cus_id'     => $grHdr->cus_id,
                'owner'      => $grNum,
                'evt_code'   => 'WGS',
                'trans_num'  => $asnDtl->ctnr_num,
                'info'       => $info,
                'created_at' => time(),
                'created_by' => $userId,
            ];

            EventTracking::insert($evtCartonCreated);

            //create invertory sumary
            //$grObj->createInventory($grHdr->whs_id, $grHdr->cus_id, $grDtl, $lot);

            if(!empty($input['completed']) && $input['completed'] == 1) {
                $this->completed = $input['completed'];
            }

            $this->createOrderXDock($grDtl->gr_dtl_id,$input['odr_num'], $palletID, $input['act_ctn_ttl'] );



            DB::commit();

            foreach ($arrDmgCartonId as $index => $ctnId) {
                if (isset($dmgCtns[$index]['image_base64'])) {
                    $name = uniqid("damaged_image_") . '.png';
                    $pathFile = storage_path("app") . '/' . $name;
                    $client = new Client();
                    $res = $client->post(
                        env('API_GOODSRECEIPT') . "damage-carton/{$ctnId}/upload/file", [
                            RequestOptions::MULTIPART   => [
                                [
                                    'name'     => 'file',
                                    'filename' => $name,
                                    'contents' => base64_decode($dmgCtns[$index]['image_base64'])
                                ]
                            ],
                            RequestOptions::HEADERS     => [
                                'Authorization' => $request->getHeader('Authorization')[0],
                                //'Content-Type' => 'multipart/form-data;'
                            ],
                            RequestOptions::HTTP_ERRORS => false
                        ]
                    );
                }
            }

            return [
                'message' => sprintf('`%s` and %d cartons has been created in %s', $rfid, ($isKg) ? 1 : $ctnTtl, $grNum)
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function validateSkuwithOrder(Request $request,$whsId,$asnDtlId) {
        try{
            $input = $request->getParsedBody();
            if(empty($input['odr_num'])) {
                return $this->response->errorBadRequest("Order number is required");
            }

            $asnDtl = AsnDtl::where('asn_dtl_id', $asnDtlId)->first()->toArray();
            if (empty($asnDtl)) {
                return $this->response->errorBadRequest('Asn dtl not existed');
            }

            if ($asnDtl["asn_dtl_sts"] == 'CC') {
                return $this->response->errorBadRequest('This item is cancelled');
            }

            $asnHdr = AsnHdr::where('asn_hdr_id', $asnDtl["asn_hdr_id"])->first()->toArray();
            $order = DB::table("odr_dtl")
                ->join("odr_hdr","odr_dtl.odr_id","=","odr_hdr.odr_id")
                ->where("odr_hdr.odr_num",$input['odr_num'])
                ->where("odr_hdr.whs_id",$whsId)
                ->where("odr_hdr.cus_id",$asnHdr['cus_id'])
                ->whereIn("odr_hdr.odr_sts",["NW","XDP"])
                ->whereIn("odr_dtl.itm_sts",["NW","XP"])
                ->whereIn("odr_hdr.odr_type",["BAC","XDK"])
                ->where("odr_dtl.item_id",$asnDtl['item_id'])
                ->first();
            if(empty($order)) {
                return $this->response->errorBadRequest("Sku does not match with this order");
            }

            $remain_qty = ($order['piece_qty'] - $order['picked_qty'])/$order['pack'];

            return(["data" => ["order" => $order['odr_num'],"remain_qty" => $remain_qty]]);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    public function createOrderXDock($gr_dtl_id, $ord_num, $plt_id, $qty, $order_detail_id = null) {

        try{
            /*
         * Update X Dock
         */
            $userId = Data::getCurrentUserId();

            $goodReceipt = GoodsReceiptDetail::where("gr_dtl_id",$gr_dtl_id)->first();
            if(empty($goodReceipt)) {
                return $this->response->errorBadRequest("Good Receipt detail is invalid");
            }

            $grHeader = GoodsReceipt::where("gr_hdr_id",$goodReceipt->gr_hdr_id)->first();
            if(empty($grHeader)) {
                return $this->response->errorBadRequest("Good Receipt header is invalid");
            }
            $qty = $qty * $goodReceipt->pack;
            $change_qty = $qty;
            $item_id = $goodReceipt->item_id;

            if(empty($ord_num)) {
                return $this->response->errorBadRequest("Order number and Sku is required");
            }

            $odr_hdr = DB::table("odr_hdr")
                ->where("odr_hdr.odr_num",$ord_num)
                ->where("whs_id",$grHeader->whs_id)
                ->where("cus_id",$grHeader->cus_id)
                ->whereIn("odr_hdr.odr_sts",["NW","XDP"])
                ->whereIn("odr_hdr.odr_type",["BAC","XDK"])
                ->where("deleted",0)
                ->first();
            if(empty($odr_hdr)) {
                return $this->response->errorBadRequest("Order header is invalid");
            }

            $parent_odr_dlt = DB::table("odr_dtl")
                ->where("odr_dtl.odr_id",$odr_hdr['odr_id'])
                ->where("item_id", $goodReceipt->item_id)
                ->whereNull("odr_dtl.type")
                ->whereIn("odr_dtl.itm_sts",["NW","XP"])
                ->where("deleted",0)
                ->first();
            if(empty($parent_odr_dlt)) {
                return $this->response->errorBadRequest("Order detail is invalid");
            }

            // Check Pallet with multi sku
            $check_plt = Pallet::where("plt_id", $plt_id)->first();
            if(!empty($check_plt->item_id) && $check_plt->item_id != $parent_odr_dlt['item_id']) {
                return $this->response->errorBadRequest("Pallet already had another SKU");
            }

            if(empty($order_detail_id)) {
                $params = [
                    'whs_id'        => $grHeader->whs_id,
                    "odr_id"        => $odr_hdr['odr_id'],
                    "cus_id"        => $odr_hdr['cus_id'],
                    "item_id"       => $item_id,
                    "uom_id"        => $goodReceipt->uom_id,
                    "uom_code"      => $goodReceipt->uom_code,
                    "lot"           => ($goodReceipt->lot)?$goodReceipt->lot:'NA',
                    "sku"           => $goodReceipt->sku,
                    "size"          => ($goodReceipt->size)?$goodReceipt->size:'',
                    "color"         => ($goodReceipt->color)?$goodReceipt->color:'',
                    "pack"          => $goodReceipt->pack,
                    "qty"           => 0,
                    "piece_qty"     => 0,
                    "back_odr"      => 0,
                    "alloc_qty"     => 0,
                    "back_odr_qty"  => 0,
                    "sts"           => "i",
                    "picked_qty"    => 0,
                    "packed_qty"    => 0,
                    //"dmg_qty"       => 0,
//                    "is_kitting"    => 0,
//                    "allow_dmg"     => 0,
                    "x_dock_qty"    => $qty,
                    "parent_id"     => $parent_odr_dlt['odr_dtl_id'],
                    "gr_dtl_id"     => $gr_dtl_id,
                    "type"          => "XD",
                    "deleted"       => 1,
                    "created_at"    => time(),
                    "updated_at"    => time(),
                    "created_by"    => $userId,
                    "updated_by"    => $userId,
                    "deleted_at"    => time()
                ];

                DB::table("odr_dtl")->insert($params);
            } else {
                $child_odr_dlt = DB::table("odr_dtl")
                    ->where("odr_dtl_id", $order_detail_id)
                    ->where("odr_dtl.odr_id",$odr_hdr['odr_id'])
                    ->whereIn("odr_dtl.type",["XD"])
                    ->where("deleted",1)
                    ->first();

                if(empty($child_odr_dlt)) {
                    return $this->response->errorBadRequest("Order detail children is invalid");
                }

                $change_qty = $qty - $child_odr_dlt->x_dock_qty;
                $params = [
                    'whs_id'        => $grHeader->whs_id,
                    "odr_id"        => $odr_hdr['odr_id'],
                    "cus_id"        => $odr_hdr['cus_id'],
                    "item_id"       => $item_id,
                    "uom_id"        => $goodReceipt->uom_code,
                    "lot"           => ($goodReceipt->lot)?$goodReceipt->lot:'NA',
                    "sku"           => $goodReceipt->sku,
                    "size"          => ($goodReceipt->size)?$goodReceipt->size:'',
                    "color"         => ($goodReceipt->color)?$goodReceipt->color:'',
                    "pack"          => $goodReceipt->pack,
                    "qty"           => 0,
                    "piece_qty"     => 0,
                    "back_odr"      => 0,
                    "alloc_qty"     => 0,
                    "back_odr_qty"  => 0,
                    "sts"           => "i",
                    "picked_qty"    => 0,
                    "packed_qty"    => 0,
                    //"dmg_qty"       => 0,
//                    "is_kitting"    => 0,
//                    "allow_dmg"     => 0,
                    "x_dock_qty"    => $qty,
                    "parent_id"     => $parent_odr_dlt['odr_dtl_id'],
                    "gr_dtl_id"     => $gr_dtl_id,
                    "type"          => "XD",
                    "deleted"       => 1,
                    "updated_at"    => time(),
                    "created_by"    => $userId,
                    "updated_by"    => $userId,
                    "deleted_at"    => time()
                ];
                DB::table("odr_dtl")->where("odr_dtl_id", $order_detail_id)->update($params);
            }

            /*
             *  Update parent Order Detail
             */
            $odr_sts = "XDP";
            $status = "XP";
            $qty_ttl = $parent_odr_dlt['picked_qty'] + $change_qty;
            $remain_qty = $parent_odr_dlt['piece_qty'] - $qty_ttl;

            if($this->completed == 1) {
                $odr_dtls = DB::table("odr_dtl")
                    ->where("odr_dtl.odr_id",$odr_hdr['odr_id'])
                    ->whereNull("odr_dtl.type")
                    ->whereIn("odr_dtl.itm_sts",["NW","XP"])
                    ->where("deleted",0)
                    ->get();
                $flag = 0;
                foreach ($odr_dtls as $odr_dtl) {
                    $inv_smr = InventorySummary::where("item_id",$odr_dtl['item_id'])
                        ->where("whs_id", $grHeader->whs_id)
                        ->where("cus_id",$odr_hdr['cus_id'])
                        ->where("lot",$goodReceipt->lot)
                        ->select(DB::raw("SUM(avail) as avail_ttl"))
                        ->groupBy("item_id")
                        ->first();
                    $dtl_remain = $odr_dtl['piece_qty'] - $odr_dtl['picked_qty'];

                    if($odr_dtl['odr_dtl_id'] == $parent_odr_dlt['odr_dtl_id']) {
                        $dtl_remain = $remain_qty;
                    }

                    if($dtl_remain > 0 && $inv_smr->avail_ttl < $dtl_remain){
                        $flag = 1;
                        break;
                    }
                }
                if($flag == 0) {
                    $status = "PK";
                    $this->completed = 2;

                    foreach ($odr_dtls as $odr_dtl) {
                        $dtl_remain = $odr_dtl['piece_qty'] - $odr_dtl['picked_qty'];

                        if($odr_dtl['odr_dtl_id'] == $parent_odr_dlt['odr_dtl_id']) {
                            $dtl_remain = $remain_qty;
                        }

                        OrderDtlModel::where("odr_dtl_id", $odr_dtl['odr_dtl_id'])->update(["itm_sts" => $status,"alloc_qty"=>$odr_dtl['piece_qty'] ]);
                        $this->doAllocate($odr_dtl['item_id'],$dtl_remain,$grHeader->whs_id,$grHeader->cus_id);
                    }
                }
            }

            if($remain_qty <= 0 ) {
                $status = "PD";
                $this-> completed = 3;
            }

            $param_parent = [
                "itm_sts"       => $status,
                "x_dock_qty"    => $parent_odr_dlt['x_dock_qty'] + $change_qty,
                "picked_qty"    => $parent_odr_dlt['picked_qty'] + $change_qty,
                "alloc_qty"     => $parent_odr_dlt['alloc_qty'] + $change_qty
            ];
            if($this->completed == 2) {
                $param_parent["alloc_qty"] = $parent_odr_dlt['piece_qty'];

            }

            OrderDtlModel::where("odr_dtl_id", $parent_odr_dlt['odr_dtl_id'])->update($param_parent);

            /*
             *  Update Order Header
             */
            if($this->completed == 2) {
                $odr_sts = "PK";
            }

            if($this->completed == 3) {
                $count_except = DB::table("odr_dtl")
                    ->where("odr_dtl.odr_id",$odr_hdr['odr_id'])
                    ->whereIn("itm_sts", ["NW","XP","PK"])
                    ->whereNull("odr_dtl.type")
                    ->where("deleted",0)
                    ->count();
                if($count_except == 0) {
                    $odr_sts = "PTD";
                } else {
                    $this->completed = 1;
                }
            }

            OrderHdrModel::where("odr_id",$odr_hdr['odr_id'])->update(["odr_sts" => $odr_sts]);

            /*
             *  Update Pallet Status
             */
            $plt = Pallet::where("plt_id", $plt_id)->first();
            if(empty($plt)) {
                return $this->response->errorBadRequest("Pallet is invalid");
            }

            $param_plt = [
                "is_xdock"      => 1,
                "plt_sts"       => "NW",
                "odr_id"        => $odr_hdr['odr_id']
            ];
            Pallet::where("plt_id", $plt_id)->update($param_plt);

            /*
             * Update Good receipt
             */
            $gr_qty = $change_qty + $goodReceipt->crs_doc_qty;
            GoodsReceiptDetail::where("gr_dtl_id",$gr_dtl_id)->update(["crs_doc_qty" => $gr_qty]);

            /*
             * Update Asn
             */
            $asnDlt = AsnDtl::where("asn_dtl_id", $goodReceipt->asn_dtl_id)->first();

            $asn_qty = $asnDlt->asn_dtl_crs_doc_qty  + $change_qty;
            $asn_qty_ctn = $asn_qty / $goodReceipt->pack;

            AsnDtl::where("asn_dtl_id", $goodReceipt->asn_dtl_id)->update(["asn_dtl_crs_doc_qty" => $asn_qty,"asn_dtl_crs_doc" => $asn_qty_ctn]);

            /*
             *  Update Carton Status
             */
            DB::table("cartons")->where("plt_id",$plt_id)->where("gr_dtl_id",$gr_dtl_id)->update(["ctn_sts" => "PD"]);


            /*
             *  Create Event Tracking
             */
            $ctn_num = $qty / $goodReceipt->pack;
            $info = sprintf('GUN - %d  cartons (%s)  to pallet  %s ,order %s', $ctn_num,$goodReceipt->sku ,$plt->rfid, $ord_num);
            $evtCartonCreated = [
                'whs_id'     => $odr_hdr['whs_id'],
                'cus_id'     => $odr_hdr['cus_id'],
                'owner'      => $odr_hdr['odr_num'],
                'evt_code'   => 'PPK',
                'trans_num'  => $odr_hdr['cus_odr_num'],
                'info'       => $info,
                'created_at' => time(),
                'created_by' => $userId,
            ];

            EventTracking::insert($evtCartonCreated);

            if($this->completed == 2) {
                $evtCartonCreatedlist = [];
                $info = sprintf('GUN - Create Wavepick for Order %s', $ord_num);
                $evtCartonCreatedlist[] = [
                    'whs_id'     => $odr_hdr['whs_id'],
                    'cus_id'     => $odr_hdr['cus_id'],
                    'owner'      => $odr_hdr['odr_num'],
                    'evt_code'   => 'OPR',
                    'trans_num'  => $odr_hdr['cus_odr_num'],
                    'info'       => $info,
                    'created_at' => time(),
                    'created_by' => $userId,
                ];

                $info = sprintf('GUN - Complete Order %s', $ord_num);
                $evtCartonCreatedlist[] = [
                    'whs_id'     => $odr_hdr['whs_id'],
                    'cus_id'     => $odr_hdr['cus_id'],
                    'owner'      => $odr_hdr['odr_num'],
                    'evt_code'   => 'ORC',
                    'trans_num'  => $odr_hdr['cus_odr_num'],
                    'info'       => $info,
                    'created_at' => time(),
                    'created_by' => $userId,
                ];

                EventTracking::insert($evtCartonCreatedlist);
                dispatch(new CreateWavePickXDock($odr_hdr['odr_id']));
            }

            if($this->completed == 3) {
                $info = sprintf('GUN - Complete Order %s', $ord_num);
                $evtCartonCreated = [
                    'whs_id'     => $odr_hdr['whs_id'],
                    'cus_id'     => $odr_hdr['cus_id'],
                    'owner'      => $odr_hdr['odr_num'],
                    'evt_code'   => 'ORC',
                    'trans_num'  => $odr_hdr['cus_odr_num'],
                    'info'       => $info,
                    'created_at' => time(),
                    'created_by' => $userId,
                ];

                EventTracking::insert($evtCartonCreated);
                dispatch(new CreateWavePickXDock($odr_hdr['odr_id']));
            }

            return 1;
        } catch(\Exception $e) {
            throw $e;
        }

    }

    public function doAllocate($item_id, $remain_qty, $whsId, $cusId, $lot ="NA") {
        try{
            $invt_smr = InventorySummary::where("item_id",$item_id)
                ->where("whs_id", $whsId)
                ->where("cus_id",$cusId)
                ->where("lot",$lot)
                ->orderBy("avail","desc")
                ->get()->toArray();
            if(empty($invt_smr)) {
                return $this->response->errorBadRequest("Inventory is invalid");
            }

            foreach ($invt_smr as $inven) {
                if($inven['avail'] > $remain_qty ) {
                    $allocate_qty = $inven['allocated_qty'] + $remain_qty;
                    $avail = $inven['avail'] - $remain_qty;
                    $param_inve = [
                        "allocated_qty"     => $allocate_qty,
                        "avail"             => $avail
                    ];
                    $remain_qty = 0;
                    InventorySummary::where("inv_sum_id",$inven['inv_sum_id'])->update($param_inve);
                    break;
                } else {
                    $avail = 0;
                    $allocate_qty = $inven['allocated_qty'] + ($remain_qty - $inven['avail']);
                    $param_inve = [
                        "allocated_qty"     => $allocate_qty,
                        "avail"             => $avail
                    ];
                    $remain_qty -= $inven['avail'];
                    InventorySummary::where("inv_sum_id",$inven['inv_sum_id'])->update($param_inve);
                }
            }
        }catch(\Exception $e) {
            throw $e;
        }
    }

}