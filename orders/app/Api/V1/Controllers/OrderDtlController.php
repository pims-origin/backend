<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 15-Aug-16
 * Time: 09:24
 */

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Transformers\OrderDtlTransformer;
use App\Api\V1\Validators\OrderDtlValidator;
use Psr\Http\Message\ServerRequestInterface as Request;
use Illuminate\Http\Response as IlluminateResponse;
use Seldat\Wms2\Models\SysBug;
use Seldat\Wms2\Utils\Message;
use Seldat\Wms2\Utils\SystemBug;
use Swagger\Annotations as SWG;

/**
 * Class OrderDtlController
 *
 * @package App\Api\V1\Controllers
 */
class OrderDtlController extends AbstractController
{
    /**
     * @var OrderDtlModel
     */
    protected $orderDtlModel;

    /**
     * OrderDtlController constructor.
     *
     * @param OrderDtlModel $orderDtlModel
     */
    public function __construct(OrderDtlModel $orderDtlModel)
    {
        $this->orderDtlModel = $orderDtlModel;
    }


    /**
     * @param $orderHdrId
     * @param $orderDtlId
     * @param OrderDtlTransformer $orderDtlTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function show($orderHdrId, $orderDtlId, OrderDtlTransformer $orderDtlTransformer)
    {
        try {
            $orderDtl = $this->orderDtlModel->getFirstWhere(['ord_hdr_id' => $orderHdrId, 'ord_dtl_id' => $orderDtlId]);

            return $this->response->item($orderDtl, $orderDtlTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param OrderDtlTransformer $orderDtlTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function search($orderHdrId, Request $request, OrderDtlTransformer $orderDtlTransformer)
    {
        // get data from HTTP
        $input = $request->getQueryParams();
        $input['ord_hdr_id'] = $orderHdrId;

        try {
            $orderDtl = $this->orderDtlModel->search($input, [

            ], array_get($input, 'limit'));

            return $this->response->paginator($orderDtl, $orderDtlTransformer);

        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
    }

    /**
     * @param $orderHdrId
     * @param Request $request
     * @param OrderDtlValidator $orderDtlValidator
     * @param OrderDtlTransformer $orderDtlTransformer
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function store(
        $orderHdrId,
        Request $request,
        OrderDtlValidator $orderDtlValidator,
        OrderDtlTransformer $orderDtlTransformer
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $input['ord_hdr_id'] = $orderHdrId;
        $orderDtlValidator->validate($input);

        //value to save
        $params = [
            'ord_hdr_id'       => $orderHdrId,
            'whs_id'           => $input['whs_id'],
            'cus_id'           => $input['cus_id'],
            'item_id'          => $input['item_id'],
            'uom_id'           => $input['uom_id'],
            'color'            => array_get($input, 'color', null),
            'sku'              => $input['sku'],
            'size'             => array_get($input, 'size', null),
            'lot'              => array_get($input, 'lot', null),
            'request_qty'      => array_get($input, 'request_qty', null),
            'qty_to_allocated' => array_get($input, 'qty_to_allocated', null),
            'allocated_qty'    => array_get($input, 'allocated_qty', null),
            'damaged'          => array_get($input, 'damaged', null),
            'special_handling' => array_get($input, 'special_handling', null),
            'back_ord'         => array_get($input, 'back_ord', null),
        ];

        try {
            if (($orderDtl = $this->orderDtlModel->create($params))) {
                return $this->response
                    ->item($orderDtl, $orderDtlTransformer)
                    ->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $orderDtlId
     * @param Request $request
     * @param OrderDtlValidator $orderDtlValidator
     * @param OrderDtlTransformer $orderDtlTransformer
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function update(
        $orderHdrId,
        $orderDtlId,
        Request $request,
        OrderDtlValidator $orderDtlValidator,
        OrderDtlTransformer $orderDtlTransformer
    ) {
        // get data from HTTP
        $input = $request->getParsedBody();

        // validation
        $input['ord_hdr_id'] = $orderHdrId;
        $orderDtlValidator->validate($input);

        $params = [
            'ord_dtl_id'       => $orderDtlId,
            'ord_hdr_id'       => $input['ord_hdr_id'],
            'whs_id'           => $input['whs_id'],
            'cus_id'           => $input['cus_id'],
            'item_id'          => $input['item_id'],
            'uom_id'           => $input['uom_id'],
            'color'            => array_get($input, 'color', null),
            'sku'              => $input['sku'],
            'size'             => array_get($input, 'size', null),
            'lot'              => array_get($input, 'lot', null),
            'request_qty'      => array_get($input, 'request_qty', null),
            'qty_to_allocated' => array_get($input, 'qty_to_allocated', null),
            'allocated_qty'    => array_get($input, 'allocated_qty', null),
            'damaged'          => array_get($input, 'damaged', null),
            'special_handling' => array_get($input, 'special_handling', null),
            'back_ord'         => array_get($input, 'back_ord', null),
        ];

        try {
            // Check Exist Order Detail
            if (!$this->orderDtlModel->checkWhere(['ord_hdr_id' => $orderHdrId, 'ord_dtl_id' => $orderDtlId])) {
                throw new \Exception(Message::get("BM017", "Order Dtl"));
            }

            if ($orderDtl = $this->orderDtlModel->update($params)) {
                return $this->response->item($orderDtl, $orderDtlTransformer);
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }

    /**
     * @param $orderDtlId
     *
     * @return \Dingo\Api\Http\Response|void
     */
    public function destroy($orderDtlId)
    {
        try {
            // Check Exist
            if (!$this->orderDtlModel->checkWhere(['ord_dtl_id' => $orderDtlId])) {
                return $this->response->errorBadRequest(Message::get("BM017", "Order Dtl"));
            }

            if ($this->orderDtlModel->deleteOrderDtl($orderDtlId)) {
                return $this->response->noContent();
            }
        } catch (\PDOException $e) {
            return  $this->response->errorBadRequest(
                SystemBug::writeSysBugs($e, SysBug::API_ORDER, __FUNCTION__)
            );
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest(Message::get("BM010"));
    }
}
