<?php

namespace App\Modules\Warehouses\Eloquent;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = "users";

    protected $fillable = [
        'username', 'email','first_name','last_name', 'created_at','updated_at',
        'password','login_at','expired_at','type','status'
    ];

    protected $primaryKey = "username";
    protected $keyType = "varchar";
    public $timestamps = true;
    protected $dateFormat = 'U';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password','login_at','expired_at','created_at','updated_at',
    ];
}