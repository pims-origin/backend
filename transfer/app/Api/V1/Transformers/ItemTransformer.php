<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\InventorySummary;

class ItemTransformer extends TransformerAbstract
{

    /**
     * @param InventorySummary $invt_smr
     *
     * @return array
     */
    public function transform(InventorySummary $invt_smr)
    {
        return [
            'item_id'       => $invt_smr->item_id,
            'cus_id'        => $invt_smr->cus_id,
            'whs_id'        => $invt_smr->whs_id,
            'color'         => $invt_smr->color,
            'size'          => $invt_smr->size,
            'lot'           => $invt_smr->lot,
            'ttl'           => $invt_smr->ttl,
            'allocated_qty' => $invt_smr->allocated_qty,
            'dmg_qty'       => $invt_smr->dmg_qty,
            'avail'         => $invt_smr->avail,
            'sku'           => $invt_smr->sku,
            'back_qty'      => $invt_smr->back_qty,
            'cus_upc'       => isset($invt_smr->cus_upc) ? $invt_smr->cus_upc : null,
            'pack'          => isset($invt_smr->pack) ? $invt_smr->pack : null,
        ];
    }
}
