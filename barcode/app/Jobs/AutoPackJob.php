<?php

namespace App\Jobs;

use App\Api\V1\Models\OrderDtlModel;
use App\Api\V1\Models\OrderHdrMetaModel;
use App\Api\V1\Models\OrderHdrModel;
use DB;
use GuzzleHttp\Client;
use Wms2\UserInfo\Data;

class AutoPackJob extends Job
{

    protected $wvId;
    protected $request;
    /**
     * AutoPackJob constructor.
     */
    public function __construct($wvId, $request)
    {
        $this->wvId = $wvId;
        $this->request = $request;
    }

    public function handle()
    {


        $orderHdrMetaModel = new OrderHdrMetaModel();
        $orders = DB::table('odr_hdr')->where(['wv_id' => $this->wvId, 'odr_sts' => 'PD'])->get();

        try{

            foreach ($orders as $order) {
                $odrId = array_get($order, 'odr_id');
                $orderFlow = $orderHdrMetaModel->getOrderFlow($odrId);
                $packingFLow = $orderHdrMetaModel->getFlow($orderFlow, 'APO');

                if (array_get($packingFLow, 'usage', -1) == 1) {
                    $client = new Client();
                    $odrDtls = DB::table('odr_dtl')->where(['odr_id' => $odrId , 'deleted' => 0])->get();
                    $data = [];
                    foreach ($odrDtls as  $odrDtl) {
                        $data[] = [
                            'odr_dtl_id' => array_get($odrDtl, 'odr_dtl_id'),
                            'itm_id' => array_get($odrDtl, 'item_id')
                        ];

                    }
                    $client->request('POST', env('API_PACKING') . '/v1/packs/auto/' . $odrId,
                        [
                            'headers'     => ['Authorization' => $this->request->getHeader('Authorization')],
                            'form_params' => $data
                        ]
                    );
                    DB::table('sys_bugs')->insert([
                        'date'       => time(),
                        'api_name'   => 'Barcode: Auto Packing',
                        'created_at' => time(),
                        'created_by' => Data::getCurrentUserId(),
                        'updated_at' => time(),
                        'error'      => env('API_PACKING') . '/v1/packs/auto/' . $odrId
                    ]);
                }
            }

        }catch (\Exception $exception) {
            DB::table('sys_bugs')->insert([
                'date'       => time(),
                'api_name'   => 'Barcode: Auto Packing',
                'created_at' => time(),
                'created_by' => Data::getCurrentUserId(),
                'updated_at' => time(),
                'error'      => $exception->getMessage()
            ]);

        }

    }
}
