<?php

namespace App\Api\V1\Models;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class AbstractModel
{
    /**
     * @var
     */
    protected $model;

    /**
     * Get empty model.
     *
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Refresh model to be clean
     */
    public function refreshModel()
    {
        $this->model = $this->model->newInstance();
    }

    /**
     * Get table name.
     *
     * @return string
     */
    public function getTable()
    {
        return $this->model->getTable();
    }

    /**
     * Make a new instance of the entity to query on.
     *
     * @param array $with
     */
    public function make(array $with = [])
    {
        return $this->model->with($with);
    }

    /**
     * Find a single entity by key value.
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getFirstBy($key, $value, array $with = [])
    {
        $query = $this->make($with);

        return $query->where($key, '=', $value)->first();
    }

    /**
     * Get One collection of models by the given query conditions.
     *
     * @param array $where
     * @param array $with
     * @param array $columns
     * @param bool  $or
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function getFirstWhere($where, array $with = [], $columns = ['*'], $or = false)
    {
        $query = $this->make($with);
        $funcWhere = ($or) ? 'orWhere' : 'where';
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $query = $query->{$funcWhere}($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $query = $query->{$funcWhere}($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $query = $query->{$funcWhere}($field, '=', $search);
                }
            } else {
                $query = $query->{$funcWhere}($field, '=', $value);
            }
        }

        return $query->first($columns);
    }

    /**
     * Find a collection of models by the given query conditions.
     *
     * @param array $where
     * @param array $with
     * @param array $columns
     * @param bool  $or
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function findWhere($where, array $with = [], $columns = ['*'], $or = false)
    {
        $query = $this->make($with);
        $funcWhere = ($or) ? 'orWhere' : 'where';
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $query = $query->{$funcWhere}($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $query = $query->{$funcWhere}($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $query = $query->{$funcWhere}($field, '=', $search);
                }
            } else {
                $query = $query->{$funcWhere}($field, '=', $value);
            }
        }

        return $query->get($columns);
    }

    /**
     * @param array $where
     * @param bool $or
     * @return mixed
     */
    public function checkWhere($where, $or = false)
    {
        $query = $this->make([]);
        $funcWhere = ($or) ? 'orWhere' : 'where';
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $query = $query->{$funcWhere}($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $query = $query->{$funcWhere}($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $query = $query->{$funcWhere}($field, '=', $search);
                }
            } else {
                $query = $query->{$funcWhere}($field, '=', $value);
            }
        }

        return $query->count();
    }

    /**
     * Retrieve model by id
     * regardless of status.
     *
     * @param int $id model ID
     * @param array $with
     *
     * @return Model
     */
    public function byId($id, array $with = [])
    {
        $query = $this->make($with)->where($this->model->getKeyName(), $id);
        $model = $query->firstOrFail();
        return $model;
    }

    /**
     * Get next model.
     *
     * @param Model $model
     * @param array $with
     *
     * @return Model|null
     */
    public function next($model, array $with = [])
    {
        return $this->adjacent(1, $model, $with);
    }

    /**
     * Get prev model.
     *
     * @param Model $model
     * @param array $with
     *
     * @return Model|null
     */
    public function prev($model, array $with = [])
    {
        return $this->adjacent(-1, $model, $with);
    }

    /**
     * Get prev model.
     *
     * @param int $direction
     * @param Model $model
     * @param array $with
     *
     * @return Model|null
     */
    public function adjacent($direction, $model, array $with = [])
    {
        $currentModel = $model;
        $models = $this->all($with);

        foreach ($models as $key => $model) {
            if ($currentModel->{$this->model->getKeyName()} == $model->{$this->model->getKeyName()}) {
                $adjacentKey = $key + $direction;
                return isset($models[$adjacentKey]) ? $models[$adjacentKey] : null;
            }
        }
    }

    /**
     * Get paginated models.
     *
     * @param int $page Number of models per page
     * @param int $limit Results per page
     * @param array $with Eager load related models
     *
     * @return stdClass Object with $items && $totalItems for pagination
     */
    public function byPage($page = 1, $limit = 10, array $with = [])
    {
        $result = new stdClass();
        $result->page = $page;
        $result->limit = $limit;
        $result->totalItems = 0;
        $result->items = [];
        $query = $this->make($with);

        $totalItems = $query->count();
        $query->skip($limit * ($page - 1))
              ->take($limit);
        $models = $query->get();
        // Put items and totalItems in stdClass
        $result->totalItems = $totalItems;
        $result->items = $models->all();
        return $result;
    }

    /**
     * Get all models.
     *
     * @param array $with Eager load related models
     *
     * @return Collection
     */
    public function all(array $with = [])
    {
        $query = $this->make($with);

        // Get
        return $query->get();
    }

    /**
     * Get all models by key/value.
     *
     * @param string $key
     * @param string $value
     * @param array $with
     *
     * @return Collection
     */
    public function allBy($key, $value, array $with = [])
    {
        $query = $this->make($with);

        $query->where($key, $value);
        // Get
        $models = $query->get();
        return $models;
    }

    /**
     * Get latest models.
     *
     * @param int $number number of items to take
     * @param array $with array of related items
     *
     * @return Collection
     */
    public function latest($number = 10, array $with = [])
    {
        $query = $this->make($with);
        return $query->take($number)->get();
    }

    /**
     * Get single model by Slug.
     *
     * @param string $slug slug
     * @param array $with related tables
     *
     * @return mixed
     */
    public function bySlug($slug, array $with = [])
    {
        $model = $this->make($with)
                      ->where('slug', '=', $slug)
                      ->firstOrFail();

        return $model;
    }

    /**
     * Return all results that have a required relationship.
     *
     * @param string $relation
     * @param array $with
     *
     * @return Collection
     */
    public function has($relation, array $with = [])
    {
        $entity = $this->make($with);
        return $entity->has($relation)->get();
    }

    /**
     * Create a new model.
     *
     * @param array $data
     *
     * @return mixed Model or false on error during save
     */
    public function create(array $data)
    {
        // Create the model

        $model = $this->model->fill($data);

        if ($model->save()) {

            return $model;
        }
        return false;
    }

    /**
     * Update an existing model.
     *
     * @param array $data
     *
     * @return mixed Model or false on error during save
     */
    public function update(array $data)
    {
        $model = $this->model->findOrFail($data[$this->model->getKeyName()]);
        $model->fill($data);

        if ($model->save()) {
            return $model;
        }

        return false;
    }

    /**
     * Sort models.
     *
     * @param array $data updated data
     *
     * @return null
     */
    public function sort(array $data)
    {
        foreach ($data['item'] as $position => $item) {
            $page = $this->model->find($item[$this->model->getKeyName()]);
            $sortData = $this->getSortData($position + 1, $item);
            $page->update($sortData);
        }
    }

    /**
     * Get sort data.
     *
     * @param int $position
     *
     * @return array
     */
    protected function getSortData($position)
    {
        return [
            'position' => $position,
        ];
    }

    /**
     * Delete model.
     *
     * @param Model $model
     *
     * @return bool
     */
    public function delete($model)
    {
        return $model->delete();
    }

    /**
     * Delete model By Ids
     *
     * @param array|int $ids
     *
     * @return bool
     */
    public function deleteById($ids)
    {
        $ids = is_array($ids) ? $ids : [$ids];

        return $this->model->destroy($ids);
    }

    /**
     * Sync related items for model.
     *
     * @param Model $model
     * @param array $data
     * @param string $table
     *
     * @return null
     */
    public function syncRelation($model, array $data, $table = null)
    {
        if (!method_exists($model, $table)) {
            return false;
        }
        if (!isset($data[$table])) {
            return false;
        }
        // add related items
        $pivotData = [];
        $position = 0;
        if (is_array($data[$table])) {
            foreach ($data[$table] as $id) {
                $pivotData[$id] = ['position' => $position++];
            }
        }
        // Sync related items
        $model->$table()->sync($pivotData);
    }

    /*
    ****************************************************************************
    */

    public function sortBuilder(&$query, $attributes = [])
    {
        $validConditions = ['asc', 'desc'];
        $validColumn = $this->model->getTableColumns();

        if (empty($attributes['sort'])) {
            return false;
        }

        foreach ($attributes['sort'] as $key => $value) {

            if (!$value) {
                $value = 'asc';
            }

            if (!in_array($value, $validConditions)) {
                continue;
            }

            if (!in_array($key, $validColumn)) {
                continue;
            }

            $query->orderBy($this->model->getTable() . '.' . $key, $value);
        }
    }

    /*
    ****************************************************************************
    */

    public function arrayPagination($data)
    {
        $limit = array_get($data, 'limit', PAGING_LIMIT);
        $page = array_get($data, 'page', 1);

        $offSet = ($page * $limit) - $limit;

        $pageData = array_slice($data['data'], $offSet, $limit, TRUE);

        $totalsRows = count($data['data']);

        $pagination = new LengthAwarePaginator(
                $pageData,
                $totalsRows,
                $limit,
                $page,
                [
                    'path' => Paginator::resolveCurrentPath(),
                ]
        );

        $results = $pagination->toArray();

        return [
            'data' => array_values($results['data']),
            'meta' => [
                'pagination' => [
                    'total' => $results['total'],
                    'per_page' => $results['per_page'],
                    'current_page' => $results['current_page'],
                    'total_pages' => ceil($totalsRows / $limit),
                    'links' => [
                        'prev' => $results['prev_page_url'],
                        'next' => $results['next_page_url'],
                    ],
                ],
            ],
        ];
    }

    /*
    ****************************************************************************
    */

    function addSearches(&$query, $data)
    {
        $searchAttr = $data['searchAttr'];
        $searches = $data['searches'];
        $attributes = $data['attributes'];
        $tablePreface = array_get($data, 'tablePreface', []);
        $dateFields = array_get($data, 'dateFields', []);

        $joinKeys = [];

        if (property_exists($this->model, 'dateFields')) {
            $dateFields = array_merge($dateFields, $this->model->dateFields);
        }

        foreach ($searchAttr as $key => $value) {
            if (! in_array($key, $searches) || ! isset($attributes[$key])) {
                continue;
            }

            $searchField = $this->searchField($key, $tablePreface, $dateFields);

            if (array_get($data, 'join') && array_get($data['join'], $key)) {
                $joinKeys = $this->searchJoins($query,
                        $data['join'][$key], $joinKeys);
            }

            $this->addSearchWhere($query, [
                'attributes' => $attributes,
                'key' => $key,
                'value' => $value,
                'searchField' => $searchField,
            ]);
        }
    }

    /*
    ****************************************************************************
    */

    function searchJoins(&$query, $searchField, $joinKeys)
    {
        foreach ($searchField as $joinKey => $joinValues) {
            if (isset($joinKeys[$joinKey])) {
                continue;
            }

            $joinKeys[$joinKey] = TRUE;

            foreach ($joinValues as $joinValue) {
                $query->join(
                    $joinValue['table'],
                    $joinValue['one'],
                    array_get($joinValue, 'operator', NULL),
                    array_get($joinValue, 'two', NULL),
                    array_get($joinValue, 'type', 'inner'),
                    array_get($joinValue, 'where', FALSE)
                );
            }
        }

        return $joinKeys;
    }

    /*
    ****************************************************************************
    */

    function searchField($key, $tablePreface, $dateFields)
    {
        $searchField = $key;

        if (key_exists($key, $tablePreface)) {

            $tablePrefix = $tablePreface[$key] ? $tablePreface[$key] :
                $this->model->getTable();

            $searchField = $tablePrefix ? $tablePrefix . '.' . $key : $key;
        }

        if (in_array($key, $dateFields)) {
            $searchField = DB::raw('
                IF(LENGTH(' . $searchField . ') >= 10,
                   DATE_FORMAT(
                       FROM_UNIXTIME(' . $searchField . '), "%Y-%m-%d"
                   ),
                   ""
                )
            ');
        }

        return $searchField;
    }

    /*
    ****************************************************************************
    */

    function addSearchWhere(&$query, $data)
    {
        $attributes = $data['attributes'];
        $key = $data['key'];
        $value = $data['value'];
        $searchField = $data['searchField'];

        if (isset($attributes['compare_operator'][$key])) {
            switch ($attributes['compare_operator'][$key]) {
                case 'like':
                    $query->where($searchField,
                            'LIKE', '%' . SelStr::escapeLike($value) . '%');
                    break;
                case 'less':
                    $query->where($searchField, '<', $value);
                    break;
                default:
                    break;
            }
        } else {
            is_array($value) ? $query->whereIn($searchField, $value) :
                $query->where($searchField, $value);
        }
    }

    /*
    ****************************************************************************
    */

}
