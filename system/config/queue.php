<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 06-Jun-16
 * Time: 08:40
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Default Queue Driver
    |--------------------------------------------------------------------------
    |
    | The Laravel queue API supports a variety of back-ends via an unified
    | API, giving you convenient access to each back-end using the same
    | syntax for each one. Here you may set the default queue driver.
    |
    | Supported: "null", "sync", "database", "beanstalkd", "sqs", "redis"
    |
    */
    'default'     => null,//env('QUEUE_DRIVER', 'sync'),
    /*
    |--------------------------------------------------------------------------
    | Queue Connections
    |--------------------------------------------------------------------------
    |
    | Here you may configure the connection information for each server that
    | is used by your application. A default configuration has been added
    | for each back-end shipped with Laravel. You are free to add more.
    |
    */
    'connections' => [
        'sync'       => [
            'driver' => env('QUEUE_SYNC_DRIVER'),
        ],
        'database'   => [
            'driver' => 'database',
            'table'  => 'jobs',
            'queue'  => 'default',
            'expire' => 60,
        ],
        'beanstalkd' => [
            'driver' => env('QUEUE_BEANSTALKD_DRIVER'),
            'host'   => env('QUEUE_BEANSTALKD_HOST'),
            'queue'  => env('QUEUE_BEANSTALKD_QUEUE'),
            'ttr'    => env('QUEUE_BEANSTALKD_TTR'),
        ],
        'sqs'        => [
            'driver' => env('QUEUE_SQS_DRIVER'),
            'key'    => env('QUEUE_SQS_KEY'),
            'secret' => env('QUEUE_SQS_SECRET'),
            'prefix' => env('QUEUE_SQS_PREFIX'),
            'queue'  => env('QUEUE_SQS_QUEUE'),
            'region' => env('QUEUE_SQS_REGION'),
        ],
        'redis'      => [
            'driver'     => env('QUEUE_REDIS_DRIVER'),
            'connection' => env('QUEUE_REDIS_CONNECTION'),
            'queue'      => env('QUEUE_REDIS_QUEUE'),
            'expire'     => env('QUEUE_REDIS_EXPIRE'),
        ],
        'rabbitmq'   => [
            'driver' => 'rabbitmq',

            'host' => env('RABBITMQ_HOST', '127.0.0.1'),
            'port' => env('RABBITMQ_PORT', 5672),

            'vhost'    => env('RABBITMQ_VHOST', '/'),
            'login'    => env('RABBITMQ_LOGIN', 'guest'),
            'password' => env('RABBITMQ_PASSWORD', 'guest'),

            'queue' => env('RABBITMQ_QUEUE'),
            // name of the default queue,

            'exchange_declare' => env('RABBITMQ_EXCHANGE_DECLARE', true),
            // create the exchange if not exists
            'queue_declare_bind' => env('RABBITMQ_QUEUE_DECLARE_BIND', true),
            // create the queue if not exists and bind to the exchange

            'queue_params' => [
                'passive'     => env('RABBITMQ_QUEUE_PASSIVE', false),
                'durable'     => env('RABBITMQ_QUEUE_DURABLE', true),
                'exclusive'   => env('RABBITMQ_QUEUE_EXCLUSIVE', false),
                'auto_delete' => env('RABBITMQ_QUEUE_AUTODELETE', false),
            ],

            'exchange_params' => [
                'name'        => env('RABBITMQ_EXCHANGE_NAME', null),
                'type'        => env('RABBITMQ_EXCHANGE_TYPE', 'direct'),
                // more info at http://www.rabbitmq.com/tutorials/amqp-concepts.html
                'passive'     => env('RABBITMQ_EXCHANGE_PASSIVE', false),
                'durable'     => env('RABBITMQ_EXCHANGE_DURABLE', true),
                // the exchange will survive server restarts
                'auto_delete' => env('RABBITMQ_EXCHANGE_AUTODELETE', false),
            ],

        ],

        'rabbitmq_edi'   => [
            'driver' => 'rabbitmq',

            'host' => env('RABBITMQ_EDI_HOST', '127.0.0.1'),
            'port' => env('RABBITMQ_EDI_PORT', 5672),

            'vhost'    => env('RABBITMQ_EDI_VHOST', '/'),
            'login'    => env('RABBITMQ_EDI_LOGIN', 'guest'),
            'password' => env('RABBITMQ_EDI_PASSWORD', 'guest'),

            'queue' => env('RABBITMQ_EDI_QUEUE'),
            // name of the default queue,

            'exchange_declare' => env('RABBITMQ_EXCHANGE_DECLARE', true),
            // create the exchange if not exists
            'queue_declare_bind' => env('RABBITMQ_QUEUE_DECLARE_BIND', true),
            // create the queue if not exists and bind to the exchange

            'queue_params' => [
                'passive'     => env('RABBITMQ_QUEUE_PASSIVE', false),
                'durable'     => env('RABBITMQ_QUEUE_DURABLE', true),
                'exclusive'   => env('RABBITMQ_QUEUE_EXCLUSIVE', false),
                'auto_delete' => env('RABBITMQ_QUEUE_AUTODELETE', false),
            ],

            'exchange_params' => [
                'name'        => env('RABBITMQ_EXCHANGE_NAME', null),
                'type'        => env('RABBITMQ_EXCHANGE_TYPE', 'direct'),
                // more info at http://www.rabbitmq.com/tutorials/amqp-concepts.html
                'passive'     => env('RABBITMQ_EXCHANGE_PASSIVE', false),
                'durable'     => env('RABBITMQ_EXCHANGE_DURABLE', true),
                // the exchange will survive server restarts
                'auto_delete' => env('RABBITMQ_EXCHANGE_AUTODELETE', false),
            ],

        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Failed Queue Jobs
    |--------------------------------------------------------------------------
    |
    | These options configure the behavior of failed queue job logging so you
    | can control which database and table are used to store the jobs that
    | have failed. You may change them to any database / table you wish.
    |
    */
    'failed'      => [
        'database' => env('DB_CONNECTION', 'mysql'),
        'table'    => 'failed_jobs',
    ],
];
