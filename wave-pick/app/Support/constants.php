<?php

CONST WAVEPICK_STATUS = [
    'NW' => 'New',
    'AS' => 'Assigned',
    'PK' => 'Picking',
    'CO' => 'Completed'
];