<?php

namespace App\Api\Xdock\Transformers;

use App\Api\Xdock\Models\AsnDtlModel;
use App\Api\Xdock\Models\AsnHdrModel;
use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\AsnHdr;
use Seldat\Wms2\Models\Container;


class AsnListsTransformer extends TransformerAbstract
{
    public function transform(AsnHdr $asnHdr)
    {
        $asnDetail = new AsnDtlModel();
        $item = $asnDetail->getListItem($asnHdr->asn_hdr_id, $asnHdr->ctnr_id);

        $item_ = [];
        $it_ct = [];
        foreach ($item as $it) {
            $it_ct['sku'] = $it['sku'];
            $it_ct['size'] = $it['size'];
            $it_ct['color'] = $it['color'];
            $it_ct['pack'] = $it['pack'];
            $it_ct['asn_dtl_ctn_ttl'] = $it['asn_dtl_ctn_ttl'];
            $it_ct['asn_dtl_pack'] = $it['asn_dtl_pack'];
            $it_ct['asn_dtl_sts'] = $asnDetail->formatStatus('default', $it['asn_dtl_sts']);
            $it_ct['asn_sts_name'] = $it['asn_sts_name'];
            $it_ct['item_id'] = $it['item_id'];
            $it_ct['asn_dtl_id'] = $it['asn_dtl_id'];
            $it_ct['asn_dtl_lot'] = $it['asn_dtl_lot'];
            $it_ct['uom_code'] = $it['uom_code'];
            $total_ = $it['asn_dtl_qty_ttl'] - $it['asn_dtl_crs_doc'];
            if ($it['uom_code'] === 'KG') {
                $actCtns = $it['asn_dtl_ctn_ttl'] ?? 'NA';
                $it_ct['total_pieces'] = $total_ . '/0';
                $it_ct['act_carton'] = $actCtns . '/0';
            } else {
                $actCtns = $total_ / $it['asn_dtl_pack'];
                $it_ct['total_pieces'] = $total_ . '/0';
                $it_ct['act_carton'] = $actCtns . '/0';
            }

            $it_ct['asn_dtl_crs_doc'] = ($it['asn_dtl_crs_doc'] ?? 0) . '/0';
            $it_ct['asn_dtl_crs_doc_qty'] = ($it['asn_dtl_crs_doc_qty'] ?? 0) . '/0';

            $item_[] = $it_ct;
        }

        $expDate = object_get($asnHdr, 'asn_hdr_ept_dt', '');
        $expDate = ($expDate) ? date('Y/m/d', $expDate) : '';

        return [
            'asn_hdr_id'     => $asnHdr->asn_hdr_id,
            'asn_hdr_num'    => $asnHdr->asn_hdr_num,
            'asn_hdr_ref'    => $asnHdr->asn_hdr_ref,
            'cus_name'       => $asnHdr->cus_name,
            'whs_id'         => $asnHdr->whs_id,
            'cus_id'         => $asnHdr->cus_id,
            'ctnr_id'        => $asnHdr->ctnr_id,
            'ctnr_num'       => $asnHdr->ctnr_num,
            'asn_hdr_ept_dt' => $expDate,
            'asn_sts'        => $asnDetail->formatStatus('default', $asnHdr->asn_sts),
            'asn_sts_name'   => $asnHdr->asn_sts_name,
            'cus_code'       => $asnHdr->cus_code,
            'total-sku'      => count($item),
            'items'          => $item_,
        ];

    }

}
