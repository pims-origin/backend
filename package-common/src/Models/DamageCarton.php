<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace Seldat\Wms2\Models;

class DamageCarton extends BaseSoftModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'damage_carton';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ctn_id';

    /**
     * @var array
     */
    protected $fillable = [
        'dmg_id',
        'dmg_note',
        'ctn_id',
        'created_by',
        'updated_by'
    ];

    public function damageType()
    {
        return $this->belongsTo(__NAMESPACE__ . '\DamageType', 'dmg_id', 'dmg_id');
    }

}
