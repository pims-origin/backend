<?php
namespace App\Api\V1\Models;

use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ServerRequestInterface as IRequest;

class RoleService extends BaseService
{
    public function __construct(IRequest $request, $baseUrl = '')
    {
        $api = $baseUrl ?: env('API_AUTHORIZATION');
        parent::__construct($request, $api);
    }

    public function getRoleUri($roleId = 0)
    {
        return env('ROLE_URI') . ($roleId ? '/' . $roleId :  '');
    }

    public function getRoleListByQueryString($queryStr)
    {
        $uri = 'roles';
        $uri = sprintf('%s?%s', $uri, $queryStr);

        try {
            $response = $this->client->get($uri);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }

    public function createRole($params)
    {
        $uri = 'roles';
        $options = [
            'form_params' => $params
        ];
        try {
            $response = $this->client->post($uri, $options);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }

    public function getRole($roleName)
    {
        $uri = 'roles/' . $roleName;

        try {
            $response = $this->client->get($uri);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }

    public function getRoleData($userId)
    {
        $response = $this->getRole($userId);
        $content = $response->getBody()->getContents();
        return $this->responseToArray($content);
    }

    public function getUserByRole($role)
    {
        $uri = 'roles/' . $role . '/users';

        try {
            $response = $this->client->get($uri);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }

    public function getRoleByUser($id, $queryStr = '')
    {
        $uri = 'users/' . $id . '/roles';
        $uri = sprintf('%s?%s', $uri, $queryStr);

        try {
            $response = $this->client->get($uri);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }

    public function getPermissionByRole($role)
    {
        $uri = 'roles/' . $role . '/permissions';

        try {
            $response = $this->client->get($uri);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }

    public function assignPermission($role, $body)
    {
        $uri = 'roles/' . $role . '/permissions';
        $options = [
            'form_params' => [
                'permissions' => $body['permissions']
            ]
        ];
        try {
            $response = $this->client->post($uri, $options);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }

    public function assignUser($id, $assigner, $body)
    {
        $uri = 'users/' . $id . '/roles';
        $options = [
            'form_params' => [
                'roles' => array_get($body, 'roles'),
                'user_id' => $assigner
            ]
        ];
        try {
            $response = $this->client->post($uri, $options);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }

    public function updateRole($roleName, $params)
    {
        $uri = 'roles';
        $roleName = str_replace(["%2520", "%20"], " ", $roleName);
        $params['name'] = urldecode($roleName);

        $options = [
            'form_params' => $params
        ];
        try {
            $response = $this->client->put($uri, $options);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }
    
    public function deleteRoles($params)
    {
        $uri = 'roles/delete';
        
        $options = [
            'form_params' => $params
        ];
        try {
            $response = $this->client->post($uri, $options);
            return $response;
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }
            return false;
        }
    }
}
