<?php

namespace App\Api\V1\Models;

use App\InventorySummary;
use Illuminate\Support\Facades\DB;

class InventorySummaryModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new InventorySummary();
    }

    public function getInvSumByOdrDtl($itemId, $lot, $whs_id, $get = 'avail')
    {
        $query = $this->make([]);
        if ($get == 'avail') {
            $query->select(DB::raw('(avail - lock_qty) as pickable_qty'));
        }
        $query = $query->where('item_id', $itemId);
        if (strtoupper($lot) != 'ANY') {
            $query->where('lot', $lot);
        }

        $query->where('lot', '<>', 'ECO');

        $query->where('whs_id', $whs_id);

        if ($get == 'avail') {
            $pickableQty = $query->get()->sum('pickable_qty');
            return ($pickableQty > 0) ? $pickableQty : 0;
        } else {
            return $query->sum($get);
        }
    }
    public function getAvailableQty($itemId)
    {
        $query = $this->make([]);
        $query->select([
            DB::raw('IFNULL(SUM(avail), 0) as total_avail'),
        ]);
        $query->where('item_id', $itemId);
        $query->where('avail', '>', 0);
        $this->model->filterData($query);

        return $query->first();
    }

    public function updateInventoryByLot($cond, $pickedQt){

        $sqlPickQty = sprintf('`picked_qty` + %d', $pickedQt);
        $sqlAvailQty = sprintf('`allocated_qty` - %d', $pickedQt);
        $res=  DB::table('invt_smr')
            ->where([
                'whs_id' => $cond['whs_id'],
                'item_id' => $cond['item_id'],
                'lot' => $cond['lot']
            ])
            ->update([
                    'picked_qty' =>    DB::raw($sqlPickQty),
                    'allocated_qty' => DB::raw($sqlAvailQty),
                ]
            );
        return $res;
    }

    public function updateInventoryByLotBackOrder($cond, $discrepancyQty){

        $sqlPickQty = sprintf('`avail` + %d', $discrepancyQty);
        $sqlAvailQty = sprintf('`allocated_qty` - %d', $discrepancyQty);
        $res=  DB::table('invt_smr')
            ->where([
                'whs_id' => $cond['whs_id'],
                'item_id' => $cond['item_id'],
                'lot' => $cond['lot']
            ])
            ->update([
                    'avail' =>    DB::raw($sqlPickQty),
                    'allocated_qty' => DB::raw($sqlAvailQty),
                ]
            );

        return $res;
    }

    public function updatePickingInventory($pickingOdrDtls) {
        foreach ($pickingOdrDtls as $pickingOdrDtl) {
            $this->refreshModel();
            $inventorySmr = $this->model->where('item_id', $pickingOdrDtl['item_id'])
                ->where('whs_id', $pickingOdrDtl['whs_id'])
                ->where('cus_id', $pickingOdrDtl['cus_id'])
                ->where('lot', $pickingOdrDtl['lot'])
                ->first();
            $inventorySmr->picked_qty = $inventorySmr->picked_qty - $pickingOdrDtl['qty'];
            $inventorySmr->ttl = $inventorySmr->ttl - $pickingOdrDtl['qty'];
            $inventorySmr->save();
        }
    }

    public function updateKittingInventory($kittingItems) {
        foreach ($kittingItems as $kittingItem) {
            $this->refreshModel();
            $inventorySummary = $this->model->where('item_id', $kittingItem['item_id'])
                ->where('whs_id', $kittingItem['whs_id'])
                ->where('cus_id', $kittingItem['cus_id'])
                ->where('lot', $kittingItem['lot'])
                ->first();

            if (!$inventorySummary) {
                $this->model->create([
                    'item_id'   => $kittingItem['item_id'],
                    'cus_id'    => $kittingItem['cus_id'],
                    'whs_id'    => $kittingItem['whs_id'],
                    'color'     => $kittingItem['color'],
                    'size'      => $kittingItem['size'],
                    'lot'       => $kittingItem['lot'],
                    'sku'       => $kittingItem['sku'],
                    'avail'     => (int)$kittingItem['ctns'] * $kittingItem['pack_size'],
                    'ttl'       => (int)$kittingItem['ctns'] * $kittingItem['pack_size'],
                ]);
            } else {
                $inventorySummary->avail += (int)$kittingItem['ctns'] * $kittingItem['pack_size'];
                $inventorySummary->ttl += (int)$kittingItem['ctns'] * $kittingItem['pack_size'];
                $inventorySummary->save();
            }
        }
    }
}
