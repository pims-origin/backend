<?php
/**
 * Created by PhpStorm.
 * User: chicu
 * Date: 9/14/2016
 * Time: 8:47 AM
 */
namespace Seldat\Wms2\Utils;

use Seldat\Wms2\Models\SysBug;

class SystemBug {

    /**
     * @param $date
     * @param $apiName
     * @param $error
     */
    public static function setSysBug($apiName, $error) {
        //check validate params
        if (!$apiName || !$error) {
            return 'Please add fully error information !';
        }

        //set array input data
        $data = [
            'date' => date('dmy'),
            'api_name' => $apiName,
            'error' => json_encode($error)
        ];
        //if is's ok, call insert to DB
        SysBug::create($data);
    }

    /**
     * @param $e
     * @param $apiName
     * @param $function
     * @return string
     */
    public static function writeSysBugs($e, $apiName, $function) {
        $debug = env('API_DEBUG', false);
        if(!$debug) {
            $arrErrors = [
                'function' => $function,
                'errors' => $e->getMessage()
            ];
            try {
                SystemBug::setSysBug($apiName, $arrErrors);
            }catch (\PDOException $e)
            {
                return SysBug::MESSAGE_ERR;
            }
        }
        return $e->getMessage();
    }

}