<?php

namespace App\Api\V1\Validators;


class OrderHdrMetaValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'qualifier' => 'required'
        ];
    }
}
