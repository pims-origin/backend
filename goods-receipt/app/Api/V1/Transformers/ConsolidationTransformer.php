<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\Carton;

class ConsolidationTransformer extends TransformerAbstract
{
    //return result
    public function transform(Carton $carton)
    {
        return [
            'ctn_id'   => object_get($carton, 'ctn_id'),
            'loc_id'   => object_get($carton, 'loc_id'),
            'loc_name' => object_get($carton, 'loc_name'),
            'loc_code' => object_get($carton, 'loc_code'),
        ];
    }

}
