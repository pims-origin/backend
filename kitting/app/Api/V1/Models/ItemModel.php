<?php

namespace App\Api\V1\Models;

use App\Item;
use Illuminate\Support\Facades\DB;
use Seldat\Wms2\Utils\SelArr;
use Seldat\Wms2\Utils\SelStr;
class ItemModel extends AbstractModel
{

    /**
     * SampleModel constructor.
     */
    protected $cartonModel;

    protected $carton;

    public function __construct()
    {
        $this->model = new Item();
    }

    public function search($attributes = [], $with = [], $limit = 15)
    {
        $attributes = SelArr::removeNullOrEmptyString($attributes);

        $query = $this->make($with);

        $arrLike = [
            'sku'
        ];
        $arrEqual = [
            'cus_id',
        ];

        // filter by request param
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                if (in_array($key, $arrLike) && !empty($value)) {
                    $query = $query->where('item.'.$key, 'like', "%" . SelStr::escapeLike($value) . "%");
                }
                if (in_array($key, $arrEqual) && !empty($value)) {
                    $query = $query->where('item.'.$key, SelStr::escapeLike($value));
                }
            }
        }

        if (!empty($attributes['has_child']) && $attributes['has_child'] == 1){
            $query->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('item_child')
                    ->whereRaw('item.item_id = item_child.parent');
            });
        }

        return $query->paginate($limit);
    }

    public function getChildren($parent, $with = []) {
        $query = $this->make($with);
        return $query->join('item_child','item.item_id', '=', 'child')
            ->select([
                'item.item_id',
                'item.sku',
                'item.color',
                'item.size',
                'item.uom_id',
                'item.pack AS inner_pack',
                'item_child.pack AS pack',
                'item.description',
            ])
            ->where('parent', $parent)
            ->groupBy('item.item_id')
            ->get();
    }
}
