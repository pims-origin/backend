<?php

use App\Api\V1\Controllers\MenusController;
use App\Api\V1\Models\AuthenticationService;
use Zend\Diactoros\ServerRequest;
use Mockery as m;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class MenuControllerTest extends TestCase
{
    protected $client;
    protected $token;
    protected $options;

    public function setUp()
    {
        $this->client = new Client(
            [
                'base_uri' => env('API_DOMAIN'),
            ]
        );

        $loginResult = $this->client->post('/v1/login', [
            'form_params' => [
                'email'    => 'tae.voxuan@gmail.com',
                'password' => '123123',
            ]
        ]);
        if ($loginResult->getStatusCode() == 200) {
            $data = json_decode($loginResult->getBody(), true);
            $this->token = $data['data']['token'];
            $this->options = [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->token,
                ]
            ];
        }
    }

    public function testGetLeftMenu()
    {
        try {
            dd($this->client);

            $response = $this->client->get('/v1/menu/left-menu', $this->options);

            dd($response);
        } catch (RequestException $e) {
            dd($e->getMessage());
        }

    }
}