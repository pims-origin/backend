<?php

namespace App\Helpers;

use App\Setting;

class SettingMailHelper
{
    public static function setting($key)
    {
        $item = Setting::where('setting_key', $key)->get();

        if (!empty($item[0]->setting_value)) {
            $smtpInfo = json_decode($item[0]->setting_value, true);

            return $smtpInfo;
        }
    }
}