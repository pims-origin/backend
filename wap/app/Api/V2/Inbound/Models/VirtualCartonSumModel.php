<?php
/**
 * Created by PhpStorm.
 * User: Sy Dai
 * Date: 26-May-16
 * Time: 09:27
 */

namespace App\Api\V2\Inbound\Models;

use Seldat\Wms2\Models\Carton;
use Seldat\Wms2\Models\VirtualCartonSummary;
use Dingo\Api\Http\Response;
use Seldat\Wms2\Utils\Status;

class VirtualCartonSumModel extends AbstractModel
{
    /**
     * VirtualCartonSumModel constructor.
     *
     * @param VirtualCartonSummary|null $model
     */
    public function __construct(VirtualCartonSummary $model = null)
    {
        $this->model = ($model) ?: new VirtualCartonSummary();
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getVirtualCartonInfo($id){
        return $this->model->where('vtl_ctn_sum_id', $id)->first();
    }

    /**
     * @param $whs_id
     * @return mixed
     */
    public function getAllReceivingGates($whs_id){
        $query = $this->model
            ->where('whs_id', $whs_id)
            ->where('vtl_ctn_sum_sts', Status::getByValue('RECEIVING', 'VIRTUAL-CARTON-SUMMARY-STATUS'))
            ->distinct();
        $this->model->filterData($query);

        return $query->get();
    }


}
