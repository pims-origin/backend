<?php

namespace App\Api\Inventory\Validators;


class CycleHdrValidator extends AbstractValidator
{
    /*
     * Define rules
     * 
     * @return array
     */
    protected function rules()
    {
        return [
            'whs_id'       => 'required|exists:warehouse,whs_id',
            'cycle_name'   => 'required|max:50',
            'cycle_count_by'     => 'required|max:10',
            'cycle_method' => 'required|max:5',
            'cycle_type'     => 'required|max:3',
            'cycle_detail' => 'required',
            'cycle_assign_to' => 'required|exists:users,user_id',
            'cycle_due_date' => 'required|date|date_format:m/d/Y',
            'cycle_has_color_size' => 'required'
        ];
    }
}
