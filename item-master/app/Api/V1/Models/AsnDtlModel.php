<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\AsnDtl;
use Seldat\Wms2\Models\AsnHdr;


class AsnDtlModel extends AbstractModel
{

    protected $asnHdr;

    /**
     * SampleModel constructor.
     */
    public function __construct()
    {
        $this->model = new AsnDtl();
        $this->asnHdr = new AsnHdr();
    }


}
