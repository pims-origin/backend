<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 02/11/2016
 * Time: 09:42
 */

namespace App\Api\V1\Validators;


class UpdateActualCycleDtlValidator extends  AbstractValidator
{
    public function rules()
    {
        return [
            'act_qty'           => 'integer|min:0',
            'act_loc_name'      => 'exists:location,loc_code'
        ];
    }
}