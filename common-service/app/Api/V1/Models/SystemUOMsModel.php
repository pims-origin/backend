<?php

namespace App\Api\V1\Models;

use Seldat\Wms2\Models\SystemUom;

class SystemUOMsModel extends AbstractModel
{
    public function __construct(SystemUom $model = null)
    {
        $this->model = ($model) ?: new SystemUom();
    }

    /*
    ****************************************************************************
    */

}
