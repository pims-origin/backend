<?php
/**
 * Created by PhpStorm.
 * User: Phuong Hong
 * Date: 16-Jun-16
 * Time: 10:29
 */

namespace App\Api\V1\Validators;


class RefillValidator extends AbstractValidator
{
    protected function rules()
    {
        return [
            'loc_code' => 'required',
            'loc_id'   => 'required|integer',
            'item_id'  => 'required|integer',
            'cus_id'   => 'required',
            'sku'      => 'required',
            'min'      => 'integer|min:0',
            'max'      => 'integer|greater_than:min'
        ];
    }
}
