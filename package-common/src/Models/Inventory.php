<?php

namespace Seldat\Wms2\Models;

class Inventory extends BaseSoftModel
{
    protected $table = 'cartons';

    protected $primaryKey = 'ctn_id';

    protected $fillable = [
        'cus_name',
        'ctnr_num',
        'gr_hdr_num',
        'sys_mea_name',
        'user',
        'created_at',
        'batch_num',
        'sku',
        'piece_remain',
        'prefix',
        'suffix',
        'length',
        'width',
        'height',
        'weight',
        'each_length',
        'each_weight',
        'each_height',
        'each_width',
        'item_code',
        'size',
        'color',
        'ctn_pack_size',
        'carton_id',
        'ctn_num',
        'ord_num',
        'plt_num',
        'loc_name',
        'ctn_sts',
    ];

    public function pallet()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Pallet', 'plt_id', 'plt_id');
    }

    public function AsnDtl()
    {
        return $this->belongsTo(__NAMESPACE__ . '\AsnDtl', 'asn_dtl_id', 'asn_dtl_id');
    }

    public function customer()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Customer', 'cus_id', 'cus_id');
    }

    public function goodReceiptDtl()
    {
        return $this->belongsTo(__NAMESPACE__ . '\GoodsReceiptDetail', 'gr_dtl_id', 'gr_dtl_id');
    }

    public function item()
    {
        return $this->belongsTo(__NAMESPACE__ . '\Item', 'item_id', 'item_id');
    }

    public function updatedUser()
    {
        return $this->belongsTo(__NAMESPACE__ . '\User', 'updated_by', 'user_id');
    }
}
