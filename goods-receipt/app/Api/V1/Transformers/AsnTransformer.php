<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use Seldat\Wms2\Models\AsnHdr;

class AsnTransformer extends TransformerAbstract
{

    /**
     * ps:Show ASN
     *
     * @param $sample
     *
     * @return array
     */
    public function transform(AsnHdr $asnHdr)
    {
        return [
            //asn_hdr
            'asn_hdr_id'      => object_get($asnHdr, 'asn_hdr_id', null),
            'asn_hdr_num'     => object_get($asnHdr, 'asn_hdr_num', null),
            'cus_id'          => object_get($asnHdr, 'cus_id', null),
            'asn_hdr_ref'     => object_get($asnHdr, 'asn_hdr_ref', null),
            'asn_hdr_ept_dt'  => object_get($asnHdr, 'asn_hdr_ept_dt', null),
            'ctnr_id'         => object_get($asnHdr, 'ctnr_id', null),
            'asn_hdr_itm_ttl' => object_get($asnHdr, 'asn_hdr_itm_ttl', null),
        ];
    }
}
