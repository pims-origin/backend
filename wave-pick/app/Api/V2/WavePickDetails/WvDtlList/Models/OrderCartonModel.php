<?php

namespace App\Api\V2\WavePickDetails\WvDtlList\Models;

use Seldat\Wms2\Models\OrderCarton;

class OrderCartonModel extends AbstractModel
{

    protected $model;

    /**
     * OrderCartonModel constructor.
     *
     * @param OrderCarton|null $model
     */
    public function __construct(OrderCarton $model = null)
    {
        $this->model = ($model) ?: new OrderCarton();
    }

    /**
     * @param $data
     */
    public function getOrderCarton($data)
    {
        return $this->model
            ->where('odr_hdr_id', $data['odr_hdr_id'])
            ->where('wv_hdr_id', $data['wv_hdr_id'])
            ->where('ctn_id', $data['ctn_id'])
            ->first();
    }
}
