<?php

namespace App\Api\V1\Models;

use App\Api\V1\Traits\CusZoneCusWarehouseService;
use \Seldat\Wms2\Models\CustomerZone;

class CustomerZoneModel extends AbstractModel
{
    use CusZoneCusWarehouseService;
    protected $model;

    public function __construct(CustomerZone $model = null)
    {
        $this->model = ($model) ?: new CustomerZone();
    }

    /**
     * @param $customerId
     * @param $zoneId
     *
     * @return mixed
     */
    public function deleteCustomerZone($customerId, $zoneId)
    {
        return $this->model
            ->where('zone_id', $zoneId)
            ->where('cus_id', $customerId)
            ->delete();
    }

    public function deleteZonesByCustomerId($customerId, array $zoneIds)
    {
        $zoneIds = is_array($zoneIds) ? $zoneIds : [$zoneIds];

        return $this->model
            ->where('cus_id', $customerId)
            ->whereIn('zone_id', $zoneIds)
            ->delete();
    }

    public function getCustomerZone($customerId, $zoneId)
    {
        $query = $this->model->where('zone_id', $zoneId)
            ->where('cus_id', $customerId);
        $this->model->filterData($query);

        return $query->first();
    }

    public function updateWithoutReturnedModel(array $data)
    {
        return $this->model
            ->where('cus_id', $data['cus_id'])
            ->where('zone_id', $data['whs_id'])
            ->update(['whs_meta_value' => $data['whs_meta_value']]);
    }

    public function update(array $data)
    {
        return $this->_update($data, 'zone_id');
    }

    /**
     * @param array $attributes
     * @param array $with
     * @param null $limit
     *
     * @return mixed
     */
    public function searchZoneByCustomer($attributes = [], $with = [], $limit = null)
    {
        $query = $this->make($with);
        $attributes = array_filter($attributes);
        if (!empty($attributes)) {
            if (!empty($attributes['cus_id'])) {
                $customerZoneModel = new CustomerZoneModel();
                $zone = $customerZoneModel->loadBy(['cus_id' => $attributes['cus_id']]);
                $cusIds = array_pluck($zone, 'cus_id');
                $query->whereIn('cus_id', $cusIds);
                unset($attributes['cus_id']);
            }

            $searchEqual = ['cus_id'];

            foreach ($attributes as $key => $value) {
                if (in_array($key, $searchEqual)) {
                    $query->where($key, $value);
                }
            }
        }
        $this->model->filterData($query);
        $this->sortBuilder($query, $attributes);
        // Get
        $models = $query->paginate($limit);

        return $models;
    }

    /**
     * @param $cusId
     *
     * @return array
     */
    public function getZoneIdsByCustomerId($cusId)
    {
        $rows = $this->model
            ->select('zone_id')
            ->where('cus_id', $cusId);
        $this->model->filterData($rows);
        $rows = $rows->get();

        $ids = [];
        if (!$rows->isEmpty()) {
            foreach ($rows as $row) {
                $ids[] = $row->zone_id;
            }
        }

        return $ids;
    }

    /**
     * @param $zoneIds
     * @param $cusId
     *
     * @return mixed
     */
    public function checkAvailableZonesForSaving($zoneIds, $cusId)
    {
        $zoneIds = is_array($zoneIds) ? $zoneIds : [$zoneIds];
        $zones = $this->model->whereIn('zone_id', $zoneIds)
            ->where('cus_id', '!=', $cusId)
            ->get();

        return $zones->isEmpty();
    }
}
