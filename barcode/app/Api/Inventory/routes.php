<?php

//Cycle Count Header
$api->get('/cycle-count', ['action' => "cycleCount", 'uses' => 'CycleHdrController@index']);
$api->get('/cycle-count/{id:[0-9]+}', ['action' => "cycleCount", 'uses' => 'CycleHdrController@view']);
$api->post('/cycle-count', ['action' => "createCycleCount", 'uses' => 'CycleHdrController@store']);
$api->put('/cycle-count/{id:[0-9]+}', ['action' => "cycleCount", 'uses' => 'CycleHdrController@update']);
$api->post('/cycle-count/delete', ['action' => "deleteCycleCount", 'uses' => 'CycleHdrController@destroy']);
$api->get('/cycle-count/print-pdf/{id:[0-9]+}', ['action' => "printPdf", 'uses' => 'CycleHdrController@printPdf']);
$api->post('/cycle-count/recount', ['action' => 'recountCycleCount', 'uses' => 'CycleHdrController@recount']);
$api->post('/cycle-count/approve', ['action' => 'approveCycleCount', 'uses' => 'CycleHdrController@approve']);
$api->post('/cycle-count/saveCycled/{id:[0-9]+}', ['action' => "cycleCount", 'uses' => 'CycleHdrController@saveCycled']);
$api->post('/cycle-count/check-cus-in-user', ['action' => "cycleCount", 'uses' => 'CycleHdrController@checkCusInUser']);
$api->get('/cycle-count/get-users-in-whs', ['action' => "cycleCount", 'uses' => 'CycleHdrController@getUserWhs']);

$api->get('/cycle/type', ['action' => "cycleCount", 'uses' => 'CycleHdrController@cycleType']);
$api->get('/cycle/status', ['action' => "cycleCount", 'uses' => 'CycleHdrController@cycleStatus']);

//Cycle Count Detail
$api->get('/cycle-detail/{id:[0-9]+}', ['action' => "cycleCount", 'uses' => 'CycleDtlController@listDtl']);
$api->get('/cycle-detail/{hdr_id:[0-9]+}/item/{dtl_id:[0-9]+}', ['action' => "cycleCount", 'uses' => 'CycleDtlController@view']);
$api->get('/rfgun-cycle-detail/{hdr_id:[0-9]+}/item/{dtl_id:[0-9]+}', ['action' => "cycleCount", 'uses' =>
'CycleDtlController@rfgunView']);
$api->put('/cycle-detail/{id}', ['action' => "cycleCount", 'uses' => 'CycleDtlController@update']);

//Location
$api->get('/location/filter', ['action' => "cycleCount", 'uses' => 'LocationController@filter']);

//Add SKU
$api->post('/cycle-count/{cycle_id:[0-9]+}/add-sku', ['action' => "cycleCount", 'uses' => 'AddSKUController@store']);

$api->get('/cycle/items', ['action' => "cycleCount", 'uses' => 'ItemController@search']);

// Block stock
// Create reason
$api->post('/block-stock/add-reason/', ['action' => "createBlockStock", 'uses' => 'ReasonController@store']);

// get reason list
$api->get('/block-stock/reason-list/', ['action' => "createBlockStock", 'uses' => 'ReasonController@reasonList']);

// Create block stock
$api->post('/block-stock', ['action' => "createBlockStock", 'uses' => 'BlockHdrController@store']);

// Get block stock list
$api->get('/block-stock/', ['action' => "viewBlockStock", 'uses' => 'BlockHdrController@index']);

// Unblock
$api->post('/block-stock/unblock/{block_hdr_id:[0-9]+}', [
'action' => 'createBlockStock',
'uses' => 'BlockHdrController@unblock'
]);

//Block stock list
$api->get('/block-stock/{id:[0-9]+}/list/', ['action' => "viewBlockStock", 'uses' => 'BlockDtlController@listDtl']);
$api->get('/block-stock/{id:[0-9]+}', ['action' => "viewBlockStock", 'uses' => 'BlockHdrController@view']);
$api->get('/block-stock/items', ['action' => "viewBlockStock", 'uses' => 'ItemController@listItems']);